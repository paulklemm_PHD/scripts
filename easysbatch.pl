#!/usr/bin/perl
#pk

use strict;
use warnings 'all';

use File::Basename;
use Cwd 'abs_path';
use Getopt::Long; # for parameter specification on the command line
$|=1; #autoflush

my $version="0.0.27"; # last updated on 2023-05-09 15:21:45

my $me = basename($0);
my $num_threads = 10;
my $dry=0;
my $keep=0;
my $save=0;
our $maxpend :shared =20;
my $squeuetest="squeue";
my $sbatch="sbatch";
my $user="";

my $usage = "$me        submit jobs using sbatch based on allready running/pending jobs by user.

SYNOPSIS

cat jobs | $me -

or

$me jobs

jobs contains lines of files that are submitted via the -sbatch command.
E.g. using -sbatch=sbatc : jobs='test1.sh\\n...' -> 'sbatch test1.sh'

-user=USER,--filter=FILTER       user name to filter the squeue, e.g. you can use '03_.*'$user (default \$USER)
-maxpending=INT  maximal number of pending jobs (default: $maxpend)
-squeue=STR      command to test on running jobs, this should include a filter for username, afterwards this is filtered by USER and counted (default: $squeuetest)
-sbatch=STR      command to submit a new job (default: $sbatch)
-dry             only print the submit command

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES -
";

my $ref_FN = "";
my $help;
my $debug;
my $interactive;

GetOptions('help!'=>\$help,'h!'=>\$help,
	'squeue=s'=>\$squeuetest,
	'maxpending=i'=>\$maxpend,
	'user=s'=>\$user,
	'filter=s'=>\$user,
	'debug!'=>\$debug,
	'i!'=>\$interactive,
	'dry!'=>\$dry,
	'version!' => sub { print "$0\tv$version\n";exit 0; },
	'v!' => sub { print "$0\tv$version\n";exit 0; }
);

if($user eq ""){ $user=$ENV{'USER'}; }

if ($help || (scalar(@ARGV) == 0 && !$debug) ){
    print $usage;
    exit(1);
}

my $query=$ARGV[0];
my @data;

if($query){
	if($query eq "-"){
		foreach my $line (<STDIN>){
			if($line=~/^#/){next}
			chomp($line);
			push(@data,$line);
		}
	}elsif(open(my $FH,'<',$query)) {
		while(<$FH>){
			if($_=~/^#/){next}
			chomp($_);
			push(@data,$_);
		}
		close($FH);
	}
}

if($interactive){

	$SIG{CHLD} = sub { wait };

	my $pid = fork;
	# now two processes are executing

	if ($pid == 0) {
		use Term::RawInput;

		my $ppid = getppid; # parent pid 

		my $prompt='';
		my ($input,$key)=('','');

		while(kill(0, $ppid)){
			($input,$key)=rawInput($prompt,0);
			if($key && ($key eq "UPARROW" || $key eq "DOWNARROW")){
				lock($maxpend);
				$maxpend+=10 if $key && $key eq "UPARROW";
				$maxpend = ($maxpend>10 ? $maxpend-10 : 0) if $key && $key eq "DOWNARROW";
				print STDERR "[$me] maxpend->$maxpend\n";
			}
			sleep(1);
		}

		exit;
	}
}

my $tic=0;
my $is_first=1;
while (scalar @data > 0) {
	# test queue
	my $c = scalar( grep { $_=~/$user/g; } `$squeuetest` );
	if( $? != 0 ){ die "[$me] ERROR in '$squeuetest': code=$? message=$!"; }

	{
		lock($maxpend);
		if($c < $maxpend){
			$tic=1;
			# submit new jobs
			for (my $i = 0; $i < $maxpend - $c; $i++) {
				my $cur_job = shift @data;

				if($dry){ print STDERR "\n$sbatch $cur_job"; }
				else{ system("$sbatch $cur_job"); }

				if( $? != 0 ){ die "\n[$me] ERROR in '$sbatch': code=$? message=$!"; }

				if($is_first){
					$c = scalar( grep { $_=~/$user/g; } `$squeuetest` );
					if( $? != 0 ){ die "\n[$me] ERROR in '$squeuetest': code=$? message=$!"; }
					if($c == 0){
						print STDERR "\n[$me] ERROR using '$sbatch': the job was not submitted";
						exit(1);
					}
					$is_first=0;
				}

				if (scalar @data == 0) { print STDERR "\n[$me] done";exit 0; }
			}
		}else{
			if($tic == 1){
				$tic=0; print STDERR "\n[$me] sleeping (waiting=".(scalar @data).", in queue=$c) ...";
			}
		}
	} # = unlock($maxpend);

	sleep(1);
}
