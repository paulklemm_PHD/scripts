#!/usr/bin/perl
#pk

use strict;
use warnings;

use Digest::MD5 qw(md5_hex);
use File::Basename;
use Getopt::Long; # for parameter specification on the command line
$|=1; #autoflush

my $version="0.0.20"; # last updated on 2023-02-22 22:24:28

my $me = basename($0);

my $usage = "$me       automatic grades input, a proof checksum is generated.

SYNOPSIS

$me MODUS LABEL

	MODUS=test,task,list
		list : list all available LABEL of \$HOME/.grades.tasks
		task : describe the problem LABEL
		test : validate the test with the given LABEL from STDIN. If correct, a proof will be stored in \$HOME/.grades.proof
	LABEL=s : the task LABEL to solve

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES -
";

my $help;

GetOptions('help!'=>\$help,'h!'=>\$help,
	'version!' => sub { print "$me\tv$version\n";exit 0; },
	'v!' => sub { print "$me\tv$version\n";exit 0; }
);

if ($help || scalar(@ARGV) == 0 ){
    print $usage;
    exit(1);
}

my $MODUS=$ARGV[0];
my $LABEL=scalar @ARGV > 1 ? $ARGV[1] : "NA";

sub generate_pem{
	# 1 = password
	# 2 = output public pem
	system("openssl genrsa -aes128 -passout pass:$1 1024 > $2.pem; openssl rsa -in $2.pem -pubout -passin pass:$1 >$2.pub");
}

sub encrypt{
	# 1 = input string
	# 2 = public pem
	# 3 = output
	system("echo '$1' | openssl rsautl -encrypt -inkey '$2.pub' -pubin -out '$3'");
}
sub decrypt{
	# 1 = private key
	# 2 = pw
	# 3 = input
	return(`openssl rsautl -decrypt -passin pass:$2 -inkey '$1.pem' -in '$3' || echo "ERROR"`);
}

# sub encrypt{ return(`echo '$1' | openssl aes-256-cbc -a -salt -pass pass:$2`); }
# sub decrypt{ return(`echo '$1' | openssl aes-256-cbc -a -d -pass pass:$2`); }

sub prompt_for_password {
    require Term::ReadKey;

    # Tell the terminal not to show the typed chars
    Term::ReadKey::ReadMode('noecho');

    print "Type in your secret password: ";
    my $password = Term::ReadKey::ReadLine(0);

    # Rest the terminal to what it was previously doing
    Term::ReadKey::ReadMode('restore');

    # The one you typed didn't echo!
    print "\n";

    # get rid of that pesky line ending (and works on Windows)
    $password =~ s/\R\z//;

    # say "Password was <$password>"; # check what you are doing :)

    return $password;
}

if($MODUS eq "list"){
	open(my $FH,"< $ENV{'HOME'}/.grades.tasks") or die "ERROR: $! $?\n";
	while(<$FH>){
		chomp;
		my @arr=split("\t",$_);
		print "$arr[0],";
	}
	print "\n";
	close($FH);

}elsif($MODUS eq "task"){
	open(my $FH,"< $ENV{'HOME'}/.grades.tasks") or die "ERROR: $! $?\n";
	my $did_print=0;
	while(<$FH>){
		chomp;
		my @arr=split("\t",$_);
		if($arr[0] eq $LABEL){
			print "$arr[0] : $arr[1]\n";
			$did_print=1;
			last;
		}
	}
	if(!$did_print){
		print STDERR "LABEL=$LABEL not found...\n";
		exit 1;
	}
	close($FH);

}elsif($MODUS eq "test"){
	open(my $FH,"< $ENV{'HOME'}/.grades.tasks") or die "ERROR: $! $?\n";
	my $did_print=0;
	while(<$FH>){
		chomp;
		my @arr=split("\t",$_);
		if($arr[0] eq $LABEL){
			
			my $answ="";
			while(<STDIN>){$_=~s/[\n\r]+//g; $answ.=$_;}

			# md5_hex ans
			$answ=md5_hex($answ);

			if($answ eq $arr[2]){
				print "\nThat is correct, I will add it to your grades...\n";
				my %proof_tar = map {$_=>1} `tar -tf \$HOME/.grades.proof`;

				my $public = `cat \$HOME/.grades.pub`;

				if(exists $proof_tar{$LABEL}){last;}

				my $proof = $rsa->encrypt ( Message => "$LABEL:$ENV{'HOME'}", Key => $public, Armour => 1, ) || die $rsa->errstr();

				# save back
				open(my $PFH,">$ENV{'HOME'}/.grades.proof.$LABEL._.$ENV{'HOME'}");
				print $PFH $proof;
				close($PFH);
				system("tar -rf \$HOME/.grades.proof \$HOME/.grades.proof.$LABEL.$ENV{'HOME'}");

			}else{
				print "\nSorry that is not correct...\n";
			}

			$did_print=1;
			last;
		}
	}
	if(!$did_print){
		print STDERR "LABEL=$LABEL not found...\n";
		exit 1;
	}
	close($FH);

}elsif($MODUS eq "validate"){
	my $pw=prompt_for_password();
	my %proof_tar = map {$_=>1} `tar -tf $LABEL/.grades.proof`;

	my $ok=0;
	foreach my $p (keys %proof_tar) {
		my $p_bn=$p; $p_bn=~s/.*\.grades\.proof\.//g;
		my $label=$p_bn; $label=~s/\._\..*//g;
		my $home=$p_bn; $label=~s/.*\._\.//g;
		my $check=`tar --to-stdout -xvf \$HOME/.grades.proof $p`;
		my $proof = decrypt("$LABEL/.grades.pem",$pw,$p);
		if($proof eq "$label:$home"){ $ok+=1; }
	}
	print "".($ok/(scalar keys %proof_tar))."\n";

}elsif($MODUS eq "ini"){

	my $pw=prompt_for_password();
	if(!-e "$LABEL/.grades.pem"){
		generate_pem($pw,"$LABEL/.grades");
	}
}

