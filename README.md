# Table of Contents
<br><b>phylogeny</b>
0. [phylogeny/treeStatistics.py](#phylogenytreestatisticspy) 
1. [phylogeny/abc2nwk.py](#phylogenyabc2nwkpy) 
2. [phylogeny/treeFindRecentDuplication.py](#phylogenytreefindrecentduplicationpy) 
3. [phylogeny/ARI.py](#phylogenyaripy) 
4. [phylogeny/RF.py](#phylogenyrfpy) 
5. [phylogeny/easyNCBITAX.py](#phylogenyeasyncbitaxpy) 
6. [phylogeny/treePruneGroup.py](#phylogenytreeprunegrouppy) 
7. [phylogeny/easyphylogeny.py](#phylogenyeasyphylogenypy) (v1.2.33)
8. [phylogeny/easyOTOL.py](#phylogenyeasyotolpy) 
9. [phylogeny/find_path_ete.py](#phylogenyfind_path_etepy) 
10. [phylogeny/treeExtractGroup.py](#phylogenytreeextractgrouppy) 
<br><b>R</b>
11. [R/pauls_functions.R](#rpauls_functionsr) 
12. [R/paulComplexHeatmapFindMarkers.R](#rpaulcomplexheatmapfindmarkersr) 
13. [R/knitr.sh](#rknitrsh) 
14. [R/RMD_make_cell_names.py](#rrmd_make_cell_namespy) 
15. [R/paulFeaturePlot.R](#rpaulfeatureplotr) 
16. [R/paulTopGo.R](#rpaultopgor) 
<br><b>AON</b>
17. [AON/predictTmValue.pl](#aonpredicttmvaluepl) (v0.0.22)
18. [AON/greedyQiagenLnaOptimizer.pl](#aongreedyqiagenlnaoptimizerpl) (v0.0.35)
19. [AON/getQiagen.pl](#aongetqiagenpl) (v0.0.26)
<br><b>MS</b>
20. [MS/mergeMSexcelTables.R](#msmergemsexceltablesr) 
<br><b>fastaAndGff</b>
21. [fastaAndGff/easybcfconsensus.sh](#fastaandgffeasybcfconsensussh) 
22. [fastaAndGff/rc.pl](#fastaandgffrcpl) (v0.0.2)
23. [fastaAndGff/highlightFasta.pl](#fastaandgffhighlightfastapl) (v0.3.2)
24. [fastaAndGff/getGeneLength.pl](#fastaandgffgetgenelengthpl) (v0.0.13)
25. [fastaAndGff/grep_k_neighbourhood.pl](#fastaandgffgrep_k_neighbourhoodpl) (v0.0.6)
26. [fastaAndGff/splitGFF.pl](#fastaandgffsplitgffpl) (v0.0.4)
27. [fastaAndGff/splitfasta.pl](#fastaandgffsplitfastapl) (v0.0.21)
28. [fastaAndGff/easyNCBI.pl](#fastaandgffeasyncbipl) 
29. [fastaAndGff/newickToAverageDistance.pl](#fastaandgffnewicktoaveragedistancepl) 
30. [fastaAndGff/faa2kDa.pl](#fastaandgfffaa2kdapl) 
31. [fastaAndGff/promotech_find_best.pl](#fastaandgffpromotech_find_bestpl) (v0.0.25)
32. [fastaAndGff/compareMummerFasta.pl](#fastaandgffcomparemummerfastapl) (v0.7.19)
33. [fastaAndGff/grepfasta.pl](#fastaandgffgrepfastapl) 
34. [fastaAndGff/easyUniprot.pl](#fastaandgffeasyuniprotpl) (v0.1.18)
35. [fastaAndGff/nrdb_gff.pl](#fastaandgffnrdb_gffpl) (v0.0.10)
36. [fastaAndGff/clusterStrings.pl](#fastaandgffclusterstringspl) (v0.0.19)
37. [fastaAndGff/getgff.pl](#fastaandgffgetgffpl) (v0.2.5)
38. [fastaAndGff/compareGffFasta.pl](#fastaandgffcomparegfffastapl) (v0.3.17)
<br><b>easysbatch.pl</b>
39. [easysbatch.pl](#easysbatchpl) 
<br><b>plots</b>
40. [plots/boxplot.R](#plotsboxplotr) 
41. [plots/draw_gff.R](#plotsdraw_gffr) 
42. [plots/drawgffd3.pl](#plotsdrawgffd3pl) (v0.9.31)
43. [plots/csvDatatables.R](#plotscsvdatatablesr) 
44. [plots/easyUpsetr.R](#plotseasyupsetrr) 
<br><b>printCSV.pl</b>
45. [printCSV.pl](#printcsvpl) (v0.0.35)
<br><b>RNAseq</b>
46. [RNAseq/identifyPhred.pl](#rnaseqidentifyphredpl) (v0.0.4)
47. [RNAseq/DNAseqMappedAssembler.pl](#rnaseqdnaseqmappedassemblerpl) (v0.1.2)
48. [RNAseq/easycarpet.R](#rnaseqeasycarpetr) 
49. [RNAseq/sgrVis_strandWrapper.pl](#rnaseqsgrvis_strandwrapperpl) (v0.0.5)
50. [RNAseq/sgrVis.R](#rnaseqsgrvisr) (v1.7.26)
51. [RNAseq/easyKEGG.pl](#rnaseqeasykeggpl) 
52. [RNAseq/easyDeseq2.R](#rnaseqeasydeseq2r) 
53. [RNAseq/RNAseq_removePolyEnds.pl](#rnaseqrnaseq_removepolyendspl) (v0.0.4)
54. [RNAseq/sgrVis_addFoldToGFFWrapper.pl](#rnaseqsgrvis_addfoldtogffwrapperpl) (v0.0.5)
55. [RNAseq/highlightGenome.R](#rnaseqhighlightgenomer) (v0.1.13)
56. [RNAseq/alignNrdbReadsPlot.sh](#rnaseqalignnrdbreadsplotsh) 
57. [RNAseq/estimate_len_sd.pl](#rnaseqestimate_len_sdpl) 
58. [RNAseq/RNAseq_counting_gene.pl](#rnaseqrnaseq_counting_genepl) (v0.8.35)
59. [RNAseq/easyTopGO.R](#rnaseqeasytopgor) 
60. [RNAseq/RNAseq_cigar_plot.pl](#rnaseqrnaseq_cigar_plotpl) (v0.0.9)
<br><b>RNAfold</b>
61. [RNAfold/RNAup_slidingWindow.pl](#rnafoldrnaup_slidingwindowpl) (v0.0.22)
62. [RNAfold/varnacmd.pl](#rnafoldvarnacmdpl) (v0.1.6)
63. [RNAfold/plot_gridRNAcofold.R](#rnafoldplot_gridrnacofoldr) 
64. [RNAfold/suboptclustplot.pl](#rnafoldsuboptclustplotpl) 
65. [RNAfold/ssps2svg.R](#rnafoldssps2svgr) 
66. [RNAfold/gridRNAcofold.pl](#rnafoldgridrnacofoldpl) (v0.0.5)
<br><b>igit.sh</b>
67. [igit.sh](#igitsh) (v0.5.12)
<br><b>conversion</b>
68. [conversion/clustal2fasta.pl](#conversionclustal2fastapl) (v0.0.9)
69. [conversion/faa2fna.pl](#conversionfaa2fnapl) (v0.0.4)
70. [conversion/embl2fasta.pl](#conversionembl2fastapl) (v0.0.25)
71. [conversion/infernal2gff.sh](#conversioninfernal2gffsh) 
72. [conversion/excel2csv.R](#conversionexcel2csvr) 
73. [conversion/fasta2vector.pl](#conversionfasta2vectorpl) (v0.0.9)
74. [conversion/tRNAscanse2gff.sh](#conversiontrnascanse2gffsh) 
75. [conversion/fna2faaPlus.pl](#conversionfna2faapluspl) 
<br><b>alignment</b>
76. [alignment/MSAconsensus.pl](#alignmentmsaconsensuspl) (v0.0.11)
77. [alignment/MSAtrimmer.pl](#alignmentmsatrimmerpl) (v0.0.39)
78. [alignment/MSAscorer.pl](#alignmentmsascorerpl) (v0.0.13)
79. [alignment/MSAplot.pl](#alignmentmsaplotpl) (v0.1.37)
80. [alignment/MSAget_indelMutationDiffof2seqs.pl](#alignmentmsaget_indelmutationdiffof2seqspl) (v0.0.3)
81. [alignment/pairwiseAverageNW.pl](#alignmentpairwiseaveragenwpl) 
82. [alignment/easyalignment.py](#alignmenteasyalignmentpy) 
<br><b>PDB</b>
83. [PDB/evaluate_pae.pl](#pdbevaluate_paepl) (v0.0.18)
84. [PDB/alphafold_visualize.py](#pdbalphafold_visualizepy) 
85. [PDB/automatic_foldseek_api.py](#pdbautomatic_foldseek_apipy) 
86. [PDB/alphafold_unpkl.py](#pdbalphafold_unpklpy) 
87. [PDB/download_pdb_af.py](#pdbdownload_pdb_afpy) 
88. [PDB/extract_seq_pdb.R](#pdbextract_seq_pdbr) 
89. [PDB/cif_to_pdb.py](#pdbcif_to_pdbpy) 
90. [PDB/extract_sse_pdb.R](#pdbextract_sse_pdbr) 
91. [PDB/split_pdb.py](#pdbsplit_pdbpy) 
92. [PDB/alphafold_prepareFasta4submission_new2.3.pl](#pdbalphafold_preparefasta4submission_new23pl) 
93. [PDB/snap_image_pymol.py](#pdbsnap_image_pymolpy) 
94. [PDB/pymolSuperimposePDBs.pl](#pdbpymolsuperimposepdbspl) (v0.0.32)
<br><b>blast</b>
95. [blast/ARBG.sh](#blastarbgsh) 
96. [blast/easyblast.sh](#blasteasyblastsh) 
<br><b>test</b>
97. [test/myproject.result.opt.html](#testmyprojectresultopthtml) 
<br><b>paulquickhelp.sh</b>
98. [paulquickhelp.sh](#paulquickhelpsh) 
<br><b>blastgraph</b>
99. [blastgraph/shuffle_blastgraph.pl](#blastgraphshuffle_blastgraphpl) 
100. [blastgraph/make_subset.pl](#blastgraphmake_subsetpl) 
<br><b>wrapper</b>
101. [wrapper/update_r_pakages.pl](#wrapperupdate_r_pakagespl) 
102. [wrapper/telewatch.sh](#wrappertelewatchsh) 
103. [wrapper/findLineWiseDiff.sh](#wrapperfindlinewisediffsh) 
104. [wrapper/easyproorigami.py](#wrappereasyproorigamipy) 
105. [wrapper/startRstudioDocker.sh](#wrapperstartrstudiodockersh) 
106. [wrapper/ddrive.py](#wrapperddrivepy) 
<br><b>grades.pl</b>
107. [grades.pl](#gradespl) 
<br><b>benchmarker</b>
108. [benchmarker/benchmark_write.sh](#benchmarkerbenchmark_writesh) 
109. [benchmarker/benchmarker_daemon.pl](#benchmarkerbenchmarker_daemonpl) (v0.0.5)

# Recent changes
 CHANGELOG                |  4 ++++<br>
 fastaAndGff/grepfasta.pl | 12 ++++++++----<br>
 3 files changed, 14 insertions(+), 6 deletions(-)<br>

# Scripts
## phylogeny/treeStatistics.py
    Traceback (most recent call last):
      File "/home/paul/Documents/scripts/./phylogeny/treeStatistics.py", line 3, in \<module\>
        from ete3 import Tree
    ModuleNotFoundError: No module named 'ete3'

---
## phylogeny/abc2nwk.py
    Traceback (most recent call last):
      File "/home/paul/Documents/scripts/./phylogeny/abc2nwk.py", line 3, in \<module\>
        from ete3 import Tree
    ModuleNotFoundError: No module named 'ete3'

---
## phylogeny/treeFindRecentDuplication.py
    Traceback (most recent call last):
      File "/home/paul/Documents/scripts/./phylogeny/treeFindRecentDuplication.py", line 3, in \<module\>
        from ete3 import Tree
    ModuleNotFoundError: No module named 'ete3'

---
## phylogeny/ARI.py
    Traceback (most recent call last):
      File "/home/paul/Documents/scripts/./phylogeny/ARI.py", line 5, in \<module\>
        from sklearn.metrics import adjusted_rand_score
    ModuleNotFoundError: No module named 'sklearn'

---
## phylogeny/RF.py
    Traceback (most recent call last):
      File "/home/paul/Documents/scripts/./phylogeny/RF.py", line 5, in \<module\>
        from ete3 import Tree
    ModuleNotFoundError: No module named 'ete3'

---
## phylogeny/easyNCBITAX.py
    Traceback (most recent call last):
      File "/home/paul/Documents/scripts/./phylogeny/easyNCBITAX.py", line 3, in \<module\>
        from ete3 import Tree
    ModuleNotFoundError: No module named 'ete3'

---
## phylogeny/treePruneGroup.py
    Traceback (most recent call last):
      File "/home/paul/Documents/scripts/./phylogeny/treePruneGroup.py", line 3, in \<module\>
        from ete3 import Tree
    ModuleNotFoundError: No module named 'ete3'

---
## phylogeny/easyphylogeny.py
    Usage: easyphylogeny.py [options] 'INPUTFILE'
    you can use 'cat << EOF | phylogeny.py' to directly read from terminal

    Options:
      --version             show program's version number and exit
      -h, --help            show this help message and exit
      -t TYPE, --type=TYPE  (input file type) dist : leaf distance matrix. adj :
                            adjacency list of a tree (each row represents an edge
                            in the form a->b if unweighted or a->b|10 if
                            weighted). aln: fasta-alignment (used for --work
                            largeParsimony) [default: dist]
      -w WORK, --work=WORK  work of type=dist: 'NJ','UPGMA': clustering
                            algorithms. 'AdditivePhylogeny': input matrix needs to
                            be additive. 'generateRandomBinaryTree','generateRando
                            mRootedBinaryTree': tree generation (needs --leaves
                            and maybe --labels). 'smallParsimony': generate edge
                            weights with minimal parsimony score.
                            'greedyNearestNeighborInterchange': change nearest
                            neighbors and apply smallParsimony. 'largeParsimony':
                            build a random binary tree and perform
                            greedyNearestNeighborInterchange. [default: NJ]
      --leaves=LEAVES       mandatory for --work largeParsimony or
                            generateRandomBinaryTree. The leave node ids (species
                            names)
      --labels=LABELS       optional for --work largeParsimony or
                            generateRandomBinaryTree. The leave node labels
                            (sequences)
      -d, --draw            generate a nx plot of the output tree
      --largeParsimonyRestarts=LARGEPARSIMONYRESTARTS
                            the number of initially randomly sampled random binary
                            trees
      --drawIDs             draw node ids instead of labels (usually species names
                            instead of sequences)
      --verbose             
      -v                    
      -x, --test            
      --bigpicture          outputs a step-by-step picture for the algorithm
      --innerNodesJoin      if set inner nodes are displayed as the joined names
                            from the leaves (for NJ and UPGMA)
      --example=EXAMPLE     perform various examples (combine this with --draw
                            and/or --bigpicture): 'corona': do a NJ of a distance
                            matrix based on the corona virus genome found in 9
                            animals. 'HIV': calculate the largeParsimony of
                            another HIV protein of the 20 patients of 'State of
                            Louisiana vs. Richard Schmidt' trial of 1998 (~10min).
                            'adh': generate largeParsimony of the alcohol
                            dehydrogenase (adh) protein of 12 yeast species
                            (~3min).
      --discrepancy         calculate discrepancy between the generated tree and
                            the input distance matrix for work
                            'NJ','UPGMA','AdditivePhylogeny'
      --parsimony           calculate parsimony score for the generated tree (sum
                            of all edge weights)

---
## phylogeny/easyOTOL.py
    Traceback (most recent call last):
      File "/home/paul/Documents/scripts/./phylogeny/easyOTOL.py", line 3, in \<module\>
        from ete3 import Tree
    ModuleNotFoundError: No module named 'ete3'

---
## phylogeny/find_path_ete.py
    Traceback (most recent call last):
      File "/home/paul/Documents/scripts/./phylogeny/find_path_ete.py", line 3, in \<module\>
        from ete3 import Tree
    ModuleNotFoundError: No module named 'ete3'

---
## phylogeny/treeExtractGroup.py
    Traceback (most recent call last):
      File "/home/paul/Documents/scripts/./phylogeny/treeExtractGroup.py", line 3, in \<module\>
        from ete3 import Tree
    ModuleNotFoundError: No module named 'ete3'

---
## R/pauls_functions.R
    ./R/pauls_functions.R: line 1: syntax error near unexpected token `('
    ./R/pauls_functions.R: line 1: `paul_X_gridpolygon_complexHeatmap=function(padding=0.05,col="black",...){'

---
## R/paulComplexHeatmapFindMarkers.R
    ./R/paulComplexHeatmapFindMarkers.R: line 21: syntax error near unexpected token `('
    ./R/paulComplexHeatmapFindMarkers.R: line 21: `paulComplexHeatmapFindMarkers <- function(so, '

---
## R/knitr.sh

---
## R/RMD_make_cell_names.py
    Usage: RMD_make_cell_names.py [options] -i input.RMD >output.RMD

    Options:
      -h, --help            show this help message and exit
      -i INPUT, --input=INPUT
                            read data from INPUT
      --number              add chapter/section numbers to output

---
## R/paulFeaturePlot.R
    ./R/paulFeaturePlot.R: line 2: syntax error near unexpected token `('
    ./R/paulFeaturePlot.R: line 2: `paulFeaturePlot <- function( title, umap, facet_class, color, filter = "", normalizeValues = F, pool = function(x){return(x)}, zoomx = c(-15,10), zoomy = c(-11,11), dpi = 500, NPM1class = 0, plot_box = 0, scale_factor = 0.6, min_dim = 10, flat_dim = 7, pointsize = 2 ){'

---
## R/paulTopGo.R
    ./R/paulTopGo.R: line 1: syntax error near unexpected token `('
    ./R/paulTopGo.R: line 1: `paulTopGo = function(genes, genes_omega, goclass="BP", title=""){'

---
## AON/predictTmValue.pl

    predictTmValue.pl     predict melting temperature Tm.

    SYNOPSIS

    predictTmValue.pl (options) FASTA/STDIN

    	FASTA/STDIN (from file or string argument)

    	--dnaTm, -d : corrects for dna Tm value : = 0.72587 * temp + 0.53782
    	--rnaTm, -r : corrects for rna Tm value : = 0.83807 * temp - 13.26109
    	--full, -f : full output for debugging

    DESCRIPTION

    	RNAcofold wrapper for searching of the self-hybridization temperature (-T), #
    	such that the dimer concentration c(A-rc(A)) equals about 
    	half of the predicted ensemble.

    EXAMPLES

    	# predict a RNA Tm value:
    	$ predictTmValue.pl -r ACACAGATAGA
    	30.21379125

    	# predict a plain Tm value:
    	$ predictTmValue.pl ACACAGATAGA
    	51.875

    VERSION v0.0.22
    AUTHOR Paul Klemm
    DEPENDENCIES perl : Cwd qw(abs_path cwd), File::Basename, RNAcofold (viennarna package)

---
## AON/greedyQiagenLnaOptimizer.pl

    greedyQiagenLnaOptimizer.pl        simple gradient descent algorithm for finding the optimal LNA modifications using the qiagen web API

    SYNOPSIS

    greedyQiagenLnaOptimizer.pl (options) RNA

    	RNA sequence given as string (U are converted to T automatically)

    	-k : set the number of restarts (default:5)
    	-a : by default adjacent positions of a modification are always unmodified. This option disables this behaviour

    DESCRIPTION

    	Simple greedy optimization:

    	for each -k restart do:
    		load the input sequence with all position being unmarked

    		while there is a unmarked position:
    	
    			add a modification to a random position that is unmodified and unmarked and mark this position
    			send this to qiagen 
    			if hybrydizationScore < 20 AND secondaryStructureScore < 20 AND rnaTm improves:
    				save this modification
    				unmark all unmodified positions

    		report this optimum

    EXAMPLES

        $ perl greedyQiagenLnaOptimizer.pl 'GCACAAGAGUAGACU'
    	
    	[greedyQiagenLnaOptimizer.pl] loaded input sequence : GCACAAGAGTAGACT
    	[greedyQiagenLnaOptimizer.pl] started with max_hybrydizationScore=20 and max_secondaryStructureScore=20
    	# sequence	rnaTm	hybrydizationScore	hybridizationLines	secondaryStructureScore	secondaryStructureLines
    	GCACAAGAGTAGACT	43	17	AGTAGACT:TCAGATGA	8	GCACAAGAGTAGACT:  ((    ))     
    	+GC+AC+AA+GAGTAGACT	71	20	AGAGTAGACT:TCAGATGAGA	12	GCACAAGAGTAGACT:  (   (      ))
    	+GC+AC+A+AG+AGT+AGACT	79	19	AGTAGACT:TCAGATGA	10	GCACAAGAGTAGACT:  ((    ))     
    	G+C+A+C+A+AG+AGT+A+G+ACT	88	20	AGACT:TCAGA	14	GCACAAGAGTAGACT: (         )   
    	GC+A+CA+AG+AGT+AGACT	81	19	AGTAGACT:TCAGATGA	14	GCACAAGAGTAGACT:  ((    ))    

    	# First line (GCACAAGAGTAGACT) corresponds to the input sequence with rnaTm,... values
    	# next lines show up to -k optimums for each restart 

        The secondary structure of an oligonucleotide with a score below 20 is unlikely to be stable at room temperature. 
        Scores above 30 are likely to produce secondary structures that are stable at room temperature.

    SEE ALSO
    	the geneglobe.qiagen API for optimizedoligo: 
    		https://geneglobe.qiagen.com/oligotoolsapi/services/oligotools/optimizedoligo?secondaryStructure=true&sequence=SEQUENCE

    	the geneglobe.qiagen API for tmprediction: 
    		https://geneglobe.qiagen.com/oligotoolsapi/services/oligotools/tmprediction?sequence=SEQUENCE

    	the geneglobe user iterface:
    		https://geneglobe.qiagen.com/us/explore/tools/

    VERSION v0.0.35
    AUTHOR Paul Klemm
    DEPENDENCIES perl : List::Util qw/shuffle/, perl : wget

---
## AON/getQiagen.pl
    Usage: getQiagen.pl    simple interface to the qiagen web API

    SYNOPSIS
     
    getQiagen.pl SEQUENCE/FASTA/-

        SEQUENCE    a single input sequence.
        FASTA       input fasta file with multiple sequences.
        -           read from STDIN (fasta format or headless list of sequences)

        Use Tm DNA for applications such as Chromatin ISH-PCR - Southern
        Use Tm RNA for applications such as RNA ISH - microarray - Northern

    EXAMPLES

    	$ getQiagen.pl ACTACACACACAGGG
    	
    	[getQiagen.pl] loaded input sequence : ACTACACACACAGGG
    	# sequence	rnaTm	hybrydizationScore	hybridizationLines	secondaryStructureScore	secondaryStructureLines
    	ACTACACACACAGGG	36	11	CACAGGG:GGGACAC	11	ACTACACACACAGGG: (  (   (   )))

        The secondary structure of an oligonucleotide with a score below 20 is unlikely to be stable at room temperature. 
        Scores above 30 are likely to produce secondary structures that are stable at room temperature.

    VERSION v0.0.26
    AUTHOR Paul Klemm
    DEPENDENCIES perl : List::Util qw/shuffle/ wget internet(:

    SEE ALSO
        the geneglobe.qiagen API for optimizedoligo: 
            https://geneglobe.qiagen.com/oligotoolsapi/services/oligotools/optimizedoligo?secondaryStructure=true&sequence=SEQUENCE
        the geneglobe.qiagen API for tmprediction: 
            https://geneglobe.qiagen.com/oligotoolsapi/services/oligotools/tmprediction?sequence=SEQUENCE

---
## MS/mergeMSexcelTables.R
    Loading required package: readr
    Loading required package: stringr
    Loading required package: optparse
    Loading required package: readxl
    Usage: this program merges excel XLS/XLSX tables based on the *Accession* column. Output is written to output.csv.


    Options:
    	-i CHARACTER, --input=CHARACTER
    		input tables (excel format). Separate excel tables by ':' (no whitespace!). E.g. '--input test.xls:othertable.xlsx:importantdata.xls:anothertable.xls'

    	-c, --condensed
    		

    	-h, --help
    		Show this help message and exit



---
## fastaAndGff/easybcfconsensus.sh
    USAGE : easybcfconsensus.sh REFFASTA READS
     This scripts generate a consensus.fa from READS compared to the reference REFFASTA (missing regions are filled in with with reference sequences)
     Reads are filtered with default bcftools filter [UNMAP,SECONDARY,QCFAIL,DUP]
     Input READS can be in sam or bam format, REFFASTA is the mapping target of READS (matching QNAME of the READS)
     Input is expected to be haploidic
     adapted from https://samtools.github.io/bcftools/howtos/consensus-sequence.html
     You can use murmer and compareMummerFasta.pl for further analysis


    VERSION 0.0.10
    AUTHOR Paul Klemm
    DEPENDANCIES bcftools, samtools

---
## fastaAndGff/rc.pl
    USAGE : ./fastaAndGff/rc.pl  generates the reverse and or complement of a given input sequence

    SYNOPSIS
      ./fastaAndGff/rc.pl (options) FASTAFILE/-

      FASTAFILE = nucleotide (fna) file OR '-' then it takes the input from STDIN (no header is needed).
      
      --complement, --comp, -c : only do the complement
      --reverse, --rev, -r 	: only reverse
      --rna                 : force to use Uracil instead of Thymin (otherwise it is set automatically)
      --dna                 : force to use Thymin instead of Uracil 
      --all, -a             : print all all variations (reversed, complemented, rc and the original sequence). Variations are separated by |
                              you should use the --dna or --rna option along with this or this can produce unwanted results! 
    EXAMPLES

      ./fastaAndGff/rc.pl - <<< 'ACGCAGCGAC'

      # search all variations of a string in a fasta file:

      grep -E "$(./fastaAndGff/rc.pl -a - <<< 'ACGCAGCGAC')" fasta.fna

    VERSION v0.0.2
    AUTHOR Paul Klemm
    DEPENDENCIES perl : Cwd 'abs_path', perl : File::Basename

---
## fastaAndGff/highlightFasta.pl
    USAGE : highlightFasta.pl   marks genomic positions in a fasta file

    SYNOPSIS 
      highlightFasta.pl \<FASTAFILE\> chromosom start end strand
      chromosom can be - for the first entry of the fasta file

      or

      highlightFasta.pl \<FASTAFILE\> \<GFFFILE\>
      start and end are 1-based indices/coordinates

      You can use 'aha' to convert the output to html (conda install aha)

    VERSION v0.3.2
    AUTHOR Paul Klemm
    DEPENDENCIES perl : Cwd 'abs_path', perl : File::Basename

---
## fastaAndGff/getGeneLength.pl
    USAGE: getGeneLength.pl \<FASTAFILE\> Returns for each '>' entry of the fasta file: 'name\tlength(gene)'

    VERSION v0.0.13
    AUTHOR Paul Klemm
    DEPENDENCIES -

---
## fastaAndGff/grep_k_neighbourhood.pl
    grep_k_neighbourhood.pl        finds k upper and lower proteins
     
    SYNOPSIS
     
    grep_k_neighbourhood.pl (OPTIONS) INFILE

    OPTIONS

    	INFILE : a gff file or - for STDIN

    	mandatory:
    		-k=	 the number of matches that are reported (up and down)
    		-q=	 the query string

    	optional:
    		-prefix= 	
    			the regularexpression matching the query string 
    			(e.g. for gff-files: gene=([^:]+))
    		-preselection=
    			regular expression for for identifing rows
    			(e.g. for genes in a gff-files: 	gene	)
    		-full         
    			if set then the full lines are printed instead of the raw IDs

    DESCRIPTION
     
    	The gff is assumed to be sorted.
           
    	Example:
     
    	perl grep_k_neighbourhood.pl -k=3 -q='POP1' gffs/saccharomyces_cerevisiae.gff3

    	STDOUT:
    		SQS1
    		ATG4
    		SSU72
    		POP1 <--------
    		ADE12
    		ALG9
    		MGS1
     
    	If the gff is not sorted, use sort first:

    	LC_ALL=C sort -k1,1 -k4,4n -t$'\t' Lactobacillus_sen.gff | grep_k_neighbourhood.pl -preselection=".*" -prefix="ID=([^;]+)" -q=6SRNA -k=1 -

    	STDOUT:
    		cds549
    		6SRNA <--------
    		cds444

    VERSION v0.0.6
    AUTHOR Paul Klemm
    DEPENDENCIES perl : Getopt::Long

---
## fastaAndGff/splitGFF.pl
    splitgff.pl (options) \<input.gff\>        splits the input.gff into chromosom and strand specific gff files (the files are created in the cwd)

    OPTIONS

    	-nostrand : disables the splitting by strand
    	-nochromosome : disables the splitting by chromosome

    VERSION v0.0.4
    AUTHOR Paul Klemm
    DEPENDENCIES perl : Getopt::Long, perl : File::Basename

---
## fastaAndGff/splitfasta.pl
    	print "USAGE: splitfasta.pl \<FASTAFILE\>
    Returns for each '>' entry of the fasta a single file inside a created *_fastaFiles/ directory (* = input fasta name)

    OPTIONS
    	-brackets split by name within the square brackets at the end of the header. 
    		>AAM01497_1 Glutamate-1-semialdehyde aminotransferase [Methanopyrus kandleri AV19]
    		results in the file: Methanopyrus_kandleri_AV19.fasta

    	-OS -OX split by name given as OS/OX end of the header. 
    		>AAM01497_1 Glutamate-1-semialdehyde aminotransferase OS=Methanopyrus kandleri AV19 OX=...
    		results in the file: Methanopyrus_kandleri_AV19.fasta

    VERSION v0.0.21
    AUTHOR Paul Klemm
    DEPENDENCIES -

---
## fastaAndGff/easyNCBI.pl
    Can't locate List/MoreUtils.pm in @INC (you may need to install the List::MoreUtils module) (@INC entries checked: /usr/lib/perl5/5.38/site_perl /usr/share/perl5/site_perl /usr/lib/perl5/5.38/vendor_perl /usr/share/perl5/vendor_perl /usr/lib/perl5/5.38/core_perl /usr/share/perl5/core_perl) at ./fastaAndGff/easyNCBI.pl line 10.
    BEGIN failed--compilation aborted at ./fastaAndGff/easyNCBI.pl line 10.

---
## fastaAndGff/newickToAverageDistance.pl
    Can't locate Bio/TreeIO.pm in @INC (you may need to install the Bio::TreeIO module) (@INC entries checked: /usr/lib/perl5/5.38/site_perl /usr/share/perl5/site_perl /usr/lib/perl5/5.38/vendor_perl /usr/share/perl5/vendor_perl /usr/lib/perl5/5.38/core_perl /usr/share/perl5/core_perl) at ./fastaAndGff/newickToAverageDistance.pl line 4.
    BEGIN failed--compilation aborted at ./fastaAndGff/newickToAverageDistance.pl line 4.

---
## fastaAndGff/faa2kDa.pl
    USAGE ./fastaAndGff/faa2kDa.pl

    0 No such file or directory

---
## fastaAndGff/promotech_find_best.pl
    ./fastaAndGff/promotech_find_best.pl        Filter output of PromoTech search by gff distance to annotated genes.
     
    SYNOPSIS
     
    ./fastaAndGff/promotech_find_best.pl (options) --promotech=PROMOTECHFILE --gff=GFFFILE

    	options
    		--gff=F : input gff
    		--promotech=F : input promotech prediction file (genome_prediction.tsv)
    		--filter=s : filter the gff by this tag => gff column 3 (input is regex-able, e.g. "CDS|ECF", default:CDS)
    		--all : output not only the best scoring promoter for each gene but simply all
    		--max_dist=i : maximal upstream distance, including the 40mere of the promoter (default:150)
    		--disable_detect_operons : if set, the automatic operon detection is disabled

    EXAMPLE/DESCRIPTION

    	Input:
    		the pDM598_pSRKGm_ecf02.gff describes the features of pDM598_pSRKGm_ecf02.fna vector:
    		Lac-Operon regulating target gene=ECF02, Promoters=lacp,lacIp,T5p

    	Exemplary PromoTech commands to produce the input for this script:

    	python promotech.py -m RF-HOT -pg -f pDM598_pSRKGm_ecf02.fna -o results_pDM598_pSRKGm_ecf02_HOT
    	python promotech.py -m RF-HOT -g -t 0.5 -i results_pDM598_pSRKGm_ecf02_HOT -o results_pDM598_pSRKGm_ecf02_HOT

    	./fastaAndGff/promotech_find_best.pl --promotech=results_pDM598_pSRKGm_ecf02_HOT/genome_predictions.csv --gff=pDM598_pSRKGm_ecf02.gff --filter="CDS|ECF"

    	pDM597_pSRKGm_ecf02     1319    1358    0.51177 +       AATGGCGCAAAACCTTTCGCGGTATGGCATGATAGCGCCC        pDM597_pSRKGm_ecf02     GenBank CDS     1388    2470    .       +       1       ID=GenBank-CDS-pDM597_pSRKGm_ecf02-1388-2470;product=lacI;vntifkey=4
    	pDM597_pSRKGm_ecf02     2629    2668    0.83978 +       TTATTTGCTTTGTGAGCGGATAACAATTATAATAGATTCA        pDM597_pSRKGm_ecf02     GenBank ECF     2725    3300    .       +       1       ID=GenBank-region-pDM597_pSRKGm_ecf02-2725-3300;product=ECF02_2817;vntifkey=21

    	The first 6 columns are the promotech output (seqname,start,end,predictionscore,strand,sequence) 
    	and the following 9 are the matching gff entry for this promoter. Only the best promoter for any
    	gff entry is printed (if any).
    	The first identified promoter is the lacIp promoter matching the lacI gene.
    	The second one is the lacZp promoter matching the target gene ECF02. 
    	
    VERSION v0.0.25
    AUTHOR Paul Klemm
    DEPENDENCIES perl : Getopt::Long, File::Basename

---
## fastaAndGff/compareMummerFasta.pl
    compareMummerFasta.pl        compares a gff+fasta with a assembled genome + mummer comparison of the 2 fasta files

    SYNOPSIS

    compareMummerFasta.pl (options) -fastaRef b.fna -gffRef b.gff -fastaNew a.fna -mums avsb.mums

    MANDATORY

    	-fastaRef and -gffRef : the reference gff and genomic fasta files
    	-fastaNew : new assembled genomic fasta file (needs to have matching chromosome names)
    	-mums : the output of 'mummer -mums -F FASTANEW FASTAREF'

    OPTIONAL

    	-gff_filter : filters the input gff (feature) [default:gene]
    	-gff_id : the attribute name used as the id (part of attribute) [default:ID]
    	-out : the output prefix name (default:compareMummerFasta.pl.project)
    	-startAt0 : instead of searching for the reading frame (first start codon...), just use the annotated start of the gff
    	-skipN : skip differences with unknown nucleotides ('N')
    DESCRIPTION

    	The input to mummer needs to be 1. new assmbled fasta and 2. the reference fasta, s.t. the mums contains:
    		chr,pos_new,pos_ref,len_match
    	The output is split in 3 files:
    		out.pos : VCF like output of single nt changes (includes codon changes according to the gff)
    		out.stat : gene-level changes (how many genes contain a indel,...)
    		out.aln : each non-identical gene is aligned (between the REF and NEW). 

    	Use case:
    		1. Map DNAseq reads against a reference genomic fasta file
    		2. Create a consensus fasta file (-fastaNew) e.g. with easybcfconsensus.sh
    		3. apply mummer to compare the fasta files
    		4. summarize differences with this script

    LIMITATIONS

    	1. for genes on - strand: the codons are printed from the - strand while the Ref/New nt are always from + strand
    	2. gene start of NEW is assumed to be identical to the one of the REF (gff start of ref with alignment position of new)
    	3. frameshift is not detected (e.g. could be #indels mod 3 != 0) and mutations are analyzed independently

    VERSION v0.7.19
    AUTHOR Paul Klemm
    DEPENDENCIES Perl Getopt::Long, File::Basename, easyalignment.py (https://gitlab.com/paulklemm_PHD/scripts)

---
## fastaAndGff/grepfasta.pl
    grepfasta.pl        greps all genes/proteins of a given fasta file
     
    SYNOPSIS
     
    grepfasta.pl (options) QUERY INFILE1 (INFILE2 ...)

    	QUERY	identifier FILE or search query STRING:
    		a)	string of one identifier e.g. 'tr|asd3|asd' OR multiple identifier separated by ',' OR '-' for STDIN
    		b)	(-file QUERY) file with the ids. The file contains either just 
    			identifier or tabseparated a mapping:
    			e.g. POP1 to the 3 identifier 
    			tr|asd|asd, tr|asd2|asd and tr|asd3|asd.
    	INFILE	file containing the query ids (database)

    	optional:
    		-tofiles, -t 	print everything to files instead of files 
    		-tofilesSource, -ts  same as -tofiles but output files have the source file name in their file names
    		-v 	    negates the search
    		-E    	enables regex matching otherwise the string is escaped (e.g. | -> |)
    		-i    	enables case insensitive search
    		-source, -s    	adds the file name to the found gene name
    		-F=s 	char delimiter for multiple identifier (default: ',')
    		-tag=s 	search by fasta tag, e.g. -tag='GN' will search for ... GN=PRPSAP2 ...
    		-seq    search in the sequence space instead of in the header
    		-acc    enables accession number search instead of a full id search (use A0A2R6RCR6 instead of tr|A0A2R6RCR6|A0A2R6RCR6_ACTCC),
    				(does not work together with -E)
    		-end    query and target string are matching if they are equal up to a dangling end.
    		        e.g. query="test42" matches "test42 and some more info" with -end.

    DESCRIPTION
     
    	This script finds all ids of a list of fasta files with identifier
    	provided in a different file and saves the output to seperate files named
    	ID_FASTA where ID is the found identifier and FASTA the 
    	name of the fasta file that contains this identifier.
    	STDERR contains ids and the number of times they are found.
    	Use GREPFASTADIR environment variable to store fa-idx files (otherwisse they are written to input files or current directory)
           
    EXAMPLES

     	# 1. most simple call:
    	perl grepfasta.pl 'tr|asd|asd' *.faa
    		STDOUT:
    			>tr|asd|asd Cell pattern formation-associated protein stuA OS=Acremon(...)
    			MNNGGPTEMYYQQHMQSAGQPQQPQTVTSGPMSHYPPAQPPLLQPGQPYSHGAPSPYQYG

     	# 2. regex search:
    	perl grepfasta.pl -E '.*a?sd[0-3].*' *.faa

     	# 3. multiple ids:
    	perl grepfasta.pl 'tr|asd|asd,tr|asd2|asd' *.faa

     	# 4. id file and write output to files:
    	cat test.idlist | grepfasta.pl - test.faa
    		-----test.idlist:---------- (does not need the first column)
    		POP1	tr|asd|asd
    		POP1	tr|asd1|asd
    		POP1	tr|asd2|asd
    		POP2	tr|asd3|asd
    		
     	# 5. id file and write output to files:
    	grepfasta.pl -tofiles test.idlist test.faa
    		-----test.idlist:---------- (does not need the first column)
    		POP1	tr|asd|asd
    		POP1	tr|asd1|asd
    		POP1	tr|asd2|asd
    		POP2	tr|asd3|asd
    		
    		------test.faa:------------
    		>tr|asd|asd Cell pattern formation-associated protein stuA OS=Acremon(...)
    		MNNGGPTEMYYQQHMQSAGQPQQPQTVTSGPMSHYPPAQPPLLQPGQPYSHGAPSPYQYG
    		>tr|asd3|asd Histone H4 OS=Acremonium chrysogenum (strain ATCC 11550 (...)
    		MTGRGKGGKGLGKGGAKRHRKILKDNIQGITKPAIRRLARRGGVKRISAMIYEETRGVLK
    		>tr|A0A086SUI7|A0A086SUI7_ACRC1 Uncharacterized protein OS=Acremonium(...)
    		MAFPLHFSREPAHAIPSMKAPFSRHEVPFGRSPSMAIPNSETHDDVPPPLPPPRHPPCTN
    		>tr|asd2|asd 1-phosphatidylinositol 3-phosphate 5-(...)
    		MAFPLHFSREPAHAIPSMKAPFSRHEVPFGRSPSMAIPNSETHDDVPPPLPPPRHPPCTN

    		OUTPUT (2 files)

    		------POP1_test:------------
    		>tr|asd|asd Cell pattern formation-associated protein stuA OS=Acremon(...)
    		MNNGGPTEMYYQQHMQSAGQPQQPQTVTSGPMSHYPPAQPPLLQPGQPYSHGAPSPYQYG
    		>tr|asd2|asd 1-phosphatidylinositol 3-phosphate 5-(...)
    		MAFPLHFSREPAHAIPSMKAPFSRHEVPFGRSPSMAIPNSETHDDVPPPLPPPRHPPCTN
    		
    		------POP2_test:------------
    		>tr|asd3|asd Histone H4 OS=Acremonium chrysogenum (strain ATCC 11550 (...)
    		MTGRGKGGKGLGKGGAKRHRKILKDNIQGITKPAIRRLARRGGVKRISAMIYEETRGVLK

    		STDERR: 	
    		tr|asd|asd	1
    		tr|asd1|asd	1
    		tr|asd2|asd	0
    		tr|asd3|asd	1

    VERSION v0.5.17
    AUTHOR Paul Klemm
    DEPENDENCIES perl : Getopt::Long, perl : File::Basename

---
## fastaAndGff/easyUniprot.pl

    !!! DPRECATED: API did change, nothing works anymore, ... !!!
    !!! DPRECATED: API did change, nothing works anymore, ... !!!

    easyUniprot.pl        simple interface to uniprot API
     
    SYNOPSIS

    easyUniprot.pl options TOOL QUERY

      TOOL : either 'uniprot' for general queries (search all proteins of XYZ) or 'uploadlists' to convert ID or proteomes
      
      if TOOL=uploadlists :
        --from, --to the ID to convert e.g. P_REFSEQ_AC and ACC (see --table=ID) 
      if TOOL=uniprot:
        e.g. query='organism:9601+AND+antigen+OR+length:[100 TO *]'
        here you can use format="fasta"
        taxid can be found here : https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi
      if TOOL=proteomes:
        e.g. query='organism:1423'
        this returns a table of proteomes 

      to download a proteome use 
        tool='uniprot', 
        format='fasta',
        query='proteome:UP000295488' <- this is unqiue 
        or query='organism:224308' <- this downloads all sub-strains too
        ( format fasta with --limit reduces the number of protein sequences and not the number of proteomes )

      QUERY : input query (or file), no STDIN.

      --format       output formats are html, tab, xls, fasta, gff, txt, xml, rdf, list, rss
      --columns      comma separated list of columns to return (default:a simple from to list) for more details see --table=column or --table=columnfull
      --colcommon    short hand for --columns='id,entry name,sequence,reviewed,protein names,genes,organism,organism-id,length,database(RefSeq),go-id,go(biological process),go(molecular function)'
      --include      If set: Include isoform sequences when the format parameter is set to fasta. Include description of referenced data when the format parameter is set to rdf. This parameter is ignored for all other values of the format parameter.
      --compress,-g  If set: A gzipped archive is returned.
      --limit        Maximum number of results to retrieve.
      --offset       Offset of the first result, typically used together with the limit parameter.
      --table        displays various tables to use 
        'ID': identifier used for --from and --to (full table: IDfull)
        'column': identifier used for --column (full table: columnfull)
    EXAMPLES
      $ easyUniprot.pl 'uniprot' 'WP_003246159.1+AND+organism:224308' --colcommon
      find everything about WP_003246159.1 in B.sub. 168 (and all sub-strains)

      $ easyUniprot.pl --from="P_REFSEQ_AC" --to="ACC" 'uploadlists' DEP_result_table.ids.csv
      converts ncbi REFSEQ to uniprot ACCESSION (2 columns table: from to)

      $ easyUniprot.pl --from="P_REFSEQ_AC" --to="ACC" --colcommon 'uploadlists' DEP_result_table.ids.csv
      finds all REFSEQ entries of the input file and outputs a list with all sorts of information + sequences
      - cannot be combined with complex queries like ...+AND+organism:...
      - the from/query column is the last one (yourlist:...)

      $ easyUniprot.pl 'proteomes' 'Bacillus subtilis 168'
      get a list of all B.subt. 168 strains -> e.g. UP000001570

      $ easyUniprot.pl 'uniprot' 'proteome:UP000001570' --limit=10
      find the first 10 proteins of this strain

      $ easyUniprot.pl 'uniprot' 'proteome:UP000001570' --format="fasta" -g >UP000001570.gz
      download the full proteome of this strain
      - use the uniprot UP-ID instead of the NCBI Organism-ID (organism:Organism ID) since it is unique

    VERSION v0.1.18
    AUTHOR Paul Klemm
    DEPENDENCIES perl : Getopt::Long, LMP::UserAgen Mozilla::CA

    !!! DPRECATED: API did change, nothing works anymore, ... !!!
    !!! DPRECATED: API did change, nothing works anymore, ... !!!


---
## fastaAndGff/nrdb_gff.pl
    nrdb_gff.pl        reduces a gff

    SYNOPSIS

    nrdb_gff.pl (options) INPUT.gff

    	-d delimiter for aggregating source, feature, attribute [default:;]
    	
    DESCRIPTION
    	identical entries with same seqname, start, end, strand are combined into one entry with aggregated informations

    VERSION v0.0.10
    AUTHOR Paul Klemm
    DEPENDENCIES Perl Getopt::Long, File::Basename

---
## fastaAndGff/clusterStrings.pl
    clusterStrings.pl        cluster input strings of same lengths (sequences)
     
    SYNOPSIS
     
    clusterStrings.pl (options) INFILE

    	INFILE  input file containing the strings (can be - for STDIN)

    	optional:
    		-fasta      parses fasta formats
    		-consensus  prints out the consensus string (with maximal character probability)
    					otherwise one member of the group is printed
    		-all        print all entries with a group number
    		-tofiles    split output in different files 
    		-stat       emits for each symbol the number input strings that agree with that symbol. The numbers are separated by |
    		-t FLOAT    cluster threshold: the maximal percentual similarity for merging 2 strings/groups (default:0.45) 
    		-maxit INT  maximal number of iterations (default:10000) 

    DESCRIPTION
     
    	TODO

    EXAMPLES

    	# use protein sequences

    	clusterStrings.pl -fasta -consensus proteins.faa

    	# use STDIN

    	awk '{print $1}' somefile | clusterStrings.pl -

    VERSION v0.0.19
    AUTHOR Paul Klemm
    DEPENDENCIES perl : Getopt::Long, File::Basename, List::Util qw( max )

---
## fastaAndGff/getgff.pl
    USAGE : getgff.pl   extracts genomic positions of a given gff file OR from command line

    SYNOPSIS 
      getgff.pl (options) FASTA(S) chromosom start end strand
        chromosom: can be - for the first entry of the fasta file
        start, end: are 1-based indices/coordinates
        FASTA(S) : one or multiple fasta file

      or

      getgff.pl (options) FASTA(S) GFF
        FASTA(S) : one or multiple fasta file (FASTA1 FASTA2 ...)
        GFF : one gff file

    OPTIONAL options

      -id=STRING 
        set the name of the output fasta entries to the gff attribute with this name 
        STRING: product, id, name, ...
          multiple fields can be specified by | (e.g. -id='ID|product|Name')
          the gff column names can be used here too:
        seqname source feature start end score strand frame attribute 

      -format
        standard 80 characters per line output

    VERSION v0.2.5
    AUTHOR Paul Klemm
    DEPENDENCIES perl: Cwd, File::Basename

---
## fastaAndGff/compareGffFasta.pl
    compareGffFasta.pl        compares gffA with gffB using sequences of the corresponding fasta files

    SYNOPSIS

    compareGffFasta.pl (options) -gffA a.gff -gffB b.gff -fastaA a.fna -fastaB b.fna -out output

    	-gff_filter : filters the input gff (feature) [default:gene]
    	-gff_id : the attribute name used as the id (part of attribute) [default:ID]

    DESCRIPTION

    VERSION v0.3.17
    AUTHOR Paul Klemm
    DEPENDENCIES Perl Getopt::Long, File::Basename, easyalignment.py (https://gitlab.com/paulklemm_PHD/scripts)

---
## easysbatch.pl
    Can't locate Term/RawInput.pm in @INC (you may need to install the Term::RawInput module) (@INC entries checked: /usr/lib/perl5/5.38/site_perl /usr/share/perl5/site_perl /usr/lib/perl5/5.38/vendor_perl /usr/share/perl5/vendor_perl /usr/lib/perl5/5.38/core_perl /usr/share/perl5/core_perl) at ./easysbatch.pl line 100.
    BEGIN failed--compilation aborted at ./easysbatch.pl line 100.

---
## plots/boxplot.R
    Usage: EXAMPLES

    boxplot.R -F='	' counts.tsv

    boxplot.R - << EOF
    1;a
    1;a
    1;a
    2;a
    2;a
    3;a
    10;a
    4;b
    3;b
    3;b
    EOF

    or simply

    boxplot.R - << EOF
    1
    1
    1
    2
    2
    3
    10
    4
    3
    3
    EOF

    VERSION v0.1.15
    AUTHOR Paul Klemm
    DEPENDENCIES ggplot2


    Options:
    	--log
    		(optional) plot log10 +1 transformed values

    	--col_names
    		(optional) enable if table contains column names

    	-l CHARACTER, --longinput=CHARACTER
    		input file in long format, (- = STDIN)

    	-w CHARACTER, --wideinput=CHARACTER
    		input file in wide format

    	-F CHARACTER, --delim=CHARACTER
    		field delimiter

    	--comment=CHARACTER
    		comment field character

    	--wideid=CHARACTER
    		id field for wide tables (comma separated list of header names, use e.g. 'X1' in lists without names)

    	-n CHARACTER, --name=CHARACTER
    		output name

    	-h, --help
    		Show this help message and exit



---
## plots/draw_gff.R
    draw_gff.R    simple script to plot gff formats (gggenes)

    SYNOPSIS

    boxplot.R GFFFILE

    	GFFFILE : input gff file

    	genes are named according to the product=... attribute
    	(if product= is not set, then name= or ID= is used)

    EXAMPLES

    draw_gff.R somegenes.gff

    VERSION v0.0.9
    AUTHOR Paul Klemm
    DEPENDENCIES ggplot2, gggenes

    Error: 
    Execution halted

---
## plots/drawgffd3.pl
    drawgffd3.pl        produces a colored representation of a given MSA
     
    SYNOPSIS
     
    drawgffd3.pl (options) GFFFILE 

    	GFFFILE	gff file

    	-group= sets the grouping/column for the different lanes/colors (default:source)

    OUTPUT
    	Static HTML page (STDOUT) using d3 to plot the entries of the gff entry.
    	Different sources are plotted as different lanes with different colors.

    VERSION v0.9.31
    AUTHOR Paul Klemm
    DEPENDENCIES -

---
## plots/csvDatatables.R
    Usage: this program converts a given csv/tsv table to DT:tatables (html). 

    AUTHOR Paul Klemm
    VERSION v0.0.20
    DEPENDENCIES optparse,crosstalk,DT,plotly,htmlwidgets,readr,optparse


    Options:
    	-i CHARACTER, --input=CHARACTER
    		input table (csv or tsv). E.g. '--input test.csv'. Use - for STDIN

    	-f, --firstRowIsNotHeader
    		sets the header to X1, X2, ... and enables automatic column namings for files ending in .gff, .sam and .bla format

    	-t CHARACTER, --type=CHARACTER
    		sets the header to 'gff', 'sam' and 'blast'. If the header contains the first row, the first row is appended to the table.

    	-s CHARACTER, --sep=CHARACTER
    		sets delimiter

    	-o CHARACTER, --open=CHARACTER
    		open directly with [firefox,safari,chrome,...]

    	-h, --help
    		Show this help message and exit



---
## plots/easyUpsetr.R
    Usage: easyUpsetr.R    simple interface to UpSetR

    SYNOPSIS

    easyUpsetr.R -i FILEA,FILEB,...

      FILE(...) : 2 or more input files, entries are compared linewise between these files
      FILE : if only one input file is given, the first column represents the entities and the second the comparators (= file names for multiple FILEs)
      output plot and intersection csv file is written to files with prefix of --project

    VERSION v0.0.30
    AUTHOR Paul Klemm
    DEPENDENCIES ggplot2,eulerr,ComplexUpset,readr,reshape2,optparse,svglite,grid,eulerr


    Options:
    	-p CHARACTER, --project=CHARACTER
    		output prefix

    	-t CHARACTER, --title=CHARACTER
    		output plot title

    	-x CHARACTER, --width=CHARACTER
    		output witdth

    	-y CHARACTER, --height=CHARACTER
    		output height

    	-i CHARACTER, --input=CHARACTER
    		comma separated list of files

    	-h, --help
    		Show this help message and exit



---
## printCSV.pl
    printCSV.pl	prints a given tab/comma/semicolon separated file to the terminal (maximal width is automatically adjusted).

    SYNOPSIS

    	printCSV.pl (options) FILENAME
    	printCSV.pl (options) -

    OPTIONS

    	FILENAME 			can also be - to idicate STDIN.

    	-width NUMBER, -w NUMBER	the maximal width of the table, can be absolute values or relative values (0.5 = half of the maximal window width) [default:auto detect maximal window width]
    	-noheader, -h			if set, every row is treated independently (otherwise the first row determines the with of each column)
    	-k a-b				only show columns from a to b, e.g. 0-3 shows the first 4 columns (0,1,2,3). a,b are 0-index numbers.
    	-k a,b,c...			only show specific columns from a,b,c,.. a,b are 0-index numbers OR column names (specified in first line)
    	-skip				skip first x lines
    	-d delim			specify the delimiter of the input table [default:auto detect]
    	-plain,-p			output plain csv table (only useful if you select some columns with -k as well)
    	-html			generate a html table (-plain suppresses the head-boilercode)
    	-header				generate headers from typical formats (blast6,gff,vcf,sam)

    	NOTE: reoccuring header lines are ignored.

    EXAMPLE

    	$ cat input.csv

     Species       Genes   Alg.-Conn.      C.faa   C2.faa  E.faa   L.faa   M.faa
    2       5       0.16    *       *       *       L_641,L_643     M_640,M_642,M_649
    4       6       0.115   C_12,C_21       *       E_313,E_315     L_313   M_313
    3       6       0.301   C_164,C_166,C_167,C_2   *       *       L_2     M_2
    2       4       0.489   *       *       *       L_645,L_647     M_644,M_646
    3       3       0.312   *       *       E_367   L_319   M_319
    4       5       0.165   C_63,C_22       *       E_19    L_19    M_19


    	$ printCSV.pl input.csv
    	$ cat input.csv | printCSV.pl -

    -------------+------------+------------+------------+------------+------------+------------+------------
       Species   |   Genes    | Alg.-Conn. |   C.faa    |   C2.faa   |   E.faa    |   L.faa    |   M.faa    
    -------------+------------+------------+------------+------------+------------+------------+------------
          2      |     5      |    0.16    |     *      |     *      |     *      |L_641,L_643 |M_640,M_642#
          4      |     6      |   0.115    | C_12,C_21  |     *      |E_313,E_315 |   L_313    |   M_313    
          3      |     6      |   0.301    |C_164,C_166#|     *      |     *      |    L_2     |    M_2     
          2      |     4      |   0.489    |     *      |     *      |     *      |L_645,L_647 |M_644,M_646 
          3      |     3      |   0.312    |     *      |     *      |   E_367    |   L_319    |   M_319    
          4      |     5      |   0.165    | C_63,C_22  |     *      |    E_19    |    L_19    |    M_19    

          The '#' indicates that the cell is cut off due to size limitations

    VERSION v0.0.35
    AUTHOR Paul Klemm
    DEPENDENCIES perl : POSIX, Getopt::Long

---
## RNAseq/identifyPhred.pl
    identifyPhred.pl        Determines the phred format (Sanger,Illumina,...) for a given fastq file
     
    SYNOPSIS
     
    identifyPhred.pl FASTQ

    	FASTQ : the fastq formatted file.

    DESCRIPTION
     
    	The program looks at up to 10k reads and excludes all formats that do not agree with the given phreds of these reads. Then the result is printed.

     	Possible formats : 

    		S = Sanger Phred+33 (!"#$%&'()*+,\-.\/0123456789:;<=>?@ABCDEFGHI)
    		X = Solexa Solexa+64 (;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ\[\\]^_`abcdefgh)
    		I = Illumina 1.3+ Phred+64 (@ABCDEFGHIJKLMNOPQRSTUVWXYZ\[\\]^_`abcdefgh)
    		J = Illumina 1.5+ Phred+64 (BCDEFGHIJKLMNOPQRSTUVWXYZ\[\\]^_`abcdefghi)
    		L = Illumina 1.8+ (very close to S!) Phred+33 (!"#$%&'()*+,\-.\/0123456789:;<=>?@ABCDEFGHIJ)
    	 
     	Source : https://en.wikipedia.org/wiki/FASTQ_format

    SEE ALSO https://en.wikipedia.org/wiki/FASTQ_format
    VERSION v0.0.4
    AUTHOR Paul Klemm

---
## RNAseq/DNAseqMappedAssembler.pl
    DNAseqMappedAssembler.pl        updates a genome from mapped reads

    SYNOPSIS

    DNAseqMappedAssembler.pl (options) FILE.sam/bam

    DESCRIPTION
    	-gff,-g : reference GFF file
    	-mapq : minimum MAPQ score [default:30]
    	-len : minimum read length [default:30]
    	-depth : minimum depth [default:50]

    VERSION v0.1.2
    AUTHOR Paul Klemm
    DEPENDENCIES Perl Getopt::Long, File::Basename

---
## RNAseq/easycarpet.R
    Loading required package: optparse
    Usage: simple deseq2 script comparing control and treatment count files


    Options:
    	-e CHARACTER, --easydeseqnorm=CHARACTER
    		The norm.tsv output of easyDeseq2.R, this table should include columns like gene, l2fc.*, padj.*, baseMean.*

    	-m CHARACTER, --matrix=CHARACTER
    		Alternative input as a matrix in wide format, first column are IDs/gene names. First row is names of columns

    	--project=CHARACTER
    		project name (default:input file name)

    	--highlight=CHARACTER
    		regex filter on ID/gene names

    	--type=CHARACTER
    		mean or median

    	--meanminmax=CHARACTER
    		max mean value > this cutoff

    	--medianminmax=CHARACTER
    		median mean value > this cutoff

    	--colorder=ARRAY
    		order of columns (,-sep)

    	--colorrownorm
    		sets rows to 0-100

    	--cluster_rows
    		cluster_rows

    	--cluster_columns
    		cluster_columns

    	--color=CHARACTER
    		default, orange-black

    	--notext
    		removes text

    	--grid
    		makes a grid

    	--delim=CHARACTER
    		matrix input field delimiter (default:,)

    	-h, --help
    		Show this help message and exit



---
## RNAseq/sgrVis_strandWrapper.pl
    sgrVis_strandWrapper.pl       This program counts (raw) the number of reads (start,end,coverage) on the + and - strands (generates 6 files for each sam file). The raw readcounts of the minus strand gets multiplied by -1.

    SYNOPSIS
     
    sgrVis_strandWrapper.pl --input SAMFILE

    		--strand [+-b] "+" : plus , "-": minus , "+,-" : both strands independently, "b" : both added together	default:+,-
    		--modus [start,end,coverage]	start:only start positions, end: only stop positions, coverage: all positions (=coverage) default:start,end,coverage
    		--input -i	FILE NEEDS TO BE IN SAM FORMAT !!

    VERSION v0.0.5
    AUTHOR Paul Klemm
    DEPENDENCIES Getopt::Long, File::Basename, sgrVis.R, RNAseq_counting_position.pl (https://gitlab.com/paulklemm_PHD/scripts)

---
## RNAseq/sgrVis.R
    Usage: this program visualizes sgr/tsv file(s) and plots the values on the sequence (given by a gff+fasta). If multiple sgr files are given, then error bars are added to the bar plots. If groups of sgr files are given (like control+delta ,...) then mutliple bar plots are drawn side-by-side. If the gff contains the atrribute Str="((...))..." the secondary structure can be drawn using the -varna option. Output files are output.pdf=all plots, output.csv=detailed txt file of all plots, output_short.csv=only significant positions are printed. If 'Error: C stack usage  xxx is too close to the limit' then set 'ulimit -s unlimited'


    Options:
    	-f CHARACTER, --fasta=CHARACTER
    		fasta file name

    	-g CHARACTER, --gff=CHARACTER
    		gff file name OR a string 'seqname start end strand (name)' (e.g. 'NC_123 100 500 - testgene', name is optional)

    	-s CHARACTER, --sgr=CHARACTER
    		sgr file or group of files separated by ',' or ':' or list of groups separated by '|'

    	--tsv=CHARACTER
    		(optional) tsv with 2 columns: first the nucleotide sequence and second some value. This overwrites the -sgr option and sets --delta 0.

    	-d NUMBER, --delta=NUMBER
    		(optional) upstream downstream regionlength around a gene (in nt), default:10

    	-k NUMBER, --k=NUMBER
    		(optional) histogram barsize (width), defines a sliding window that sums up k values at a time (the higher k, the lower the resolution), default:1

    	-n CHARACTER, --norm=CHARACTER
    		(optional) normalize modus ax,sum,globalmax,globalsum,nothing] the global* option define the max/sum over all given values while the other 2 define the max/sum for each vector of values (then all values are devided by this normalization-value) , default:globalmax

    	-p CHARACTER, --plot=CHARACTER
    		(optional) plot type [bar,box,line,violin,linebar]-plot default:bar

    	-q REGEXSTRING, --grep=REGEXSTRING
    		(optional) grep the given gff with this query string (regex enabled)

    	-c [RESIDUE,SGRGROUPS], --color=[RESIDUE,SGRGROUPS]
    		(optional) color, default:sgrgroups if at least 2 groups are given and and residue otherwise. residue coloring requires the fasta or tsv option.

    	--colormanual=
    		(optional) mapping from color names to color hex codes, in the from NAME:#fffff,NAME:#00000,...

    	-a NUMBER, --alpha=NUMBER
    		(optional) alpha level (type 1 error) threshold for --test, default:0.05

    	-o DIRECTORY, --outputdir=DIRECTORY
    		(optional) output directory. 2 files are generated a output.pdf containing all plots and a output.csv containing all the information as txt file. default:cwd

    	--name=CHARACTER
    		(optional) output name. default:name of the input sgr or tsv file

    	-m NUMBER, --mintest=NUMBER
    		(optional) minimum value for applying the test (after normalization, so this is in %), default:0.05

    	--minvalue=NUMBER
    		(optional) minimum value displaying entries, default:10

    	--ignore=INTEGER
    		(optional) ignores sgr information of Xnt of the start (left). If X is negative the same for the end position

    	--nofold=INTEGER
    		(optional) do not fold given X nt of the start if positive. if negative the same for the end

    	--varna
    		(optional) invoke varna system call. The gff needs the attribute Str="((.)(..." or the program will invoke RNAfold. You need to be able to call 'java -cp /usr/local/bin/VARNA*.jar fr.orsay.lri.varna.applications.VARNAcmd'. Requires the fasta option.

    	--facet
    		(optional) make a facet grid instead, based on the -color variable.

    	--firstIsCTRL
    		(optional) if number of groups >= 2 : this enables that the first group is marked as control for testing (all other groups are compared against this group only). Otherwise Kruskal-Wallis group comparison is perfomed.

    	--relativeToFirst
    		(optional) if number of groups >= 2 : All possible 2 paired differences against the control are displayed (and the first group is discarded).

    	--highlight=CHARACTER
    		(optional) sequence hightlight (e.g. GATC, then all GATC are marked yellow)

    	--highlightF=CHARACTER
    		(optional) sequence hightlight positions in a file, format: seqname\tposition

    	--maxY=DOUBLE
    		(optional) maximal y-value

    	--csv
    		(optional) output various csv tables

    	--circular
    		(optional) circularizes the plot

    	--rnafold
    		(optional) generate RNAfold secondary structure

    	--rds
    		(optional) output RDS

    	-w INTEGER, --width=INTEGER
    		(optional) set the width of the plot

    	--strand=[+,-]
    		(optional) only plot gff entries of plus or minus strand

    	--gggenes=BOOL
    		(optional) if set, then no sequence is printed but instead gggenes representation supplied gff file/string. The -gff should then correspond to a larger region is plotted (simply give a string input). Either give a gff file or a string defining one gff entry in the format : 'seqname start end strand (name)' (e.g. 'NC_123 100 500 - 6SRNA')

    	--zoom=[LEFTRIGHT,MINMAX,BOTH,LEFTMAX]
    		(optional) plot zoomed subplots of the left and right region and/or the minimum and maximum region

    	--ylab=
    		(optional) set the ylabel

    	-h, --help
    		Show this help message and exit



---
## RNAseq/easyKEGG.pl
    easyKEGG.pl        simple KEGG interface through http requests
     
    SYNOPSIS
     
    easyKEGG.pl (options) OPERATION ARGUMENT

    	OPERATION
    		pathway2gene : builds a table that maps pathway ID to gene ID. requires the organism ID (e.g. hna, vna) 

    	ARGUMENT
    		additional arguments, see OPERATIONS

    	options
    		--verbose : print all wget commands and outputs
    		--delay=i : delay between each query in seconds (default:1)

    DESCRIPTION
     
    	
           
    EXAMPLES
     
    	easyKEGG.pl pathway2gene vna

    VERSION v0.0.25
    AUTHOR Paul Klemm
    DEPENDENCIES perl : Getopt::Long, File::Basename

---
## RNAseq/easyDeseq2.R
    Loading required package: optparse
    Usage: simple deseq2 script comparing control and treatment count files


    Options:
    	-c CHARACTER, --counts=CHARACTER
    		count file with the format 'gene	filename	count'

    	--sraruntable=CHARACTER
    		sra file converting filename to some other condition name (tab or ,-separated). If there is a column 'Biosample': collapse technical replicates if biosample id is identical (disable this behaviour with --sranotechno).

    	--sracol=CHARACTER
    		(optional) the 2 columnnames that convert the file of counts to another column separated by ':' (e.g. 'Run:Condition'). You only need to define the basename of counts$file. You can combine columns with + (e.g. 'Run:Condtion+time'), then columns 'Condition' and 'time' are merged (-> resistant_24hpi)

    	--sracmp=CHARACTER
    		(optional) list of comparisons in the form of 'A:B,A:C,...'. You can use '*:A' to compare everything against A. If empty all pairwise comparisons are made

    	--sraref=CHARACTER
    		(optional) identify reference level with this regex (important in combination with --complexdesign).

    	--highlight=CHARACTER
    		(optional) highlight genes in plots and export ','-separated.

    	--remove=CHARACTER
    		(optional) remove experiments ,-separated list (file column of counts or replaced names from sraruntable)

    	--control=CHARACTER
    		control replicates/filenames separated by ','

    	--treatment=CHARACTER
    		treament replicates/filenames separated by ','. Multiple treatment group can be separated by ':'

    	--treatmentNames=CHARACTER
    		(optional) treament group names separated by ':' too, needs to match the groups of --treatment

    	--controlNames=CHARACTER
    		(optional) control group names separated by ':' too, needs to match the groups of --control. If omitted, then all are control entries are called 'ctrl' and compared against all treatments

    	--project=CHARACTER
    		(optional) prefix for all outputfiles

    	--readcount_cutoff=INTEGER
    		(optional) genes with a sum count (across all samples) with less than readcount_cutoff are excluded

    	--html
    		(optional) enables html output of the MAplot

    	--featureCounts=CHARACTER
    		(optional) input count format is wide instead of long (the featureCounts output). Here specify the name of the column conting the IDs (usually Geneid or product)

    	--noshrink
    		(optional) disables ashr l2fc shrinkage

    	--noupsetr
    		(optional) disables ashr upsetr

    	--nocor
    		(optional) disables correlation plots

    	--sranotechno
    		(optional) if you use --sraruntable and there is 'Biosample' present -> ignore this information

    	--sraregex
    		(optional) enables regex search for --sracmp

    	--sranorm
    		(optional) common clean up (e.g. 'hours post infection'->'hpi')

    	--prepare
    		exit after preparation step (just before calling deseq)

    	--umap
    		(optional) enables umap

    	--useunadjustedpvalue
    		(optional) use pval instead of padj

    	--keep
    		(optional) keep intermediate files

    	--complexdesign
    		(optional) if sraruntable is given and sracol specifies more than one feature (e.g.Run:treat+time) then this option builds a design matrix according with all feature combinations instead of concatenating everything

    	--correctReplicateOutlier
    		(optional) removes DEG between replicates based on correlation

    	--sd_cutoff=INTEGER
    		(optional) minimum number of sd that a outlier needs to deviate from the mean residual error for --correctReplicateOutlier (default:4)

    	--l2fc_cutoff=INTEGER
    		(optional) l2fc cutoff (default:1)

    	--plotRData
    		rerun all plots for the RDS file of the project. Does currently not work with --complexdesign

    	--cores=INTEGER
    		(optional) number of cores to use (default:1)

    	--basemeancut=INTEGER
    		(optional) displays a * at entries with basemean larger than this (default:-1 = disabled)

    	--max_rows_heatmap=INTEGER
    		(optional) maximal number of rows in the heatmap, if exceeded extract the HVG

    	--max_rows_pca=INTEGER
    		(optional) maximal number HVGs used for the PCA

    	--max_gene_label_volcano=INTEGER
    		(optional) maximal number of rows to display a name in the vulcano plot, if exceeded extract the HVG

    	--max_cmp_venn=INTEGER
    		(optional) maximal number of comparisons for venn diagram plot

    	--annotation=CHARACTER
    		(optional) file containing further annotations of the genes given in --counts (tab or ,-separated: gene to descriptions).

    	--r2_cutoff=DOUBLE
    		(optional) maximum R2 to correct for outlier for --correctReplicateOutlier (default:0.9)

    	--install
    		install all required packages, this may take a while

    	-h, --help
    		Show this help message and exit



---
## RNAseq/RNAseq_removePolyEnds.pl
    RNAseq_polyXtrimmer.pl        trimms polyX ends/starts (polyA, polyC, ...)
     
    SYNOPSIS
     
    RNAseq_polyXtrimmer.pl (options) INPUT

    	INPUT	file/string/STDIN containing the sequences (for files : fasta,fq). - : STDIN. String input, e.g. "ACACA"

    	optional:
    		-polyX	defines the nucleotide that needs to be trimmed (A : polyA, C: polyC,...) [default:A]
    		-min	miminal length of the polyX sequences [default:5]
    		-greedy	if set, then the first sequence is cut of length > min
    		-onlyt	if set, then only trimmed sequences are outputted
    		-d		direction of cutting (3: at 3' end, 5: at 5'end) [default:3]
    DESCRIPTION
     
    	This script finds the longest continues sequences of the specified -poly NT and trimmes the sequences
    	and everything following the match.
           
    EXAMPLES
     
     	# 1. most simple call:

    	RNAseq_polyXtrimmer.pl -polyX C - <<< "ACCCCGCCCCCCCGTGT"
    	RNAseq_polyXtrimmer.pl -polyX C "ACCCCGCCCCCCCGTGT"
    	RNAseq_polyXtrimmer.pl -polyX C myfile.fasta
    	
    	-> ACCCCG

    VERSION v0.0.4
    AUTHOR Paul Klemm
    DEPENDENCIES perl : Getopt::Long, File::Basename

---
## RNAseq/sgrVis_addFoldToGFFWrapper.pl
    sgrVis_addFoldToGFFWrapper.pl       Auxillary program to run sgrVis.R --varna 

    SYNOPSIS
     
    sgrVis_addFoldToGFFWrapper.pl --gff GFFFILE --fasta GENOMEFILE

    		adds secondary struture and sequence to the gff entries for the use in sgrVis.R

    		# first call this program
    		sgrVis_addFoldToGFFWrapper.pl --gff GFFFILE --fasta GENOMEFILE >GFFFILE.sgrvis.gff

    		# then feed the new gff to sgrVis.R
    		sgrVis.R -g GFFFILE.sgrvis.gff -f GENOMEFILE ...

    VERSION v0.0.5
    AUTHOR Paul Klemm
    DEPENDENCIES Getopt::Long, File::Basename, RNAfold (vienna package), getgff.pl 

---
## RNAseq/highlightGenome.R
    Usage: highlights features
    EXAMPLES

      # draw only CDS features and highlight the features of LISTGENES
      highlightGenome.R -i LISTGENES.txt -g GENOME.gff --gff_id NAME
      # highlight CDS123 + CDS100 in multiple gff files
      highlightGenome.R -i 'CDS123,CDS100' -g GENOME.gff,GENOME2.gff --gff_id ID
      # highlight proteinortho output
      highlightGenome.R -p myproject.proteinortho.tsv -g GENOME.gff --gff_id Name

    VERSION v0.1.13
    AUTHOR Paul Klemm



    Options:
    	-g CHARACTER, --gff=CHARACTER
    		gff file name(s) (multiple files can be separated by comma)

    	-i CHARACTER, --input=CHARACTER
    		one of the two: (i) a filename with a list of features names to highlight (based on gff_id). (ii) a string with a list of names (comma separated)

    	-p CHARACTER, --proteinortho=CHARACTER
    		a proteinortho.tsv file, if the file only contains one group a zoomed version is printed else all groups are shown

    	--gff_id=CHARACTER
    		the gff attribute key name (e.g. id=XY)

    	--gff_filter=CHARACTER
    		filter gff by those values (comma separated list of features). if nothing is given this filter is disabled

    	-E, --regex
    		Enables regex search

    	--proteinortho_delta=INTEGER
    		if only one proteinortho group is given, plot proteinortho_delta adjacent gff entries around the group

    	--height=INTEGER
    		sets output height

    	--width=INTEGER
    		sets output width

    	-h, --help
    		Show this help message and exit



---
## RNAseq/alignNrdbReadsPlot.sh
    USAGE: alignNrdbReadsPlot.sh (options) \<SAMFILE\>
    Simple pipeline that converts a sam file to an aligned html.
    redundant reads are pooled together, the name encodes the number of reads (x_NUMBEROFREADS).
    Output files are: SAMFILE.fna, SAMFILE.fna.nrdb, SAMFILE.fna.nrdb.aln, SAMFILE.fna.nrdb.aln.html

    OPTIONS:
     -r \<STRING\> : Reference sequence that will be displayed at the top of the output
     -g start|end|length|rname : group reads by the start or end position or the RNAME column
     -a cigar|qual|muscle|clustalo : align by either one of these option (cigar: insertions are omitted, qual: Phred+33), default: muscle
     -n : exclude groups of size 1
     -x : only print largest representant for each group
     -k : if set intermediate files are kept as well

     common options are -a cigar -g rname

    VERSION v0.8.10
    AUTHOR Paul Klemm
    DEPENDENCIES muscle, nrdb.pl, MSAplot, aha

---
## RNAseq/estimate_len_sd.pl
    Can't open -h: No such file or directory at ./RNAseq/estimate_len_sd.pl line 8.
    Illegal division by zero at ./RNAseq/estimate_len_sd.pl line 16.

---
## RNAseq/RNAseq_counting_gene.pl
    RNAseq_counting_gene.pl        converts given sam/bam files to a count table. The output format is sgr.

    SYNOPSIS

    RNAseq_counting_gene.pl (options) FILE1.sam/bam FILE2.sam/bam FILE3.sam/bam ...
    	
    	-outputReads	
    		If set, then reads are outputted for each target (for each gff entry, or mapped target). 
    		WARNING: this can produce a lot of files and it can take a long time...

    	-multimap, -multimapUniform
    		If set, all mapped reads are counted but multimap mapped reads are equally distributed among all hits. 
    		So if a read maps to 3 targets, then each target gets +1/3
    	
    	-multimapOne
    		If set, all mapped reads are counted as if they are uniquely mapped reads. 
    		So if a read maps to 3 targets, then each target gets +1
    	
    	-multimapDistribution FILE
    		Same as --multimapUniform but the input file specifies the relative quantities of the targets.
    		FILE : a mapping (tab separated) of the name of the target and the amount of that entry (other targets are dismissed)

    	-position (start|end|coverage|startend)
    		instead of counting features (genes, CDS, ...) count the number of start/end positions or calculate the coverage.
    		- The end of a read is calculated with the CIGAR string (alignment end position in target).
    		- The strand information is used as well.
    		- This option will add the coloumn '0based-position-to-start' to the output table indicating:
    			0  = first nt/start of the gff entry (the so-called '+1'), 
    			-i = i nt in front of the start (gff) (e.g. -35 region for CDS)
    			This is position is relative to the strand of the gff entry.
    		- Can be used together with -gff. Use -examplesPosition for some examples.

    	-gff, -g 	
    		A gff file for counting: if at least 50% of the read mapped inside the start-end range of a gff entry, if genes (gff) overlap then all entries are counted
    		The sam CIGAR string is used for the overlap calculations !

    		counted Reads :        |=>  |=>  |=> |=>     |=>  |=>           
    		omitted Reads : |=>|=>                                   |=>                   
    		GFF           : _____|=======Gene1======>___________________
    		GFF           : ____________________|=======Gene2======>____

    	-gff_filter : filter the gff file by feature (the 3. column). Usually this is set to 'gene' or 'CDS'.
    				  You can use 'CDS|rRNA|exon' or '.*RNA|gene' to define multiple filters at once [default:'']
    	-gff_id 	: The id that is used for counting (do not use Name/product since it is usually not unique!)
    				  You can use 'ID|product' to combine the 2 attributes ID and product [default:ID|product]
    	-gff_start=3 : If set, only reads are counted that overlap the +-3nt area around the start, 
    	               you can also encode this in the gff (add 'gff_start=3;' to the gff attribute field).
    	               The start is adaptively chosen for + and - stranded features if -sc is set, otherwise this is always the gff start position.
    	               This disables the requirement of a 50% overlap.
    	-gff_end=3   : Same for the end.
    	-gff_overlap: minimal percentual overlap of a read with a gff entry (relative to read length) [default:0.5].
                      A value of 0 corresponds to the default featureCount (at least 1 nt).

    	automatic for -gff : 
    		strand specific counting : omitting reads that do not match the strandness of the gff entry (see -examples for more details) 
    		(If -gff is not used, strand information is omitted)

    	-s	: a tab separated grouping file that maps from a groupid to target names (<-as provided in the sam file). 
    		  This can be used for counting isoforms, splicing variations, ... 
    		  If a read maps uniquely to only target names of a group, the groupid counts +1. 
    		  All other target names are treated normally.
    		  This option can be combined with -gff option (the grouping should then reference the names/ids of -gff_id)

    	-filter
    		exclude decimal flags including these bits, given in comma separated list. 
    		If no multimap options is used the flags 256 and 2048 are used as well [default:4,512,1024]
    			1 : Read paired
    			2 : Read mapped in proper pair
    			4 : Read unmapped
    			8 : Mate unmapped
    			16 : Read reverse strand
    			32 : Mate reverse strand
    			64 : First in pair
    			128 : Second in pair
    			256 : Not primary alignment
    			512 : Read fails platform/vendor quality checks
    			1024 : Read is PCR or optical duplicate
    			2048 : Supplementary alignment
    			
    	Hint for large gff files: You can use the 'split -l 1000 --numeric-suffixes some.gff some.gff' to split your gff into smaller chunks, then run this program multiple times (using the fast approach with -m 1000) and add the results together.

    DESCRIPTION
    	- input can be sam or bam (samtools is needed for bam)
    	- output is written to STDOUT (sgr format)
    	- unmapped reads are omitted by default (bit flag 4, -filter)
    	- CIGAR string is used for end positions/coverage !
    	- RNASeq sam/bam input files are assumend to be 1based

    EXAMPLES
    	-examplesGFF   : displays various examples for the -gff option
    	-examplesPosition   : displays various examples for the -position option

    VERSION v0.8.35
    AUTHOR Paul Klemm
    DEPENDENCIES Perl Getopt::Long, File::Basename

---
## RNAseq/easyTopGO.R
    installing topGO...
    Installing package into /home/paul/R/x86_64-pc-linux-gnu-library/4.4
    (as lib is unspecified)
    Warning messages:
    1: package topGO is not available for this version of R

    A version of this package for your version of R might be available elsewhere,
    see the ideas at
    https://cran.r-project.org/doc/manuals/r-patched/R-admin.html#Installing-packages 
    2: In library(package, lib.loc = lib.loc, character.only = TRUE, logical.return = TRUE,  :
      there is no package called topGO
    Usage: topGO analysis wrapper (mostly usable only for homo sapiens), either set --deseqlfc OR --regulated_genes and --omega_genes


    Options:
    	-d FILE, --deseqlfc=FILE
    		the deseq_lfc.csv file from simpleDeseq.R containing the columns Gene;comparison;l2fc;baseMean;padj

    	-f NUMBER, --l2fcfilter=NUMBER
    		the cutoff for filtering the --deseqlfc table (l2fc), 1 -> only genes above 1, -1 -> only genes below -1 [default: 1]

    	-r REGULATED_GENES, --regulated_genes=REGULATED_GENES
    		list of regulated genes, ',' or ' ' or ';'-separated

    	-o OMEGA_GENES, --omega_genes=OMEGA_GENES
    		list of all possible genes, ',' or ' ' or ';'-separated

    	-g GOCLASS, --goclass=GOCLASS
    		goclass to analyse one or multiple of BP,CC,MF, ','-separated [default: BP,CC,MF]

    	-t TITLE, --title=TITLE
    		plot title [default: easy]

    	--sep=SEP
    		table column separator

    	-c, --correct
    		do typical cleanup of input data (trim ",_,...)

    	--org=CHARACTER
    		the species annotation https://www.bioconductor.org/packages/2.10/data/annotation/ [default: org.Hs.eg.db]

    	-v, --version
    		

    	-h, --help
    		Show this help message and exit



---
## RNAseq/RNAseq_cigar_plot.pl
    USAGE : RNAseq_cigar_plot.pl   simple motif plot of the CIGAR field of a sam/bam file

    SYNOPSIS 
      RNAseq_cigar_plot.pl INPUT.sam|bam

    DESCRIPTION
    	- The output is written to INPUT.cigar_plot.pdf
    	- A intermediate INPUT.cigar_plot.R file is used to store data
    	- The '=' are converted to M
    	- n is used to (right) pad short reads to same lengths
    	- If there are more than 10k reads -> randomly choose 10k entries.
    	- Unmapped reads are omitted and reads on reverse strand are reversed 

    VERSION v0.0.9
    AUTHOR Paul Klemm
    DEPENDENCIES R: ggseqlogo, ggplot2

---
## RNAfold/RNAup_slidingWindow.pl

    RNAup_slidingWindow.pl     performes RNAup in k sized windows with step size s.

    SYNOPSIS

    RNAup_slidingWindow.pl (options) FASTA/-

    	FASTA file or - for STDIN 
    		The input should have at least 2 sequences. The first sequence is used as the (longer) target to 
    		compare to all other (shorter) sequences.

    	-w : window size (default 1500), needs to be smaller than the long target
    	-s : step size (default 300 = 20% of the window size), needs to be smaller than the window size (at most 0.5*w)
    	--cpus, -t : number of threads (default: 6)
    	--arg, -a : additional RNAup parameters. The parameters are substituted for ARG in: 
    		RNAup ARG --interaction_first --no_output_file << INPUP'

    DESCRIPTION

    	A w sized window slides in s steps across the input sequence and RNAup is perfomed and
    	the optimal/minimal binding energy is printed. Bindings to the edges of the window 
    	(first and last area of size s) are omitted.
    	The result should correspond to a full RNAup call if there are no long-range 
    	interaction (longer than window size 1500).

    	!! Either the first input or the other input sequences needs to be longer than the window size w !!
    	
    	The RNAup modus is '--interaction_first --no_output_file'.

    VERSION v0.0.22
    AUTHOR Paul Klemm
    DEPENDENCIES perl : Cwd qw(abs_path cwd), List::Util qw(max), File::Basename, Thread::Queue, RNAup (viennarna package)

---
## RNAfold/varnacmd.pl
    varnacmd.pl  a simple varna interface

    SYNOPSIS
        varnacmd.pl (options)

        -seq SEQ             : the sequence (varnas -sequenceDBN)
        -fold FOLD           : the fold information (varnas -structureDBN)
        -highlight HIGHLIGHT : varnas -highlightRegion. 
                               e.g. 1-15:fill=#cfcfcf;5-20:outline=#cfcfcf
                               -> highlights pos 1-16 (1-based index!)
                               or you can supply one subsequence that you want to highlight (or multiple delimitted by ;)
        -output OUTPUTNAME   : the output name (default:varna.svg). Needs a suffix like png,jpg,svg,... !
        -resolution R        : resolution of the output image (default:resolution)
        -varnadir DIR        : varna directory (default:cwd and /usr/local/bin/)
        --ignore NUM         : ignore the first NUM positions (secondary structure)
                               if NUM is negative: ignore the left most NUM positions
        --anno STR           : annotation vector (;-separated)
                               e.g. :type=B,anchor=2;texthere:type=B,anchor=3;...
                               (1-based index)

        instead of seq and fold you can also provide a fold file as given by RNAfold through the STDIN (RNAfold myseq.fasta | varnacmd.pl)

        NOTE : svg images can be easily converted to pdf using 'inkscape --export-pdf=output.pdf input.svg'

    VERSION v0.1.6
    AUTHOR Paul Klemm
    DEPENDENCIES java : VARNA*.jar (http://varna.lri.fr/index.php?lang=en&css=varna&page=downloads), perl : File::Basename

---
## RNAfold/plot_gridRNAcofold.R
    Loading required package: readr
    Loading required package: ggplot2
    Loading required package: reshape2
    Error: object 'ARGV' not found
    Execution halted

---
## RNAfold/suboptclustplot.pl

       suboptclustplot (v0.1) 

    suboptclustplot.pl        Suboptimal folding structures of a given input sequence are clustered and plotted in this program. 

    SYNOPSIS

    suboptclustplot.pl (options) --input FASTA

    	--input FASTA
    		The input RNA (/DNA) fasta file with the input that should be evaluated. 

    	OPTIONAL

    	--subopte,-e INTEGER
    		the max. allowed difference to the mfe [default:5 (in kcal/mol)]

    	--maxsuboptrows INTEGER
    		the max. allowed number of suboptimal structures. The --subopte,-e is reduced until there this requirement is fulfilled ! [default:750]

    	--cwd DIRECTORY    The current working directory. [default:the current directory]
    	--project STRING   The general prefix for all generated files (this should prevent name clashes) [default:myproject]	
    	--cpus,-t INTEGER  number of threads [default:1]
    		
    DESCRIPTION

    	The following external programs are needed for execution:

    		RNAsubopt,RNAfold,RNAplot   : For the general fold energy of all target files 
    		clusterStrings.pl, splitfasta.pl (https://gitlab.com/paulklemm_PHD/scripts)

    EXAMPLES

    	perl suboptclustplot.pl --input file.fna

    AUTHOR
    	Paul Klemm

    DEPENDENCIES
    	perl : Getopt::Long, File::Basename, List::Util qw( max ), threads, Threads::Queue
    	vienna rna: RNAsubopt, RNAfold, RNAplot
    	clusterStrings.pl, splitfasta.pl

---
## RNAfold/ssps2svg.R
    Loading required package: ggplot2
    Loading required package: svglite
    Loading required package: ggrepel
    ssps2svg.R    convert postscript files generated by viennas RNAfold, RNAcofold... to SVG.

    SYNOPSIS

    ssps2svg.R *.ps (options)

      needs to be *ss.ps or *dp.ps files (secondary structure or dotplot) of RNAfold/RNAup/RNAcofold... 
      You can also provide a folder and ssps2svg searches for all ss.ps files inside

      options (needs to be at the end of the line and in that order)
        NUMBER    size factor, scales the output svg image
        -title    if set, then the file name is printed with +ggtitle()
        SEQUENCE  highlighted sequence

    EXAMPLE

        # simple call for all subdirectories 
        ssps2svg.R */*ss.ps
        # include filename as title
        ssps2svg.R */*ss.ps -title
        # scale by factor 10
        ssps2svg.R */*ss.ps 10
        # include title and highlight 'GAGAGAGAGA'
        ssps2svg.R */*ss.ps -title GAGAGAGAGA
        # all together
        ssps2svg.R */*ss.ps 10 -title GAGAGAGAGA

    VERSION v0.0.4
    AUTHOR Paul Klemm
    DEPENDENCIES ggplot2,svglite,ggrepel


---
## RNAfold/gridRNAcofold.pl

    gridRNAcofold.pl        This program executes RNAcofold on a grid of parameters (different monomer concentrations and temperatures). 

    SYNOPSIS

    gridRNAcofold.pl (options) FASTA

    	FASTA : input with 2 sequences as one entry in the RNAcofold format (ACACA...&...ACACA)

    DESCRIPTION

    	temperature grid: 
    		from 0C to 100C in 5C steps

    	concentration grid:
    		input sequence from 1e-06 to 0.2 increased by the factor 1.5 in each step
    		rev.comp.(input) sequence from 1e-06 to 0.2 increased by the factor 1.5 in each step

    EXAMPLES

    	input.fna file with:
    	>test
    	ACAGCGTTGTGTAAGA&AAAAGTGTGTGTGAAA

    	produces a output file input.fna.out : 

    	# T	concA	concB	AB, AA, BB, A, B
    	0 1e-06 1e-06 0.00000 0.24983 0.00001 0.00033 0.49998
    	0 1e-06 1.5e-06 0.00000 0.19987 0.00002 0.00027 0.59996
    	0 1e-06 2.25e-06 0.00000 0.15374 0.00004 0.00021 0.69223
    	0 1e-06 3.375e-06 0.00000 0.11421 0.00006 0.00015 0.77131
    	(...)

    VERSION v0.0.5
    AUTHOR Paul Klemm
    DEPENDENCIES perl : Cwd qw(abs_path cwd), perl : File::Basename, RNAcofold (viennarna package)

---
## igit.sh
    USAGE: igit.sh : a simple script to update version numbers and generate git commits. 

    	(i) if .gitignore is missing, I create a general purpose one.
    	(ii) looks for all files in current directory and searches for git updates.
    	(iii) I find 'version=' strings and will calculate a new version number according to the changes (see below).
    		supported languages: pl|R|sh|py|js|php|html.
    	(iv) A file called 'version' will recive a combined version update and 
    		'CHANGELOG' will update with a more detailed summary 
    		(only if those files are generated by this script).
    	
    	Format of updated versions : 'Major:Median:Minor'
    		Every 10 changes the Minor gets incremented by one. 
    		40 Minor changes = one Median update. 
    		10 Median changes = one Major update

    	Only works if there is no pending commit (git log -1)!

    SYNOPSIS igit.sh (options)
    	no option : No changes are made (safe to execute).
    	-c : Changes are made to the version strings and a commit is created from all changes. 'git push' can be called afterwards
    	-c -r : A README.md is automatically created from all help pages of the scripts within the directory. 
    		In further commits -r is not needed anymore and the README will be updated automatically.
    AUTHER Paul Klemm
    VERSION v0.5.12

---
## conversion/clustal2fasta.pl
    clustal2fasta.pl        coverts alignments in clustalw to fasta format
     
    SYNOPSIS
     
    clustal2fasta.pl (options) ALIGNMENTFILE 

    	ALIGNMENTFILE	clustaw format or - for STDIN

    		e.g.:
    		...
    		Dolosicoccus_paucivorans__GCF_      ATGT-TATGGTATA-CTTATGATGCAA----------
    		Lactobacillus_hayakitensis_DSM      CTAAT-ATGGTATT-ATAATTAT--------------
    		Lactobacillus_salivarius__GCF_      ATGATTATGCTATG-ATTGATTT--------------
    		Leuconostoc_garlicum__GCF_0019      ATTTTCGCGGTATA-ATAATTGATG------------
    		...

    	OUTPUT

    		>Lactobacillus_salivarius__GCF_
    		ATGATTATGCTATGATTGATTT---
    		>Lactobacillus_hayakitensis_DSM
    		CTAAT-ATGGTATTATAATTAT---
    		>Dolosicoccus_paucivorans__GCF_
    		ATGT-TATGGTATACTTAT-GATGC
    		>Leuconostoc_garlicum__GCF_0019
    		ATTTTCGCGGTATAATAATTGATG-

    VERSION v0.0.9
    AUTHOR Paul Klemm
    DEPENDENCIES -

---
## conversion/faa2fna.pl
    USAGE: faa2fna.pl  Converts fasta files containing  amino acids to nucleotides.

    SYNOPSIS 

        faa2fna.pl (OPTIONS) FAAFILE 

        -transl_table=i : the translation table from NCBI (see link below) (default:1) 
        1 The Standard Code (-transl_table=1)
        2 The Vertebrate Mitochondrial Code (-transl_table=2)
        3 The Yeast Mitochondrial Code (-transl_table=3)
        4 The Mold, Protozoan, and Coelenterate Mitochondrial Code and the Mycoplasma/Spiroplasma Code (-transl_table=4)
        5 The Invertebrate Mitochondrial Code (-transl_table=5)
        6 The Ciliate, Dasycladacean and Hexamita Nuclear Code (-transl_table=6)
        9 The Echinoderm and Flatworm Mitochondrial Code (-transl_table=9)
        10 The Euplotid Nuclear Code (-transl_table=10)
        11 The Bacterial, Archaeal and Plant Plastid Code (-transl_table=11)
        12 The Alternative Yeast Nuclear Code (-transl_table=12)
        13 The Ascidian Mitochondrial Code (-transl_table=13)
        14 The Alternative Flatworm Mitochondrial Code (-transl_table=14)
        16 Chlorophycean Mitochondrial Code (-transl_table=16)
        21 Trematode Mitochondrial Code (-transl_table=21)
        22 Scenedesmus obliquus Mitochondrial Code (-transl_table=22)
        23 Thraustochytrium Mitochondrial Code (-transl_table=23)
        24 Pterobranchia Mitochondrial Code (-transl_table=24)
        25 Candidate Division SR1 and Gracilibacteria Code (-transl_table=25)
        (updated 3.2.2020)

    DESCRIPTION

        Unknown aa are converted to '???'

        NCBI translation table : https://www.ncbi.nlm.nih.gov/Taxonomy/taxonomyhome.html/index.cgi?chapter=cgencodes

    SEE ALSO https://www.ncbi.nlm.nih.gov/Taxonomy/taxonomyhome.html/index.cgi?chapter=cgencodes
    VERSION v0.0.4
    AUTHOR Paul Klemm

---
## conversion/embl2fasta.pl
    embl2fasta.pl        coverts EMBL to fasta format
     
    SYNOPSIS
     
    embl2fasta.pl (options) EMBLFILE 

    VERSION v0.0.25
    AUTHOR Paul Klemm
    DEPENDENCIES -

---
## conversion/infernal2gff.sh
    Usage: infernal2gff.sh \<infernal.tblout\>
    Returns gff format.
      Input looks like this:
    ------
    target name         accession query name           accession mdl mdl from   mdl to seq from   seq to strand trunc pass   gc  bias  score   E-value inc description of target
    ------------------- --------- -------------------- --------- --- -------- -------- -------- -------- ------ ----- ---- ---- ----- ------ --------- --- ---------------------
    NC_013967.1          -         5S_rRNA              RF00001    cm        1      119  1603076  1603197      +    no    1 0.59   0.0   86.8   1.7e-18 !   Haloferax volcanii DS2 complete genome
    NC_013967.1          -         5_8S_rRNA            RF00002    cm        1      154  1600063  1600219      +    no    1 0.58   0.0   41.3   7.8e-09 !   Haloferax volcanii DS2 complete genome
    (...)
    ------

      Output looks like this:
    ------
    NC_013967.1     infernal        5S_rRNA 1603076 1603197 .	+      .       accession="RF00001"; mdl="cm"; mdlfrom="1"; mdlfrom="119"; trunc="no"; pass="1"; gc="0.59"; bias="0.0"; score="86.8"; E-value="1.7e-18"; inc="!"; descriptionOfTarget="Haloferax volcanii DS2 complete genome "
    NC_013967.1     infernal        5S_rRNA 2766751 2766630 .	-      .       accession="RF00001"; mdl="cm"; mdlfrom="1"; mdlfrom="119"; trunc="no"; pass="1"; gc="0.59"; bias="0.0"; score="86.8"; E-value="1.7e-18"; inc="!"; descriptionOfTarget="Haloferax volcanii DS2 complete genome "
    (...)
    ------

    AUTHOR Paul Klemm
    DEPENDENCIES sed, perl


---
## conversion/excel2csv.R
    Loading required package: optparse
    Loading required package: readr
    Loading required package: writexl
    Installing package into /home/paul/R/x86_64-pc-linux-gnu-library/4.4
    (as lib is unspecified)
    Error in contrib.url(repos, type) : 
      trying to use CRAN without setting a mirror
    Calls: install.packages -> startsWith -> contrib.url
    In addition: Warning message:
    In library(package, lib.loc = lib.loc, character.only = TRUE, logical.return = TRUE,  :
      there is no package called writexl
    Execution halted

---
## conversion/fasta2vector.pl
    fasta2vector.pl        reads a fasta (file or STDIN) and returns a linear version: 
    	name\tsequence
    	name\tsequence
    	name\tsequence
    	(...)
     
    SYNOPSIS
     
    fasta2vector.pl INFILE

    	INFILE can also be - for STDIN

    VERSION v0.0.9
    AUTHOR Paul Klemm
    DEPENDENCIES perl : Getopt::Long

---
## conversion/tRNAscanse2gff.sh
    Usage: tRNAscanse2gff.sh \<tRNAscansePREDICTEDSTRUCTE.csv\>
    Returns gff format. tRNAscanse reports - strand hits with reversed start stop indices.
      Input looks like this:
    ------
    NZ_CP037857_1_Escherichia_coli_BW25113__complete_genome.trna1 (221868-221944)	Length: 77 bp
    Type: Ile	Anticodon: GAT at 35-37 (221902-221904)	Score: 75.8
             *    |    *    |    *    |    *    |    *    |    *    |    *    |    * 
    Seq: AGGCTTGTAGCTCAGGTGGTtAGAGCGCACCCCTGATAAGGGTGAGGtCGGTGGTTCAAGTCCACTCAGGCCTACCA
    Str: >>>>>>>..>>>>.........<<<<.>>>>>.......<<<<<.....>>>>>.......<<<<<<<<<<<<....

    (...)
    ------

      Output looks like this:
    ------
    NC_013967.1	tRNAscan-SE	tRNA	11661	11733	.	+	.	Length=73; Type=Gln; Anticodon=CTGat35-37(11695-11697); Score=57.4; Seq=AGTCCCATGGGGTAGTGGCcaATCCTGTTGCCTTCTGGGGGCAACGACCCAGGTTCGAATCCTGGTGGGACTA; Str=(((((((..(((...........))).(((((.......)))))....(((((.......)))))))))))).
    NC_013967.1	tRNAscan-SE	tRNA	121223	121296	.	+	.	Length=74; Type=Arg; Anticodon=TCTat35-37(121257-121259); Score=78.9; Seq=GGGCGCGTAGCTCAGTCGGAcAGAGCGTCGGACTTCTAATCCGATGGtCGCGGGTTCGAATCCCGTCGCGCTCG; Str=(((((((..((((.........)))).(((((.......))))).....(((((.......)))))))))))).
    (...)
    ------

    AUTHOR Paul Klemm
    DEPENDENCIES sed, perl


---
## conversion/fna2faaPlus.pl
    USAGE: fna2faaPlus.pl  Converts fasta files containing nucleotides to amino acids (automatically detecting reading frames)

    SYNOPSIS

        fna2faaPlus.pl (OPTIONS) FNAFILE

        -transl_table=i : the translation table from NCBI (see link below) (default:1) 
    		1 The Standard Code (-transl_table=1)
                    AAs  = FFLLSSSSYY**CC*WLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG
                  Starts = ---M------**--*----M---------------M----------------------------
                  Base1  = TTTTTTTTTTTTTTTTCCCCCCCCCCCCCCCCAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGG
                  Base2  = TTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGGTTTTCCCCAAAAGGGG
                  Base3  = TCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAGTCAG

    		2 The Vertebrate Mitochondrial Code (-transl_table=2)
    		3 The Yeast Mitochondrial Code (-transl_table=3)
    		4 The Mold, Protozoan, and Coelenterate Mitochondrial Code and the Mycoplasma/Spiroplasma Code (-transl_table=4)
    		5 The Invertebrate Mitochondrial Code (-transl_table=5)
    		6 The Ciliate, Dasycladacean and Hexamita Nuclear Code (-transl_table=6)
    		9 The Echinoderm and Flatworm Mitochondrial Code (-transl_table=9)
    		10 The Euplotid Nuclear Code (-transl_table=10)
    		11 The Bacterial, Archaeal and Plant Plastid Code (-transl_table=11)
    		12 The Alternative Yeast Nuclear Code (-transl_table=12)
    		13 The Ascidian Mitochondrial Code (-transl_table=13)
    		14 The Alternative Flatworm Mitochondrial Code (-transl_table=14)
    		16 Chlorophycean Mitochondrial Code (-transl_table=16)
    		21 Trematode Mitochondrial Code (-transl_table=21)
    		22 Scenedesmus obliquus Mitochondrial Code (-transl_table=22)
    		23 Thraustochytrium Mitochondrial Code (-transl_table=23)
    		24 Pterobranchia Mitochondrial Code (-transl_table=24)
    		25 Candidate Division SR1 and Gracilibacteria Code (-transl_table=25)
    		(updated 3.2.2020)

        -shift=i -frame=i [0,1,2] : the frame shift. If nothing given, then all possible shifts are analysed.
        -all : if set, then all possible translated sequences starting in a START codon are printed
        -full : if set, then a start codon is not needed anymore
        -M : if set, then only the aminoacid Methionin is used as a start codon (for -transl_table=1 Leucine=L could also represent a start codon).
        -ATG : if set, then only the sequence ATG is used as a start codon (for -transl_table=1 this is equivalent to -M).
        -fna : include the fna sequence in the header of the output 
        -suffix=X : prioritize peptides with this ending sequence (suffix)
        -edu : show output in a more detailed way

    DESCRIPTION

        Converts fasta files containing nucleotides to amino acids:
        	* If there are ORFs : Find the longes ORF (start-stop sequence)
    	* Unknown codons are encoded with 'X'
        	* If there are multiple STARTS take the ORF that maximizes

        		numOfCodons(START->STOP/END) - numOfUnknownCodons + 5*hasSTOPcodon

        NCBI translation table : https://www.ncbi.nlm.nih.gov/Taxonomy/taxonomyhome.html/index.cgi?chapter=cgencodes

    SEE ALSO https://www.ncbi.nlm.nih.gov/Taxonomy/taxonomyhome.html/index.cgi?chapter=cgencodes
    VERSION v0.0.27
    AUTHOR Paul Klemm (modified version of the fna2faa.pl)

---
## alignment/MSAconsensus.pl
    MSAconsensus.pl        calculates a consensus of a given alignment (columnwise).
     
    SYNOPSIS
     
    MSAconsensus.pl (options) ALIGNMENTFILE 

    	ALIGNMENTFILE	alignment file (clustaw or fasta format) or - for STDIN

    		e.g. CLUSTAL W formated input:

    		...
    		Dolosicoccus_paucivorans__GCF_      ATGT-TATGGTATA-CTTATGATGCAA----------
    		Lactobacillus_hayakitensis_DSM      CTAAT-ATGGTATT-ATAATTAT--------------
    		Lactobacillus_salivarius__GCF_      ATGATTATGCTATG-ATTGATTT--------------
    		Leuconostoc_garlicum__GCF_0019      ATTTTCGCGGTATA-ATAATTGATG------------
    		...

    	options

    		-includeGaps, -g : include gaps in the consensus
    		-ambiguous, -a   : if there are MULTIPLE dominant characters, then print ONE at random.
    		-dominant=, -d=  : if the frequency of the most dominant character is below this threshold (given as percent), then omit this character. (default:0 = disabled)
    							e.g. use 100 to only print the characters that are 100% conserved in all sequences.
    		-multiple, -m    : if there are MULTIPLE dominant characters, then print ALL if the format '{TA}' if T and A are the dominat characters. 
    		-plain, -p       : remove no-domiant/ambiguous characters '#?' entirely.
    		-c       : outputs tsv positionwise conservation to STDERR (0-1).
    		-varna   : outputs varna style conservation scores to STDERR (usable with varnas -colorMap option).

    		# = no domiant character 
    			- if all chars are gaps and -g is NOT set 
    			- if the most dominant character is below the -d threshold
    		? = there are multiple dominant characters found (set -a to display any)

    EXAMPLES

    	MSAconsensus.pl test.aln 

    		ATG?TTATGGTATA#AT?ATTAT??AA##########

    	MSAconsensus.pl test.aln -p

    		ATGATTTATGGTATAATATATTAT

    	MSAconsensus.pl test.aln -a
    	
    		ATGTTTATGGTATA#ATAATTATTCAA##########
    		---^-------------^-----^^------------ the '?' position can vary each call

    	MSAconsensus.pl test.aln -a
    	
    		ATGATTATGGTATA#ATTATTATGGAA##########
    		---^-------------^-----^^------------ the '?' position can vary each call

    	MSAconsensus.pl test.aln -a -g

    		ATGTTTATGGTATA-ATAATTAT--------------

    	MSAconsensus.pl test.aln -m

    		ATG{AT}TTATGGTATA#AT{AT}ATTAT{GT}{CG}AA##########

    VERSION v0.0.11
    AUTHOR Paul Klemm
    DEPENDENCIES -

---
## alignment/MSAtrimmer.pl
    MSAtrimmer.pl        trimmes a given alignment (column- and row-wise) and realigns it. By default only gaprich (>0.5) columns are trimmed.
     
    SYNOPSIS
     
    MSAtrimmer.pl (options) ALIGNMENTFILE 

    	ALIGNMENTFILE	alignment file (clustaw or fasta format) or - for STDIN

    		e.g. CLUSTAL W formated input:
    		...
    		Dolosicoccus_paucivorans__GCF_      ATGT-TATGGTATA-CTTATGATGCAA----------
    		Lactobacillus_hayakitensis_DSM      CTAAT-ATGGTATT-ATAATTAT--------------
    		Lactobacillus_salivarius__GCF_      ATGATTATGCTATG-ATTGATTT--------------
    		Leuconostoc_garlicum__GCF_0019      ATTTTCGCGGTATA-ATAATTGATG------------
    		...

    	options 

    		-p=(clustalw|musle) : the algorithm that is called after trimming to realign [default:clustalw]. This also defines the output format!
    		-realn      :	this option enables a re-alignment call on the results (using clustal)
    		-fasta      :	outputs fasta format (is the default now)
    		-clustal    :	outputs clustalw format


    	options VERTICAL (columns)

    		-VmaxGap=number, -g=number :	the maximum number/percentage (with a % suffix) of gap characters in each column [default:50%]
    		-VminId=number,  -t=number :	the minimum number/percentage (with a % suffix) of the dominant nongap-characters in each column [default:0 <=> disabled]
    		
    		percentage numbers [0% - 100%]
    		integer numbers [1 - 999999]
    		NOTE: If you want to specify a percentage number the % symbol is neccesary 

    	options HORIZONTAL (rows)

    		-HminPairId=percentage, -h=percentage:	the minimum average pairwise identity to all other rows. Scoring gap-gap=0 [default:0% <=> disabled]
    		-HmaxGap=number, -j=number:	the maximum number/percentage (with a % suffix, relative to the number of columns) of gaps per row. [default:0% <=> disabled]

    		percentage numbers [0% - 100%] (this option needs the % symbol !)

    	options display

    		-color      :	showing original aln with all removed rows/cols colored (+ this option disables the realn call on the results)

    EXAMPLE

    	MSAtrimmer.pl -VmaxGap=25% -VminId=3 test.aln

        Dolosicoccus_paucivorans__GCF_      ATGT-TATGGTATA-CTTATGATGCAA----------
        Lactobacillus_hayakitensis_DSM      CTAAT-ATGGTATT-ATAATTAT--------------
        Lactobacillus_salivarius__GCF_      ATGATTATGCTATG-ATTGATTT--------------
        Leuconostoc_garlicum__GCF_0019      ATTTTCGCGGTATA-ATAATTGATG------------
                                              ^^ ^        ^  ^ ^ ^ ^^^^^^^^^^^^^^

        --> VmaxGap=25% -> a maximum of 1 gap per column, VminId=3 -> each column should now have at least 3 common non-gap characters
        --> Trimmed coloumns are marked red

    OUTPUT
    	alingment file

    VERSION v0.0.39
    AUTHOR Paul Klemm
    DEPENDENCIES clustalw (optional for re-alignment)

---
## alignment/MSAscorer.pl
    Died at ./alignment/MSAscorer.pl line 52.
    MSAscorer.pl        scores a given alignment (average linewise identity, number of gaps, ...)
     
    SYNOPSIS
     
    MSAscorer.pl ALIGNMENTFILE 

    	ALIGNMENTFILE	alignment file (clustaw or fasta format) or - for STDIN

    		e.g. CLUSTAL W formated input:

    		...
    		Dolosicoccus_paucivorans__GCF_      ATGT-TATGGTATA-CTTATGATGCAA----------
    		Lactobacillus_hayakitensis_DSM      CTAAT-ATGGTATT-ATAATTAT--------------
    		Lactobacillus_salivarius__GCF_      ATGATTATGCTATG-ATTGATTT--------------
    		Leuconostoc_garlicum__GCF_0019      ATTTTCGCGGTATA-ATAATTGATG------------
    		...

    	OUTPUT : clustalw formatted alignment

    VERSION v0.0.13
    AUTHOR Paul Klemm
    DEPENDENCIES -

---
## alignment/MSAplot.pl
    MSAplot.pl        produces a colored representation of a given MSA
     
    SYNOPSIS
     
    MSAplot.pl (options) ALIGNMENTFILE 

    	ALIGNMENTFILE	alignment file (clustaw or fasta format) or - for STDIN

    		e.g. CLUSTAL W formated input:
    		...
    		Dolosicoccus_paucivorans__GCF_      ATGT-TATGGTATA-CTTATGATGCAA----------
    		Lactobacillus_hayakitensis_DSM      CTAAT-ATGGTATT-ATAATTAT--------------
    		Lactobacillus_salivarius__GCF_      ATGATTATGCTATG-ATTGATTT--------------
    		Leuconostoc_garlicum__GCF_0019      ATTTTCGCGGTATA-ATAATTGATG------------
    		...

    OUTPUT
    	Sequence identity color code: 100%, >90%, >80%, >50%.
    	Gaps are viewed as non-dominant characters and never colored.
    	The output is ordered by descending number of gaps.
    	You can use 'aha' to convert the output to html (conda install aha)
    	If there is a weblogo present (same name with .svg suffix), then it will be included into the output in an img tag 
    	if you use aha with an weblogo add: ... | aha | sed -E 's/&lt;(img.*)&gt/<\1>/g;'

    VERSION v0.1.37
    AUTHOR Paul Klemm

---
## alignment/MSAget_indelMutationDiffof2seqs.pl
    Useless use of private variable in void context at ./alignment/MSAget_indelMutationDiffof2seqs.pl line 110.
    MSAget_indelMutationDiffof2seqs.pl	calculates the indel/mutation difference between 2 given fasta files

    SYNOPSIS	

    	MSAget_indelMutationDiffof2seqs.pl ALNFILE

    	ALNFILE : alignment file of 2 sequences in FASTA format. 
    	STDOUT : a list of areas with indes/mutations (and the genomic position of the first sequences)

    VERSION v0.0.3
    AUTHOR Paul Klemm

---
## alignment/pairwiseAverageNW.pl
    pairwiseAverageNW.pl        performs an all versus all pairwise averaged length-normalized needlemanwunsch
     
    SYNOPSIS
     
    pairwiseAverageNW.pl (-help|-h) (-noselfhit) (-notlengthnormed) (-verbose) FASTA1 (FASTA2 ...)

    	FASTA	Fasta file containing genes/proteins
    		if one file is given, all sequences are compared
    		if multiple files are given, the average score is calculated between the files

    	optional:
    		-noself	do not compare the same file/sequence
    		-nonorm	do not normalizing the scores to the maximal length
    		-verbose	displaying the progress (STDERR)
    		-all	print the list of all scores instead of the mean (STDERR)
    		-wide   outputs a matrix format instead of a long list of pairs
    		-help	displays this.

    DESCRIPTION 

    	This script computes all pairwise averaged (normalized) needlemanwunsch 
    	global alignment score of all genes/proteins versus all genes/proteins of
    	a set of fasta files. 
        e.g. 2 files (A,B) with 2 genes each (A1,A2,B1,B2) 
    	-> score for A vs B = mean(needlemanwunsch(A1,B1),needlemanwunsch(A1,B2),needlemanwunsch(A1,B2),needlemanwunsch(A2,B1),needlemanwunsch(A2,B2)) 
     	scores are length normed (/max length of the two sequences)

     	Careful: This will fail if there are many input sequences (curse of dimensionality), many sequences are on average always similar

    EXAMPLES
     
    	perl pairwiseAverageNW.pl Test1.faa Test2.faa
    	
    	with
    		------Test1.faa:------------
    		>G1_1
    		ABC
    		>G1_2
    		AB

    		------Test2.faa:------------
    		>G2_1
    		AB
    		>G2_2
    		AA

    	STDOUT:
    		Test1.faa       Test1.faa       0.7778
    		Test1.faa       Test2.faa       0.25
    		Test2.faa       Test2.faa       0.6667

    	perl pairwiseAverageNW.pl Test1.faa --> comparing the sequences of Test1.faa against each other

    VERSION v0.0.18
    AUTHOR Paul Klemm
    DEPENDENCIES Getopt::Long, File::Basename, easyalignment.py (https://gitlab.com/paulklemm_PHD/scripts)

---
## alignment/easyalignment.py
    WARNING: Did not found 'progressbar2', this package is not essential but enhances the program. I will skip it but consider installing it...
    WARNING: Did not found 'psutil', this package is not essential but enhances the program. I will skip it but consider installing it...
    Usage: easyalignment.py [options] 'SEQUENCE_A' 'SEQUENCE_B'
    or: easyalignment.py [options] 'FASTAFILE'
    or: easyalignment.py [options] 'SEQUENCE_A' 'FASTAFILE'

    Options:
      --version             show program's version number and exit
      -h, --help            show this help message and exit
      -t TYPE, --type=TYPE  comparison type: auto, nucl or prot [default: auto]
      -m MODUS, --modus=MODUS
                            comparison modus: needlemanwunsch/nw (global),
                            smithwaterman/sw (local), fitted (first longer
                            sequence locally and second shorter sequence
                            globally), overlapped (local suffix with local
                            prefix), levenshtein (edit distance) [default:
                            needlemanwunsch]
      -o GAPOPEN, --gapopen=GAPOPEN
                            gap open penalty [default 10 for prot and 15 for nucl]
      -e GAPEXTEND, --gapextend=GAPEXTEND
                            gap extend penalty, if 0 then affine gap penalty are
                            disabled [default 0.1 for prot and 6.66 for nucl]
      -d MAXDIAG, --maxdiag=MAXDIAG
                            restricts the similarity matrix by this value up and
                            down from the diagonal, corresponds to the maximal
                            possible gap length [default: 200]
      -r, --getAllEquivalentResult
                            if set, then all possible alignments with same score
                            are printed
      -g, --progress        print further informations progressbar, ...
      -l, --plot            print the similarity matrix build by the algorithm
                            (only works nicely for smaller examples). You can send
                            the output to aha to produce a html version.
      -p, --path            print matrix path (combine this with -v).
      -x, --test            
      --maxmemp=MAXMEMP     maximal virtual_memory percent (if exceeded the job is
                            killed).
      -s STRING, --scoring=STRING
                            comparison scoring file or one of the predefined
                            matrices: BLOSUM62, PAM250, IUB (match=1.9,
                            missmatch=0), NOMM (match=1.9, missmatch=-999), ONE
                            (match=1, missmatch=-1). By default IUB is used for
                            nucl and PAM250 for prot comparisons

---
## PDB/evaluate_pae.pl
    evaluate_pae.pl        evaluates the predicted aligned error (PAE) of the alphafold prediction.

    SYNOPSIS

    evaluate_pae.pl DIRECTORY ...

    DESCRIPTION
    	-TODO

    VERSION v0.0.18
    AUTHOR Paul Klemm
    DEPENDENCIES python numpy,jax,jaxlib

---
## PDB/alphafold_visualize.py
    sh: line 339: ./PDB/alphafold_visualize.py: cannot execute: required file not found

---
## PDB/automatic_foldseek_api.py
    ./PDB/automatic_foldseek_api.py: line 5: from: command not found
    ./PDB/automatic_foldseek_api.py: line 7: syntax error near unexpected token `('
    ./PDB/automatic_foldseek_api.py: line 7: `def post_request(pdb_file):'

---
## PDB/alphafold_unpkl.py
    sh: line 347: ./PDB/alphafold_unpkl.py: cannot execute: required file not found

---
## PDB/download_pdb_af.py
    usage: download_pdb_af.py [-h] [-p [PDB ...]] [--prefix PREFIX]
                              [-u [UNIPROT ...]] [-f {cif,pdb,pdb_split}] [-k]

    Fetch PDB and AlphaFold files. Note: HETATM MSE are converted to ATOM MET,
    other HETATM are omitted

    options:
      -h, --help            show this help message and exit
      -p [PDB ...], --pdb [PDB ...]
                            PDB ID(s) (can be multiple). Use underscore to
                            indicate a chain e.g. 6P3S_A (only works with
                            format=pdb_split). Note: first the label_asym_id (pdb
                            picked chain names) are used than the auth_asym_id
                            (auther picked chain names)
      --prefix PREFIX       Output name prefix
      -u [UNIPROT ...], --uniprot [UNIPROT ...]
                            UniProt ID(s) to download AlphaFold predictions (can
                            be multiple)
      -f {cif,pdb,pdb_split}, --pdb_file_format {cif,pdb,pdb_split}
                            File format for the PDB structure, pdb_split=split by
                            entity id (default: pdb_split).
      -k, --keep            keep intermediate files (mainly for format=pdb_split)

---
## PDB/extract_seq_pdb.R
    Loading required package: bio3d
    Loading required package: readr
    USEAGE: extract_sse_pdb.R PDB1 PDB2 ... extracts the secondary structure information (H:helix,S:sheet,-:unstructured)
    AUTHOR Paul Klemm
    VERSION v0.0.12
    DEPENDENCIES R packges : bio3d,readr

---
## PDB/cif_to_pdb.py
    Traceback (most recent call last):
      File "/home/paul/Documents/scripts/./PDB/cif_to_pdb.py", line 5, in \<module\>
        from Bio.PDB import MMCIFParser, PDBIO, Select
    ModuleNotFoundError: No module named 'Bio'

---
## PDB/extract_sse_pdb.R
    Loading required package: bio3d
    Loading required package: readr
    USEAGE: extract_sse_pdb.R PDB1 PDB2 ... extracts the secondary structure information (H:helix,S:sheet,-:unstructured), NEEDS dssp in execution path
    AUTHOR Paul Klemm
    VERSION v0.0.9
    DEPENDENCIES R packges : bio3d,readr

---
## PDB/split_pdb.py
    Traceback (most recent call last):
      File "/home/paul/Documents/scripts/./PDB/split_pdb.py", line 5, in \<module\>
        from Bio.PDB import PDBParser, PDBIO, Select
    ModuleNotFoundError: No module named 'Bio'

---
## PDB/alphafold_prepareFasta4submission_new2.3.pl
    Could not parse parameter -h

---
## PDB/snap_image_pymol.py
    ./PDB/snap_image_pymol.py: line 3: from: command not found
    import: unable to grab mouse '': Resource temporarily unavailable @ error/xwindow.c/XSelectWindow/9354.
    import: unable to read X window image '': Success @ error/xwindow.c/XImportImage/4961.
    import: unable to read X window image '': Resource temporarily unavailable @ error/xwindow.c/XImportImage/5068.
    import: unable to grab mouse '': Resource temporarily unavailable @ error/xwindow.c/XSelectWindow/9354.
    import: unable to read X window image '': Success @ error/xwindow.c/XImportImage/4961.
    import: unable to read X window image '': Resource temporarily unavailable @ error/xwindow.c/XImportImage/5068.
    ./PDB/snap_image_pymol.py: line 7: from: command not found
    ./PDB/snap_image_pymol.py: line 9: syntax error near unexpected token `('
    ./PDB/snap_image_pymol.py: line 9: `def draw_with_timeout(pdb, dpi, width, height, ray, timeout, frames, angle, color):'

---
## PDB/pymolSuperimposePDBs.pl
    pymolSuperimposePDBs.pl        superimposes all input PDB files against one/set of reference PDB file(s). Outputs the superimposed PDB as well as the RMSD. If output is allready present, pymol is not invoked again (only for -keep).

    SYNOPSIS

    pymolSuperimposePDBs.pl -ref=REFPDB PDB1 PDB2 ...

    or

    pymolSuperimposePDBs.pl PDB1 PDB2 ...

    DESCRIPTION
    	-ref=FILE : reference PDB file, multiple files can be given as ;-separated list
    				if no -ref is defined: compare all input PDBs versus all (only A-B, not B-A) 
    	-cpus=i   : number of threads [default:10]
    	-dry      : prints out the work instead
    	-keep     : keeps intermediate files (stored in ./.pymolSuperimposePDB_tmp/)
    	-save     : keeps superimposed pdb files, sets -keep

    VERSION v0.0.32
    AUTHOR Paul Klemm
    DEPENDENCIES pymol

---
## blast/ARBG.sh
    USAGE : ARBG.sh (OPTIONS) FASTA_A FASTA_B
     FASTA_A, FASTA_B   :  Two fasta files

    This interface calls easyblast.sh (using 4 cores) 2 times and compares the blast outputs to create the adaptive reciprocal blast graph.

    OPTIONS:
     -s DOUBLE (0 to 1)  :  The minimal similarity for adaptive best matches [default:0.95]. -s 1 results in the reciprocal best(!) blast hit graph. [-s 0 disables this feature]
     -c DOUBLE (0 to 1)  :  The minimal alignment coverage [default:0.5]. NOTE: this option does not make sense if you compare fna with faa files (fnas are way longer anyways). [-c 0 disables this feature]
     -e DOUBLE (0 to 1)  :  evalue cutoff [default:1e-5], [-e 0 disables this feature]
     -t INT  :  number of threads [default:4].

     (all temporary files are written to /tmp)

    AUTHOR Paul Klemm
    DEPENDANCIES easyblast.sh (https://gitlab.com/paulklemm_PHD/scripts), awk, grep, xargs, ls, perl

---
## blast/easyblast.sh
    USAGE : easyblast.sh (OPTIONS) QUERY(FILE/STRING) DB(FILE)
     QUERY(FILE/STRING)    :  The input query, e.g. /home/paul/pRNA.fa or just a string with a sequence like 'AATAATAATA'.
     DB(FILE)              :  The database (path to file).

    (Note: 4 cores are used by default)

    OPTIONS:
     -b \<STRING\>           :  The blast type [blastn, blastp, tblastn, blastx]. If not set, then it will try to auto detect the right one.
     -t \<INTEGER/PERCENT%\> :  This option sets the threshold for the number of returned hits per query (sorted by bitscore). If 1 then only the best match is returned (for each query). Given a percentile (with a % suffix), all hits within this percentile times the maximum are returned (bitscore). So -t 90% -> from maximal bitscore to 10% below. If set, the blastoption have to contain -outfmt 6 because of the bitscores (by default this is the case).
     -e \<INTEGER\>          :  The -evalue threshold (default:10) 
     -j \<INTEGER\>          :  The number of threads used (default:4)
     -d                    :  If set, then for each query no duplicated hits are returned like A->B and A->B again with a different bitscore (hit with best bitscore is printed).
     -h                    :  If set, then the header for outfmt 6 is printed as well.
     -s                    :  If set, then the following options are set -soft_masking false -dust no.
     -g                    :  If set, the (sstart,send) positions are converted to strand +/- and +based-positions.
     -x                    :  If set, a summarized output is produced.
     -i                    :  If set, only identical hits are reported.
     -m diamond            :  Use diamond instead of ncbi-blast.
     -o \<STRING\>           :  Explicit blast+ options, by default '-outfmt 6 -num_threads 4' is used. NOTE: this overwrites all other options (like -j threads and -e evalue).

    EXAMPLES:
     bash easyblast.sh 'KSWQIIFLVVGLITVASAPIVYW' /home/Ecoli.faa
     bash easyblast.sh /some/file.fasta /home/Ecoli.faa -> 
     bash easyblast.sh -t 1 'KSWQIIFLVVGLITVASAPIVYW' /home/Ecoli.faa -> This gives only the best match
     bash easyblast.sh -t 90% 'KSWQIIFLVVGLITVASAPIVYW' /home/Ecoli.faa -> Up to 10% from maximum is returned
     bash easyblast.sh -t 90% -d 'KSWQIIFLVVGLITVASAPIVYW' /home/Ecoli.faa -> same as ^ and duplicated matches are excluded.
      bash easyblast.sh -j 16 'AAGTATAGATAGATCCACACACAGGCACGAGT' /home/Ecoli.faa -> automatic detecting blastx and using 16 cores
      bash easyblast.sh -o '-outfmt 6 -num_threads 16' -b blastx 'AAGTATAGATAGATCCACACACAGGCACGAGT' /home/Ecoli.faa -> forcing blastx algorithm
    (The result is written to stdout)

    Output is sorted by qseqid (column 1) first and then by evalue (column 12)

    Here the some useful BLAST commands: 
    makeblastdb -dbtype nucl -in DBFILE
    makeblastdb -dbtype prot -in DBFILE -out OUTFILE
    blastp -query QUERYFILE -db DBFILE -outfmt 6 -num_threads 4
    blastp -query QUERYFILE -db DBFILE -outfmt '6 qseqid sseqid bitscore' -num_threads 4 | sort -k1,1 -k3,3nr | awk '{if(last != $1){last=$1;print $0;}}'

    Here the some useful options: 
    -evalue 1e-4 : sets the evalue cut-off
    -soft_masking false -dust no : disables soft masking (low complexity filtering)
    -word_size 11 : should be 3 for FAA vs FAA and 11-28 for FNA vs FNA
    -task blastn : for blastn, this sets -word_size 11
    -strand {both,plus,minus} : by default blast searches both strands (so also the rc)
    extract matches : easyblast.sh query db.fna | awk '{print $2}' | grepfasta.pl - db.fna

    VERSION 0.2.14
    AUTHOR Paul Klemm
    DEPENDANCIES ncbi blast+, awk, grep, xargs, ls, perl

---
## paulquickhelp.sh


---
## blastgraph/shuffle_blastgraph.pl
    Can't open -h: No such file or directory at ./blastgraph/shuffle_blastgraph.pl line 10.

---
## blastgraph/make_subset.pl
    syntax error at ./blastgraph/make_subset.pl line 16, near "exit"
    Execution of ./blastgraph/make_subset.pl aborted due to compilation errors.

---
## wrapper/update_r_pakages.pl

---
## wrapper/telewatch.sh
    USAGE : ./wrapper/telewatch.sh (OPTIONS) QUERY
     QUERY    :  The input query, e.g. rmarkdown.

    Needs the telegram_token= and telegram_chatid= environment set

    OPTIONS:
     -s \<STRING\> :  program to test for [default:ps aux]
     -p          :  pass message through (simple GET request)
     -w          :  wc -l the result and send it
     -o          :  terminate after one iteration

    VERSION 0.0.13
    AUTHOR Paul Klemm
    missing telegram_chatid and telegram_token env (load a .env with 'export $(cat .env | xargs)')...

---
## wrapper/findLineWiseDiff.sh
    line	-h	
    paste: invalid option -- 'h'
    Try 'paste --help' for more information.

---
## wrapper/easyproorigami.py
    usage: easyproorigami.py [-h] num_threads pdb_files [pdb_files ...]

    Process PDB files using proorigami.

    positional arguments:
      num_threads  Number of threads to use
      pdb_files    List of PDB files to process

    options:
      -h, --help   show this help message and exit

---
## wrapper/startRstudioDocker.sh
     please install docker and bioconductor/bioconducter_docker container first (https://docs.docker.com/engine/install/ubuntu/)
     sudo docker pull bioconductor/bioconductor_docker:devel

     use run -d for background
     use -it for interactive session
     add bash at the end to start in a shell instead of starting /init script

    echo "# visit >> localhost:8787 << with user=rstudio pw=bioconductor123#"
    docker run -p 127.0.0.1:8787:8787 --rm --mount "type=bind,src=/home/$(id -un),dst=/home/$(id -un)" -e PASSWORD=bioconductor123# bioconductor/bioconductor_docker:devel bash -c "sudo echo \".libPaths(c(\\\"/home/rstudio/R.dockerlibs\\\",.libPaths()))\" >/home/rstudio/.Rprofile && chmod -R 777 /usr/local/lib/R && usermod -u $(id -u) rstudio && groupmod -g $(id -g) rstudio  && ln -s /home/$(id -un) /home/rstudio/$(id -un) && su rstudio && /init"

     --mount "type=bind,src=/home/$(id -un),dst=/home/$(id -un)" 
     -> mounts the home directory to /home. Later a symbolic link is created in the home of rstudio pointing to this folder.

     --mount "type=bind,src=/home/$(id -un)/R.dockerlibs,dst=/home/rstudio/R.dockerlibs"
     -> mounts the R.docklibs, all libraries should be installed here automatically (install.packages...)

     usermod -u $(id -u) rstudio && groupmod -g $(id -g) rstudio
     -> this will fix permission issues with the share folders. The rstudio user has now the same UID as you

     su rstudio && /init
     -> starts the rstdio server, ...

     sudo docker ps
     sudo docker attach ID # if you run with -d and -it
     sudo docker stop ID

     start a bash of an existing/running docker container 
     sudo docker exec -it IDHERE bash

     Communicate the proxy configuration to docker
     cat /etc/systemd/system/docker.service.d/http-proxy.conf
     [Service]
     Environment="HTTP_PROXY=http://proxy.example.com:80/"
     ...
     sudo systemctl daemon-reload && sudo systemctl restart docker
#


---
## wrapper/ddrive.py
    usage: ddrive.py [-h]
                     [--exclude-patterns EXCLUDE_PATTERNS [EXCLUDE_PATTERNS ...]]
                     [--max-file-size MAX_FILE_SIZE] [--max-dir-size MAX_DIR_SIZE]
                     [--max-dir-files MAX_DIR_FILES]
                     {push,pull} files [files ...]

    ddrive script to push/pull files with various options.

    positional arguments:
      {push,pull}           Mode of operation: push or pull
      files                 List of files to push or pull

    options:
      -h, --help            show this help message and exit
      --exclude-patterns EXCLUDE_PATTERNS [EXCLUDE_PATTERNS ...]
                            List of patterns to exclude
      --max-file-size MAX_FILE_SIZE
                            Maximum file size (e.g., 200M, 2G)
      --max-dir-size MAX_DIR_SIZE
                            Maximum directory size (e.g., 200M, 2G)
      --max-dir-files MAX_DIR_FILES
                            Maximum number of files in a directory

---
## grades.pl
    Global symbol "$rsa" requires explicit package name (did you forget to declare "my $rsa"?) at ./grades.pl line 143.
    Global symbol "$rsa" requires explicit package name (did you forget to declare "my $rsa"?) at ./grades.pl line 143.
    Execution of ./grades.pl aborted due to compilation errors.

---
## benchmarker/benchmark_write.sh
    110 MB/s

---
## benchmarker/benchmarker_daemon.pl

    Usage: benchmarker_daemon.pl    simple perl daemon that watches the input command over time (memory usage, %cpu).

    SYNOPSIS
         
        benchmarker_daemon.pl (options) cmd

            cmd    the input command given as a string. Note that the command cannot be a asynchronous (e.g. ending with &)...

        options
            
            -update=N   The refresh interval in seconds (default:2)
            -output=F   The output file path, please put " around the path (default:'' -> the standard error output)

    DESCRIPTION

        A child is forked that checks every 2 seconds the current 'rss,%cpu' of the parent (output is printed to STDERR).
        The parent (this script) will just execute the given command.
        If parent does exit/terminate -> the child is killed (daemon vanishes).

    VERSION v0.0.5
    AUTHOR Paul Klemm
    DEPENDENCIES pstree, tail, grep, xargs, awk
