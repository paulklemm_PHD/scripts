#pk
import sys
import subprocess

version="0.2.7" # last updated on 2025-02-24 18:01:50

def print_section(title):
    GREEN = "\033[0;32m"
    NC = "\033[0m"
    print(f"#====[{GREEN} {title} {NC}]====#")

def run_command(commands):
    for cmd in commands:
        print(f"  {cmd}")

def main():
    select = sys.argv[1] if len(sys.argv) > 1 else ""

    commands = {
        "tmux": ["tmux new-session -d -s test", "tmux a -d"],
        "history": [
            "LASTHISTID=$(history | tail -n1 | awk '{print $1}')",
            "history | awk '$1>'$LASTHISTID'{print $0}'",
            "HISTTIMEFORMAT='%d/%m/%y ' history | grep $(date '+%d/%m/%y')"
        ],
        "node": ["npm install http-server -g", "http-server"],
        "script": ["script -a README.script"],
        "intersect": ["comm -12 <(sort file1) <(sort file2)"],
        "merge": ["awk -F'\\t' 'FNR==NR{d[$2]=$0;next}{print $0\"\\t\"d[$2]}' file1 file2"],
        "inkscape": [
            "# clean the textLength property:",
            "sed -E -i 's/ textLength='[0-9][^'= ]+'//g' *svg",
            "# svg 2 pdf:",
            "for f in *svg; do y=${f%.svg}; inkscape $f --export-pdf=$y.pdf; done"
        ],
        "awk":[
            "# read from a compressed fq and merge by id with a table (first column are ids)",
            "zcat reads.fq.gz | awk -F'\t' 'BEGIN{OFS=\"\\t\"}FNR==NR && (NR-1) % 4 == 0{id=$0}FNR==NR && (NR-1) % 4 == 1{d[id]=$0}NR != FNR{print $0,d[$1]}' - tableinput "
        ],
        "docker": [
            "docker system prune",

            "# start a new container with interactive bash (vanishes afterwards):",
            "docker run --rm -it --privileged ubuntu:bionic bash",

            "# start a new container with a mounted directory:",
            'docker run --rm -it --user "$(id -u):$(id -g)" --mount "type=bind,src=$PWD,dst=/mounted" -w /mounted ubuntu:bionic bash',

            "# get the container id",
            "DOID=$(docker ps | grep NAME | awk '{print $1}' | head -n1)",

            "# run a command in a container",
            "docker exec $DOID bash -c 'ls -las'",

            "# login to running container",
            "docker exec -it $DOID bash",

            "# kill dead docker:",
            "docker ps -q -a -f status=exited | xargs -n 100 docker rm -v",

            "# stop all: ",
            "docker stop $(docker ps -aq)"
        ],
        "pymol":[
            "split_chains input",
            "# scenes",
            "scene 1,update",
            "# movie:",
            "mset 1x600 # + activate S on bottom right to enable the interpolation between frames",
            "mview store,1",
            "mview store,100",
            "(...)",
            "# color entries (h=helix,t=turn,s=sheet)",
            "color red, ss h",
        ],
        "perl":[
            "perl -e '$s="";while(<>){$s.=$_}while($s=~/^(>.*?)$([^>]*)/smxg){$a=$1;$b=$2;$b=~s/[\\n\\r]+//g;print \"$a\\n$b\\n\"}' INPUT.FASTA"
        ],
        "iptables":[
            "# remember to do the same in ip6tables",
            "# list all, add -t CHAIN to the end to filter",
            "iptables -L -n -v",
            "# save and load (iptables are non-persistent)",
            "iptables-save >ip.bak",
            "iptables-restore <ip.bak"
        ],
        "header":[
            "head -n1 input.tsv | tr '\\t' '\\n' | awk '{print NR\" \"$1}'"
        ],
        "diamond":[
            "diamond makedb -p 10 --in input.faa -d input.diamondv$(diamond version | cut -d' ' -f3)",
            "diamond blastp -p 10 --more-sensitive --query query.faa -e 1e-5 --outfmt 6 -d input.diamondv$(diamond version | cut -d' ' -f3)",
            "# --subject-cover 90 --query-cover 90",
            "# --max-target-seqs 99999",
            "# --masking seq (for low complexity sequences)"
        ],
        "blast":[
            "makeblastdb -dbtype nucl -in DBFILE",
            "makeblastdb -dbtype prot -in DBFILE -out OUTFILE",
            "blastp -query QUERYFILE -db DBFILE -outfmt 6 -num_threads 4",
            "blastp -query QUERYFILE -db DBFILE -outfmt '6 qseqid sseqid bitscore' -num_threads 4 | sort -k1,1 -k3,3nr | awk '{if(last != $1){last=$1;print $0;}}'",
            "# default tab=6 header",
            "qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore",
            "# Here the some useful options:",
            "-evalue 1e-4 : sets the evalue cut-off",
            "-soft_masking false -dust no : disables soft masking (low complexity filtering)",
            "-word_size 11 : should be 3 for FAA vs FAA and 11-28 for FNA vs FNA",
            "-task blastn : for blastn, this sets -word_size 11",
            "-strand {both,plus,minus} : by default blast searches both strands (so also the rc)",
            "# extract matches : ",
            "easyblast.sh query db.fna | awk '{print $2}' | grepfasta.pl - db.fna",
            "1.  qseqid      query or source (gene) sequence id",
            "2.  sseqid      subject or target (reference genome) sequence id",
            "3.  pident      percentage of identical positions",
            "4.  length      alignment length (sequence overlap)",
            "5.  mismatch    number of mismatches",
            "6.  gapopen     number of gap openings",
            "7.  qstart      start of alignment in query",
            "8.  qend        end of alignment in query",
            "9.  sstart      start of alignment in subject",
            "10.  send        end of alignment in subject",
            "11.  evalue      expect value",
            "12.  bitscore    bit score",
        ],
        "sshfs": [
            "# make sure that TARGETDIR exists and is empty",
            "sshfs -p PORT SSHTARGET:/home/paul/ ~/TARGETDIR -oauto_cache,reconnect,defer_permissions,negative_vncache,noappledouble,volname=NAMEOFVOLUME",
            "# unmount with umount (no n)",
            "umount ~/TARGETDIR"
        ],
        "git": [
            "# remove large .git/objects/pack/tmp_* make some housekeeping tasks, e.g. from a wrong large 'git add .' (before commit)",
            "# !! keeps all local files as they are !!",
            "git gc --aggressive --prune=now",
            
            "# remove only on commit but KEEP LOCAL FILES",
            "git rm --cached file_to_remove.txt",

            "# Delete the most recent commit, keeping the work you've done:",
            "git reset --soft HEAD~1",

            "# Delete the most recent commit, destroying the work you've done:",
            "git reset --hard HEAD~1"
        ],
        "rsync":[
            'rsync -r -auhP --stats --info=progress2 --exclude={".DS_Store","._DS_Store",".nfs00*"} FROM TO'
        ],
        "bowtie2": [
            "bowtie2-build input.faa input.idx",
            "# ALWAYS USE --xeq, otherwise M is either X or =",
            "# single-end:",
            "for f in ../RAW/*fastq.gz; do bowtie2 -p 20 -k30 --xeq -x input.idx -U $f 2>$(basename $f).err | samtools sort --threads 4 >$(basename $f).bam; done",
            "# paired-end indicated by \"_R1_001.fastq.gz\" and \"_R2_001.fastq.gz\"",
            "for f in $(ls ../RAW/*fastq.gz | sed -E 's/_R.*//g' | sort | uniq); do bowtie2 -p 64 --xeq -x ../GENOME/genome.idx -k30 -1 ${f}_R1_001.fastq.gz -2 ${f}_R2_001.fastq.gz 2>$(basename $f).err | samtools sort --threads 4 >$(basename $f).bam; done",
            "# !!!",
            "# USE the --local to allow for soft-clipping, which usually is just better than the global alignment",
            "# !!!"
        ],
        "bwa":[
            "bwa index FASTA.fna",
            "for f in *fastq*gz; do bwa mem -t 20 FASTA.fna $f | samtools sort -@ 8 > $f.bam; done"
        ]
        "start":[
            "for f in ../RAW/*/*fastq.gz; do STARtup.py -o $(basename $f) -t 48 ../genome/Bacillus_subtilis_2017_03_21_NCIB3610.GCF_002055965.1_ASM205596v1_genomic.fna $f; done"
        ],
        "segemehl":[
            "segemehl -x input.idx -d input.faa",
            "# single-end:",
            "segemehl -minsize 16 -E 0.001 -t 64 -i input.idx -d input.fna -q $f -b -o $(basename $f).bam"
        ],
        "featurecounts":[
            "# -M --fraction, does not work with bowtie, since it is missing the NH:i:X tags",
            "# imporant use capital C => featureCounts, install via 'subread'",
            "featureCounts -T 10 -M --fraction -g ID --extraAttributes gene -t gene -a bla.gff -o counts.tsv *bam",
            "# CDS,... with product info",
            "featureCounts -T 10 -M --fraction -g ID --extraAttributes product -t CDS,rRNA,tRNA,tmRNA,RNase_P_RNA,SRP_RNA -a bla.gff -o counts.tsv *bam",
            "# paired-end:",
            "featureCounts -T 10 -p --countReadPairs ..."
        ],
        "heatmap":[
            'Heatmap(mat, width=1.5,',
            'name = "l2fc",',
            'cluster_rows = F,',
            'row_names_gp = gpar(fontsize = 10),',
            'row_split = ifelse(l2fc>0," ","  "),',
            'cluster_columns = F, ',
            'show_row_dend = FALSE,',
            'column_title_gp = gpar(fontsize = 8),',
            'column_gap = unit(0, "mm"),',
            'row_title_rot=0, border = TRUE,',
            'show_heatmap_legend = F,',
            'cell_fun = function(j, i, x, y, w, h, fill) { grid.text(mat_text[i,j],2), x, y, gp = gpar(fontsize = 8)) },',
            'col = colorRamp2(c(-3, 0, 3), c("#3465eb", "#e8e8e8", "#eb4034")))',
        ],
        "wide2long":[
            'library(reshape2)',
            'melt(olddata_wide, id.vars=c("subject", "sex"))',
            'data_long <- melt(',
            '   olddata_wide,',
            '   id.vars=c("subject", "sex"),',
            '   measure.vars=c("control", "cond1", "cond2" ),',
            '   variable.name="condition",',
            '   value.name="measurement" )',
        ],
        "knitter":[
            "Rscript -e \"rmarkdown::render('myfileRMD')\"",
            "knitr::opts_chunk$set(warning=F, mesage=F, dpi=200, fig.path='output_plots/', dev=c('png', 'svg'))",
            "knitr::opts_chunk$set(echo = TRUE, collapse = TRUE) # hide output in code block"
        ],
        "gff":[
            "1 seqname - name of the chromosome or scaffold; chromosome names can be given with or without the 'chr' prefix. Important note: the seqname must be one used within Ensembl, i.e. a standard chromosome name or an Ensembl identifier such as a scaffold ID, without any additional content such as species or assembly. See the example GFF output below.",
            "2 source - name of the program that generated this feature, or the data source (database or project name)",
            "3 feature - feature type name, e.g. Gene, Variation, Similarity",
            "4 start - Start position* of the feature, with sequence numbering starting at 1.",
            "5 end - End position* of the feature, with sequence numbering starting at 1.",
            "6 score - A floating point value.",
            "7 strand - defined as + (forward) or - (reverse).",
            "8 frame - One of '0', '1' or '2'. '0' indicates that the first base of the feature is the first base of a codon, '1' that the second base is the first base of a codon, and so on..",
            "9 attribute "
        ],
        "sam":[
            "1  QNAME   String  Query template NAME",
            "2  FLAG    Int     bitwise FLAG",
            "   1   1   Read paired",
            "   2   2   Read mapped in proper pair",
            "   3   4   Read unmapped",
            "   4   8   Mate unmapped",
            "   5   16  Read reverse strand",
            "   6   32  Mate reverse strand",
            "   7   64  First in pair",
            "   8   128     Second in pair",
            "   9   256     Not primary alignment",
            "   10  512     Read fails platform/vendor quality checks",
            "   11  1024    Read is PCR or optical duplicate",
            "   12  2048    Supplementary alignment",
            "3  RNAME   String  References sequence NAME",
            "4  POS     Int     1- based leftmost mapping POSition",
            "5  MAPQ    Int     MAPping Quality",
            "6  CIGAR   String  CIGAR string",
            "7  RNEXT   String  Ref. name of the mate/next read",
            "8  PNEXT   Int     Position of the mate/next read",
            "9  TLEN    Int     observed Template LENgth",
            "10     SEQ     String  segment SEQuence",
            "11     QUAL    String  ASCII of Phred-scaled base QUALity+33 ",
        ],
    }

    for key, cmds in commands.items():
        if not select or key in select:
            print_section(key)
            run_command(cmds)
            print("")

if __name__ == "__main__":
    main()
