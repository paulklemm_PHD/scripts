#!/usr/bin/env perl
#pk

use strict;
use warnings "all";

use Getopt::Long; # for parameter specification on the command line
use File::Basename; # basename()
$|=1; #autoflush

my $version="0.1.2"; # last updated on 2022-04-01 11:04:33

my $me = basename($0);

my $mapq_cutoff=30;
my $len_cutoff=30;
my $depth_cutoff=50;

my $usage = "$me        updates a genome from mapped reads

SYNOPSIS

$me (options) FILE.sam/bam

DESCRIPTION
	-gff,-g : reference GFF file
	-mapq : minimum MAPQ score [default:$mapq_cutoff]
	-len : minimum read length [default:$len_cutoff]
	-depth : minimum depth [default:$depth_cutoff]

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES Perl Getopt::Long, File::Basename
";

my $gfffile = "";
my $verbose; 
my $help;
my $debug;
my $numFiles=0;
our @gff_full_data;
our %stats;
$stats{"ambiguous"}=0;
$stats{"counting read"}=0;
$stats{"counting gff"}=0;
$stats{"gff"}=0;
$stats{"bad read"}=0;
$stats{"counting group"}=0;
$stats{"no gff found"}=0;

GetOptions('help!'=>\$help,'h!'=>\$help,
	'gff=s'=>\$gfffile,
	'g=s'=>\$gfffile,
	'mapq=i'=>\$mapq_cutoff,
	'len=i'=>\$len_cutoff,
	'depth=i'=>\$depth_cutoff,
	'version!' => sub { print "$0\tv$version\n";exit 0; },
	'v!' => sub { print "$0\tv$version\n";exit 0; },
	'verbose' => \$verbose,
	'x' => \$debug
);

if ($help || scalar(@ARGV) == 0 ){
    print $usage;
    exit(1);
}

if($gfffile ne ""){
	print STDERR "[$me]  processing ".$gfffile."...\n";

	my $num_of_entries = 0;

	my %check_id_clash;

	my $numOfProcessedReads=0;

	open(FH,"<",$gfffile) or die("ERROR input gff file: $!");while(<FH>){
		chomp; if(length($_)<2||substr($_,0,1) eq "#"){next;} # empty line / no information
		
		my $line = $_;
		my @line_split = split "\t",$line;
		# with $line_split[2] => feature 
		# and $line_split[8] => attributes

		if($debug){print STDERR "$line\n"}

		$stats{"gff total"}++;

		my @a=split("\t",$line);

		my $name="$a[0]-$a[3]-$a[4]-$a[6]";

		if(exists $check_id_clash{$name}){
			print STDERR "[WARNING]  the id '$name' of the given gff did clash, skipping this entry (try to adjust the -gff_id)...\n";
			next;
		} 
		$check_id_clash{$name}=1;

		$name=~s/["' ;]+$//g;

		if($name eq ""){next;}
		

		my $chr=$a[0];
		if($a[4]<$a[3]){my $tmp=$a[3];$a[3]=$a[4];$a[4]=$tmp} # reorder if start and end are mixed
		my $start=$a[3];
		my $end=$a[4];
		my $strand=$a[6];
		my $is_plus_strand;

		if( $strand eq "+" ){ $is_plus_strand=1 }elsif($strand eq "-"){ $is_plus_strand=0;}else{ $is_plus_strand=-1;}
		push(@a,$name); # the 10th entry ([9]) is the extracted name/id (gff_id) ! 

		$stats{"gff"}++;
		push(@gff_full_data,\@a);
	
	}close(FH);

	print STDERR "[$me]  loaded ".$stats{"gff"}."/".$stats{"gff total"}." entries from the gff file\n";
}

our %count;
our @curSamArray;
our $fp;
our $cur_read;
our %ambiguous_reads;

my %seen_gff_entries = (); 

sub approxProgress{
	my $in=shift;
	if($in > 0.999){$in=0.999}
	return $in;
}

foreach $fp (@ARGV) {

	$numFiles++;
	my $numOfProcessedReads=0;
	my $wcl = 1;

	my %mapping;
	my %numReadsPerPos;

	my $FH;
	if($fp=~m/\.bam$/){
		open($FH,"samtools view $fp |") or die("ERROR input bam file: $!");
		my $filesize = (-s $fp);
		$wcl= $filesize * 0.0042 * 10; # aproximate line count (bam are aprox 10 times smaller than sam files)
	}else{
		open($FH,"<",$fp) or die("ERROR input sam file: $!");
		my $filesize = (-s $fp);
		$wcl= $filesize * 0.0042; # aproximate line count
	}
	if($wcl==0){$wcl=1}

	$wcl *= 0.5; # further correction

	if(int($wcl/1000000) == 0){
		print STDERR "[$me]  counting $fp (approximately ~".(int($wcl))." reads)...\n";
	}else{
		print STDERR "[$me]  counting $fp (approximately ~".(int($wcl/1000000))."e+6 reads)...\n";
	}

	while(<$FH>){

		chomp;
		my $curLine = $_;
		@curSamArray = split("\t",$_); 	# 0 = read ID, 1 = bitflag (0x4=is unmapped), 2 = target gene 
		$numOfProcessedReads++;

		if(length($_)<1 || substr($_,0,1)eq "#" || scalar @curSamArray < 10){next}

		my $readname = $curSamArray[0];
		my $bitflag = $curSamArray[1];

		my $RNAME_chromosome = $curSamArray[2]; # chrosome / plasmid name 
		if($RNAME_chromosome eq "*" || $bitflag & hex("0x4")){
			next;
		}

		if( $curSamArray[4] eq "" || $curSamArray[4] eq "*" ){next}

		my $MAPQ = int($curSamArray[4]);
		if($MAPQ == 255 || $MAPQ<$mapq_cutoff){next}

		if(length($curSamArray[9])<$len_cutoff){next}

		my $leftmostpos = $curSamArray[3];
		my $seq = $curSamArray[9];
		my $is_plus_strand;
		if($bitflag & hex("0x10")){$is_plus_strand=0;}else{$is_plus_strand=1;} # 0x10 = bit 16 = is rc = minus strand 

		my $name="";

		my $CIGAR_expanded="";
		my $CIGAR=$curSamArray[5];
		while($CIGAR=~/([0-9]+)([=IHXMD])/g){
			my $howmuch = $1;
			my $what = $2;
			$CIGAR_expanded .= ("".$what) x int($howmuch);
		}

		my $genom_pos = 0;
		my $cur_I = 1;
		my $last_cig = "";
		for (my $i = 0; $i < length($seq); $i++) {

			my $nucl = substr($seq,$i,1);
			my $cig = substr($CIGAR_expanded,$i,1);

			if($cig eq "=" || $cig eq "M" || $cig eq "X"){

				if(!exists $numReadsPerPos{$RNAME_chromosome}){ $numReadsPerPos{$RNAME_chromosome} = () }
				if(!exists $numReadsPerPos{$RNAME_chromosome}{$leftmostpos+$genom_pos}){ $numReadsPerPos{$RNAME_chromosome}{$leftmostpos+$genom_pos} = [] }
				if(!exists $numReadsPerPos{$RNAME_chromosome}{$leftmostpos+$genom_pos}[0]){ $numReadsPerPos{$RNAME_chromosome}{$leftmostpos+$genom_pos}[0] = 0 }

				if(!exists $mapping{$RNAME_chromosome}){ $mapping{$RNAME_chromosome} = () }
				if(!exists $mapping{$RNAME_chromosome}{$leftmostpos+$genom_pos}){ $mapping{$RNAME_chromosome}{$leftmostpos+$genom_pos} = [] }
				if(!exists $mapping{$RNAME_chromosome}{$leftmostpos+$genom_pos}[0]){ $mapping{$RNAME_chromosome}{$leftmostpos+$genom_pos}[0] = () }
				if(!exists $mapping{$RNAME_chromosome}{$leftmostpos+$genom_pos}[0]{ $nucl }){ $mapping{$RNAME_chromosome}{$leftmostpos+$genom_pos}[0]{ $nucl } = 0 }

				$mapping{$RNAME_chromosome}{$leftmostpos+$genom_pos}[0]{ $nucl }++;

				$genom_pos++;
				$numReadsPerPos{$RNAME_chromosome}{$leftmostpos+$genom_pos}[0]++;
				$cur_I=1;

			}elsif($cig eq "I"){

				if($last_cig eq "D"){die "uhuh"}

				if(!exists $numReadsPerPos{$RNAME_chromosome}){ $numReadsPerPos{$RNAME_chromosome} = () }
				if(!exists $numReadsPerPos{$RNAME_chromosome}{$leftmostpos+$genom_pos-1}){ $numReadsPerPos{$RNAME_chromosome}{$leftmostpos+$genom_pos-1} = [] }
				if(!exists $numReadsPerPos{$RNAME_chromosome}{$leftmostpos+$genom_pos-1}[0]){ $numReadsPerPos{$RNAME_chromosome}{$leftmostpos+$genom_pos-1}[0] = 0 }

				if(!exists $mapping{$RNAME_chromosome}){ $mapping{$RNAME_chromosome} = () }
				if(!exists $mapping{$RNAME_chromosome}{$leftmostpos+$genom_pos-1}){ $mapping{$RNAME_chromosome}{$leftmostpos+$genom_pos-1} = [] }
				if(!exists $mapping{$RNAME_chromosome}{$leftmostpos+$genom_pos-1}[0]){ $mapping{$RNAME_chromosome}{$leftmostpos+$genom_pos-1}[0] = () }
				if(!exists $mapping{$RNAME_chromosome}{$leftmostpos+$genom_pos-1}[0]{ $nucl }){ $mapping{$RNAME_chromosome}{$leftmostpos+$genom_pos-1}[0]{ $nucl } = 0 }
				

				if(!exists $numReadsPerPos{$RNAME_chromosome}){ $numReadsPerPos{$RNAME_chromosome} = () }
				if(!exists $numReadsPerPos{$RNAME_chromosome}{$leftmostpos+$genom_pos-1}){ $numReadsPerPos{$RNAME_chromosome}{$leftmostpos+$genom_pos-1} = [] }
				if(!exists $numReadsPerPos{$RNAME_chromosome}{$leftmostpos+$genom_pos-1}[$cur_I]){ $numReadsPerPos{$RNAME_chromosome}{$leftmostpos+$genom_pos-1}[$cur_I] = 0 }

				if(!exists $mapping{$RNAME_chromosome}){ $mapping{$RNAME_chromosome} = () }
				if(!exists $mapping{$RNAME_chromosome}{$leftmostpos+$genom_pos-1}){ $mapping{$RNAME_chromosome}{$leftmostpos+$genom_pos-1} = [] }
				if(!exists $mapping{$RNAME_chromosome}{$leftmostpos+$genom_pos-1}[$cur_I]){ $mapping{$RNAME_chromosome}{$leftmostpos+$genom_pos-1}[$cur_I] = () }
				if(!exists $mapping{$RNAME_chromosome}{$leftmostpos+$genom_pos-1}[$cur_I]{ $nucl }){ $mapping{$RNAME_chromosome}{$leftmostpos+$genom_pos-1}[$cur_I]{ $nucl } = 0 }

				$mapping{$RNAME_chromosome}{$leftmostpos+$genom_pos-1}[$cur_I]{ $nucl }++;
				$numReadsPerPos{$RNAME_chromosome}{$leftmostpos+$genom_pos-1}[$cur_I]++;

				$cur_I++;

			}elsif($cig eq "D"){

				$genom_pos++;
				# nothing

			}else{
			 	die "unknown CIGAR '$cig'\n";
			}
			$last_cig=$cig;
		}

		if(!$debug && $numOfProcessedReads % 10000 == 0){
			print STDERR "\033[J"." processed ~".(int(approxProgress(($numOfProcessedReads)/int($wcl))*10000)/100)."% of the reads ..."."\033[G";
			#last;
		} # log

	}close(FH);


	foreach my $chr (keys %mapping) {
		print ">$chr";
		my $cnt = 0;
		foreach my $pos (sort { $a <=> $b } keys %{$mapping{$chr}}) {
			for (my $sub_pos = 0; $sub_pos < scalar @{$mapping{$chr}{$pos}}; $sub_pos++) { # sub-position : 0 = normal genomic position, 1,2,3,... = possible insertion positions
				
				if(!exists $numReadsPerPos{$chr} || !exists $numReadsPerPos{$chr}{$pos} || !exists $numReadsPerPos{$chr}{$pos}[$sub_pos]){die "-> $chr $pos $sub_pos $numReadsPerPos{$chr}{$pos}[$sub_pos]\n"} 

				if($numReadsPerPos{$chr}{$pos}[$sub_pos] < $depth_cutoff ){ next }

				if($cnt == 0){
					print " start=$pos\n";
				}

				my $maxU = 0;
				my $maxW = "";
				foreach my $w (sort keys %{$mapping{$chr}{$pos}[$sub_pos]}) {
					if( $maxU < $mapping{$chr}{$pos}[$sub_pos]{$w} ){ #|| ( $maxU == $mapping{$chr}{$pos}{$w} && $maxW eq " " ) ){
						$maxU = $mapping{$chr}{$pos}[$sub_pos]{$w};
						$maxW = $w
					}
				}

				if($maxW ne ""){
					if($cnt % 80 == 0 && $cnt > 0){print "\n"}
					print $maxW;
					$cnt++;
				}
			}
		}
		print "\n";
	}

}
