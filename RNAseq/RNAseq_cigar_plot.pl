#!/usr/bin/perl
#pk

use strict;
use warnings 'all';
use List::Util qw(max min shuffle);

my $version="0.0.9"; # last updated on 2022-04-05 15:33:33

if(scalar @ARGV != 0 && $ARGV[0] =~ /-?-version|-v$/ ){ print "$0\tv$version\n"; exit 0; }

if( scalar(@ARGV) < 1 || $ARGV[0] =~ /^-+(help|h)$/ ){
  print "".("USAGE : RNAseq_cigar_plot.pl   simple motif plot of the CIGAR field of a sam/bam file

SYNOPSIS 
  RNAseq_cigar_plot.pl INPUT.sam|bam

DESCRIPTION
	- The output is written to INPUT.cigar_plot.pdf
	- A intermediate INPUT.cigar_plot.R file is used to store data
	- The '=' are converted to M
	- n is used to (right) pad short reads to same lengths
	- If there are more than 10k reads -> randomly choose 10k entries.
	- Unmapped reads are omitted and reads on reverse strand are reversed 

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES R: ggseqlogo, ggplot2
"); exit 0;
}

if(!-e $ARGV[0]){die "input '$ARGV[0]' file not found..."};
if(-e "$ARGV[0].cigar_plot.R"){die "$ARGV[0].cigar_plot.R is present, exit"}; if(-e "$ARGV[0].cigar_plot.pdf"){die "$ARGV[0].cigar_plot.pdf is present, exit"}

print STDERR "collecting CIGAR...\n";

my $f=$ARGV[0];
open(my $FH,"samtools view $f |");
my @all_cigar_long;
while(<$FH>){
	chomp;
	my @arr = split("\t",$_);
	if(scalar @arr < 10){next}
	my $rev=0;
	if($arr[1] & 16){$rev=1}

	if($arr[1] & 4){next} # skip unmapped reads

	my $CIGAR = $arr[5];
	if($CIGAR eq ""){next}
	my $CIGAR_long="";
	while ($CIGAR =~ /([0-9]+)([^0-9])/ig) { $CIGAR_long .= $2 x int($1); }
	if($rev){$CIGAR_long = reverse($CIGAR_long)}
	if($CIGAR_long eq ""){next}
	push(@all_cigar_long,$CIGAR_long);
}
close($FH);

print STDERR "doing R plotting...\n";

if(scalar @all_cigar_long > 10000){ @all_cigar_long = shuffle(@all_cigar_long); @all_cigar_long = @all_cigar_long[0..10000] }

my $max_len = max map {length($_)} @all_cigar_long;

@all_cigar_long = map {$_="'$_'"} 
					map {$_=~s/=/M/g;$_} 
					map {if(length($_) < $max_len){$_ .= "n" x ($max_len - length($_))}else{$_}} 
					grep {length($_)>0} @all_cigar_long;

my $arr_cigar = join ",",@all_cigar_long;

open(my $FHo,">$ARGV[0].cigar_plot.R");
print $FHo "for(f in c('ggplot2','ggseqlogo')){if(!require(f,character.only=T)){install.packages(f, repos='https://cloud.r-project.org/');require(f,character.only=T)}};a=c($arr_cigar); p<-ggplot() + geom_logo( a, seq_type='other',namespace=unique(unlist(strsplit(paste0(a,collapse=''),''))) ) + theme_logo(); ggsave('$ARGV[0].cigar_plot.pdf');";
close($FHo);
`R --vanilla <$ARGV[0].cigar_plot.R 2>/dev/null; rm $ARGV[0].cigar_plot.R`;
