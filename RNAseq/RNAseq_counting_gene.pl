#!/usr/bin/env perl
#pk

use strict;
use warnings "all";

use Getopt::Long; # for parameter specification on the command line
use File::Basename; # basename()
$|=1; # autoflush

my $version="0.8.35"; # last updated on 2023-12-19 08:59:13

my $maxFast=10000;
my $gff_filter="";
my $gff_overlap=0.5;
my $gff_id="ID|product";
our $filter="4,512,1024";
my $usage = "RNAseq_counting_gene.pl        converts given sam/bam files to a count table. The output format is sgr.

SYNOPSIS

RNAseq_counting_gene.pl (options) FILE1.sam/bam FILE2.sam/bam FILE3.sam/bam ...
	
	-outputReads	
		If set, then reads are outputted for each target (for each gff entry, or mapped target). 
		WARNING: this can produce a lot of files and it can take a long time...

	-multimap, -multimapUniform
		If set, all mapped reads are counted but multimap mapped reads are equally distributed among all hits. 
		So if a read maps to 3 targets, then each target gets +1/3
	
	-multimapOne
		If set, all mapped reads are counted as if they are uniquely mapped reads. 
		So if a read maps to 3 targets, then each target gets +1
	
	-multimapDistribution FILE
		Same as --multimapUniform but the input file specifies the relative quantities of the targets.
		FILE : a mapping (tab separated) of the name of the target and the amount of that entry (other targets are dismissed)

	-position (start|end|coverage|startend)
		instead of counting features (genes, CDS, ...) count the number of start/end positions or calculate the coverage.
		- The end of a read is calculated with the CIGAR string (alignment end position in target).
		- The strand information is used as well.
		- This option will add the coloumn '0based-position-to-start' to the output table indicating:
			0  = first nt/start of the gff entry (the so-called '+1'), 
			-i = i nt in front of the start (gff) (e.g. -35 region for CDS)
			This is position is relative to the strand of the gff entry.
		- Can be used together with -gff. Use -examplesPosition for some examples.

	-gff, -g 	
		A gff file for counting: if at least 50\% of the read mapped inside the start-end range of a gff entry, if genes (gff) overlap then all entries are counted
		The sam CIGAR string is used for the overlap calculations !

		counted Reads :        |=>  |=>  |=> |=>     |=>  |=>           
		omitted Reads : |=>|=>                                   |=>                   
		GFF           : _____|=======Gene1======>___________________
		GFF           : ____________________|=======Gene2======>____

	-gff_filter : filter the gff file by feature (the 3. column). Usually this is set to 'gene' or 'CDS'.
				  You can use 'CDS|rRNA|exon' or '.*RNA|gene' to define multiple filters at once [default:'$gff_filter']
	-gff_id 	: The id that is used for counting (do not use Name/product since it is usually not unique!)
				  You can use 'ID|product' to combine the 2 attributes ID and product [default:$gff_id]
	-gff_start=3 : If set, only reads are counted that overlap the +-3nt area around the start, 
	               you can also encode this in the gff (add 'gff_start=3;' to the gff attribute field).
	               The start is adaptively chosen for + and - stranded features if -sc is set, otherwise this is always the gff start position.
	               This disables the requirement of a 50\% overlap.
	-gff_end=3   : Same for the end.
	-gff_overlap: minimal percentual overlap of a read with a gff entry (relative to read length) [default:$gff_overlap].
                  A value of 0 corresponds to the default featureCount (at least 1 nt).

	automatic for -gff : 
		strand specific counting : omitting reads that do not match the strandness of the gff entry (see -examples for more details) 
		(If -gff is not used, strand information is omitted)

	-s	: a tab separated grouping file that maps from a groupid to target names (<-as provided in the sam file). 
		  This can be used for counting isoforms, splicing variations, ... 
		  If a read maps uniquely to only target names of a group, the groupid counts +1. 
		  All other target names are treated normally.
		  This option can be combined with -gff option (the grouping should then reference the names/ids of -gff_id)

	-filter
		exclude decimal flags including these bits, given in comma separated list. 
		If no multimap options is used the flags 256 and 2048 are used as well [default:$filter]
			1 : Read paired
			2 : Read mapped in proper pair
			4 : Read unmapped
			8 : Mate unmapped
			16 : Read reverse strand
			32 : Mate reverse strand
			64 : First in pair
			128 : Second in pair
			256 : Not primary alignment
			512 : Read fails platform/vendor quality checks
			1024 : Read is PCR or optical duplicate
			2048 : Supplementary alignment
			
	Hint for large gff files: You can use the 'split -l 1000 --numeric-suffixes some.gff some.gff' to split your gff into smaller chunks, then run this program multiple times (using the fast approach with -m 1000) and add the results together.

DESCRIPTION
	- input can be sam or bam (samtools is needed for bam)
	- output is written to STDOUT (sgr format)
	- unmapped reads are omitted by default (bit flag 4, -filter)
	- CIGAR string is used for end positions/coverage !
	- RNASeq sam/bam input files are assumend to be 1based

EXAMPLES
	-examplesGFF   : displays various examples for the -gff option
	-examplesPosition   : displays various examples for the -position option

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES Perl Getopt::Long, File::Basename
";

#	-m	: the maximum number of elements that the gff can contain to use the fast and memory intensive method (set this to 0 to save memory on the cost of time) [default:$maxFast]. 
#- multimap mapped reads are omitted by default (NH:i:X, X>1)
#- if NH:i:X read attribute is missing all reads are treated as uniquely mapped
#	For Bowtie2 use : 'samtools view -q 1 input' (Bowtie2 assigns a MAPQ of 1 to unique reads)
#	Other : grep -v 'XT:A:R' input.sam or grep -v 'XA:Z:' input.sam 

my $usage_examples_position="EXAMPLES 
	# total coverage ignoring the strands:
	-position coverage
		counts        :  112121211 1221  222 111
		Reads         :    |=> |=>  <=|  |=>        
		Reads         :  |=> <=|   |=>   |=> |=>       

	# ignore strand information of the reads:
	-position start
		counts        :  1 1   2   1  1  2   1
		Reads         :    |=> |=>  <=|  |=>        
		Reads         :  |=> <=|   |=>   |=> |=>       

	# count start positions of reads:
	-position start
		counts        :  1 1   2   1  1  2   1
		Reads         :    |=> |=>  <=|  |=>        
		Reads         :  |=> <=|   |=>   |=> |=>    

	# now the end positions:
	-position end
		counts        :    1 2   1  11     2   1
		Reads         :    |=> |=>  <=|  |=>        
		Reads         :  |=> <=|   |=>   |=> |=>    

	=> if you want strand-specific information (e.g. only start position of + stranded reads), filter the input first (e.g. for minus strand => samtools view -f 16)
";

#		-uc  strand unspecific counting, treat all reads and gff entries as part of the same strand
# # ignore any strand information
# -gff x.gff -uc 
# 	counted Reads :    |=> |=>  <=|   |=> |=>       
# 	omitted Reads : |=>                       |=>
# 	GFF           : ____|=======Gene1======>____
# 	=> OUTPUT     : Gene1	5


my $usage_examples="EXAMPLES 
	# use strand information
	-gff x.gff  
		counted Reads :    |=> |=>        |=> |=>       
		omitted Reads : |=>          <=|          |=>
		GFF           : ____|=======Gene1======>____
		=> OUTPUT     : Gene1	4

		counted Reads :              <=|            
		omitted Reads : |=>|=> |=>        |=> |=> |=>
		GFF           : ____<=======Gene1======|____
		=> OUTPUT     : Gene1	1

	# gff_start option:
	-gff x.gff -gff_start=1

		# if gff entry is on + : only take reads mapping to PLUS strand and 
		# only those who start within 1nt from the start position of the gff entry
			counted Reads :     |=>        
			omitted Reads : |=>     |=>  <=|  |=> |=>
			GFF           : ____|=======Gene1======>____
		=> OUTPUT         : Gene1	1

		# if gff entry is on - : only take reads mapping to MINUS strand and 
		# only those who end within 1nt from the end position of the gff entry
			counted Reads :                      <=|        
			omitted Reads : |=>     |=>  <=|  |=> |=>
			GFF           : ____<=======Gene1======|____
			                                       \\- gff end
		=> OUTPUT         : Gene1	1

	# gff_end option:
	-gff x.gff -gff_end=3

		# if gff entry is on + : only take reads mapping to PLUS strand and 
		# only those who end within 3nt from the end position of the gff entry
			counted Reads :                      |=> 
			omitted Reads : |=>     |=>  <=|  |=> <=|
			GFF           : ____|=======Gene1======>____
			                                        \\- gff end
		=> OUTPUT         : Gene1	1
";

#	--multimap (deprecated)		If set, all mapped reads are counted

my $gfffile = "";
my $groupfile = "";

# only for gff option:
my $unstranded; # DEACTIVATED: -uc : overwrites all strand info with +
my $stranded; # -sc : only take reads that are in same orientation as gff entry

my $verbose; 
my $outputReads; 
my $multimap=0; 
my $multimapUniform;
my $multimapOne;
my $multimapDistribution;
my $gff_count_only_in_start; 
my $gff_count_only_in_end; 
my $numOfProcessedReads;
my $help;
my $debug;
my $numFiles=0;
our $position_modus="";
our %stats;
$stats{"multimap"}=0;
$stats{"counting alignment"}=0;
$stats{"counting gff"}=0;
$stats{"gff"}=0;
$stats{"gff filtered"}=0;
$stats{"counting group"}=0;

GetOptions('help!'=>\$help,'h!'=>\$help,
	's=s'=>\$groupfile,
	'multimapDistribution=s' => \$multimapDistribution,
	'multimapUniform' => \$multimapUniform ,
	'multimap' => \$multimapUniform ,
	'ambiguousDistribution=s' => \$multimapDistribution, # deprecated name
	'ambiguousUniform' => \$multimapUniform , # deprecated name
	'filter=s'=>\$filter,
	'gff=s'=>\$gfffile,
	'g=s'=>\$gfffile,
	'gff_filter=s'=>\$gff_filter,
	'gff_id=s'=>\$gff_id,
	'gff_overlap=f'=>\$gff_overlap,
	'gff_start=i'=>\$gff_count_only_in_start,
	'gff_end=i'=>\$gff_count_only_in_end,
	'position=s'=>\$position_modus,
#	'm=i'=>\$maxFast,
#	'uc' => \$unstranded,
#	'sc' => \$stranded,
	'version!' => sub { print "$0\tv$version\n";exit 0; },
	'v!' => sub { print "$0\tv$version\n";exit 0; },
	'examplesGFF!' => sub { print "$usage_examples\n";exit 0; },
	'examplesPosition!' => sub { print "$usage_examples_position\n";exit 0; },
	'verbose' => \$verbose,
	'outputReads' => \$outputReads,
	'multimapUniform' => \$multimap,
	'multimapOne' => \$multimapOne,
	'x' => \$debug
);
if($gfffile ne ""){$stranded=1}

if ($help || scalar(@ARGV) == 0 ){
    print $usage;
    exit(1);
}
# if($multimap){print STDERR "[WARNING] --multimap is deprecated\n"}

if($groupfile ne "" && $outputReads){
	print STDERR "[ERROR] the combination of -s and -outputReads is not supported";
	exit 1;
}

if(($gff_count_only_in_start || $gff_count_only_in_end) && !$gfffile){
	print STDERR "[ERROR] the combination of -gff_start or -gff_end without -gff is not supported";
	exit 1;
}

if($multimapDistribution){$multimap=1}
if($multimapUniform){$multimap=1}
if($multimapOne){$multimap=1}

#if(!$multimap && $gfffile eq ""){$filter = ($filter eq "" ? "256,2048" : "$filter,256,2048")}


if( $gfffile ne "" && ( !defined $stranded && !defined $unstranded ) && $position_modus ne "" ){print STDERR $usage."\n[ERROR] Please specify either -uc for strand unspecific counting OR -sc for strand specific counting (and not both).\n";exit 1;}
if( $gfffile eq ""){$unstranded=1}

my %gff_plus;
my %gff_minus;
my $do_gff;
my @gff_full_data;
my $db;
my %gff_lokus2idx;
my $gff_len = 0;
if($gfffile ne ""){
	if(!-e $gfffile){die "$gfffile does not exists."}
	print STDERR "[RNAseqCounting] Using strand ".( $unstranded ? "unspecific":"specific")." counting\n";
	$gff_len = `wc -l $gfffile`;
	$gff_len =~ s/^([0-9]+ ).*/$1/g;
}

if($verbose){print STDERR " -verbose is set \n"}

our %multimapping;
if($multimapDistribution && -e $multimapDistribution){
	open(my $FH,"<",$multimapDistribution) or die("ERROR input multimapDistribution file: $!");
	while(<$FH>){ 
		chomp;
		if(length($_)<2 || substr($_,0,1) eq "#" || $_ eq ""){next;}
		my @arr=split("\t",$_); 
		## 1. = target name, 2. = frequency/amount (number)
		if( scalar @arr != 2 ){print STDERR "[RNAseqCounting] multimapping file ERROR : $multimapDistribution wrong format (10 columns are needed)...\n";exit 1;}
		if( exists($multimapping{$arr[0]}) ){print STDERR "[RNAseqCounting] multimapping file ERROR : ".$arr[0]." is multimap...\n";exit 1;}
		$multimapping{$arr[0]} = $arr[1];
	}close($FH);
	print STDERR "[RNAseqCounting]  loaded a multimapping file with ".(scalar keys %multimapping)." entries...\n";
	if(scalar keys %multimapping == 0){print STDERR "[RNAseqCounting] ERROR: the multimapping file seems to be empty..."; exit 1;}
}

if($gfffile ne ""){
	print STDERR "[RNAseqCounting]  processing ".$gfffile."...\n";

	my $num_of_entries = 0;
	my $num_of_filtered_entries = 0;
	my $num_of_id_entries = 0;

	my %check_id_clash;

	$numOfProcessedReads=0;

	open(FH,"<",$gfffile) or die("ERROR input gff file: $!");while(<FH>){
		chomp; if(length($_)<2||substr($_,0,1) eq "#"){next;} # empty line / no information
		
		my $line = $_;
		my @line_split = split "\t",$line;
		# with $line_split[2] => feature 
		# and $line_split[8] => attributes

		if(scalar @line_split<8){next}

		if($debug){print STDERR "$line\n"}

		# if multiple gff_id are defined -> combine the values of those attributes
		if($gff_id=~/[#+|]/){
			my $cumm_value="";
			foreach my $id (split /[#+|]/,$gff_id) {
			 	if($line_split[8] =~ m/(?:${id})="?([^;]+)"?/i){$cumm_value.="$1 "}
			}
			#print STDERR $cumm_value;
			if($cumm_value ne ""){ $line_split[8].="gff_id=\"$cumm_value\";"} # set gff_id 
		}

		$stats{"gff total"}++;

		# if multiple gff_filter are defined -> search all
		my $has_one_gff_filter = 0;
		if($gff_filter=~/[#+|]/){
			foreach my $id (split /[#+|]/,$gff_filter) {
				if($line_split[2] =~ m/${id}/i){
					$has_one_gff_filter = 1; last;
				}
			}
		}else{
			if($line_split[2] =~ m/${gff_filter}/i){ $has_one_gff_filter=1 }
		}

		if( $gff_filter ne "" && !$has_one_gff_filter ){ $stats{"gff filtered"}++ }

		if( $gff_filter eq "" || $has_one_gff_filter ){

			$num_of_filtered_entries++;
			if( $line_split[8] =~ m/gff_id="?([^;]+)"?/i || $line_split[8] =~ m/(?:${gff_id})="?([^;]+)"?/i || $line_split[8]=~m/^(.*)$/){	

				$num_of_id_entries++;

				my $name=$1; # this defines the NAME column ($gff_lokus2idx...[9]) 

				if($name eq "" || exists $check_id_clash{$name}){

					if(index($name,$line_split[0]."-".$line_split[3]."..".$line_split[4].$line_split[6]) == -1){
						$name=$line_split[0]."-".$line_split[3]."..".$line_split[4].$line_split[6]."|".$name;
					}
					if(exists $check_id_clash{$name}){
						print STDERR "[WARNING]  the id '$name' of the given gff did clash, skipping this entry (try to adjust the -gff_id)...\n";
						next;
					}
				}
				$check_id_clash{$name}=1;

				$name=~s/["' ;]+$//g;

				if($name eq ""){next;}
				
				my @a=split("\t",$line);
				my $chr=$a[0];
				if($a[4]<$a[3]){my $tmp=$a[3];$a[3]=$a[4];$a[4]=$tmp} # reorder if start and end are mixed
				my $start=$a[3];
				my $end=$a[4];
				my $strand=$a[6];
				my $gff_atttr=$a[8];
				my $is_plus_strand;

				if( $unstranded ){ $strand="+"; }
				if( $strand eq "+" ){ $is_plus_strand=1 }elsif($strand eq "-"){ $is_plus_strand=0;}else{ $is_plus_strand=-1;}
				push(@a,$name); # the 10th entry ([9]) is the extracted name/id (gff_id) ! 

				$stats{"gff"}++;

				my $offset=0;
				if($gff_count_only_in_start){ $offset=$gff_count_only_in_start; }
				if($gff_count_only_in_end){ $offset=$gff_count_only_in_end; }
				if($gff_atttr =~ /gff_start=([0-9-]+)/){ $offset = $1; }elsif($gff_atttr =~ /gff_start/){ $offset = 3; }
				if($gff_atttr =~ /gff_end=([0-9-]+)/){ $offset = $1; }elsif($gff_atttr =~ /gff_end/){ $offset = 3; }

				for(my $i = $start -$offset ; $i < $end +$offset; $i++){ # store for all positions inside the gene the current gff entry -> arrays of arrays since these can overlap	
					$gff_lokus2idx{$is_plus_strand}{$chr}{$i}[ scalar @{$gff_lokus2idx{$is_plus_strand}{$chr}{int($i)}} ] = scalar @gff_full_data;
					if($debug){print STDERR "gff $name -> gff_lokus2idx{$is_plus_strand}{$chr}{$i} = ".(scalar @gff_full_data)." \n"}
				}
				$numOfProcessedReads++;
				if($numOfProcessedReads % 100 == 0){print STDERR "\033[J"." processed ".($numOfProcessedReads)."/".int($gff_len)." gff entries ..."."\033[G"} 

				push(@gff_full_data,\@a);
			}
		}
	}close(FH);

	print STDERR "[RNAseqCounting]  loaded ".$stats{"gff"}."/".$stats{"gff total"}." entries (adjust this with the -gff_filter option) from the gff file\n";
	
	if($stats{"gff"}<1){
		print STDERR "[ERROR]  did load nothing out of the given gff (adjust this with the -gff_filter option)...";
		if($stats{"gff total"}==0){
			print STDERR " the gff seems to be empty.\n";
		}elsif($num_of_id_entries == 0 && $num_of_filtered_entries > 0 ){
			print STDERR " please adjust the -gff_id option, currently '${gff_id}' is used but no gff entry has this attribute.\n";
		}elsif($num_of_filtered_entries == 0){
			print STDERR " please adjust the -gff_filter option, currently '${gff_filter}' is used but no gff entry has this type (third column of the gff). Set '-gff_filter \"\"' to disable the filtering completly.\n";
		}
		exit 1;
	}

	$do_gff=1;

#	if($stats{"gff total"} <= $maxFast){print STDERR "[RNAseqCounting]  since there are less than $maxFast (-m option) entries in the gff, the fast and memory heavy method is used. If you experience memory overflow use the -m option (-m=0 disables the fast approach).\n";}else{print STDERR "[RNAseqCounting]  since there are more than $maxFast (-m option) entries in the gff, the slow memory and conservative method is used.\n";}
}

our %grouping;
our $docheckgrouping=0; # grouping is the grouping of the genes to the group id (replace all genes with that group id)
if($groupfile ne ""){
	open(FH,"<",$groupfile) or die("ERROR input group file: $!");while(<FH>){ # build grouping
		chomp;
		if(length($_)<2 || substr($_,0,1) eq "#" || $_ eq ""){next;}
		my @arr=split("\t",$_); 
		if(scalar @arr !=2){next;}
		my $groupid=$arr[0];
		foreach my $trans (split(",",$arr[1])){ 
			if( !exists($grouping{$trans}) ){$grouping{$trans}=""}else{$grouping{$trans}.=","}
			$grouping{$trans}.=$groupid;
			$docheckgrouping=1;
		}
	}close(FH);
	print STDERR "[RNAseqCounting]  loaded a grouping file with ".(scalar keys %grouping)." entries...\n";
	if(scalar keys %grouping == 0){print STDERR "[RNAseqCounting] ERROR: the grouping file seems to be empty..."; exit 1;}
}

our %count;
our @curSamArray;
our $fp;
our $cur_read;
our %read_count_data;
our %is_filtered_removed_read;
our %bitflag2count;
our %read2alingmentNumber;
our %additional_info_position;

sub makePercent{
	my $a = shift;
	return( (int($a*10000)/100)."%" );
}
sub approxProgress{
	my $in=shift;
	if($in > 0.999){$in=0.999}
	return $in;
}
sub doCounting{

	my $readname = $curSamArray[0];
	my $bitflag = $curSamArray[1];
	my $targetname = $curSamArray[2]; # this is either the sam target (could be chromosome or transcript name of mapping) OR the name of the gff target (changed if -gff is set)

	# replace the name with the grouping name if present
	my $thisIsAGroupingEntry=0;
	if($docheckgrouping==1 && exists($grouping{$targetname})){
		$stats{"counting group"}++;
		$targetname=$grouping{$targetname};
		$thisIsAGroupingEntry=1;		
		# skip duplicated entries (same read id and same target name)
		if(exists $read_count_data{$readname} && exists $read_count_data{$readname}{$targetname}){ return; }
	}; # replace the gene id with group id

	foreach my $bit_test (split(",","1,2,4,8,16,32,64,128,256,512,1024,2048")) { 
		if($bitflag & int("$bit_test")){ if(!exists $bitflag2count{$bit_test}){$bitflag2count{$bit_test}=0} $bitflag2count{$bit_test}++; }
	}

	my $check_filter=1;
	foreach my $bit_test (split(",",$filter)) { 
		$check_filter *= (($bitflag & int("$bit_test")) == 0);
	}
	if( ($bitflag & int("256")) ){$stats{"scondary alignments"}++}
	if( ($bitflag & int("4")) ){$stats{"unmapped"}++}

	if( $docheckgrouping==1 &&  # count in groups -> only take reads that are multimapped -> not NH:i:1
		$targetname ne "*" && 
		$thisIsAGroupingEntry &&
		$check_filter && 
		( $multimap || $read2alingmentNumber{$readname}==1 ) && 
		!exists($is_filtered_removed_read{$readname}) ){ 

		if($debug){print STDERR "counted group\n"}

		$read_count_data{$readname}{$targetname}=1; # here save the read name -> mapping target (= replaced by group name, so they have all the same $targetname)
		# this ^ mapping should only be 1-1 if only target of the group are hit (all elements of the group have same $targetname)

		# otherwise do the following:
		#if(!$multimap && scalar keys(%{$read_count_data{$readname}})>1){ # correct for these later
		#	$is_filtered_removed_read{$readname}=1; 
		#	return;
		#}

	}elsif( $targetname ne "*" && 
		$check_filter && 
		( !exists($is_filtered_removed_read{$readname}) ) &&  # this checks if there is allready a readname present -> skip (except if multimap is set)
		( $multimap || $read2alingmentNumber{$readname}==1 ) # this makes sure that a read cannot map to same target multiple times (except if multimap is set)
		){

		# multimap mapped read add to stack to analyse later
		# multimapped reads have all the same readname 
		$read_count_data{$readname}{$targetname}=1; 

		if($debug){print STDERR "normal/multimapped\n"}

		# otherwise do the following:
		#if(!$multimap && scalar keys(%{$read_count_data{$readname}})>1){ # correct for these later
		#	$is_filtered_removed_read{$readname}=1; 
		#	return;
		#}

	}else{

		$read_count_data{$readname}{$targetname}=1; 
		$is_filtered_removed_read{$readname}=1;

		if($debug){print STDERR "[doCounting] e=".exists($read_count_data{$readname})." => not counted $readname:$targetname\n"; }
		# unmapped read / multimap mapped read 
	}
}

my %seen_gff_entries = (); 

foreach $fp (@ARGV) {

	$stats{"multimap"}=0;
	$stats{"counting alignment"}=0;
	$stats{"counting group"}=0;
	$stats{"scondary alignments"}=0;
	$stats{"unmapped"}=0;

	$numFiles++;

	if($outputReads){
		my $basefp = $fp; 
		$basefp=~s/^.*\/([^\/]+)$/$1/g;
		mkdir($basefp."_countedreads");
	}
			
	undef %count;				# count is a mapping from the gene (or grouping group) to all read ids that map to that gene (for aeach file) # gene just contains all genes that are mapped
	undef %read_count_data; %read_count_data=(); 	# this contains for a given read all genes / groups that are mapped to by this read (should be only one group/gene)
	undef %is_filtered_removed_read;
	undef %bitflag2count;

	$numOfProcessedReads=0;
	
	my $wcl = 1;

	my $FH;
	if($fp=~m/\.bam$/){
		system("samtools --help >/dev/null 2>&1");
		if($? != 0){die "[ERROR] samtools : $?"}
		open($FH,"samtools view $fp |") or die("ERROR input bam file: $!");
		my $filesize = (-s $fp);
		$wcl= $filesize * 0.0042 * 10; # aproximate line count (bam are aprox 10 times smaller than sam files)
	}else{
		open($FH,"<",$fp) or die("ERROR input sam file: $!");
		my $filesize = (-s $fp);
		$wcl= $filesize * 0.0042; # aproximate line count
	}
	if($wcl==0){$wcl=1}

	$wcl *= 0.5; # further correction

	if(int($wcl/1000000) == 0){
		print STDERR "[RNAseqCounting] counting $fp (approximately ~".(int($wcl))." reads)...\n";
	}else{
		print STDERR "[RNAseqCounting] counting $fp (approximately ~".(int($wcl/1000000))."e+6 reads)...\n";
	}
	my $num_alignments=0;
	my %tot_primary_reads;

	my $no_gff_alignment=0;

	while(<$FH>){
		chomp;
		my $curLine = $_;
		@curSamArray = split("\t",$_); 	# 0 = read ID, 1 = bitflag (0x4=is unmapped), 2 = target gene 

		if(length($_)<1 || substr($_,0,1)eq "#" || scalar @curSamArray < 10){next;}
#		my @cur_gff_overlapping_this_read;

		$num_alignments++;
		if(!exists $tot_primary_reads{$curSamArray[0]}){$tot_primary_reads{$curSamArray[0]}=1}

		if($outputReads){$cur_read = $curLine};

		my $readname = $curSamArray[0];

		my $bitflag = $curSamArray[1];
		my $targetname = $curSamArray[2]; # chrosome / plasmid name 
		my $leftmostpos = $curSamArray[3];
		my $seq = $curSamArray[9];
		my $is_plus_strand;

		if($docheckgrouping==1 && exists($grouping{$targetname})){
			if(exists $read_count_data{$readname} && exists $read_count_data{$readname}{$grouping{$targetname}}){ next; }
		};


		if(!exists $read2alingmentNumber{$readname}){$read2alingmentNumber{$readname}=0}
		$read2alingmentNumber{$readname}++;
		
		if(!$multimap && $read2alingmentNumber{$readname}>1){ $is_filtered_removed_read{$readname}=1; }

		#
		# correct the length of the read with the CIGAR string 
		#
		my $lengthOfRead = length($curSamArray[9]);
		my $CIGAR = $curSamArray[5];
		my $numOfInsertions = 0;
		my $numOfDeletions = 0;
		while ($CIGAR =~ /([0-9]+)D/ig) { $numOfDeletions+=$1; } # D : deletion
		while ($CIGAR =~ /([0-9]+)[ISH]/ig) { $numOfInsertions+=$1; } # I : insertion , S+H : clipping (Segment of the query sequence that does not appear in the alignment)

		# missmatches (X) and matches (=) can be ignored
		my $len = $lengthOfRead - $numOfInsertions + $numOfDeletions; # correct for CIGAR : - insertion and + deletions

		$curSamArray[10] = $len;

		if($position_modus ne ""){ 
			$additional_info_position{$readname}{$targetname}{"len"}=$len;
			$additional_info_position{$readname}{$targetname}{"leftmostpos"}=$curSamArray[3];
			$additional_info_position{$readname}{$targetname}{"bitflag"}=$bitflag;
		}

		if($bitflag & hex("0x10")){$is_plus_strand=0;}else{$is_plus_strand=1;} # 0x10 = bit 16 = is rc = minus strand 
		# ^ equivalent formulation : if( $bitflag & 16 )

		if( $unstranded ){ $is_plus_strand=1; }

		my %seen = (); # exclude duplicated names (NAME id of attribute in [9])

		if( $do_gff ){ # if gff is enabled then replace the target name with the corresponding gff entry

#			if($debug){print STDERR "do_gff:\n"}

			# collect gff entries that have some overlap with the current read
			# IF -sc : take only gff entries that have same orientation as the read ($is_plus_strand from the read)
			my @cur_gff_overlapping_this_read; #=@gff_full_data;
#			if(scalar keys %gff_lokus2idx == 0){@cur_gff_overlapping_this_read=@gff_full_data;}else{
				
				if(exists $gff_lokus2idx{$is_plus_strand} && exists $gff_lokus2idx{$is_plus_strand}{$targetname}{int($leftmostpos)}){ 
					# collect all gff entries that intersect with the start of the read
					foreach my $i (@{$gff_lokus2idx{$is_plus_strand}{$targetname}{int($leftmostpos)}}){
						if(!exists $seen{$gff_full_data[$i][9]} ){push(@cur_gff_overlapping_this_read,$gff_full_data[$i]); $seen{$gff_full_data[$i][9]} = 1;$seen_gff_entries{$gff_full_data[$i][9]} = 1;}
					}
				}
				if(exists $gff_lokus2idx{$is_plus_strand} && exists $gff_lokus2idx{$is_plus_strand}{$targetname}{int($leftmostpos+int($len/2))}){ 
					# or the middle
					foreach my $i (@{$gff_lokus2idx{$is_plus_strand}{$targetname}{int($leftmostpos+int($len/2))}}){
						if(!exists $seen{$gff_full_data[$i][9]} ){push(@cur_gff_overlapping_this_read,$gff_full_data[$i]); $seen{$gff_full_data[$i][9]} = 1;$seen_gff_entries{$gff_full_data[$i][9]} = 1;}
					}
				}
				if(exists $gff_lokus2idx{$is_plus_strand} && exists $gff_lokus2idx{$is_plus_strand}{$targetname}{int($leftmostpos+$len)}){ 
					# or the end of the read of the read
					foreach my $i (@{$gff_lokus2idx{$is_plus_strand}{$targetname}{int($leftmostpos+$len)}}){
						if(!exists $seen{$gff_full_data[$i][9]} ){push(@cur_gff_overlapping_this_read,$gff_full_data[$i]); $seen{$gff_full_data[$i][9]} = 1;$seen_gff_entries{$gff_full_data[$i][9]} = 1;}
					}
				}

				# => now @cur_gff_overlapping_this_read only contains all unique entries (therefore the %seen) that overlap the read anywhere (3 points make sure that all entries are looked at for a >50% coverage) 
#			}			
			if($debug){print STDERR "$readname : cur_gff_overlapping_this_read=".(scalar @cur_gff_overlapping_this_read)." !! \n"}

			my $did_find_a_gff=0;
			for(my $i = 0 ; $i < scalar @cur_gff_overlapping_this_read ; $i++){ # go over all gff entries that overlap the given read (if -sc then only same orientated gff entries are looked at)

				$targetname=""; # this will be replaced with the gff entry NAME/ID (if one matches)

				if($debug){print STDERR " $i gff:$cur_gff_overlapping_this_read[$i][0]:$cur_gff_overlapping_this_read[$i][9]\n"}

				my $gff_start  = $cur_gff_overlapping_this_read[$i][3];
				my $gff_end    = $cur_gff_overlapping_this_read[$i][4];
				my $gff_atttr  = $cur_gff_overlapping_this_read[$i][8];
				my $gff_strand = $cur_gff_overlapping_this_read[$i][6];

				my $contains_gff_start = $gff_count_only_in_start ? $gff_count_only_in_start : 0;
				my $contains_gff_end = $gff_count_only_in_end ? $gff_count_only_in_end : 0;
				if($gff_atttr =~ /gff_start=([0-9-]+)/){ $contains_gff_start = $1; }elsif($gff_atttr =~ /gff_start/){ $contains_gff_start = 3; }
				if($gff_atttr =~ /gff_end=([0-9-]+)/){ $contains_gff_end = $1; }elsif($gff_atttr =~ /gff_end/){ $contains_gff_end = 3; }
				
				# if -gff_start
				if( $contains_gff_start>0 ){ # if gff_start is set -> only search reads that overlap the start area of the gff entry
					
					if($gff_strand eq "+"){
						if( $leftmostpos >= $gff_start-$contains_gff_start && $leftmostpos <= $gff_start+$contains_gff_start){ # gff is + strand -> take start of read as start
							$targetname = $cur_gff_overlapping_this_read[$i][9]; 
							$seen_gff_entries{$targetname}=1;
						}
					}else{
						if($debug){print STDERR "contains_gff_start=$contains_gff_start \n".($leftmostpos+$len-1)." ".($gff_end-$contains_gff_start)."\n"}
						if( $leftmostpos+$len-1 >= $gff_end-$contains_gff_start && $leftmostpos+$len-1 <= $gff_end+$contains_gff_start){ # gff is - strand -> take end of read as start
							$targetname = $cur_gff_overlapping_this_read[$i][9];
							$seen_gff_entries{$targetname}=1;
						}
					}
					
				# if -gff_end
				}elsif( $contains_gff_end>0 ){ # same for gff_end (end position = leftmost+len)
					if($gff_strand eq "+"){
						if( $leftmostpos+$len-1 >= $gff_end-$contains_gff_end && $leftmostpos+$len-1 <= $gff_end+$contains_gff_end){ # gff is + strand -> take end of read as end
							$targetname = $cur_gff_overlapping_this_read[$i][9]; 
							$seen_gff_entries{$targetname}=1;
						}
					}else{
						if( $leftmostpos >= $gff_start-$contains_gff_end && $leftmostpos <= $gff_start+$contains_gff_end){ # gff is - strand -> take start of read as end
							$targetname = $cur_gff_overlapping_this_read[$i][9]; 
							$seen_gff_entries{$targetname}=1;
						}
					}

				# if normal counting
				}else{
					if( $leftmostpos+$len-1 < $gff_end && $leftmostpos+$len-1 > $gff_start && $leftmostpos<$gff_start && ($leftmostpos+$len - $gff_start)/($len) > $gff_overlap){
						$targetname = $cur_gff_overlapping_this_read[$i][9]; # read overlaps with start gffarea
						$seen_gff_entries{$targetname}=1;
					}elsif( $leftmostpos+$len-1 < $gff_end && $leftmostpos>=$gff_start ){ # if the whole read is inside = 100% is covered
						$targetname = $cur_gff_overlapping_this_read[$i][9]; # read totally inside the gffgene
						$seen_gff_entries{$targetname}=1;
					}elsif( $leftmostpos+$len-1 >= $gff_end && $leftmostpos < $gff_end && ($gff_end - $leftmostpos)/($len) > $gff_overlap){
						$targetname = $cur_gff_overlapping_this_read[$i][9]; # read overlaps the end gffarea
						$seen_gff_entries{$targetname}=1;
					}
				}

				if($targetname ne ""){
					$did_find_a_gff = 1;
					# $curSamArray[0] = "$readname:$targetname";
					$curSamArray[2] = $targetname;
					if($gff_strand eq "+"){
						$curSamArray[3] = $leftmostpos - $gff_start ;
					}else{ # - strand:
						$curSamArray[3] = $gff_end - $leftmostpos;
						#if($position_modus eq "start" || $position_modus eq "coverage"){ $curSamArray[3] -= 2*$len -2} # correcting factor
						# since the start position is calculated by $leftmostpos + $len -1
						# here it should be $leftmostpos - $len +1 
						# same for coverage
					}
					if($position_modus ne ""){ 
						$additional_info_position{$readname}{$targetname}{"len"}=$len;
						$additional_info_position{$readname}{$targetname}{"leftmostpos"}=$curSamArray[3];
						$additional_info_position{$readname}{$targetname}{"bitflag"}=$bitflag;
					}
					doCounting();
					$targetname="";
				}
			}
			if(!$did_find_a_gff){
				if($debug){print STDERR "$readname (# $num_alignments) -> no GFF found !! \n"}
				#$is_filtered_removed_read{$readname}=1;
				$no_gff_alignment+=1;
				#$read_count_data{$readname}={}; # without target
			}
		}elsif(!$do_gff && $targetname ne ""){ # "normal" with target in RNAME field (3. sam column), the filter e.g. for unmapped reads are inside the doCounting function !!
			doCounting();
		}

		$numOfProcessedReads++;
		if(!$debug && $numOfProcessedReads % int($wcl/10+1) == 1){print STDERR "\033[J"." processed ~".(int(approxProgress(($numOfProcessedReads)/int($wcl))*100))."% of the reads ..."."\033[G"} # log
 
	}close(FH);

	print STDERR "\033[J"."                                           "."\033[G";# clean up progress output

	print STDERR "[RNAseqCounting] $fp post-processing counts...\n";

	$stats{"multimap"}=0;
	foreach my $readname (keys %read_count_data) {
		# collect all the amounts of the given multimapping file for normalization
		my $sum=0; # sum all amounts of targets (if no multimapUniformFile this is equal to the number of targets for that multi read)
		if($multimapDistribution){
			foreach my $targetname (keys %{$read_count_data{$readname}}) {
				#print STDERR "TEST : $targetname\n";
				#print STDERR "TEST : $multimapping{$targetname}\n";
				if(exists $multimapping{$targetname}){ $sum += $multimapping{$targetname} } #[1]
			}
		}else{ # $multimapUniform
			$sum = $read2alingmentNumber{$readname}; #scalar keys %{$read_count_data{$readname}};
		}

		#if($debug){print STDERR "$readname = $sum\n"}
		if($sum == 0){ $is_filtered_removed_read{$readname}=1; next; }
		if($read2alingmentNumber{$readname} > 1){ $stats{"multimap"}++ }
		if($multimapOne){ $sum=1 }

		if($position_modus ne ""){
			# count positions

			if(exists $is_filtered_removed_read{$readname} ){next}
			$stats{"counting alignment"}++;

			foreach my $targetname (keys %{$read_count_data{$readname}}) {

				my $val = 1;
				if( $multimap ){ $val=1/$sum }

				my $cur_pos;
				my $other_cur_pos;
				my $len = $additional_info_position{$readname}{$targetname}{"len"};
				my $bitflag = $additional_info_position{$readname}{$targetname}{"bitflag"};
				my $leftmostpos = $additional_info_position{$readname}{$targetname}{"leftmostpos"};
				my $cur_strand="+";

				if($bitflag & hex("0x10")){$cur_strand="-";}

				if($position_modus eq "start"){
					$stats{"counting position"}++;

					if($cur_strand eq "-"){
						if($do_gff){
							$cur_pos = $leftmostpos - $len +1;
						}else{
							$cur_pos = $leftmostpos + $len -1; 
						}
					}else{
						$cur_pos = $leftmostpos; 
					}
				}elsif($position_modus eq "end"){
					$stats{"counting position"}++;

					if($cur_strand eq "-"){
						if($do_gff){
							$cur_pos = $leftmostpos;
						}else{
							$cur_pos = $leftmostpos; 
						}
					}else{
						$cur_pos = $leftmostpos + $len -1;
					}
				}elsif($position_modus eq "startend"){
					$stats{"counting position"}++;

					if($cur_strand eq "-"){
						if($do_gff){
							$cur_pos = $leftmostpos - $len +1;
						}else{
							$cur_pos = $leftmostpos + $len -1; 
						}
					}else{
						$cur_pos = $leftmostpos; 
					}

					my $cur_pos_end;
					if($cur_strand eq "-"){
						if($do_gff){
							$cur_pos_end = $leftmostpos;
						}else{
							$cur_pos_end = $leftmostpos; 
						}
					}else{
						$cur_pos_end = $leftmostpos + $len -1;
					}

					if( !exists($count{ $fp }{ $targetname }{ $cur_pos }{ $cur_pos_end }) ){ $count{ $fp }{ $targetname }{ $cur_pos }{ $cur_pos_end }=0; }
					
					# increment the position by val (or less if -multimapUniform is set)
					$count{ $fp }{ $targetname }{ $cur_pos }{ $cur_pos_end } += $val;

				}elsif($position_modus eq "coverage"){

					if($do_gff && $cur_strand eq "-"){
						$cur_pos = $leftmostpos - $len +1;
						$other_cur_pos = $leftmostpos; 
					}else{
						$cur_pos = $leftmostpos; 
						$other_cur_pos = $leftmostpos + $len -1;
					}

					$stats{"counting position"} += $len;
					for( my $it=$cur_pos ; $it <= $other_cur_pos ; $it++ ){

						if( !exists($count{ $fp }{ $targetname }{ $it }) ){
							$count{ $fp }{ $targetname }{ $it }=0;
						}

						# increment the given position
						$count{ $fp }{ $targetname }{ $it } += $val;
					}
				}else{
					print $usage."\n\n ### ERROR ### wrong -position value\n";
					exit(1);
				}

				if($position_modus eq "start" || $position_modus eq "end"){

					if( !exists($count{ $fp }{ $targetname }{ $cur_pos }) ){ $count{ $fp }{ $targetname }{ $cur_pos }=0; }
					
					# increment the position by val (or less if -multimapUniform is set)
					$count{ $fp }{ $targetname }{ $cur_pos } += $val;
				}
			}

		}else{
			# count genes not positions

			if(exists $is_filtered_removed_read{$readname} ){next}
			$stats{"counting alignment"}++;

			if( $multimap ){
				
				foreach my $targetname (keys %{$read_count_data{$readname}}) {
					if( exists $multimapping{ $targetname }){ $count{$fp}{ $targetname } += $multimapping{ $targetname }/$sum } #[1]
					else{ $count{$fp}{ $targetname } += 1 / $sum }
				}
			}elsif( $sum == 1 ){

				foreach my $targetname (keys %{$read_count_data{$readname}}) {

					if(!exists $count{$fp} || !exists $count{$fp}{$targetname}){
						if($outputReads){
							my @arr = ($cur_read);
							$count{$fp}{$targetname} = \@arr;
						}else{
							$count{$fp}{$targetname} = 1;
						}
					}else{
						if($outputReads){
							push(@{$count{$fp}{$targetname}},$cur_read);
						}else{
							$count{$fp}{$targetname}++;
						}
					}
				}
			}
		}
	}

	# print results :
	if($position_modus eq "startend"){
		print("# gene\tfile\t0based-position-to-start\t0based-position-to-end\tcount\n");
	}elsif($position_modus ne ""){
		print("# gene\tfile\t0based-position-to-start\tcount\n");
	}else{
		print("# gene\tfile\tcount\n");
	}
	foreach my $gene (sort keys %{$count{$fp}} ){
		if($outputReads){

			my $basefp = $fp; 
			$basefp=~s/^.*\/([^\/]+)$/$1/g;

			print "$gene\t$fp\t".scalar(@{$count{$fp}{$gene}})."\n";

			open(FH,">".$basefp."_countedreads/readsOf_".$gene.".sam") or die("ERROR output read file: $!");
			foreach my $read (@{$count{$fp}{$gene}}){
				print FH $read."\n";
			}
			close(FH);
		}else{
			if($position_modus eq "startend"){
				foreach my $cur_pos (keys %{$count{$fp}{$gene}}){
					foreach my $cur_pos_end (keys %{$count{$fp}{$gene}{$cur_pos}}){
						print "$gene\t$fp\t$cur_pos\t$cur_pos_end\t".($count{$fp}{$gene}{$cur_pos}{$cur_pos_end})."\n";
					}
				}
			}elsif($position_modus ne ""){
				foreach my $cur_pos (keys %{$count{$fp}{$gene}}){
					print "$gene\t$fp\t$cur_pos\t".($count{$fp}{$gene}{$cur_pos})."\n";
				}
			}else{
				print "$gene\t$fp\t".($count{$fp}{$gene})."\n";
			}
		}
	}

	my $primary_reads = scalar keys %tot_primary_reads; # 
	my $counted_primary_reads = scalar keys %read_count_data;
	my $bad_reads = scalar keys %is_filtered_removed_read;
	my $found_gff = scalar keys %seen_gff_entries;

	print STDERR "[RNAseqCounting] I processed '$fp' and found: 
  - ".$primary_reads." (=100\%) reads and $num_alignments alignments/entries\n";

  	print STDERR "  - bitflag per alignments:\n";
  	foreach my $bit_test (sort {$a <=> $b} keys %bitflag2count) { print STDERR "    - $bit_test : ".$bitflag2count{$bit_test}." (".($num_alignments>0?makePercent( $bitflag2count{$bit_test}/$num_alignments ):"-").")\n" }

  print STDERR "".(  $multimap ? "  - ".($stats{"multimap"})." (".($primary_reads>0?makePercent( $stats{"multimap"}/$primary_reads ):"-").") multimap reads with ".($stats{"scondary alignments"})." annotated secondary alignments\n" : "" );
  print STDERR "  - ".$bad_reads." (".($primary_reads>0?makePercent( $bad_reads/$primary_reads ):"-").") filtered reads ($filter)".( !$multimap ? "
    - ".($stats{"multimap"})." (".($primary_reads>0?makePercent( $stats{"multimap"}/$primary_reads ):"-").") multimap reads with ".($stats{"scondary alignments"})." annotated secondary alignments" : "" )."
    - ".$stats{"unmapped"}." (".($primary_reads>0?makePercent( $stats{"unmapped"}/$primary_reads ):"-").") unmapped reads".($do_gff ? "
    - ".$no_gff_alignment." alignments did not find any gff targets
      - ".($found_gff)."/".$stats{"gff"}." gff entries where covered by at least one read
      - ".($stats{"gff filtered"})."/".$stats{"gff"}." filtered gff entries":"").($groupfile ne "" ? "
  - ".$stats{"counting group"}." alignments are part of the given group file" : "")."
  => counted ".$stats{"counting alignment"}." (".($num_alignments>0 ? makePercent(  $stats{"counting alignment"}/$num_alignments  ):"-").") alignments.\n";
  # and ".($counted_primary_reads-$bad_reads)." (".($primary_reads > 0 ? makePercent(  ($counted_primary_reads-$bad_reads)/$primary_reads ):"-").") reads

	# perl unset stuff:
	%read_count_data=();
	undef %count; undef %read_count_data; undef %is_filtered_removed_read; undef %read2alingmentNumber;
}

if($gfffile ne "" && $position_modus ne ""){print STDERR "[WARNING] position are 0-based (0: the canonical +1)\n"}

print STDERR "all good\n";
