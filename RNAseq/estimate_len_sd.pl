#!/usr/bin/perl
#pk

use strict;

my $i=0;
my @lens;
while(<>){
	chomp;
	if($i++ % 4 == 1){push(@lens,length $_)}
}

# Calculate the mean (average) of the data
my $mean = 0;
$mean += $_ for @lens;
$mean /= @lens;

# Calculate the sum of squared differences from the mean
my $sum_squared_diff = 0;
$sum_squared_diff += ($_ - $mean) ** 2 for @lens;

# Calculate the variance
my $variance = $sum_squared_diff / (@lens - 1);

# Calculate the standard deviation
my $standard_deviation = sqrt($variance);

print "$mean $standard_deviation\n";