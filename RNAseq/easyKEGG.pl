#!/usr/bin/perl 

use strict;
use warnings "all";

use Getopt::Long; # for parameter specification on the command line
use File::Basename; # basename()
my $version="0.0.25"; # last updated on 2022-01-28 11:22:52

my $usage = <<ENDUSAGE;
easyKEGG.pl        simple KEGG interface through http requests
 
SYNOPSIS
 
easyKEGG.pl (options) OPERATION ARGUMENT

	OPERATION
		pathway2gene : builds a table that maps pathway ID to gene ID. requires the organism ID (e.g. hna, vna) 

	ARGUMENT
		additional arguments, see OPERATIONS

	options
		--verbose : print all wget commands and outputs
		--delay=i : delay between each query in seconds (default:1)

DESCRIPTION
 
	
       
EXAMPLES
 
	easyKEGG.pl pathway2gene vna

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES perl : Getopt::Long, File::Basename
ENDUSAGE

my $help;
my $argument;
my $operation;
my $verbose="2>/dev/null";
my $delay=1;

GetOptions('help!'=>\$help,'h!'=>\$help,
	'version!'=>sub { print "$0\tv$version\n";exit 0; },
	'verbose!'=>sub { $verbose="" },
	'delay=i'=>\$delay
);

$operation = $ARGV[0];
shift(@ARGV);
$argument = $ARGV[0];
shift(@ARGV);

if ( $help || !defined $operation || !defined $argument ){ print $usage; exit(1); } # USAGE

my $res;

if($operation eq "pathway2gene"){

	print STDERR "[INFO] retrieving list of all pathways (http://rest.kegg.jp/list/pathway/$argument)...\n";

	$res = `wget -O - http://rest.kegg.jp/list/pathway/$argument $verbose`;
	my %pathways = map { chomp; my @a=split("\t",$_); $a[0]=~s/^path://g; ; $a[0] => { "descr" => $a[1], "genes"=>[] } } split "\n",$res;
	sleep($delay);

	print STDERR "[INFO] retrieving list of all genes (http://rest.kegg.jp/list/$argument)...\n";
	$res = `wget -O - http://rest.kegg.jp/list/$argument $verbose`;
	my %genes = map { chomp; my @a=split("\t",$_); $a[0]=~s/^${argument}://g; ; $a[0] => {"descr" => $a[1]} } split "\n",$res;
	sleep($delay);

	print STDERR "[INFO] retrieving list of all genes for each pathway (https://www.genome.jp/entry/pathway+ID)...\n";
	foreach my $pw (sort keys %pathways) {

		print STDERR "[INFO] processing '$pw'...\n";

		$res = `wget -O - https://www.genome.jp/entry/pathway+$pw $verbose`;
		sleep($delay);

		if($res=~m/<nobr>Gene<\/nobr><\/th>\n([^\n]*)/g){
			$res=$1;

			while($res=~/\/entry\/[^:">]+:([^:">]+)/g){

				my $cur_gene=$1;
				if(defined $genes{$cur_gene}){
					push(@{$pathways{$pw}{"genes"}},$cur_gene);
				}else{
					print STDERR "[ERROR] something went wrong with the gene : '$cur_gene' !?\n";
					die;
				}
			}
		}else{
			print STDERR "[INFO] did not found any genes associated with this pathway ...\n";
		}
	}

	print "# pathway\tpathway description\tgene\tgene description\n";
	foreach my $pw (sort keys %pathways) {
		foreach my $gene (sort @{$pathways{$pw}{"genes"}}) {
			print "$pw\t".$pathways{$pw}{"descr"}."\t$gene\t".$genes{$gene}{"descr"}."\n";
		}
	}

}else{
	print STDERR "[ERROR] invalid operation '$operation'\n";
	exit(1);
}
print STDERR "[INFO] done\n";
