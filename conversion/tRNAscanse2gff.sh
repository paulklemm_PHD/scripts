#!/usr/bin/env bash
#pk

arg=$(echo "$1") # chomps newline
if [[ "$#" != 1 ]] || [[ "$arg" = "-h" ]] || [[ "$arg" = "-help" ]] || [[ "$arg" = "--help" ]]; then
    echo "Usage: tRNAscanse2gff.sh <tRNAscansePREDICTEDSTRUCTE.csv>"
    echo "Returns gff format. tRNAscanse reports - strand hits with reversed start stop indices."
    echo "  Input looks like this:
------
NZ_CP037857_1_Escherichia_coli_BW25113__complete_genome.trna1 (221868-221944)	Length: 77 bp
Type: Ile	Anticodon: GAT at 35-37 (221902-221904)	Score: 75.8
         *    |    *    |    *    |    *    |    *    |    *    |    *    |    * 
Seq: AGGCTTGTAGCTCAGGTGGTtAGAGCGCACCCCTGATAAGGGTGAGGtCGGTGGTTCAAGTCCACTCAGGCCTACCA
Str: >>>>>>>..>>>>.........<<<<.>>>>>.......<<<<<.....>>>>>.......<<<<<<<<<<<<....

(...)
------

  Output looks like this:
------
NC_013967.1	tRNAscan-SE	tRNA	11661	11733	.	+	.	Length=73; Type=Gln; Anticodon=CTGat35-37(11695-11697); Score=57.4; Seq=AGTCCCATGGGGTAGTGGCcaATCCTGTTGCCTTCTGGGGGCAACGACCCAGGTTCGAATCCTGGTGGGACTA; Str=(((((((..(((...........))).(((((.......)))))....(((((.......)))))))))))).
NC_013967.1	tRNAscan-SE	tRNA	121223	121296	.	+	.	Length=74; Type=Arg; Anticodon=TCTat35-37(121257-121259); Score=78.9; Seq=GGGCGCGTAGCTCAGTCGGAcAGAGCGTCGGACTTCTAATCCGATGGtCGCGGGTTCGAATCCCGTCGCGCTCG; Str=(((((((..((((.........)))).(((((.......))))).....(((((.......)))))))))))).
(...)
------

AUTHOR Paul Klemm
DEPENDENCIES sed, perl
"
    exit 1
fi

sed 's/^[ |*]*$//g' "$1" | sed '/^$/d' | sed 's/\t/ /g' | sed 's/ at /at/g' | sed 's/: /="/g'| sed 's/ bp//g' | sed -E 's/([^ ]*)/"; \1/g' | sed 's/ "/"/g' | tr -d '\n' | sed -E 's/; ([^\t ]*)\.trna[^\t ]*/;\n\1.trna/g' | sed -E 's/trna \(([0-9]+)-([0-9]+)\)"; L/trna\ttRNAscan-SE\ttRNA\t\1\t\2\t.\t+\t.\tL/g' | sed -E 's/"; \([^)]+\)//g' | sed -E 's/^([A-Za-z][A-Za-z])_?([^\t_ .]*)_?([0-9])[^.]*\.trna\t/\1_\2.\3\t/g' | sed 's/>/(/g' | sed 's/</)/g' | tail -n +2 | sed 's/";$/"/g'>.tRNAscanse2gff.tmp
echo "\"" >>.tRNAscanse2gff.tmp

#getGeneLength.pl $2>.tRNAscanse2gff.fasta_len.tmp

perl -lne '
$cl=$_; 
@a=split("\t",$cl); 
if(int($a[3])>int($a[4])){

	$t=$a[3];
	$a[3]=$a[4];
	$a[4]=$t;
	$id=$a[0];$id=~s/[_.]//g;
	$a[3]=$a[3];
	$a[4]=$a[4];
	$a[6]="-"; 

	#if($a[8]=~m/Seq="([A-Za-z]+)"/){
	#	$seq=reverse($1);
	#	$a[8]=~s/Seq="([A-Za-z]+)"/Seq="$seq"/;
	#} 

	#if($a[8]=~m/Pre="([\[\]A-Za-z]+)"/){
	#	$pre=reverse($1);$str=~tr/][/[]/; 
	#	$a[8]=~s/Pre="([\[\]A-Za-z]+)"/Pre="$pre"/;
	#} 

	#if($a[8]=~m/Str="([\(\).]+)"/){
	#	$str=reverse($1);$str=~tr/)(/()/; 
	#	$a[8]=~s/Str="([\(\).]+)"/Str="$str"/;
	#} 
}
print join("\t",@a)' .tRNAscanse2gff.tmp 

rm .tRNAscanse2gff.tmp
#rm .tRNAscanse2gff.fasta_len.tmp
