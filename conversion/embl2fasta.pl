#!/usr/bin/perl
#pk

use strict;
use warnings "all";

our $RED="\033[0;41m";
our $PURPLE="\033[0;45m";
our $YELLOW="\033[0;43m";
our $ORANGE="\033[1;33m";
our $NC="\033[0m"; # No Color
my $version="0.0.25"; # last updated on 2021-10-01 14:11:18

my $usage = "embl2fasta.pl        coverts EMBL to fasta format
 
SYNOPSIS
 
embl2fasta.pl (options) EMBLFILE 

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES -
";

if(scalar @ARGV != 0 && $ARGV[0]=~m/-version|-v$/){print "$0\tv$version\n";exit 0;}
if(scalar @ARGV == 0 || $ARGV[0]=~m/-h/){print $usage; exit 0;}

my $seq=0;
while(<>){
	chomp;
	if(/^\/\//){$seq=0}
	if($seq){
		$_=~s/[ 0-9]//g;
		if($_ eq ""){next}
		print "$_\n";
	}
	if(/^ID *([^ ]+)/){print ">$1\n";}
	elsif(/^SQ/){$seq=1}
}
