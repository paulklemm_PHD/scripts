#!/usr/bin/env perl
# pk

use strict;
use warnings "all";

# RUN : perl clustal2fasta.pl somealn.aln
# given an alignment in aln format, this script converts this into fasta format

our $RED="\033[0;41m";
our $PURPLE="\033[0;45m";
our $YELLOW="\033[0;43m";
our $ORANGE="\033[1;33m";
our $NC="\033[0m"; # No Color
my $version="0.0.9"; # last updated on 2021-10-01 14:11:18

my $usage = "clustal2fasta.pl        coverts alignments in clustalw to fasta format
 
SYNOPSIS
 
clustal2fasta.pl (options) ALIGNMENTFILE 

	ALIGNMENTFILE	clustaw format or - for STDIN

		e.g.:
		...
		Dolosicoccus_paucivorans__GCF_      ATGT-TATGGTATA-CTTATGATGCAA----------
		Lactobacillus_hayakitensis_DSM      CTAAT-ATGGTATT-ATAATTAT--------------
		Lactobacillus_salivarius__GCF_      ATGATTATGCTATG-ATTGATTT--------------
		Leuconostoc_garlicum__GCF_0019      ATTTTCGCGGTATA-ATAATTGATG------------
		...

	OUTPUT

		>Lactobacillus_salivarius__GCF_
		ATGATTATGCTATGATTGATTT---
		>Lactobacillus_hayakitensis_DSM
		CTAAT-ATGGTATTATAATTAT---
		>Dolosicoccus_paucivorans__GCF_
		ATGT-TATGGTATACTTAT-GATGC
		>Leuconostoc_garlicum__GCF_0019
		ATTTTCGCGGTATAATAATTGATG-

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES -
";

if(scalar @ARGV != 0 && $ARGV[0]=~m/-version|-v$/){print "$0\tv$version\n";exit 0;}
if(scalar @ARGV == 0 || $ARGV[0]=~m/-h/){print $usage; exit 0;}

my %data;

sub processLine{
	$_ = shift;
	chomp($_);
	my $entry=$_;
	$entry=~s/^[ \t]+//g;
	$entry=~s/[ \t]+\d+$//g;
	#print STDERR "_'$entry'\n";
	my @arr=split(/[ \t]+/,$entry);
	if( scalar(@arr) == 2 && $arr[0] !~ /^[.:*\|]+$/ && $arr[1] !~ /^[.:*\|]+$/ ){
		if(!exists($data{$arr[0]})){$data{$arr[0]}="";}
		$data{$arr[0]}.=$arr[1];
	}
}

if($ARGV[0] eq "-"){
	while(<>){ &processLine($_) }
}else{		
	open (FH,'<',$ARGV[0]);
	while(<FH>){ &processLine($_) }
	close(FH);
}

foreach my $key (keys %data){ print ">".$key."\n".$data{$key}."\n" }
