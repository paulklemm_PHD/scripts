#!/usr/bin/env python3
#pk
import argparse
import os
import re
import shutil
import subprocess
from pathlib import Path

VERSION = "0.0.12"  # last updated on 2024-08-06

def parse_size(size_str):
    """Convert a size string (e.g., 10M, 2G) to bytes."""
    size_str = size_str.upper()
    if size_str.endswith('G'):
        return int(size_str[:-1]) * 1024 ** 3
    elif size_str.endswith('M'):
        return int(size_str[:-1]) * 1024 ** 2
    elif size_str.endswith('K'):
        return int(size_str[:-1]) * 1024
    else:
        return int(size_str)

def parse_arguments():
    parser = argparse.ArgumentParser(description='ddrive script to push/pull files with various options.')
    parser.add_argument('modus', choices=['push', 'pull'], help='Mode of operation: push or pull')
    parser.add_argument('files', nargs='+', help='List of files to push or pull')
    parser.add_argument('--exclude-patterns', default=['git/','.DS.*','[.]_.*','[.]env.*','[.]svg'], nargs='+', help='List of patterns to exclude')
    parser.add_argument('--max-file-size', type=parse_size, default='200M', help='Maximum file size (e.g., 200M, 2G)')
    parser.add_argument('--max-dir-size', type=parse_size, default='200M', help='Maximum directory size (e.g., 200M, 2G)')
    parser.add_argument('--max-dir-files', type=int, default=100, help='Maximum number of files in a directory')
    return parser.parse_args()

def check_file_size(file_path, max_size):
    if not os.path.exists(file_path):
        return True
    return os.path.getsize(file_path) < max_size

def check_directory(dir_path, max_size, max_files):
    total_size = 0
    total_files = 0
    for root, dirs, files in os.walk(dir_path):
        total_files += len(files)
        for file in files:
            total_size += os.path.getsize(os.path.join(root, file))
    return total_size < max_size and total_files <= max_files

def check_exclude_patterns(file_path, patterns):
    for pattern in patterns:
        if re.match(pattern, file_path):
            return False
    return True

def main():
    args = parse_arguments()

    dest_dir = Path.home() / 'herkules'
    modus = args.modus
    files = args.files
    exclude_patterns = args.exclude_patterns
    max_file_size = args.max_file_size
    max_dir_size = args.max_dir_size
    max_dir_files = args.max_dir_files

    if modus == 'pull':
        comb = []
        for file in files:
            if file == ".":
                file_dir = Path(file).resolve()
                dest_file_dir = dest_dir / file_dir.relative_to('/')
                dest_file_dir.mkdir(parents=True, exist_ok=True)
                comb.append(str( dest_file_dir ))
            elif check_exclude_patterns(file, exclude_patterns) and check_file_size(file, max_file_size):
                file_dir = Path(file).resolve().parent
                dest_file_dir = dest_dir / file_dir.relative_to('/')
                dest_file_dir.mkdir(parents=True, exist_ok=True)
                comb.append(str(dest_file_dir / Path(file).name))
        
        subprocess.run(['drive', 'pull', '-no-prompt'] + comb)
        
        # for file in files:
        #     if check_exclude_patterns(file, exclude_patterns) and check_file_size(file, max_file_size):
        #         file_dir = Path(file).resolve().parent
        #         dest_file_dir = dest_dir / file_dir.relative_to('/')
        #         subprocess.run(['rsync', '-auhP', str(dest_file_dir / Path(file).name), '.', '--perms'])
    
    # print(files)

    for file in files:
        if file == "." or (check_exclude_patterns(file, exclude_patterns) and check_file_size(file, max_file_size)):
            
            file_dir = Path(file).resolve()
            if file != ".":
                file_dir = file_dir.parent
            dest_file_dir = dest_dir / file_dir.relative_to('/')
            dest_file_dir.mkdir(parents=True, exist_ok=True)
            if modus == 'push':
                # exit(1)
                subprocess.run(['rsync', '-auhP', file, str(dest_file_dir)])
            elif modus == 'pull':
                # exit(1)
                if file != ".":
                    subprocess.run(['rsync', '-auhP', str(dest_file_dir / Path(file).name), '.', '--perms'])
                else:
                    subprocess.run(['rsync', '-auhP', str( dest_file_dir ) , '..', '--perms'])
    

    if modus == 'push':
        comb = []
        for file in files:
            if file == ".":
                file_dir = Path(file).resolve()
                comb.append(str(dest_dir / file_dir.relative_to('/') / Path(file).name))
            elif check_exclude_patterns(file, exclude_patterns) and check_file_size(file, max_file_size):
                file_dir = Path(file).resolve().parent
                comb.append(str(dest_dir / file_dir.relative_to('/') / Path(file).name))
        # print(comb)
        # exit(1)
        if len(comb)>0:
            subprocess.run(['drive', 'push', '-no-prompt'] + comb)

    if os.path.exists(dest_dir):
        shutil.rmtree(dest_dir)

if __name__ == '__main__':
    main()
