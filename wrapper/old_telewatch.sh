#!/usr/bin/env bash
#
# USAGE : telewatch.sh
#

RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
NC='\033[0m' # No Color

version="0.0.25"; # last updated on 2024-12-15 14:35:02
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

export LC_ALL=C

USAGE () { 
    echo -e "${ORANGE}USAGE$NC : $0 (OPTIONS) QUERY"
    echo " QUERY    :  The input query, e.g. rmarkdown."
    echo ""
    echo "Needs the telegram_token= and telegram_chatid= environment set"
    echo ""
    echo -e "${ORANGE}OPTIONS$NC:"
    echo " -s <STRING> :  program to test for [default:ps aux]"
    echo " -p          :  pass message through (simple GET request)"
    echo " -w          :  wc -l the result and send it"
    echo " -o          :  terminate after one iteration"
    echo ""
    echo -e "${ORANGE}VERSION$NC $version"
    echo -e "${ORANGE}AUTHOR$NC Paul Klemm"

if [ "$telegram_token" == "" ]; then if [ "$telegram_chatid" == "" ]; then echo "missing telegram_chatid and telegram_token env (load a .env with 'export \$(cat .env | xargs)')..."; fi fi

}
if [ "$#" -lt 1 ]; then
    USAGE;
    exit
fi

searchprog="ps aux"
onetime=0
wcl=0
pass=0
while getopts ":owhps:" o; do
    case "${o}" in
        s)
            searchprog=$(echo $OPTARG | sed "s/^[-=]//")
            ;;
        o)
            onetime=1
            ;;
        w)
            wcl=1
            ;;
        p)
            pass=1
            ;;
        \? )
            USAGE;
            echo "Invalid Option: -$OPTARG" 1>&2
            exit 1
            ;;
        h )
            USAGE;
            exit 1
            ;;
		: )
            USAGE;
			echo "Invalid Option: -$OPTARG requires an argument" 1>&2
			exit 1
			;;
    esac
done
shift $((OPTIND-1))

if [ "$telegram_token" == "" ]; then if [ "$telegram_chatid" == "" ]; then echo "missing telegram_chatid and telegram_token env (load a .env with 'export \$(cat .env | xargs)')..."; exit 1; fi fi
if [ "$telegram_token" == "" ]; then echo "missing telegram_token env (load a .env with 'export \$(cat .env | xargs)')..."; exit 1; fi
if [ "$telegram_chatid" == "" ]; then echo "missing telegram_chatid env (load a .env with 'export \$(cat .env | xargs)')..."; exit 1; fi

input=$1;

msg () { 
    curl --get \
    --data "chat_id=${telegram_chatid}" \
    --data-urlencode "text=[$HOSTNAME@$USER] $1" \
    "https://api.telegram.org/bot${telegram_token}/sendMessage";
    echo "";
}
#    curl "https://api.telegram.org/bot${telegram_token}/sendMessage?chat_id=${telegram_chatid}&text='[$HOSTNAME@$USER]${1}'"; echo ""; }

if [ "$pass" == "1" ];then msg "$input"; exit 0; fi

if [ "$input" != "" ]; then
    if [ "$wcl" == "1" ]; then
        #msg "wcl $searchprog '$input'"; 
        last="";
        while [ 1 ]; do 
            cur=$($searchprog | grep -P "$input" | grep -v grep | grep -v $0 | wc -l); 
            if [ "$cur" != "$last" ]; then msg "$searchprog '$input' => $cur"; fi; 
            if [ "$onetime" == "1" ];then break; fi;
            last=$cur;
            sleep 10;
        done;
        #msg "exit $searchprog '$input'"; 
    else    
    	#msg "looking at $searchprog '$input'"; 
    	modus="";
    	while [ 1 ]; do 
    		cur=$($searchprog | grep -P "$input" | grep -v grep | grep -v $0 | head -n1); 
    		if [ "$cur" != "" ]; then if [ "$modus" != "ON" ]; then modus="ON"; msg "$searchprog '$input' is running"; fi; fi; 
    		if [ "$cur" == "" ]; then if [ "$modus" != "OFF" ]; then modus="OFF"; msg "$searchprog '$input' is done"; if [ "$onetime" == "1" ];then break; fi; fi; fi; 
    		sleep 10;
    	done;
    	#msg "exit $searchprog '$input'"; 
    fi;
fi;
