#!/usr/bin/env bash
#pk 

echo '# please install docker and bioconductor/bioconducter_docker container first (https://docs.docker.com/engine/install/ubuntu/)
# sudo docker pull bioconductor/bioconductor_docker:devel

# use run -d for background
# use -it for interactive session
# add bash at the end to start in a shell instead of starting /init script
';

echo 'echo "# visit >> localhost:8787 << with user=rstudio pw=bioconductor123#"';

if [[ "$OSTYPE" == "darwin"* ]]; then

if [ ! -d "/Users/$(id -un)/R.dockerlibs" ]; then
echo '# unable to detected R.dockerlibs directory...
';
echo "mkdir /Users/$(id -un)/R.dockerlibs;
";
fi

# --mount "type=bind,src=/Users/$(id -un)/R.dockerlibs,dst=/home/rstudio/R.dockerlibs"
echo '
docker run -p 127.0.0.1:8787:8787 --rm --mount "type=bind,src=/Users/$(id -un),dst=/home/$(id -un)" -e PASSWORD=bioconductor123# bioconductor/bioconductor_docker:devel bash -c "sudo echo \".libPaths(c(\\\"/home/rstudio/R.dockerlibs\\\",.libPaths()))\" >/home/rstudio/.Rprofile && chmod -R 777 /usr/local/lib/R && ln -s /home/$(id -un) /home/rstudio/$(id -un) && /init"';

else


if [ ! -d "/home/$(id -un)/R.dockerlibs" ]; then
echo '# unable to detected R.dockerlibs directory...
';
echo "mkdir /home/$(id -un)/R.dockerlibs;
";
fi

if [ ! -z "$HTTP_PROXY" ]; then

# 
echo '# detected proxy settings... 

docker run -p 127.0.0.1:8787:8787 --rm --mount "type=bind,src=/home/$(id -un),dst=/home/$(id -un)" -e PASSWORD=bioconductor123# bioconductor/bioconductor_docker:devel bash -c "sudo echo -e \"HTTP_PROXY=$HTTP_PROXY\nHTTPS_PROXY=$HTTP_PROXY\" >/home/rstudio/.Renviron && cat /home/rstudio/.Renviron && chmod -R 777 /usr/local/lib/R && sudo echo \".libPaths(c(\\\"/home/rstudio/R.dockerlibs\\\",.libPaths()))\" >/home/rstudio/.Rprofile && usermod -u $(id -u) rstudio && groupmod -g $(id -g) rstudio && ln -s /home/$(id -un) /home/rstudio/$(id -un) && su rstudio && /init"';
else 
echo 'docker run -p 127.0.0.1:8787:8787 --rm --mount "type=bind,src=/home/$(id -un),dst=/home/$(id -un)" -e PASSWORD=bioconductor123# bioconductor/bioconductor_docker:devel bash -c "sudo echo \".libPaths(c(\\\"/home/rstudio/R.dockerlibs\\\",.libPaths()))\" >/home/rstudio/.Rprofile && chmod -R 777 /usr/local/lib/R && usermod -u $(id -u) rstudio && groupmod -g $(id -g) rstudio  && ln -s /home/$(id -un) /home/rstudio/$(id -un) && su rstudio && /init"';	
fi

fi

echo '
# --mount "type=bind,src=/home/$(id -un),dst=/home/$(id -un)" 
# -> mounts the home directory to /home. Later a symbolic link is created in the home of rstudio pointing to this folder.

# --mount "type=bind,src=/home/$(id -un)/R.dockerlibs,dst=/home/rstudio/R.dockerlibs"
# -> mounts the R.docklibs, all libraries should be installed here automatically (install.packages...)

# usermod -u $(id -u) rstudio && groupmod -g $(id -g) rstudio
# -> this will fix permission issues with the share folders. The rstudio user has now the same UID as you

# su rstudio && /init
# -> starts the rstdio server, ...

# sudo docker ps
# sudo docker attach ID # if you run with -d and -it
# sudo docker stop ID

# start a bash of an existing/running docker container 
# sudo docker exec -it IDHERE bash

# Communicate the proxy configuration to docker
# cat /etc/systemd/system/docker.service.d/http-proxy.conf
# [Service]
# Environment="HTTP_PROXY=http://proxy.example.com:80/"
# ...
# sudo systemctl daemon-reload && sudo systemctl restart docker
#
'

