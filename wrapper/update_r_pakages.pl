#!/usr/bin/perl
#pk

use strict;
use warnings 'all';

#my @pkgs= split(" ","umap mclust Rcpp mixdist randomForest caTools dplyr stringr ggnewscale org.Hs.eg.db gridExtra ggalt tidygraph ggraph igraph doSNOW foreach doParallel optparse crosstalk DT plotly htmlwidgets UpSetR stringr lme4 scales purrr cowplot scater tidyverse cowplot Matrix.utils edgeR magrittr Matrix purrr reshape2 S4Vectors tibble pheatmap apeglm png RColorBrewer ggplot2 readr svglite pheatmap ggrepel ComplexHeatmap circlize");
my @pkgs= split(" ","");

while (scalar @pkgs >0) {

	print STDERR "-TODO: ".(scalar @pkgs)."\n".join(",",@pkgs)."\n";

	my $p = shift @pkgs;

	my $res = `echo "if(!require("$p")){install.packages('$p', repos='https://cloud.r-project.org/')}" | /usr/bin/R --vanilla 2>&1`;

	if($res=~/ERROR: failed to lock directory/){
		print STDERR "$res\n";
		die;
	}

	if($res=~/ERROR: lazy loading failed for package/){

		while($res=~/Error: package(.*)installed before.*please re-instal/g){
			my $g = $1;
			while( $g =~ /‘([^’]+)’/g ){
				unshift(@pkgs,$1);
			}
		}
		push(@pkgs,$p);

		@pkgs = do { my %seen; grep { !$seen{$_}++ } @pkgs };
	}
}
