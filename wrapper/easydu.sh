#!/usr/bin/env bash
#pk

version="0.0.18"; # last updated on 2024-11-13 13:19:56
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

export LC_ALL=C

USAGE () { 
    echo -e "${ORANGE}USAGE$NC : $0 (OPTIONS) [DIRECTORY]"
    echo ""
    echo "runs du and du --inodes and combines the output to a nice table. Just run this in a given directory and the output is printed to STDOUT as well as to a file called du.TIME with TIME= cureent timestamp"
    echo ""
    echo -e "${ORANGE}OPTIONS$NC:"
    echo " -d <INT> :  max depth [default:5]"
    #echo " -p :  plot output table"
    echo ""
    echo -e "${ORANGE}VERSION$NC $version"
    echo -e "${ORANGE}AUTHOR$NC Paul Klemm"
}

d="5"
plot_flag=0
while getopts ":owhs:p" o; do
    case "${o}" in
        d )
            d=$OPTARG
            ;;
        p )
            plot_flag=1
            ;;
        \? )
            USAGE;
            echo "Invalid Option: -$OPTARG" 1>&2
            exit 1
            ;;
        h )
            USAGE;
            exit 1
            ;;
		: )
            USAGE;
			echo "Invalid Option: -$OPTARG requires an argument" 1>&2
			exit 1
			;;
    esac
done
shift $((OPTIND-1))

directory="."
dir_name=""
if [ -z "$1" ]; then
    directory="."
else
    directory=$1
    dir_name="."$(sed -E 's/[^a-zA-Z_0-9]/_/g' <<< $directory)
fi

cleanup() {
  echo "Terminating background processes..."
  kill $size_pid $inode_pid 2>/dev/null
  rm "$size_file" "$inode_file" "du$dir_name.$timestamp.time" "du$dir_name.$timestamp" 2>/dev/null
  exit 1
}

trap cleanup SIGINT SIGTERM

timestamp=$(date +%Y%m%d_%H%M%S)
size_file=".tmp.du_size$dir_name.$timestamp"
inode_file=".tmp.du_inodes$dir_name.$timestamp"

time du -d $d $directory > $size_file 2>du$dir_name.$timestamp.time &
size_pid=$!
du --inodes -d $d $directory > $inode_file &
inode_pid=$!

wait $size_pid $inode_pid

awk -F'\t' 'BEGIN {OFS="\t"; print "path", "level", "size", "inodes"}
     NR==FNR {d[$2]=$1;next}
     {a=$2;print $2, gsub(/\//, "", a), $1, d[$2]}' $inode_file $size_file > du$dir_name.$timestamp
rm $inode_file $size_file

#time (awk 'BEGIN{OFS="\t";print "path","level","size","inodes"}NR==FNR{d[$2]=$1}NR!=FNR{print $2,gsub(/\//,$2),$1,d[$2]}' <(du -d 5 .) <(du --inodes -d 5 .) > du.$timestamp) 2>du.$timestamp.time

output_file=du$dir_name.$timestamp

echo ""
echo "# worst entries regarding size (in GB):"
sort -k3,3nr $output_file | awk -F'\t' 'BEGIN{OFS="\t"}{$3=$3/1e+6"GB"; print $0}' | head

echo ""
echo "# worst entries regarding inodes (in 1e+6):"
sort -k4,4nr $output_file | awk -F'\t' 'BEGIN{OFS="\t"}{$4=$4/1e+6"Mil"; print $0}' | head

echo ""
echo "# total:"
tail -n1 $output_file | awk -F'\t' 'BEGIN{OFS="\t"}{$3=$3/1e+6"GB";$4=$4/1e+6"Mil"; print $0}'

if [ $plot_flag -eq 1 ]; then
    pdu $directory
fi

echo ""
echo "done, saved output to $output_file"
