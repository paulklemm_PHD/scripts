#!/usr/bin/python3
# pk
import sys
import os
import urllib.request
import urllib.parse
import json

version = "0.3.8" # last updated on 2024-12-15 14:35:02

# Function to load environment variables from .env file
def load_env():
    env_path = os.path.join(os.path.dirname(__file__), '.env')
    if not os.path.exists(env_path):
        raise FileNotFoundError(f"No .env file found at {env_path}")
    with open(env_path) as f:
        for line in f:
            if line.strip() and not line.startswith('#'):
                key, value = line.strip().split('=', 1)
                os.environ[key] = value

if __name__ == "__main__":
    # Load environment variables
    load_env()

    # get the chat id: https://api.telegram.org/bot<YourBOTToken>/getUpdates
    # generate a .env with:
    # ---------------------
    # telegram_chatid=
    # telegram_token=
    # ---------------------

    token = os.environ['telegram_token']
    chat_id = int(os.environ['telegram_chatid'])

    message = sys.argv[1] if len(sys.argv) == 2 else ""
    if message == "":
        print("missing message")
        sys.exit(1)

    url = f"https://api.telegram.org/bot{token}/sendMessage"
    data = {
        "chat_id": chat_id,
        "text": message
    }
    
    # Encode the data for the POST request
    encoded_data = urllib.parse.urlencode(data).encode("utf-8")
    
    # Make the HTTP request
    try:
        with urllib.request.urlopen(url, data=encoded_data) as response:
            response_data = json.loads(response.read().decode("utf-8"))
            print(response_data)
    except Exception as e:
        print(f"Error: {e}")
