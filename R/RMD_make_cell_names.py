#!/usr/bin/python
#pk
import re
from sys import argv,exit
from optparse import OptionParser
version="0.0.17" # last updated on 2024-07-18 15:10:31

def sanit(name):
    name = re.sub(r'\W+', '_', name)
    return name

def process_rmd_file(file_path,number_chapter=False):
    with open(file_path, 'r') as file:
        lines = file.readlines()
    
    headers = {}
    header2int={}
    seen = {}
    modus="normal"
    
    for line in lines:
        line=line.rstrip()
        if modus == "normal" and (header_match := re.match(r'^(#{1,6})\s(.*)', line)):
            level, header = header_match.groups()
            level = len(level)
            if level == 1:
                headers={}
            header=sanit(header)

            if number_chapter:
                if level==1:
                    header2int={l:header2int[l] for l in header2int if l==1} # reset
                if level not in header2int:
                    header2int[level] = {}
                if header not in header2int[level]:
                    header2int[level][header] = len(header2int[level])+1
                header = f"{header2int[level][header]}_{header}"
            headers[int(level)]=header
            print(line)

        elif rcodegrp := re.match(r'^([`]{3}[{]r)([^},]*)(.+)', line):

            modus="r"
            
            # collect he current header path:
            cur_h = '_'.join([headers[k] for k in sorted(headers.keys())])
            if f"{cur_h}" in seen:
                it=0
                while f"{cur_h}_{it}" in seen:
                    it=it+1
                cur_h=f"{cur_h}_{it}"
            seen[cur_h]=1
            
            if cur_h != "":
                print(f'{rcodegrp.group(1)} {cur_h}{rcodegrp.group(3)}')
            else:
                print(line)
        elif re.match(r'^([`]{3})', line):
            print(line)
            modus="normal"
        else:
            print(line)
    
if __name__ == "__main__":
    parser = OptionParser(usage="usage: %prog [options] -i input.RMD >output.RMD")
    parser.add_option("-i", "--input", dest="input_path",
                      help="read data from INPUT", metavar="INPUT")
    parser.add_option("--number", default=False, action="store_true",help="add chapter/section numbers to output")
    
    (options, args) = parser.parse_args()
    if not options.input_path:
        parser.error("Input RMD file not given")

    process_rmd_file(options.input_path, options.number)
