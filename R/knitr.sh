#!/usr/bin/bash
#pk
version="0.0.3"; # last updated on 2024-07-18 15:34:32
if [ $# -lt 1 ]; then
    echo "Usage: knitr.sh <file.RMD> (more RMD files ...)"
    exit 1
fi
FILES=("$@")
for FILE in "${FILES[@]}"; do
    if [ ! -e "$FILE" ]; then continue; fi
    bn=$(sed -E 's/[.][^.]+$//g' <<< "$FILE");
    RMD_make_cell_names.py --number -i $FILE >$bn.tmp.Rmd && \
    Rscript -e "rmarkdown::render('$bn.tmp.Rmd')" && \
    mv $bn.tmp.html $bn.html && \
    rm $bn.tmp.Rmd;
done
