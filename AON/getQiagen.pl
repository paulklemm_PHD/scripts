#!/usr/bin/env perl
#pk

use List::Util qw/shuffle/;
my $version="0.0.26"; # last updated on 2022-02-18 17:43:59

my $usage = "Usage: getQiagen.pl    simple interface to the qiagen web API

SYNOPSIS
 
getQiagen.pl SEQUENCE/FASTA/-

    SEQUENCE    a single input sequence.
    FASTA       input fasta file with multiple sequences.
    -           read from STDIN (fasta format or headless list of sequences)

    Use Tm DNA for applications such as Chromatin ISH-PCR - Southern
    Use Tm RNA for applications such as RNA ISH - microarray - Northern

EXAMPLES

	\$ getQiagen.pl ACTACACACACAGGG
	
	[getQiagen.pl] loaded input sequence : ACTACACACACAGGG
	# sequence	rnaTm	hybrydizationScore	hybridizationLines	secondaryStructureScore	secondaryStructureLines
	ACTACACACACAGGG	36	11	CACAGGG:GGGACAC	11	ACTACACACACAGGG: (  (   (   )))

    The secondary structure of an oligonucleotide with a score below 20 is unlikely to be stable at room temperature. 
    Scores above 30 are likely to produce secondary structures that are stable at room temperature.

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES perl : List::Util qw/shuffle/ wget internet(:

SEE ALSO
    the geneglobe.qiagen API for optimizedoligo: 
        https://geneglobe.qiagen.com/oligotoolsapi/services/oligotools/optimizedoligo?secondaryStructure=true&sequence=SEQUENCE
    the geneglobe.qiagen API for tmprediction: 
        https://geneglobe.qiagen.com/oligotoolsapi/services/oligotools/tmprediction?sequence=SEQUENCE
";

# + = %2B
# only ACGT
# https://geneglobe.qiagen.com/oligotoolsapi/services/oligotools/tmprediction?sequence=AC%2BGTA%2BC%2BG

###########################
###########################
###########################

if(scalar @ARGV != 0 && $ARGV[0] =~ m/-version|-v$/){print "$0\tv$version\n";exit 0;}
if(scalar @ARGV == 0 || join(" ",@ARGV) =~ m/-help|-h/){print $usage; exit 0}

sub getQiagen{

    my $input=shift;
    my $name=shift;
    $input=~s/u/t/ig;
    $input=uc $input;
    my $org_input=$name ne "" ? $name : $input;
    $input =~ s/\+(.)/%2B$1/g;

    print STDERR "[getQiagen.pl] loaded input sequence : '$org_input'\n";


    my $send=join("",$input);
    my $lnaOpt_RAW = `wget -q -O - "https://geneglobe.qiagen.com/oligotoolsapi/services/oligotools/optimizedoligo?secondaryStructure=true&sequence=$send" 2>/dev/null`;

    my $hybridizationLines="";
    if($lnaOpt_RAW=~m/hybridizationLines":"([^\\]+)\\n[^\\]+\\n([^\\]+)/){$hybridizationLines=$1." :".$2;my $i = 0; $hybridizationLines=join "", grep { $i++ % 2 } split //, $hybridizationLines }
    my $secondaryStructureLines="";
    if($lnaOpt_RAW=~m/secondaryStructureLines":"[^\\]+\\n([^\\]+)\\n([^\\]+)/){$secondaryStructureLines=$1." :".$2; my $i = 0; $secondaryStructureLines=join "", grep { $i++ % 2 } split //, $secondaryStructureLines }

    my $hybrydizationScore=9999;
    my $max_hybrydizationScore = 20;
    if($lnaOpt_RAW=~m/"hybrydizationScore":"([^"]+)"/){$hybrydizationScore=$1;}
    if($max_hybrydizationScore < $hybrydizationScore){$max_hybrydizationScore=$hybrydizationScore;}
    my $max_secondaryStructureScore = 20;
    my $secondaryStructureScore=9999;
    if($lnaOpt_RAW=~m/"secondaryStructureScore":"([^"]+)"/){$secondaryStructureScore=$1;}
    if($max_secondaryStructureScore < $secondaryStructureScore){$max_secondaryStructureScore=$secondaryStructureScore;}

    my $tm_RAW = `wget -q -O - "https://geneglobe.qiagen.com/oligotoolsapi/services/oligotools/tmprediction?sequence=$send" 2>/dev/null`;

    my $rnaTm_opt = "-";
    my $dnaTm_opt = "-";
    if($tm_RAW=~m/"rnaTm":"([^ "]+) /){$rnaTm_opt=$1;}
    if($tm_RAW=~m/"dnaTm":"([^ "]+) /){$dnaTm_opt=$1;}
    my $hybrydizationScore_opt=0;
    my $secondaryStructureScore_opt=0;

    my $did_found_one_opt=0;

    print "$org_input\t$rnaTm_opt\t$dnaTm_opt\t$hybrydizationScore\t$hybridizationLines\t$secondaryStructureScore\t$secondaryStructureLines\n";
}

print "# sequence\trnaTm\tdnaTm\thybrydizationScore\thybridizationLines\tsecondaryStructureScore\tsecondaryStructureLines\n";

if(-e $ARGV[0]){
    # parse fasta format
    open(my $FH,$ARGV[0]);
    my $cur_header="";
    my $cur_fasta="";
    my $isFasta=0;
    while(<$FH>){
        chomp;
        if(/^>/){
            $isFasta=1;
            if($cur_fasta ne ""){&getQiagen($cur_fasta,$cur_header); sleep(1)} # sleep, since this is a API call
            $_=~s/^>//g;
            $cur_header=$_;$cur_fasta=""}
        else{
            $cur_fasta.=$_;
        }
        if(!$isFasta){&getQiagen($cur_fasta,""); sleep(1);$cur_fasta="";}
    }
    if($cur_fasta ne ""){&getQiagen($cur_fasta,$cur_header)}
    close($FH);
}elsif($ARGV[0] eq "-"){
    # parse fasta format
    my $cur_header="";
    my $cur_fasta="";
    my $isFasta=0;
    while(<>){
        chomp;
        if(/^>/){
            $isFasta=1;
            if($cur_fasta ne ""){&getQiagen($cur_fasta,$cur_header); sleep(1)} # sleep, since this is a API call
            $_=~s/^>//g;
            $cur_header=$_;$cur_fasta=""}
        else{
            $cur_fasta.=$_;
        }
        if(!$isFasta){&getQiagen($cur_fasta,""); sleep(1);$cur_fasta="";}
    }
    if($cur_fasta ne ""){&getQiagen($cur_fasta,$cur_header)}
}else{
    # sequence as input
    &getQiagen($ARGV[0],"")  
}
