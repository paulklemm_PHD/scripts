#!/usr/bin/env perl
#pk

use strict;
use warnings "all";

use List::Util qw/shuffle/;

my $version="0.0.35"; # last updated on 2022-02-18 12:51:44

my $restarts = 5 ; # the number of restarts (first 3 are used for single value greedy search, last 2 are used for brute force) 
my $input="";
my $help;
my $modify_all_positions=0; # keep adjacent positions unmodified
my $usage = "
greedyQiagenLnaOptimizer.pl        simple gradient descent algorithm for finding the optimal LNA modifications using the qiagen web API

SYNOPSIS

greedyQiagenLnaOptimizer.pl (options) RNA

	RNA sequence given as string (U are converted to T automatically)

	-k : set the number of restarts (default:5)
	-a : by default adjacent positions of a modification are always unmodified. This option disables this behaviour

DESCRIPTION

	Simple greedy optimization:

	for each -k restart do:
		load the input sequence with all position being unmarked

		while there is a unmarked position:
	
			add a modification to a random position that is unmodified and unmarked and mark this position
			send this to qiagen 
			if hybrydizationScore < 20 AND secondaryStructureScore < 20 AND rnaTm improves:
				save this modification
				unmark all unmodified positions

		report this optimum

EXAMPLES

    \$ perl greedyQiagenLnaOptimizer.pl 'GCACAAGAGUAGACU'
	
	[greedyQiagenLnaOptimizer.pl] loaded input sequence : GCACAAGAGTAGACT
	[greedyQiagenLnaOptimizer.pl] started with max_hybrydizationScore=20 and max_secondaryStructureScore=20
	# sequence	rnaTm	hybrydizationScore	hybridizationLines	secondaryStructureScore	secondaryStructureLines
	GCACAAGAGTAGACT	43	17	AGTAGACT:TCAGATGA	8	GCACAAGAGTAGACT:  ((    ))     
	+GC+AC+AA+GAGTAGACT	71	20	AGAGTAGACT:TCAGATGAGA	12	GCACAAGAGTAGACT:  (   (      ))
	+GC+AC+A+AG+AGT+AGACT	79	19	AGTAGACT:TCAGATGA	10	GCACAAGAGTAGACT:  ((    ))     
	G+C+A+C+A+AG+AGT+A+G+ACT	88	20	AGACT:TCAGA	14	GCACAAGAGTAGACT: (         )   
	GC+A+CA+AG+AGT+AGACT	81	19	AGTAGACT:TCAGATGA	14	GCACAAGAGTAGACT:  ((    ))    

	# First line (GCACAAGAGTAGACT) corresponds to the input sequence with rnaTm,... values
	# next lines show up to -k optimums for each restart 

    The secondary structure of an oligonucleotide with a score below 20 is unlikely to be stable at room temperature. 
    Scores above 30 are likely to produce secondary structures that are stable at room temperature.

SEE ALSO
	the geneglobe.qiagen API for optimizedoligo: 
		https://geneglobe.qiagen.com/oligotoolsapi/services/oligotools/optimizedoligo?secondaryStructure=true&sequence=SEQUENCE

	the geneglobe.qiagen API for tmprediction: 
		https://geneglobe.qiagen.com/oligotoolsapi/services/oligotools/tmprediction?sequence=SEQUENCE

	the geneglobe user iterface:
		https://geneglobe.qiagen.com/us/explore/tools/

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES perl : List::Util qw/shuffle/, perl : wget
";

foreach my $a (@ARGV) {
	if ($a =~ m/^--?(help?|h)/) {$help = 1;}
	elsif ($a =~ m/^--?(version?|v)/) {print "$0\tv$version\n";exit 0}
	elsif ($a =~ m/^--?k=([0-9]+)/) {$restarts = $1;}
	elsif ($a =~ m/^--?a/) {$modify_all_positions = 1;}
	else{
		$input=$a;
		chomp($input);
	}
}

if($help || $input eq ""){print $usage; exit 0}

# + = %2B
# only ACGT
# https://geneglobe.qiagen.com/oligotoolsapi/services/oligotools/tmprediction?sequence=AC%2BGTA%2BC%2BG

###########################
###########################
###########################

###########################
###########################
###########################

$input=~s/u/t/ig;
$input=uc $input;

my @input_arr=split(//,$input);

my %modify = (
    "A" => '%2BA',
    "C" => '%2BC',
    "G" => '%2BG',
    "U" => '%2BU',
    "T" => '%2BT'
);
my %decode = (
    '%2BA' => "+A",
    '%2BC' => "+C",
    '%2BG' => "+G",
    '%2BU' => "+U",
    '%2BT' => "+T"
);
my %demodify = (
    '%2BA' => "A",
    '%2BC' => "C",
    '%2BG' => "G",
    '%2BU' => "U",
    '%2BT' => "T"
);

print STDERR "[greedyQiagenLnaOptimizer.pl] loaded input sequence : $input\n";

my $send=join("",@input_arr);
my $lnaOpt_RAW = `wget -q -O - "https://geneglobe.qiagen.com/oligotoolsapi/services/oligotools/optimizedoligo?secondaryStructure=true&sequence=$send" 2>/dev/null`;

my $hybridizationLines="";
if($lnaOpt_RAW=~m/hybridizationLines":"([^\\]+)\\n[^\\]+\\n([^\\]+)/){$hybridizationLines=$1." :".$2;my $i = 0; $hybridizationLines=join "", grep { $i++ % 2 } split //, $hybridizationLines }
my $secondaryStructureLines="";
if($lnaOpt_RAW=~m/secondaryStructureLines":"[^\\]+\\n([^\\]+)\\n([^\\]+)/){$secondaryStructureLines=$1." :".$2; my $i = 0; $secondaryStructureLines=join "", grep { $i++ % 2 } split //, $secondaryStructureLines }

my $hybrydizationScore=9999;
my $max_hybrydizationScore = 20;
if($lnaOpt_RAW=~m/"hybrydizationScore":"([^"]+)"/){$hybrydizationScore=$1;}
if($max_hybrydizationScore < $hybrydizationScore){$max_hybrydizationScore=$hybrydizationScore;}
my $max_secondaryStructureScore = 20;
my $secondaryStructureScore=9999;
if($lnaOpt_RAW=~m/"secondaryStructureScore":"([^"]+)"/){$secondaryStructureScore=$1;}
if($max_secondaryStructureScore < $secondaryStructureScore){$max_secondaryStructureScore=$secondaryStructureScore;}

my $tm_RAW = `wget -q -O - "https://geneglobe.qiagen.com/oligotoolsapi/services/oligotools/tmprediction?sequence=$send" 2>/dev/null`;

my $rnaTm_opt = 999;
if($tm_RAW=~m/"rnaTm":"([^ "]+) /){$rnaTm_opt=$1;}
my $hybrydizationScore_opt=0;
my $secondaryStructureScore_opt=0;

print STDERR "[greedyQiagenLnaOptimizer.pl] started with max_hybrydizationScore=$max_hybrydizationScore and max_secondaryStructureScore=$max_secondaryStructureScore\n";

my $did_found_one_opt=0;

print "# sequence\trnaTm\thybrydizationScore\thybridizationLines\tsecondaryStructureScore\tsecondaryStructureLines\tmodifications\n";
print "$input\t$rnaTm_opt\t$hybrydizationScore\t$hybridizationLines\t$secondaryStructureScore\t$secondaryStructureLines\t0\n";

my %seen;

for (my $rep = 0; $rep < $restarts; $rep++) {
		
	my $rnaTm_opt_cur=$rnaTm_opt;
	my $hybrydizationScore_opt_cur=$hybrydizationScore_opt;
	my $secondaryStructureScore_opt_cur=$secondaryStructureScore_opt;
	my $hybridizationLines_cur=$hybridizationLines;
	my $secondaryStructureLines_cur=$secondaryStructureLines;
	my $resultingString_cur="";

	#print STDERR "[greedyQiagenLnaOptimizer.pl] restart #$rep\n";
	
	my @candidates = map { $_ } keys @input_arr; # array of potential positions that can be modified (test each position only once)

	# make a copy of the input array
	my @current_working_array=@input_arr;
	my $num_mods=0;

	while(scalar @candidates > 0){ # while there are positions that are either unmodified or are not tested yet -> do loop

		# select a random position out of candidates
		my $random=int rand @candidates;
		my $cur_pos = $candidates[$random]; # the actual position
		splice @candidates, $random, 1; # remove that position

		if($modify_all_positions == 0){
			# test if adjcent positions are allready modified (only need to check previous)
			if(($cur_pos>0 && exists $demodify{$current_working_array[$cur_pos-1]}) || ($cur_pos+1 < scalar @current_working_array && exists $demodify{$current_working_array[$cur_pos+1]} )){
				next; # ignore this position !
			}
		}

		# next modifify this position (this gets reversed if it does not improve the tm)
		$current_working_array[$cur_pos]=$modify{uc $current_working_array[$cur_pos]};

		# send data to oligotoolsapi
		$send=join("",@current_working_array);

		$lnaOpt_RAW = `wget -q -O - "https://geneglobe.qiagen.com/oligotoolsapi/services/oligotools/optimizedoligo?sequence=$send&sortOrder=0&minHybridization=0&maxHybridization=1000&secondaryStructure=true&selfOnly=false" 2>/dev/null`;

		# parse output
		$hybrydizationScore = 999;
		if($lnaOpt_RAW=~m/"hybrydizationScore":"([^"]+)"/){$hybrydizationScore=$1;}
		$secondaryStructureScore = 999;
		if($lnaOpt_RAW=~m/"secondaryStructureScore":"([^"]+)"/){$secondaryStructureScore=$1;}

		# if this modification is still below the 2 bounds

		if($hybrydizationScore <= $max_hybrydizationScore && $secondaryStructureScore <= $max_secondaryStructureScore ){

			# get the tm value

			my $tm_RAW = `wget -q -O - "https://geneglobe.qiagen.com/oligotoolsapi/services/oligotools/tmprediction?sequence=$send" 2>/dev/null`;

			my $rnaTm = 999;
			if($tm_RAW=~m/"rnaTm":"([^ "]+) /){$rnaTm=$1;}

			# if the tm is better -> save

			if($rnaTm>$rnaTm_opt_cur){

				$resultingString_cur=join("",@current_working_array);
				$resultingString_cur=~s/(%2B.)/$decode{$1}/g;
				$num_mods=$num_mods+1;

				#print STDERR "[greedyQiagenLnaOptimizer.pl] improved to $rnaTm ($resultingString) with hybrydizationScore=$hybrydizationScore and secondaryStructureScore=$secondaryStructureScore\n";
				#print STDERR "https://geneglobe.qiagen.com/oligotoolsapi/services/oligotools/tmprediction?sequence=$send\n";
				#print STDERR "https://geneglobe.qiagen.com/oligotoolsapi/services/oligotools/optimizedoligo?sequence=$send&sortOrder=0&minHybridization=0&maxHybridization=1000&secondaryStructure=true&selfOnly=false\n";
				
				$hybridizationLines="";
				if($lnaOpt_RAW=~m/hybridizationLines":"([^\\]+)\\n[^\\]+\\n([^\\]+)/){$hybridizationLines=$1." :".$2;my $i = 0; $hybridizationLines=join "", grep { $i++ % 2 } split //, $hybridizationLines }
				$secondaryStructureLines="";
				if($lnaOpt_RAW=~m/secondaryStructureLines":"[^\\]+\\n([^\\]+)\\n([^\\]+)/){$secondaryStructureLines=$1." :".$2; my $i = 0; $secondaryStructureLines=join "", grep { $i++ % 2 } split //, $secondaryStructureLines }

				$did_found_one_opt=1;

				$rnaTm_opt_cur=$rnaTm;
				$hybrydizationScore_opt_cur=$hybrydizationScore;
				$secondaryStructureScore_opt_cur=$secondaryStructureScore;

				$hybridizationLines_cur=$hybridizationLines;
				$secondaryStructureLines_cur=$secondaryStructureLines;

				#print STDERR "$resultingString_cur\t$rnaTm_opt_cur\t$hybrydizationScore_opt_cur\t$hybridizationLines_cur\t$secondaryStructureScore_opt_cur\t$secondaryStructureLines_cur\n";

				# here refresh the candidates to those that have no modifications (but other positions are allowed again)
				@candidates = map { $_ } grep { !exists $decode{$current_working_array[$_]} } keys @current_working_array;

				#print STDERR join(",",@candidates)."\n";

			}else{ $current_working_array[$cur_pos]=$demodify{$current_working_array[$cur_pos]};}
		}else{ $current_working_array[$cur_pos]=$demodify{$current_working_array[$cur_pos]};}
	}

	if(!exists $seen{$resultingString_cur}){
		print "$resultingString_cur\t$rnaTm_opt_cur\t$hybrydizationScore_opt_cur\t$hybridizationLines_cur\t$secondaryStructureScore_opt_cur\t$secondaryStructureLines_cur\t$num_mods\n";
	}

	$seen{$resultingString_cur}=undef;
}
