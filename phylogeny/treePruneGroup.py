#!/usr/bin/python3
#pk
from ete3 import Tree
from optparse import OptionParser
import csv, sys, re

version="0.0.15" # last updated on 2022-07-07 16:39:06

def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)

if __name__=="__main__":
	parser = OptionParser(version=version,usage="usage: %prog [options] -g FILE -t FILE\n the output collapses the leave groups and scales the nodes according to the maximal distance of the grouped leaves to the corresponding LCA of the group. The branch that contains the group gets adjusted by the maximal depth of the collapsed group (disable this with --noadjust).")
	parser.add_option("-g", "--group", 
		help="the group file contains lists of leaves (linewise) separated by an either an empty line (automatically assign numbers to groups) or a group name starting with '# '. Or you can give a tsv file with leave name and group id as the second column.", metavar="FILE")
	parser.add_option("-t", "--tree", 
		help="tree input", metavar="FILE")
	parser.add_option("--oformat", default=1, metavar="INT",type="int",
		help="output newick format, see here: http://etetoolkit.org/docs/latest/tutorial/tutorial_trees.html#reading-and-writing-newick-trees")
	parser.add_option("--iformat", default=1, metavar="INT",type="int",
		help="input newick format, see here: http://etetoolkit.org/docs/latest/tutorial/tutorial_trees.html#reading-and-writing-newick-trees")
	parser.add_option("--noadjust", action="store_true", default=False,
		help="disables the branch adjustment")

	(options, args) = parser.parse_args()

	if options.group is None:
		parser.error("--group is missing")
	if options.tree is None:
		parser.error("--tree is missing")

	eprint(f"processing --group...")

	f=open(options.group, "r")
	groups={}
	cur_group=0
	parse_modus="?"
	known_names=[]
	for line in f.read().split("\n"):

		spl=line.split("\t")
		if len(spl)==2:
			parse_modus="tsv"
			#eprint(f"{spl[0]} -> {spl[1]}")
			if spl[1] not in groups:
				groups[spl[1]]=[]
			groups[spl[1]]+=[ spl[0] ]
			known_names+=[ spl[0] ]
			continue

		if parse_modus=="tsv" and line == "":
			continue

		if parse_modus=="tsv":
			parser.error(f"for a tsv input as --group, the line '{line}' is invalid")

		m=re.search("# (.+)",line)
		if (parse_modus == "int" and m) or (parse_modus == "name" and line == ""):
			parser.error(f"please use either empty lines to separate groups OR names (starting with '# ') but not both\nfirst invalid line is {line}")

		if parse_modus != "int" and m:
			cur_group=m.group(0)
			if cur_group in groups:
				parser.error(f"found multiple occurences of the group id {cur_group}...")
			groups[cur_group]=[]
			parse_modus="name"
		elif parse_modus != "int" and line == "":
			parse_modus="int"
			cur_group+=1
			groups[cur_group]=[]
		else:
			line=re.sub(" ","_",line)
			groups[cur_group] += [line]
			known_names += [line]

	f.close()
	
	eprint(f"loaded {len(groups)} groups")
	
	eprint(f"processing --tree...")
	# load main tree
	t = Tree(options.tree,format=options.iformat)

	collapse_labels=[]

	print(t.write(format=options.oformat))

	for node in t.traverse():
		if node.is_leaf() and node.name not in known_names:
			collapse_labels += [node.name]

	for cur_group in groups:
		if len(groups[cur_group])==1:
			continue
		collapse_labels+=[cur_group]
		v=t.get_common_ancestor(groups[cur_group])
		v.name = f"{cur_group}"
		if not options.noadjust:
			subtree=Tree(v.write())
			farthest, dist = subtree.get_common_ancestor(groups[cur_group]).get_farthest_node()
			v.dist += dist

	eprint(collapse_labels)

	def collapsed_leaf(node):
		return True if node.name in [f"{i}" for i in collapse_labels] else False

	node2labels = t.get_cached_content(store_attr="name")

	print(t.write(is_leaf_fn=collapsed_leaf,format=options.oformat))
