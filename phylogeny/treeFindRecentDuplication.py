#!/usr/bin/python3
#pk
from ete3 import Tree
from optparse import OptionParser
import csv, sys, re

version="0.0.21" # last updated on 2022-07-14 08:23:10

if __name__=="__main__":
	parser = OptionParser(version=version,usage="usage: %prog [options] -t FILE -m MODUS\nfinds recent duplication and extracts corresponding groups of leaves")
	parser.add_option("--checkspeciesnames", action="store_true", default=False,
		help="if names encode the species, check if they are identical (up to a suffix number e.g. Zea_mays-8 = Zea_mays-10)")
	parser.add_option("-t","--tree", type="string",
		help="input newick file", metavar="FILE")
	parser.add_option("--iformat", metavar="INT", default=1, type="int",
		help="input tree format, see http://etetoolkit.org/docs/latest/tutorial/tutorial_trees.html#reading-and-writing-newick-trees")

	(options, args) = parser.parse_args()

	if options.tree is None:
		parser.error("--tree is missing")

	# load main tree
	t = Tree(options.tree,format=options.iformat)

	if not hasattr(t, 'D'):
		parser.error("--tree is missing the duplication tag (NHX format)")

	# check all leaves
	# go to parent -> if duplication event -> take all leaves of that given node => one group
	# in case first we look at c ((a,b)D=Y,c)D=Y and then at a anb b -> only take the largest group
	# check for all leaves of the parent if the path also only contains duplication events ! (*)
	# do not want: ((a,b)D=N,c)D=Y -> without (*) => for c then all a,b,c would be collected  

	node2group={}
	cur_id=0
	for v in t.traverse():
		if v.is_leaf():
			cur_id+=1
			parent=v.up
			if parent.D == "Y":
				siblings = parent.get_leaves()
				species_name = re.sub("-?[0-9]+$","",v.name)
				
				# check if only duplication events are on the way to the parent
				is_valid_node=1
				
				for w in siblings:
					if options.checkspeciesnames and re.sub("-?[0-9]+$","",w.name) != species_name:
						is_valid_node=0
					cur_w = w.up
					while cur_w.up and cur_w is not parent:
						is_valid_node = is_valid_node and cur_w.D == "Y"
						cur_w=cur_w.up

				if not is_valid_node:
					continue

				cur_size = len(siblings)
				for w in siblings:
					if w.name not in node2group or node2group[w.name]["size"] < cur_size:
						node2group[w.name]={"id":cur_id,"size":cur_size}

	for v in node2group.keys():
		print(f"{v}\t{node2group[v]['id']}\t{node2group[v]['size']}")
