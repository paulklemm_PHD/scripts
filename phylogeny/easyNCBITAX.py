#!/usr/bin/python3
#pk
from ete3 import Tree
from optparse import OptionParser
import csv, sys, os, tarfile, re
# import json
import urllib.request
from os.path import exists

version="0.3.10" # last updated on 2024-07-24 19:31:38

def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)

def extract(fname):
	tar = tarfile.open(fname, "r:gz")
	tar.extractall("new_taxdump")
	tar.close()

if __name__=="__main__":
	parser = OptionParser(version=version,usage="usage: %prog [options] -l FILE\nautomatically downloads all files needed to extract the pruned subtree that contains all given leaves using the ete3 framework\nEXAMPLE coarse bacteria species tree: --tax Bacteria --depth 2 --minspecies 10 --noleaves --nononame --nosp --nocandidatus")
	parser.add_option("-l", "--leaves", 
		help="file with linewise species names (e.g. 'Ustilago maydis') or ncbi ids (e.g. '4529' for Oryza rufipogon), the immediate pruned subtree at the LCA/mrca of the input set is returned", metavar="FILE")
	parser.add_option("-t", "--tax", 
		help="a taxonomic name (e.g. 'Lactobacillaceae'), the immediate pruned subtree of this node is returned")
	parser.add_option("--up", default=0, metavar="INT", type="int",
		help="go up a levels from the point of LCA")
	parser.add_option("--minspecies", default=0, metavar="INT", type="int",
		help="minimum number of (leave) species for any internal node (use in combination with --depth). Any branches with less than the specified leaves are aggregated to 'others', you may add --depthAddLeafNum to count those")
	parser.add_option("--resolve", action="store_true", default=False,
		help="resolves polytomies randomly")
	parser.add_option("--noprune", action="store_true", default=False,
		help="disables pruning, i.e. the output includes ALL nodes below the LCA/mrca of the input leaves. WARNING this usually results in a massive output")
	parser.add_option("--withleaveid", action="store_true", default=False,
		help="include the taxid for leaves")	
	parser.add_option("--withinnerid", action="store_true", default=False,
		help="include the taxid for inner nodes")
	parser.add_option("--withid", action="store_true", default=False,
		help="include the taxid for inner nodes and leaves")
	parser.add_option("--format", default=8, metavar="INT", type="int",
		help="output format, see here: http://etetoolkit.org/docs/latest/tutorial/tutorial_trees.html#reading-and-writing-newick-trees")
	parser.add_option("-d","--depth", default=0, metavar="INT", type="int",
		help="prune to a depth relative to the root (0=disabled,1=root and direct children, ...)")
	parser.add_option("--depthAddLeaf", action="store_true", default=False,
		help="adds leave names to depth trimmed tree (node_leaf1|leaf2|...)")
	parser.add_option("--depthAddLeafNum", action="store_true", default=False,
		help="adds leave names to depth trimmed tree (nodeX_210/500)")
#	parser.add_option("--addSpeciesNum", action="store_true", default=False,
#		help="adds number of species to trimmed tree (nodeX_500)")
	parser.add_option("--force", action="store_true", default=False,
		help="disables santiy checks")
	parser.add_option("--nosubspecies", action="store_true", default=False,
		help="removes any subspecies form the tree (usefull for --depthAddLeafNum)")
	parser.add_option("--noleaves", action="store_true", default=False,
		help="removes any leaves form the tree (usefull for --tax)")
	parser.add_option("--nocandidatus", action="store_true", default=False,
		help="remove candidatus and candidate")
	parser.add_option("--returnLeaves", action="store_true", default=False,
		help="return leaves as a list")
	parser.add_option("--nosp", action="store_true", default=False,
		help="remove sp and spp")
	parser.add_option("--nononame", action="store_true", default=False,
		help="remove NoName")
	parser.add_option("--tsv",
		help="outputs the --depthAddLeafNum or depthAddLeaf as a table", metavar="FILE")

	(options, args) = parser.parse_args()

	if (options.leaves is None and options.tax is None) or (options.leaves is not None and options.tax is not None):
		parser.error("--leaves OR --ncbi OR --tax is required")

	if options.leaves is not None:
		eprint(f"[WARNING] names are not unique and can lead to ambiguties consider to use --ncbi if possible...")
		f=open(options.leaves, "r")
		leaves = list(set([ i for i in f.read().split("\n") if i != "" ]))
		eprint(f"loaded {len(leaves)} unique entries from --leaves {options.leaves}")

	elif options.tax is not None:
		options.tax = options.tax.rstrip()

	if not os.path.isdir(f"new_taxdump"):
		eprint(f"downloading new_taxdump.tar.gz...")
		url=f"ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/new_taxdump/new_taxdump.tar.gz"
		urllib.request.urlretrieve(url, f"new_taxdump.tar.gz")
		extract(f"new_taxdump.tar.gz")
		os.remove(f"new_taxdump.tar.gz")  

	# load main tree
	building_tax_tree=False

	if exists(f"new_taxdump/taxtree.tre"):
		eprint(f"reading new_taxdump/taxtree.tre...")
		t = Tree(f"new_taxdump/taxtree.tre",format=8)
	else:
		t = Tree()
		building_tax_tree=True
		eprint(f"I have to build the taxonomic tree, this takes a while but I will save the output to new_taxdump/taxtree.tre to save time in the future...")

	# check if nodes are found in tree -> if not take parent ncbitax
	# there are ncbitax ids that are unplaced and are not marked as such !
	all_valid_treenodesll=set([v.name for v in t.traverse()])

	eprint(f"processing new_taxdump/names.dmp...")

	# mapping ncbitax id -> species name
	id2outputname = {}
	is_done_leave = []
	subspecies = []
	data={}
	isfirst=True
	with open(f"new_taxdump/names.dmp", newline='\n') as csvfile:
		reader = csv.reader(csvfile, delimiter='\t', quotechar='#')
		for row in reader:
			row=row[::2] # skip every second entry (table separator is '\t|\t' ...)
			# 0: ncbitax id, 2:species name, ...
			if row[0] not in id2outputname:
				id2outputname[row[0]]= [row[0]]
			if row[3] == "scientific name":
				id2outputname[row[0]] = [row[1]] + id2outputname[row[0]] # push front
			else:
				id2outputname[row[0]] += [row[1]] # push back
			#print(f"{row[0]} -> {id2outputname[row[0]]}")

	if options.nosubspecies:
		with open(f"new_taxdump/nodes.dmp", newline='\n') as csvfile: 
			reader = csv.reader(csvfile, delimiter='\t', quotechar='#')
			for row in reader:
				row=row[::2] # skip every second entry (table separator is '\t|\t' ...)
				if row[3] in ["strain","subspecies","varietas","species subgroup"]:
					subspecies+=[row[0]]

	if building_tax_tree:
		eprint(f"processing new_taxdump/nodes.dmp...")

		with open(f"new_taxdump/nodes.dmp", newline='\n') as csvfile: 
			reader = csv.reader(csvfile, delimiter='\t', quotechar='#')
			for row in reader:
				row=row[::2] # skip every second entry (table separator is '\t|\t' ...)
				# 0: ncbitax id, 2:species name, ...
				if row[0] == "uid":
					continue
				if row[3] == "no rank - terminal": # SKIP for names: no rank - terminal ... usually weird labels 
					continue
				#if row[3] == "no rank": # SKIP for names: no rank
				#	continue

				if row[0]==row[1]:
					# roooooot
					x=Tree()
					v=x.add_child(name=row[0])
					data[row[0]]=v
				elif (row[1] not in data) and (row[0] not in data):
					# parent and current node are new, create a new tree combining both
					x=Tree()
					v=x.add_child(name=row[1])
					data[row[1]]=v
					w=v.add_child(name=row[0])
					data[row[0]]=w
				elif (row[1] in data) and (row[0] in data):
					# both parent and the current node exists, concat them
					v=data[row[1]].add_child(data[row[0]])
					data[row[0]]=v
				elif row[1] in data:
					# parent exists but the current node not, so just add it to the tree
					v=data[row[1]].add_child(name=row[0])
					data[row[0]]=v
				else:
					# current tree exists but the parent is new, create a new tree with the parent node and add the current tree as a leaf
					x=Tree()
					v=x.add_child(name=row[1])
					data[row[1]]=v
					data[row[0]]=v.add_child(data[row[0]])

		t = data["1"] # 1 is the universal root of the ncbi tax tree

		f = open(f"new_taxdump/taxtree.tre", "w")
		f.write(t.write(format=8))
		f.close()

	for x in t.traverse():
		x.add_features(id=x.name)

	tax_node=""
	found_id=[]
	found_names=[]
	for x in t.traverse():
		if options.tax is not None and (x.name == options.tax or (x.name in id2outputname and options.tax in id2outputname[x.name])):
			tax_node = x.name
			break
		elif options.leaves is not None and x.name in id2outputname and (x.name in leaves or (len([x for x in id2outputname[x.name] if x in leaves])>0)):
			if x.name in leaves:
				# if searching with taxids -> just take first name
				id2outputname[ x.name ] =  [ id2outputname[x.name][0] , x.name ]
			else:
				# if searching with names -> take the one name that is actually found
				id2outputname[ x.name ] = [ x for x in id2outputname[x.name] if x in leaves ] + [ x.name ]
			found_id += [ x ]
			found_names += id2outputname[ x.name ]

	if options.tax is not None:
		search_node=t.search_nodes(name=tax_node)
		if len(search_node) == 0:
			eprint(f"Found unexpected number of tax name (number of matches={len(search_node)} != 1).")
			exit(1)

		q = search_node[0]
		if options.up>0:
			for u in range(options.up):
				q=q.up
		t = q

	elif options.leaves is not None:
		if not options.force and len(found_id) != len(leaves):
			eprint(f"ERROR: I did find {len(found_id)} entries but the input contains {len(leaves)}")
			if len([ l for l in leaves if l not in found_names]):
				eprint("input entries that are not found:",[ l for l in leaves if l not in found_names ] )
			#if len([ l for l in found_names if l not in leaves]):
			#	eprint("leaves that are not in the input:",[ l for l in found_names if l not in leaves] )
			eprint("please look at the nodes.dmp and names.dmp if your query is present or not... Or use --force to proceed anyway.")
			exit(1)
		else:
			eprint(f"found {len(found_id)} entries in {len(leaves)} leaves")

		eprint(f"searching for the LCA/mrca...")
		q = t.get_common_ancestor(found_id) # the LCA node contains the immediate subtree of that node

		if options.up>0:
			for u in range(options.up):
				q=q.up
		t=q

		if not options.noprune:
			eprint(f"pruning...")
			t.prune(found_id) # returns void, well ok...
	
	if options.resolve:
		eprint(f"resolving polytomies randomly...")
		t.resolve_polytomy(recursive=True)
	
	if options.nosubspecies:
		eprint(f"removing subspecies...")
		t.prune([x for x in t.traverse() if x.name not in subspecies])

	if options.nononame:
		eprint(f"removing NoName...")
		t.prune([x for x in t.traverse() if x.name != "" and x.name != "NoName"])

	if options.nocandidatus:
		eprint(f"removing Candidatus...")
		t.prune([x for x in t.traverse() if not re.match("[Cc]andidat(us|e)",x.name)])

	if options.nosp:
		eprint(f"removing sp and ssp...")
		t.prune([x for x in t.traverse() if not re.match("[sS]?[sS]p\\.",x.name)])

	eprint(f"relabeling nodes/adding features...")
	if options.minspecies>0 or options.depthAddLeaf or options.depthAddLeafNum:
		for node in t.traverse():
			node.add_features(score=0,total=0,leaves="",percent=0)
			if not node.is_leaf():
				if options.leaves is not None:
					score=len([x for x in node.traverse() if x in found_id])
				else:
					score=0
				total=len(node.get_leaves())
				node.add_features(score=score,total=total,percent=score/total if total>0 else 0)
				if options.depthAddLeaf:
					node.add_features(leaves="|".join([v.name for v in node.traverse() if v.name != ""]))

	if options.noleaves:
		eprint(f"removing leaves...")
		t.prune([x for x in t.traverse() if not x.is_leaf()])

	for node in t.traverse():
		if len(node.name) == 0:
			continue
		if re.match("mrca",node.name):
			node.name=""
			continue

		new_label=""
		if node.name in id2outputname:
			if not node.is_leaf() and (options.withinnerid or options.withid) and len(id2outputname[node.name])>1:
				new_label = id2outputname[node.name][0]+"_"+node.name
			if node.is_leaf() and (options.withleaveid or options.withid) and len(id2outputname[node.name])>1:
				new_label = id2outputname[node.name][0]+"_"+node.name
			if new_label == "":
				new_label = id2outputname[node.name][0]
		new_label=re.sub("[,()]",".",new_label) # sanity clean

		#eprint([node.name, new_label])

		node.name = new_label if len(new_label)>0 else ""

	if options.depth>0:
		eprint(f"depth pruning...")
		Q=[t]
		Q_new=[]
		Q_leaves=[]
		for i in range(options.depth): # search deeper into the tree until depth cutoff is satisfied
			for node in Q:
				if not node.is_leaf():
					Q_new += node.get_children()
				elif node.is_leaf() and node in found_id:
					Q_leaves += [node] # otherwise those would vanish
				elif node.is_leaf():
					Q_leaves += [node.up]
			Q=list(set(Q_new))
			Q_new=[]					
		Q=list(set(Q+Q_leaves))

#		if options.leaves is not None:
#			Q=[x for x in Q if x == t or (x.name in found_names)]

		t.prune(Q)
		
	if options.minspecies>0:
		eprint(f"minspecies pruning...")
		remove_nodes=[]
		for node in t.traverse('levelorder'): 
			if node not in remove_nodes and node.total <= options.minspecies:
				upnodes=[x for x in node.up.get_children() if x.name == "other" and x not in remove_nodes]
				if len(upnodes)==0:
					#eprint(["new other",node, node.up])
					other_node=node.up.add_child(name="other")
					other_node.add_features(total=node.total)
				else:
					#eprint(["found agg other",node, node.up])
					other_node=upnodes[0]
					other_node.total+=node.total
				if len(upnodes)>1:
					eprint(["wtf", node, node.up])
					exit(1)
				for w in node.traverse():
					remove_nodes+=[w]
		
		Q=[x for x in t.traverse('levelorder') if x not in remove_nodes]

		t.prune(Q)

	if options.nononame:
		t.prune([x for x in t.traverse() if x.name != "" and x.name != "NoName"])

	if options.returnLeaves:
		for node in t.traverse('levelorder'): 
			if len(node)==1:
				print(f"{node.id}\t{node.name}")

	elif options.depthAddLeafNum:
		if options.tsv is not None:
			with open(options.tsv, 'w') as out:
				for node in t.traverse():
					out.write(("\t".join([node.name,f"{node.score}",f"{node.total}",f"{node.percent}"] if options.leaves is not None else [node.name,f"{node.total}"]))+"\n")
		
		print(t.write(format=options.format,features=["score","total","percent"] if options.leaves is not None else ["total"]))
	elif options.depthAddLeaf:
		if options.tsv is not None:
			with open(options.tsv, 'w') as out:
				for node in t.traverse():
					out.write(node.name+'\t'+";".join(node.leaves)+"\n")
		
		print(t.write(format=options.format,features=["leaves"]))
	else:
		print(t.write(format=options.format))

	#eprint(t)
	eprint(f"done, all good...")
