#pk
import argparse
from ete3 import Tree

def root_by_id(tree, node_id):
    node = tree.search_nodes(name=node_id)
    if node:
        tree.set_outgroup(node[0])
        return tree
    else:
        raise ValueError(f"Node with ID '{node_id}' not found in the tree.")

def root_by_midpoint(tree):
    midpoint = tree.get_midpoint_outgroup()
    tree.set_outgroup(midpoint)
    return tree

def root_by_furthest(tree):
    root, dist = tree.get_farthest_node()
    tree.set_outgroup(root)
    return tree

def main():
    parser = argparse.ArgumentParser(description='Root a Newick tree based on a given method.')
    parser.add_argument('input_file', help='Input Newick tree file')
    #parser.add_argument('output_file', help='Output file for rooted tree')
    
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('--id', help='ID of the node to root the tree by')
    group.add_argument('--midpoint', action='store_true', help='Root the tree by the midpoint')
    group.add_argument('--furthest', action='store_true', help='Root the tree by the node that is the furthest from all other nodes')
    
    args = parser.parse_args()
    
    tree = Tree(args.input_file, format=1)

    if args.id:
        tree = root_by_id(tree, args.id)
    elif args.midpoint:
        tree = root_by_midpoint(tree)
    elif args.furthest:
        tree = root_by_furthest(tree)

    print(tree.write(format=1))
    
if __name__ == "__main__":
    main()
