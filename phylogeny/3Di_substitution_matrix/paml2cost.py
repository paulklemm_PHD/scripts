#pk
import numpy as np
import argparse

def load_lower_triangle_paml(filename):
    lower_triangle = []
    frequencies = []
    was_empty_line=False
    with open(filename, 'r') as file:
        for line in file:
            line=line.rstrip()
            if line == "":
                was_empty_line=True
                continue
            if was_empty_line:
                frequencies=[float(x) for x in line.split() if x != ""]
            else:
                lower_triangle.append([float(x) for x in line.split() if x != ""])
    
    size = len(lower_triangle)
    for i in range(size):
        lower_triangle[i] = lower_triangle[i]+[frequencies[i+1]]
    lower_triangle=[[frequencies[0]]] + lower_triangle
    size = len(lower_triangle)
    exchange_rates = np.zeros((size, size))

    for i in range(size):
        for j in range(i + 1):
            exchange_rates[i, j] = lower_triangle[i][j]
            exchange_rates[j, i] = lower_triangle[i][j]

    return exchange_rates,frequencies

def compute_substitution_matrix(exchange_rates, frequencies):
    subst_matrix = np.zeros_like(exchange_rates)
    
    for i in range(len(exchange_rates)):
        for j in range(len(exchange_rates)):
            if i == j:
                subst_matrix[i][j] = -np.log(frequencies[i])  # Diagonal (self substitution)
            else:
                subst_matrix[i][j] = -np.log(exchange_rates[i][j] * frequencies[j])
    
    return subst_matrix

def output_matrix(matrix, amino_acids, output_file):
    with open(output_file, 'w') as file:
        file.write(f"{len(amino_acids)}\n")
        for i, aa in enumerate(amino_acids):
            row = '\t'.join(f"{round(score)}" for score in matrix[i]) + '\n'
            file.write(row)

# Main function to handle arguments and execution
def main():
    parser = argparse.ArgumentParser(description="Convert PAML amino acid exchange rates and frequencies to a substitution matrix (like BLOSUM).")
    parser.add_argument('paml_file', type=str, help="Path to the PAML file containing amino acid exchange rates and frequencies.")
    parser.add_argument('output_file', type=str, help="Path to the output file where the substitution matrix will be saved.")
    parser.add_argument('--amino_acids', type=str, default='ARNDCQEGHILKMFPSTWYV',  help="String representing the order of amino acids (default: 'ARNDCQEGHILKMFPSTWYV').")
    
    args = parser.parse_args()
    
    amino_acids = list(args.amino_acids)
    exchange_rates, frequencies = load_lower_triangle_paml(args.paml_file)
    substitution_matrix = compute_substitution_matrix(exchange_rates, frequencies)
    output_matrix(substitution_matrix, amino_acids, args.output_file)

    print(f"Substitution matrix saved to {args.output_file}")

if __name__ == '__main__':
    main()
