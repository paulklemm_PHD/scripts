#!/usr/bin/python3
#pk
from ete3 import Tree
from optparse import OptionParser
import csv, sys, re

version="0.0.6" # last updated on 2023-07-11 12:32:09

def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)

if __name__=="__main__":
	parser = OptionParser(version=version,usage="usage: %prog [options] -t FILE\n find all subtrees that are depth deep from the root and extract the set of leaves (each line correspond to a subtree).")
	parser.add_option("-t", "--tree", 
		help="tree input", metavar="FILE")
	parser.add_option("-d","--depth", default=1, metavar="INT",type="int",
		help="depth to cut the tree")
	parser.add_option("--iformat", default=1, metavar="INT",type="int",
		help="input newick format, see here: http://etetoolkit.org/docs/latest/tutorial/tutorial_trees.html#reading-and-writing-newick-trees")

	(options, args) = parser.parse_args()

	if options.tree is None:
		parser.error("--tree is missing")

	eprint(f"processing --tree...")
	# load main tree
	t = Tree(options.tree,format=options.iformat)

	collapse_labels=[]

	cur_depth=0
	Q = [ t ]
	while cur_depth < options.depth:
		new_Q = []
		for node in Q:
			if node.is_leaf():
				new_Q += [ child ]
			else:
				for child in node.children:
					new_Q += [ child ]
		Q=new_Q
		cur_depth+=1

	for subtree in Q:
		print(" ".join([v.name for v in subtree.get_leaves()]))
