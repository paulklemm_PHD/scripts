#!/usr/bin/python3
#pk
from ete3 import Tree
from optparse import OptionParser
import csv, sys, os, re
from os.path import exists

# this script will parse the synonyms too

version="0.0.8" # last updated on 2023-05-12 13:12:50

def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)

def extract(fname):
	tar = tarfile.open(fname, "r:gz")
	tar.extractall()
	tar.close()

if __name__=="__main__":
	parser = OptionParser(version=version,usage="usage: %prog [options] -i FILE\nautomatically converts a abc graph to newick tree format")
	parser.add_option("-i", "--input", 
		help="input abc file", metavar="FILE")
	parser.add_option("--format", default=8, metavar="INT", type="int",
		help="output format, see here: http://etetoolkit.org/docs/latest/tutorial/tutorial_trees.html#reading-and-writing-newick-trees")
	parser.add_option("--ott", action="store_true", default=False,
		help="ott input format")

	(options, args) = parser.parse_args()

	if (options.input is None):
		parser.error("--input is required")

	eprint(f"processing {options.input}...")

	isfirst=True
	t=Tree()
	data = {}

	with open(f"{options.input}", newline='\n') as csvfile:
		reader = csv.reader(csvfile, delimiter='\t', quotechar='#')
		for row in reader:
			if options.ott:
				row=row[::2] # skip every second entry (table separator is '\t|\t' ...)
				# 0: ott id, 2:species name, ...
				if row[0] == "uid":
					continue
				if row[3] == "no rank - terminal": # SKIP for names: no rank - terminal ... usually weird labels 
					continue

			cur_ottid=row[0]
		
			if isfirst:
				t=t.add_child(name=row[0])
				data[row[0]]=t
				isfirst=False
			else:
				if row[1] not in data:
					eprint(f"found node with no parent {row[0]}")
					exit(1)
				data[row[0]]=data[row[1]].add_child(name=row[0])
				
	print(t.write(format=options.format))

	eprint(f"done, all good...")
