#!/usr/bin/python3
#pk
from ete3 import Tree
from optparse import OptionParser
import csv, sys, os, tarfile, re
# import json
import urllib.request
from os.path import exists

version="0.2.4" # last updated on 2024-01-10 15:53:41

def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)

def extract(fname):
	tar = tarfile.open(fname, "r:gz")
	tar.extractall()
	tar.close()

if __name__=="__main__":
	parser = OptionParser(version=version,usage="usage: %prog [options] -l FILE\nautomatically downloads all files needed to extract the pruned subtree that contains all given leaves using the ete3 framework\nEXAMPLE coarse bacteria species tree: --tax Bacteria --justtax --depth 2 --minspecies 10 --noleaves --nononame --nosp --nocandidatus")
	parser.add_option("-l", "--leaves", 
		help="file with linewise species names (e.g. 'Ustilago maydis') or ncbi ids (e.g. '4529' for Oryza rufipogon), the immediate pruned subtree at the LCA/mrca of the input set is returned", metavar="FILE")
	parser.add_option("-t", "--tax", 
		help="a taxonomic name (e.g. 'Lactobacillaceae'), the immediate pruned subtree of this node is returned")
	parser.add_option("--up", default=0, metavar="INT", type="int",
		help="go up a levels from the point of LCA")
#	parser.add_option("-n", "--ncbi", 
#		help="file with linewise ncbi ids (e.g. '4529' for Oryza rufipogon), same as the --leaves option but this is more precise ! Output names include the species name + ncbi id", metavar="FILE")
	parser.add_option("--ott", default="ott3.3", 
		help="annotation files, needs to match the opentree version ! see here: https://files.opentreeoflife.org/ott/", metavar="VERSION")
	parser.add_option("--opentree", default="opentree13.4", 
		help="tree of life, needs to match the ott version ! see here: https://files.opentreeoflife.org/synthesis/", metavar="VERSION")
	parser.add_option("--resolve", action="store_true", default=False,
		help="resolves polytomies randomly")
	parser.add_option("--noprune", action="store_true", default=False,
		help="disables pruning, i.e. the output includes ALL nodes below the LCA/mrca of the input leaves. WARNING this usually results in a massive output")
	parser.add_option("--ncbiwithname", action="store_true", default=False,
		help="if you search with --ncbi, then only output the species names as well for all nodes")	
	parser.add_option("--ncbiinnername", action="store_true", default=False,
		help="if you search with --ncbi, then output the species names for inner nodes")
	parser.add_option("--format", default=8, metavar="INT", type="int",
		help="output format, see here: http://etetoolkit.org/docs/latest/tutorial/tutorial_trees.html#reading-and-writing-newick-trees")
	parser.add_option("-d","--depth", default=0, metavar="INT", type="int",
		help="prune to a depth relative to the root (0=disabled,1=root and direct children, ...)")
	parser.add_option("--depthAddLeaf", action="store_true", default=False,
		help="adds leave names to depth trimmed tree (node_leaf1|leaf2|...)")
	parser.add_option("--depthAddLeafNum", action="store_true", default=False,
		help="adds leave names to depth trimmed tree (nodeX_210/500)")
	parser.add_option("--force", action="store_true", default=False,
		help="disables santiy checks")
	parser.add_option("--nosubspecies", action="store_true", default=False,
		help="removes any subspecies form the tree (usefull for --depthAddLeafNum)")
	parser.add_option("--noleaves", action="store_true", default=False,
		help="removes any leaves form the tree (usefull for --tax)")
	parser.add_option("--justtax", action="store_true", default=False,
		help="only extract the taxonomic tree")
	parser.add_option("--nocandidatus", action="store_true", default=False,
		help="remove candidatus and candidate")
	parser.add_option("--nosp", action="store_true", default=False,
		help="remove sp and spp")
	parser.add_option("--nononame", action="store_true", default=False,
		help="remove NoName")
	parser.add_option("--minspecies", default=0, metavar="INT", type="int",
		help="minimum number of (leave) species for any internal node (use in combination with --depth)")
	parser.add_option("--tsv",
		help="outputs the --depthAddLeafNum or depthAddLeaf as a table", metavar="FILE")

	(options, args) = parser.parse_args()

	if (options.leaves is None and options.tax is None) or (options.leaves is not None and options.tax is not None):
		parser.error("--leaves OR --ncbi OR --tax is required")

	if options.leaves is not None:
		eprint(f"[WARNING] names are not unique and can lead to ambiguties consider to use --ncbi if possible...")
		f=open(options.leaves, "r")
		leaves = [ i for i in f.read().split("\n") if i != "" ]
		eprint(f"loaded {len(leaves)} entries from --leaves {options.leaves}")
#	elif options.ncbi is not None:
#		modus="ncbi"
#		f=open(options.ncbi, "r")
#		leaves = [ i for i in f.read().split("\n") if i != "" ]
#		eprint(f"loaded {len(leaves)} entries from --ncbi {options.ncbi}")
	elif options.tax is not None:
		options.tax = options.tax.rstrip()

	if not os.path.isdir(f"{options.ott}"):
		eprint(f"downloading {options.ott}...")
		url=f"https://files.opentreeoflife.org/ott/{options.ott}/{options.ott}.tgz"
		urllib.request.urlretrieve(url, f"{options.ott}.tgz")
		extract(f"{options.ott}.tgz")
		os.remove(f"{options.ott}.tgz") 
		
	if not os.path.isdir(f"{options.opentree}_tree"):
		eprint(f"downloading {options.opentree}...")
		url=f"http://files.opentreeoflife.org/synthesis/{options.opentree}/{options.opentree}_tree.tgz"
		urllib.request.urlretrieve(url, f"{options.opentree}_tree.tgz")
		extract(f"{options.opentree}_tree.tgz")
		os.remove(f"{options.opentree}_tree.tgz") 

	# load main tree
	building_tax_tree=False
	if not options.justtax:
		eprint(f"reading {options.opentree}_tree/labelled_supertree/labelled_supertree.tre...")

		t = Tree(f"{options.opentree}_tree/labelled_supertree/labelled_supertree.tre",format=8)
	else:
		if exists(f"{options.ott}/taxtree.tre"):
			eprint(f"reading {options.ott}/taxtree.tre...")
			t = Tree(f"{options.ott}/taxtree.tre",format=8)
		else:
			t = Tree()
			building_tax_tree=True
			eprint(f"I have to build the taxonomic tree, this takes a while but I will save the output to {options.ott}/taxtree.tre to save time in the future...")

	# check if nodes are found in tree -> if not take parent ott
	# there are ott ids that are unplaced and are not marked as such !
	all_valid_treenodesll=set([v.name for v in t.traverse()])

	eprint(f"processing {options.ott}/taxonomy.tsv...")

	# mapping ott id -> species name
	ott2outputname = {}
	ott2outputnamespecies = {}
	found_ottid=[]
	found_names=[]
	is_done_leave = []
	subspecies = []
	data={}
	isfirst=True
	with open(f"{options.ott}/taxonomy.tsv", newline='\n') as csvfile:
		reader = csv.reader(csvfile, delimiter='\t', quotechar='#')
		for row in reader:
			row=row[::2] # skip every second entry (table separator is '\t|\t' ...)
			# 0: ott id, 2:species name, ...
			if row[0] == "uid":
				continue
			if row[3] == "no rank - terminal": # SKIP for names: no rank - terminal ... usually weird labels 
				continue
			#if row[3] == "no rank": # SKIP for names: no rank
			#	continue

			cur_ottid=f"ott{row[0]}"
			nameOrNCBI = [ row[2] ]
			m=re.search("ncbi:([0-9]+)",row[4])
			if m:
				nameOrNCBI = [ m[1] , nameOrNCBI[0] ]

			if options.justtax and building_tax_tree:
				# if row[1] == "":
				# 	t=t.add_child(name=f"ott{row[0]}")
				# else:
				# 	anchor=t.search_nodes(name=f"ott{row[1]}")
				# 	if len(anchor) == 1:
				# 		anchor[0].add_child(name=f"ott{row[0]}")
				# 	else:
				# 		eprint(f"found node with no parent {row[0]}")
				if isfirst:
					t=t.add_child(name=f"ott{row[0]}")
					data[f"ott{row[0]}"]=t
					isfirst=False
				else:
					if f"ott{row[1]}" not in data:
						eprint(f"found node with no parent {row[0]}")
						exit(1)
					data[f"ott{row[0]}"]=data[f"ott{row[1]}"].add_child(name=f"ott{row[0]}")

			for cur_nameOrNCBI in nameOrNCBI:

				if row[6] == "hybrid":
					continue
					#cur_ottid = f"ott{row[1]}" # take the parent of the node if the current node is a hybrid !

				if not options.justtax and cur_ottid not in all_valid_treenodesll:
					continue;
					#cur_ottid = f"ott{row[1]}" # if node is not found -> take parent as fallback
					#if cur_ottid not in all_valid_treenodesll:
					#	continue # node and parent are unplaced -> skip

				ott2outputnamespecies[cur_ottid]=row[2]
				if cur_nameOrNCBI=="" or cur_nameOrNCBI in is_done_leave:
					continue

				ott2outputname[cur_ottid] = f"{nameOrNCBI[0]}@{nameOrNCBI[1]}" if len(nameOrNCBI)>1 and options.ncbiwithname else cur_nameOrNCBI

				#eprint(["assign",cur_ottid,ott2outputname[cur_ottid],ott2outputnamespecies[cur_ottid], row[6] == "hybrid"])

				if options.leaves is not None and cur_nameOrNCBI in leaves:
					is_done_leave += [ cur_nameOrNCBI ]
					found_ottid += [ cur_ottid ]
					found_names += [ cur_nameOrNCBI ] 

				if options.tax is not None and cur_nameOrNCBI == options.tax and row[3] not in [ "species" , "subspecies" ]:
					found_ottid += [ cur_ottid ]
					found_names += [ cur_nameOrNCBI ] 
					eprint(f"found '{options.tax}' in the taxonomy...")

				if options.nosubspecies and row[3] == "subspecies":
					subspecies += [ott2outputname[ cur_ottid ]]

	if building_tax_tree:
		f = open(f"{options.ott}/taxtree.tre", "w")
		f.write(t.write(format=8))
		f.close()

	eprint(f"processing {options.ott}/synonyms.tsv...")

	# and iterate through synonyms !
	with open(f"{options.ott}/synonyms.tsv", newline='\n') as csvfile:
		reader = csv.reader(csvfile, delimiter='\t', quotechar='#')
		for row in reader:
			row=row[::2] 
			# 0: species name, 1:ott id # <- YES different order than in taxonomy ...
			cur_ottid=f"ott{row[1]}"

			nameOrNCBI = [ row[0] ]
			m=re.search("ncbi:([0-9]+)",row[4])
			if m:
				nameOrNCBI += [ m[1] ]

			for cur_nameOrNCBI in nameOrNCBI:

				if cur_ottid not in all_valid_treenodesll:
					continue # node is unplaced -> skip

				if cur_ottid not in ott2outputnamespecies:
					ott2outputnamespecies[cur_ottid]=row[0]
				#eprint(cur_nameOrNCBI)
				if cur_nameOrNCBI=="" or cur_nameOrNCBI in is_done_leave:
					continue
				if options.leaves is not None and cur_nameOrNCBI in leaves:
					is_done_leave+=[ cur_nameOrNCBI ]
					found_ottid += [ cur_ottid ]
					found_names += [ cur_nameOrNCBI ]
					if cur_ottid not in ott2outputname:
						ott2outputname[ cur_ottid ]=""
					else:
						ott2outputname[ cur_ottid ]+="|"

					ott2outputname[ cur_ottid ] += f"{nameOrNCBI[0]}@{nameOrNCBI[1]}" if len(nameOrNCBI)>1 and options.ncbiwithname else cur_nameOrNCBI # replace name only if the synonym is used
					
				if options.tax is not None and len(found_names)==0 and cur_nameOrNCBI == options.tax:
					found_ottid += [ cur_ottid ]
					found_names += [ cur_nameOrNCBI ] 
					eprint(f"found '{options.tax}' in the synonyms...")

	if options.tax is not None:
		if len(found_names) != 1:
			eprint(f"Found unexpected number of tax name (number of matches={len(found_names)} != 1), please look at the taxonomy.tsv/synonyms.tsv.")
			exit(1)

		q = t.search_nodes(name=found_ottid[0])[0]
		if options.up>0:
			for u in range(options.up):
				q=q.up
		t = q

	elif options.leaves is not None:
		if not options.force and len(found_ottid) != len(leaves):
			eprint(f"ERROR: I did find {len(found_ottid)} entries but the input contains {len(leaves)}")
			if len([ l for l in leaves if l not in found_names]):
				eprint("entries in --input that are not found:",[ l for l in leaves if l not in found_names] )
			if len([ l for l in found_names if l not in leaves]):
				eprint("leaves that are not in the --input:",[ l for l in found_names if l not in leaves] )
			eprint("please look at the taxonomy.tsv/synonyms.tsv in the last column (https://github.com/OpenTreeOfLife/reference-taxonomy/blob/master/doc/taxon-flags.md#taxon-flags). E.g. 'hybrid' or 'unplaced' flag is suppressed in the synthetic tree. For hybrid you could just choose the parent NCBI id of this entry... Or use --force to proceed anyway.")
			exit(1)
		else:
			eprint(f"found {len(found_ottid)}/{len(leaves)} entries in {options.ott}")

		eprint(f"searching for the LCA/mrca...")
		q = t.get_common_ancestor(found_ottid) # the LCA node contains the immediate subtree of that node

		if options.up>0:
			for u in range(options.up):
				q=q.up
		t=q

		if not options.noprune:
			eprint(f"pruning...")
			t.prune(found_ottid) # returns void, well ok...
	
	if options.resolve:
		eprint(f"resolving polytomies randomly...")
		t.resolve_polytomy(recursive=True)
	
	if options.nosubspecies:
		eprint(f"removing subspecies...")
		t.prune([x for x in t.traverse() if x.name not in subspecies])

	if options.nononame:
		eprint(f"removing NoName...")
		t.prune([x for x in t.traverse() if x.name != "" and x.name != "NoName"])

	if options.nocandidatus:
		eprint(f"removing Candidatus...")
		t.prune([x for x in t.traverse() if not re.match("[Cc]andidat(us|e)",x.name)])

	if options.nosp:
		eprint(f"removing ?s?sp...")
		t.prune([x for x in t.traverse() if not re.match("[sS]?[sS]p\\.",x.name)])

	eprint(f"relabeling nodes/adding features...")
	if options.depthAddLeaf or options.depthAddLeafNum:
		for node in t.traverse():
			node.add_features(score=0,total=0,leaves="",percent=0)
			if not node.is_leaf():
				if options.depthAddLeafNum:
					if options.leaves is not None:
						score=len([x for x in node.traverse() if x.name in found_ottid])
					else:
						score = 0
					total=len(node.get_leaves())
					node.add_features(score=score,total=total,percent=score/total if total>0 else 0)
				else:
					node.add_features(leaves="|".join([v.name for v in node.traverse() if v.name != ""]))

	if options.noleaves:
		eprint(f"removing leaves...")
		t.prune([x for x in t.traverse() if not x.is_leaf()])

	for node in t.traverse():
		if len(node.name) == 0:
			continue
		if re.match("mrca",node.name):
			node.name=""
			continue

		new_label=""
		if not node.is_leaf() and options.ncbiinnername and node.name in ott2outputnamespecies:
			new_label = ott2outputnamespecies[node.name]
		elif node.name in ott2outputname:
			new_label = ott2outputname[node.name] 
		new_label=re.sub("[,()]",".",new_label) # sanity clean

		node.name = new_label if len(new_label)>0 else ""

	if options.depth>0:
		eprint(f"depth pruning...")
		Q=[t]
		Q_new=[]
		Q_leaves=[]
		data=[]
		#if options.depthAddLeafNum and len(t)>1 and len(t.name)!=0:
		#	t.name=f"{t.name}_{len(t)}"
		for i in range(options.depth): # search deeper into the tree until depth cutoff is satisfied
			for node in Q:
				if not node.is_leaf():
					Q_new += node.get_children()
				elif node.is_leaf() and node.name in found_names:
					Q_leaves += [node] # otherwise those would vanish
				elif node.is_leaf():
					Q_leaves += [node.up]
			Q=list(set(Q_new))
			Q_new=[]
		#	if options.depthAddLeafNum:
		#		for node in Q:
		#			if len(node)>1 and len(node.name)!=0:
		#				node.name=f"{node.name}_{len(node)}"
		Q=list(set(Q+Q_leaves))
		
		# if options.leaves is not None:
		# 	Q=[x for x in Q if x == t or (x.name in found_names)]

		t.prune(Q)

	if options.minspecies>0:
		eprint(f"minspecies pruning...")
		remove_nodes=[]
		for node in t.traverse('levelorder'): 
			if node not in remove_nodes and node.total <= options.minspecies:
				upnodes=[x for x in node.up.get_children() if x.name == "other" and x not in remove_nodes]
				if len(upnodes)==0:
					#eprint(["new other",node, node.up])
					other_node=node.up.add_child(name="other")
					other_node.add_features(total=node.total)
				else:
					#eprint(["found agg other",node, node.up])
					other_node=upnodes[0]
					other_node.total+=node.total
				if len(upnodes)>1:
					eprint(["wtf", node, node.up])
					exit(1)
				for w in node.traverse():
					remove_nodes+=[w]
		
		Q=[x for x in t.traverse('levelorder') if x not in remove_nodes]

		t.prune(Q)

	if options.nononame:
		t.prune([x for x in t.traverse() if x.name != "" and x.name != "NoName"])

	if options.depthAddLeafNum:
		if options.tsv is not None:
			with open(options.tsv, 'w') as out:
				for node in t.traverse():
					out.write("\t".join([node.name,f"{node.score}",f"{node.total}",f"{node.percent}"] if options.leaves is not None else [node.name,f"{node.total}"]))

		print(t.write(format=options.format,features=["score","total","percent"]))
	elif options.depthAddLeaf:
		if options.tsv is not None:
			with open(options.tsv, 'w') as out:
				for node in t.traverse():
					out.write(node.name+'\t'+";".join(node.leaves))

		print(t.write(format=options.format,features=["leaves"]))
	else:
		print(t.write(format=options.format))

	#eprint(t)
	eprint(f"done, all good...")
