#!/usr/bin/python3
#pk
from ete3 import Tree
from optparse import OptionParser
import csv, sys, re

version="0.0.15" # last updated on 2022-07-14 08:23:10

if __name__=="__main__":
	parser = OptionParser(version=version,usage="usage: %prog [options] -t FILE -m MODUS\noutputs different statistics based on --modus")
	parser.add_option("-t", "--tree", type="string",
		help="input newick file", metavar="FILE")
	parser.add_option("-m","--modus", metavar="MODUS", default="print",
		help="*basic*: output basic statistics about the input tree. *avgdist*: for each leave outputs average distance to all other leaves\n*pairdist*: for each leave outputs distances to all other leaves\n*rootdist*: for each leave outputs distances to the root\n\n*depth*: output the farthest leave and the distance\n*resolve*: resolves polytomies randomly into a bifurcated structure\n*print*: plot the tree in ASCII format\n*format*: output in --oformat format")
	parser.add_option("--iformat", metavar="INT", default=1,type="int",
		help="input tree format, see http://etetoolkit.org/docs/latest/tutorial/tutorial_trees.html#reading-and-writing-newick-trees")
	parser.add_option("--oformat", metavar="INT", default=1,type="int",
		help="output tree format, see http://etetoolkit.org/docs/latest/tutorial/tutorial_trees.html#reading-and-writing-newick-trees")

	(options, args) = parser.parse_args()

	if options.tree is None:
		parser.error("--tree is missing")
	if options.modus not in ["format","avgdist","pairdist","topoavgdist","topopairdist","rootdist","toporootdist","depth","resolve","print","basic"]:
		parser.error("--modus is invalid")

	# load main tree
	t = Tree(options.tree,format=options.iformat)

	if options.modus == "basic":
		num_nodes=len([v for v in t.traverse()])
		num_leaves=len(t)

		if hasattr(t, 'D'):
			print(f"tree\tleaves\tnodes\tduplication events\n{options.tree}\t{num_leaves}\t{num_nodes}\t{len([v.D for v in t.traverse() if v.D == 'Y'])}")
		else:
			print(f"tree\tleaves\tnodes\n{options.tree}\t{num_leaves}\t{num_nodes}")
		exit(0)

	if options.modus == "format":
		print(t.write(format=options.oformat))
		exit(0)

	if options.modus == "format":
		print(t.write(format=options.oformat))
		exit(0)

	if options.modus == "print":
		print(t)
		exit(0)

	if options.modus == "resolve":
		t.resolve_polytomy(recursive=True)
		print(t.write(format=options.oformat))
		exit(0)

	if options.modus == "depth":
		farthest, dist = t.get_farthest_node()
		print(f"{farthest}\t{dist}")
		exit(0)

	seen={}
	topo_only=options.modus in ["topopairdist","topoavgdist","toporootdist"]
	avg_dist =options.modus in ["avgdist","topoavgdist"]
	if options.modus in ["rootdist","toporootdist"]:
		for v1 in t.traverse("postorder"):
			cur_nam=v1.name
			if not v1.is_leaf():
				cur_nam = ";".join(sorted([w.name for w in v1.get_leaves()]))
			print(f"root\t{cur_nam}\t{t.get_distance(v1,topology_only = topo_only )}")
	else:
		for v1 in t.traverse("postorder"):
			if not v1.is_leaf():
				continue
			dists=[]

			for v2 in t.traverse("postorder"):
				if not v2.is_leaf() or v1 is v2 or v1.name+"_"+v2.name in seen or v2.name+"_"+v1.name in seen:
					continue
				seen[v1.name+"_"+v2.name]=1
				seen[v2.name+"_"+v1.name]=1
				if avg_dist:
					dists+=[v1.get_distance(v2)]
				else:
					print(f"{v1.name}\t{v2.name}\t{v1.get_distance(v2,topology_only = topo_only )}")
			if avg_dist:
				print(f"{v1.name}\t{sum(dists)/len(dists)}")
