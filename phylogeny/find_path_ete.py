#!/usr/bin/python3
#pk
from ete3 import Tree
from optparse import OptionParser
import regex as re
import sys

version="0.0.10" # last updated on 2024-07-23 14:08:31

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def find_path(tree, node1, node2):
    node1 = tree & node1
    node2 = tree & node2
    
    common_ancestor = tree.get_common_ancestor(node1, node2)

    node=node1
    path1=[]
    while node.up and node != common_ancestor:
      path1.append(node)
      node = node.up
    node=node2
    path2=[]
    while node.up and node != common_ancestor:
      path2.append(node)
      node = node.up

    path=[x.name for x in path1+[common_ancestor]+path2[::-1]]

    return path

def find_path_to_root(tree, node1):
    node1 = tree & node1
    
    node=node1
    path1=[]
    while node.up:
      path1.append(node)
      node = node.up

    path=[x.name for x in path1]

    return path

def main():
    parser = OptionParser()
    parser.add_option("-f", "--file", dest="filename", help="Tree file in Newick format (format=8)", metavar="FILE")
    parser.add_option("-a", "--node1", dest="node1", help="First node name", metavar="NODE1")
    parser.add_option("-b", "--node2", dest="node2", help="(optional) second node name (if not specified, the root is taken)", metavar="NODE2")
    parser.add_option("-l", "--list", dest="list", help="list of inputs in the form of a:b,... or just a,b,... (path to root)", metavar="list")

    (options, args) = parser.parse_args()

    if not options.filename or (not options.node1 and not options.list):
        parser.error("All arguments -f, -n, and -m are required.")
    
    # Load the tree
    eprint("# reading tree ...")
    tree = Tree(options.filename,format=8)
    eprint("# reading tree done")
    
    if options.list:
        for case in options.list.split(","):
            if ":" in case:
                a,b = case.split(":",2)
                path = find_path(tree,a,b)
            else:
                path = find_path_to_root(tree, case)

            print(f"{case}\t"+(','.join(path)))
    else:
        if options.node2:
            path = find_path(tree, options.node1, options.node2)
        else:
            path = find_path_to_root(tree, options.node1)
        print(",".join(path))

if __name__ == "__main__":
    main()
