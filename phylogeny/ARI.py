#!/usr/bin/env python3
#pk
from optparse import OptionParser
import csv, sys, re
from sklearn.metrics import adjusted_rand_score

version="0.0.10" # last updated on 2023-07-11 12:32:09

def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)

if __name__=="__main__":
	parser = OptionParser(version=version,usage="usage: %prog [options] -a FILE1 -b FILE2\n calculate the adjusted rand score of two clusterings.")
	parser.add_option("-a", "--fileA", 
		help="input clustering file 1. The file contains linewise clusters that are separated by whitespaces", metavar="FILE")
	parser.add_option("-b", "--fileB", 
		help="input clustering file 2", metavar="FILE")
	parser.add_option("-p","--proteinortho", action="store_true", default=False,
		help="input are proteinortho.tsv output files")

	(options, args) = parser.parse_args()

	if options.fileA is None:
		parser.error("--a is missing")
	if options.fileB is None:
		parser.error("--b is missing")

	clustersA = {}
	cur_clusterA=0
	with open(options.fileA, 'r') as file:
		for line in file:
			cluster=[]
			if options.proteinortho:
				if re.match(r"^#",line):
					continue
				cluster = re.split(r"[\t,]",line.strip())
				cluster = cluster[3:]
				cluster = [ re.sub(r"^[^\|]+\|(.+)\|[^\|]+",r"\1",i) for i in cluster if i != "*"]
			else:
				cluster = line.strip().split(" ")
			for i in cluster:
				clustersA[i]=cur_clusterA
				cluster = [ i for i in cluster if i != "*"]
			cur_clusterA+=1
	
	clustersB = {}
	cur_clusterB=0
	with open(options.fileB, 'r') as file:
		for line in file:
			cluster=[]
			if options.proteinortho:
				if re.match(r"^#",line):
					continue
				cluster = re.split(r"[\t,]",line.strip())
				cluster = cluster[3:]
				cluster = [ re.sub(r"^[^\|]+\|(.+)\|[^\|]+",r"\1",i) for i in cluster if i != "*"]
			else:
				cluster = line.strip().split(" ")
			for i in cluster:
				clustersB[i]=cur_clusterB
			cur_clusterB+=1

	missingA=[i for i in clustersA.keys() if i not in clustersB.keys()]
	missingB=[i for i in clustersB.keys() if i not in clustersA.keys()]

	for i in missingA:
		clustersA[i]=cur_clusterA
		cur_clusterA+=1

	for i in missingB:
		clustersB[i]=cur_clusterB
		cur_clusterB+=1

	labels = [i for i in clustersA.keys() if i in clustersB.keys()]

	print(clustersA)
	print(clustersB)

	# Flatten the clusters into a single list of labels
	labels1 = [ clustersA[i] for i in labels ]
	labels2 = [ clustersB[i] for i in labels ]

	ari = adjusted_rand_score(labels1, labels2)
	print(ari)
