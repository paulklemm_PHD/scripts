#!/usr/bin/env python3
#pk
from optparse import OptionParser
import csv, sys, re
from ete3 import Tree

version="0.0.7" # last updated on 2024-10-09 08:20:50

def eprint(*args, **kwargs):
	print(*args, file=sys.stderr, **kwargs)

if __name__=="__main__":
	parser = OptionParser(version=version,usage="usage: %prog [options] -a FILE1 -b FILE2\n calculate robinson-foulds distance of two trees.")
	parser.add_option("-a", "--fileA", 
		help="input tree file 1.", metavar="FILE")
	parser.add_option("-b", "--fileB", 
		help="input tree file 2", metavar="FILE")
	parser.add_option("--iformat", default=1, metavar="INT",type="int",
		help="input newick format, see here: http://etetoolkit.org/docs/latest/tutorial/tutorial_trees.html#reading-and-writing-newick-trees")
	parser.add_option("-u","--unrooted_trees", action="store_true", default=False,
		help="enables the unrooted_trees option in the RF calculation")

	(options, args) = parser.parse_args()

	if options.fileA is None:
		parser.error("--a is missing")
	if options.fileB is None:
		parser.error("--b is missing")

	eprint(f"processing trees...")
	# load main tree
	A = Tree(options.fileA,format=options.iformat)
	B = Tree(options.fileB,format=options.iformat)

	rf=-1
	rf_max=-1
	common_leaves=[]
	if options.unrooted_trees:
		rf, rf_max, common_leaves, names, edges_t1, edges_t2, discarded_edges_t1 = A.robinson_foulds(B,unrooted_trees=True)
	else:
		rf, rf_max, common_leaves, names, edges_t1, edges_t2, discarded_edges_t1 = A.robinson_foulds(B)
   
	if len(common_leaves) == 0:
		raise Exception('There are no common leaves in the input trees')

	print(f"normalized RF, RF distance:\n{rf/rf_max if rf_max>0 else 'NA'}\t{rf}") 
