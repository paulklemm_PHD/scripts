#!/usr/bin/env python
#pk
import sys
import copy 
import time
import math
import regex as re
from optparse import OptionParser
import fileinput
import functools
from random import shuffle

version="1.2.33" # last updated on 2022-06-03 21:06:50

def eprint(*args, **kwargs):
	print(*args, file=extra_packages["sys"].stderr, **kwargs) if "sys" in list(extra_packages.keys()) else print(*args, **kwargs)

extra_packages = {"networkx":None, "matplotlib.pyplot":None}

class tree:
	"""
	weighted (or unweighted) unrooted (or rooted) tree
	"""

	def getDeepCopy(self):
		T = tree()
		T.adjacencyList_d = copy.deepcopy(self.adjacencyList_d)
		T.label = copy.deepcopy(self.label)
		T.nodes = copy.deepcopy(self.nodes)
		T.leaves = copy.deepcopy(self.leaves)
		T.degree = copy.deepcopy(self.degree)
		T.children = copy.deepcopy(self.children)
		T.root = copy.deepcopy(self.root)
		return T

	def __init__(self, inputString:str = "", root=None):
		"""
		root = None : unrooted tree, root = -1 : take the last node id as the root, otherwise the value is the root node id
		reads input formatted like this:
		0->4:11
		1->4:2
		2->5:6
		...
		or without weights 
		"""
		self.adjacencyList_d = dict() # central datastructure (holds both directions), in the lecture this is small "d"
		self.nodes = [] # can be build from adjacencyList_d
		self.leaves = [] # can be build from adjacencyList_d
		self.degree = {} # can be build from adjacencyList_d
		self.children = {} # can be build from adjacencyList_d

		self.root = None # None = unrooted
		self.label = {} # sequences are stored here (should not be used as node ids since different nodes can have same sequences)

		if inputString != "":
			for line in inputString.split("\n"):
				if line == "" or "->" not in line:
					continue
				weight=None
				if len(re.findall("->|:",line))==1:
					a, b = re.split("->|:",line)
				else:
					a, b, weight = re.split("->|:",line)
					if float(int(float(weight))) == float(weight):
						weight = int(float(weight))
				self.addEdge(a,b,weight)
				self.label[a]=str(a)
				self.label[b]=str(b)

			if root == -1:
				self.housekeeping()
				v = self.leaves[0]
				max_len = 0
				root = -1
				for w in self.leaves:
					p = self.path(v,w)
					if p is not None and len(p)>max_len:
						max_len = len(p)
						root = p[int(len(p)/2)]
				self.root = root
			else:
				self.root = str(root) if root is not None else None

			self.housekeeping()

	def housekeeping(self):
		"""
		function to construct many datastructures out of the adjacencyList_d
		"""
		self.nodes = list(self.adjacencyList_d.keys())
		self.degree = { v:0 for v in self.nodes }
		for v in self.nodes:
			for w in self.nodes:
				if v in self.adjacencyList_d and w in self.adjacencyList_d[v]:
					self.degree[v] = self.degree[v]+0.5 # in/out is each .5 = 1 full node degree
					self.degree[w] = self.degree[w]+0.5
		self.degree = { v:int(self.degree[v]) for v in self.nodes }
		self.leaves = [ v for v in self.degree if self.degree[v]==1 ]
		if self.root is not None:
			todo = [self.root]
			done = []
			while len(todo)>0:
				cur_e = todo.pop()
				children = [ i for i in self.adjacencyList_d[cur_e] if i not in done ]
				self.children[cur_e] = children
				done += [cur_e]
				todo += children

	def addEdge(self,a,b,weight=None):
		"""
		add edge a-b (node ids) and also the nodes if are not part of self.nodes
		"""
		if weight is not None:
			weight=float(weight)
		a=str(a)
		b=str(b)

		if a > b:
			(a,b) = (b,a)
		if a not in self.adjacencyList_d:
			self.adjacencyList_d[a]={}
		if b not in self.adjacencyList_d:
			self.adjacencyList_d[b]={}
		self.adjacencyList_d[a][b] = weight
		self.adjacencyList_d[b][a] = weight
		if a not in self.nodes:
			self.nodes += [a]
		if b not in self.nodes:
			self.nodes += [b]

	def removeEdge(self,a,b):
		self.adjacencyList_d = { v:{ w:self.adjacencyList_d[v][w] for w in self.adjacencyList_d[v] if not ( (a == v and b == w) or (a == w and b == v) ) } for v in self.nodes }
		self.housekeeping()

	def path(self,a,b):
		"""
		BFS search for path from a->b
		"""
		a=str(a)
		b=str(b)
		queue = [[a]]
		while len(queue)>0:
			cur_path = queue.pop()
			cur_v = cur_path[-1]
			if cur_v in self.adjacencyList_d:	
				for w in self.adjacencyList_d[cur_v]:
					if w in cur_path:
						continue
					if w == b:
						return cur_path+[b]
					elif self.degree[w] != 1:
						queue.append(cur_path+[w])
		return None

	def pathDist(self,a,b):
		if a==b:
			return 0
		path = self.path(a,b)
		result = 0
		for i in range(len(path)-1):
			result += self.adjacencyList_d[path[i]][path[i+1]]
		return result

	def getNodeAtDistance(self,i,k,dist,numberOfLeaves):
		"""
		return v ← the (potentially new) node in T at distance dist from i on the path between i and k
		"""
		v = numberOfLeaves
		while str(v) in self.nodes:
			v = v+1
		v=str(v)
		dist = int(dist)
		path = self.path(i,k)
		score=0
		for i in range(len(path)-1):
			a = path[i]
			b = path[i+1]
			d = self.adjacencyList_d[a][b]

			if score + d > dist:
				del self.adjacencyList_d[a][b]
				del self.adjacencyList_d[b][a]
				self.addEdge(a,v,dist-score)
				self.addEdge(b,v,(score + d)-dist)
				break
			elif score+d == dist:
				return b
			score += self.adjacencyList_d[a][b]
		return v

	def addLeaf(self,v,dist,newNodeName=""):
		"""
	    add leaf newNodeName back to T by creating a limb (v, newNodeName) of length limbLength
		"""
		if newNodeName=="":
			newNodeName = len(self.leaves)
		dist = int(dist)
		self.addEdge(newNodeName,v,dist)
		self.housekeeping()

	def toLeaveDistMatrix(self):
		result = [[0 for w in range(len(self.leaves))] for v in range(len(self.leaves))]
		for v in range(len(self.leaves)):
			for w in range(v+1,len(self.leaves)):
				pd = self.pathDist(self.leaves[v],self.leaves[w])
				result[v][w] = pd
				result[w][v] = pd
		return result

	def printWithOneDigitNodeId(self):
		node2id = { self.nodes[i]:str(i) for i in range(len(self.nodes)) }
		d = { node2id[i]:{node2id[j]:self.adjacencyList_d[i][j] for j in self.adjacencyList_d[i] } for i in self.adjacencyList_d }
		return("\n".join([ "\n".join([ f"{a}->{b}:"+"{:.3f}".format(d[a][b]) for b in d[a] ]) for a in d ]))

	@staticmethod
	def HammingDistance(a,b):
		"""
		returns number of positions that differ
		"""
		result = int(0)
		for i in range(len(a)):
			if i >= len(b) or a[i] != b[i]:
				result+=1
		return result

	def SmallParsimony_Sankoff(self, aln_length=None, alphabet = None, bigPictureMode=False):
		"""
		input: rooted tree (= small) or unrooted tree
		outpu: finds the sequences of all inner nodes (tree is given) that minimizes parsimony evolution distance (overall hamming distance between all inner nodes)
		aln_length : the length of the alignment (all leaves should be equal length), if None take len(first leaf)
		Assumption: no indels/gap -> remove those columns beforehand ! Rooted tree is needed as input, use NJ / UPGMA to generate one (NJ for tree and UPGMA for root)

		adjacent nodes with identical sequences are merged into one node (no difference)
		"""

		if bigPictureMode:
			bigPictureMode_data = []

		if alphabet is None:
			alphabet=[]
			for v in self.leaves:
				cur_seq = v
				if v in self.label:
					cur_seq = self.label[v]
				for c in cur_seq:
					if c not in alphabet:
						alphabet += [c]

		unrooted_case=False
		unrooted_root_edge=[]
		if self.root is None:
			
			#print("ini unrooted")
			#self.draw()

			unrooted_case=True
			v = self.leaves[0]
			max_len = 0
			a = -1
			b = -1
			for w in self.leaves:
				p = self.path(v,w)
				if p is not None and len(p)>max_len:
					max_len = len(p)
					a = p[int(len(p)/2)]
					b = p[int(len(p)/2)+1]
			self.addEdge(a,"ROOT")
			self.addEdge(b,"ROOT")
			unrooted_root_edge = [a,b]
			self.root = "ROOT"
			self.adjacencyList_d={v:{w:self.adjacencyList_d[v][w] for w in self.adjacencyList_d[v] if not ( (a == v and b == w) or (a == w and b == v) ) } for v in self.nodes}

			self.housekeeping()
			#print("ini unrooted-> now rooted")
			#self.draw()

			if bigPictureMode:
				bigPictureMode_data += [{"T":self.getDeepCopy(),"title":f"add artifical root"}]
		else:
			if bigPictureMode:
				bigPictureMode_data += [{"T":self.getDeepCopy(),"title":f"start"}]

		tag=dict()
		sk=dict()
		if aln_length is None:
			aln_length = len(self.label[self.leaves[0]])

		for v in self.nodes:
			tag[v] = 0
			if v in self.leaves:
				tag[v] = 1
				sk[v] = [ { a:int(0) if a is i else float('inf') for a in alphabet } for i in self.label[v] ] 
			else:
				sk[v] = [ { a:int(0) for a in alphabet } for i in range(aln_length)]

		if bigPictureMode:
			Tmp = self.getDeepCopy()
			for v in self.nodes:
				if v in sk:
					Tmp.label[v] = str(sk[v]).replace("},","}\n")
			bigPictureMode_data += [{"T":Tmp.getDeepCopy(),"title":f"create sk","font_size_fct":0.5}]

		while True:
			#print(sk)

			# search for a ripe node
			ripe_v=None
			for v in self.nodes:
				if tag[v] == 0 and functools.reduce(lambda x,y: x*y, [ tag[i] for i in self.children[v] ]) == 1:
					ripe_v = v
					break
			if ripe_v is None:
				break # if there is no ripe node in T : done

			#print("ripe_v",ripe_v)
			tag[ripe_v]=1

			#print(ripe_v,"->",self.children[ripe_v])

			for pos in range(aln_length):
				for a in alphabet:
					min_val = 0
					for child in self.children[ripe_v]:
						min_val += min([ sk[child][pos][m] + ( 0 if m == a else 1 ) for m in alphabet ])
					# sk(v) ← minimumall symbols i {si(Daughter(v))+αi,k} + minimumall symbols j {sj(Son(v))+αj,k}
					sk[ripe_v][pos][a] = min_val

			if bigPictureMode:
				Tmp = self.getDeepCopy()
				for v in self.nodes:
					if v in sk:
						Tmp.label[v] = str(sk[v]).replace("},","}\n")
				bigPictureMode_data += [{"T":Tmp.getDeepCopy(),"title":f"building sk for {ripe_v}","font_size_fct":0.5}]

		# backtrack
		min_sk = { v:["" for i in range(aln_length)] for v in self.nodes }
		todo = [ self.root ]
		done = []
		parent = {} # { w:v for w in self.children[v] for v in self.nodes }
		for v in self.nodes:
			for w in self.children[v]:
				parent[w]=v

		while len(todo)>0:
			cur_v = todo.pop()
			if cur_v in self.leaves:
				continue

			for pos in range(aln_length):
				min_val = None
				min_a = []
				for a in alphabet:
					if min_val is None or sk[cur_v][pos][a] < min_val:
						min_val = sk[cur_v][pos][a]
						min_a = a
				for a in alphabet:
					if cur_v in parent and sk[cur_v][pos][a] == min_val and a == min_sk[ parent[cur_v] ][pos]:
						min_a = a

				min_sk[cur_v][pos] = min_a

			todo += [ c for c in self.children[cur_v] if c not in done ]
			done += todo

			if bigPictureMode:
				Tmp = self.getDeepCopy()
				for v in self.nodes:
					if v in min_sk:
						Tmp.label[v] = "".join(min_sk[v])
				bigPictureMode_data += [{"T":Tmp.getDeepCopy(),"title":f"deciding minimum for {cur_v}"}]

		if unrooted_case:
			#print("done still rooted")
			#self.draw()
			#print(self.adjacencyList_d)
			self.adjacencyList_d = {v:{w:self.adjacencyList_d[v][w] for w in self.adjacencyList_d[v] if ( v != self.root and w != self.root ) } for v in self.nodes if v != self.root}
			#print(self.adjacencyList_d)
			self.addEdge( unrooted_root_edge[0], unrooted_root_edge[1] )
			self.root = None
			self.housekeeping()
			#print("done now unrooted",self.nodes)
			#self.draw()

			if bigPictureMode:
				bigPictureMode_data += [{"T":self.getDeepCopy(),"title":f"remove artifical root"}]

		#print(min_sk,self.root)

		# replace nodes with new label
		for v in self.nodes:
			if v in self.leaves:
				continue

			new_label = ""
			for pos in range(aln_length):
				new_label += min_sk[v][pos]
			
			#print(v,"->",new_label)

			#if v == self.root:
			#	self.root = new_label

			self.label[v]=new_label
			#self.adjacencyList_d = {(new_label if i == v else i):{(new_label if j == v else j):self.adjacencyList_d[i][j] for j in self.adjacencyList_d[i]} for i in self.adjacencyList_d}

		if bigPictureMode:
			bigPictureMode_data += [{"T":self.getDeepCopy(),"title":f"adding leaves back"}]

		#print(self)

		self.housekeeping()
		#print(self.nodes)

		# build edge weights
		for v in self.nodes:
			#print(v)
			for w in self.adjacencyList_d[v]:
				self.adjacencyList_d[v][w] = self.HammingDistance(self.label[v],self.label[w])

		#print("done done")
		#self.draw()

		if bigPictureMode:
			bigPictureMode_data += [{"T":self.getDeepCopy(),"title":f"calculate edge distances"}]
			nxDrawStuff.doBigPicture(bigPictureMode_data,"SmallParsimony_Sankoff")

	def parsimonyScore(self):
		"""
		returns the total sum of all edge weights 
		"""
		result=0
		for v in self.nodes:
			for w in self.adjacencyList_d[v]:
				result += self.adjacencyList_d[v][w]
		result/=2
		if float(result) == float(int(result)):
			result = int(result)
		return result

	def BFS(self,start):
		todo = [start]
		done = []
		while len(todo)>0:
			cur_v = todo.pop()
			done += [cur_v]
			for v in self.adjacencyList_d[cur_v]:
				if v not in done:
					todo += [v]
		return done

	def _NearestNeighborsTreeProblem(self,a,b):
		"""
    	Input: Two internal nodes a and b specifying an edge e, followed by an adjacency list of an unrooted binary tree.
    	Output: Two adjacency lists representing the nearest neighbors of the tree with respect to e. Separate the adjacency lists with a blank line.
		!! Assumption: input is binary tree !!
		Part of the greedyNearestNeighborInterchange
		"""
		a=str(a)
		b=str(b)

		w,x = [i for i in list(self.adjacencyList_d[a].keys()) if i != b]
		y,z = [i for i in list(self.adjacencyList_d[b].keys()) if i != a]

		# 1. output: swapping a-x with b-y
		
		A = tree()
		A.adjacencyList_d = {i:{} for i in self.nodes}
		A.label=self.label
		for i in self.adjacencyList_d:
			for j in self.adjacencyList_d[i]:
				if i == a and j == x:
					A.adjacencyList_d[i][y] = self.adjacencyList_d[i][j]
				elif i == x and j == a:
					A.adjacencyList_d[y][j] = self.adjacencyList_d[i][j]
				elif i == b and j == y:
					A.adjacencyList_d[i][x] = self.adjacencyList_d[i][j]
				elif i == y and j == b:
					A.adjacencyList_d[x][j] = self.adjacencyList_d[i][j]
				else:
					A.adjacencyList_d[i][j] = self.adjacencyList_d[i][j]
		A.housekeeping()
		
		# 2. output: swapping a-x with b-z

		B = tree()
		B.adjacencyList_d = {i:{} for i in self.nodes}
		B.label=self.label
		for i in self.adjacencyList_d:
			for j in self.adjacencyList_d[i]:
				if i == a and j == x:
					B.adjacencyList_d[i][z] = self.adjacencyList_d[i][j]
				elif i == x and j == a:
					B.adjacencyList_d[z][j] = self.adjacencyList_d[i][j]
				elif i == b and j == z:
					B.adjacencyList_d[i][x] = self.adjacencyList_d[i][j]
				elif i == z and j == b:
					B.adjacencyList_d[x][j] = self.adjacencyList_d[i][j]
				else:
					B.adjacencyList_d[i][j] = self.adjacencyList_d[i][j]
		B.housekeeping()
		
		return A,B

	@staticmethod
	def largeParsimony(leaves:list,leave_labels:list=[],verbose=False,restarts=1,bigPictureMode=False,bigPictureMode_withLabelsInsteafOfIds=True):
		"""
		leaves : unique species names
		leaves_labels : sequences
		(will also work if you just give the squences in leaves)
		use draw(False) to draw by species names !
		generate a unrooted tree with minimum parsimony score (greedy)
		"""
		bestT=tree()
		bestScore=float('inf')
		if bigPictureMode and restarts>1:
			bigPictureMode_data = []
			
		for it in range(restarts):
			T = tree.generateRandomBinaryTree(leaves,leave_labels,bigPictureMode = bigPictureMode and restarts==1,bigPictureMode_withLabelsInsteafOfIds=bigPictureMode_withLabelsInsteafOfIds)
			if bigPictureMode and restarts>1:
				bigPictureMode_data += [{"T":T.getDeepCopy(),"title":f"it={it} generateRandomBinaryTree"}]
			T.greedyNearestNeighborInterchange(verbose,bigPictureMode = bigPictureMode and restarts==1,bigPictureMode_withLabelsInsteafOfIds=bigPictureMode_withLabelsInsteafOfIds)
			score = T.parsimonyScore()
			if bigPictureMode and restarts>1:
				bigPictureMode_data += [{"T":T.getDeepCopy(),"title":f"it={it} greedyNNI->{score}"}]
			if verbose:
				print(f"restart #{it} with score {score}")
			if score < bestScore:
				bestScore = score
				# deep copy tree
				bestT = T.getDeepCopy()

		if bigPictureMode and restarts>1:
			bigPictureMode_data += [{"T":bestT.getDeepCopy(),"title":f"best:{bestScore}"}]
			nxDrawStuff.doBigPicture(bigPictureMode_data,"largeParsimony",False,withLabelsInsteafOfIds=bigPictureMode_withLabelsInsteafOfIds)

		return bestT

	@classmethod
	def generateRandomBinaryTree(cls,leaves:list,leave_labels:list=[],rooted=False,bigPictureMode=False,bigPictureMode_withLabelsInsteafOfIds=True):
		"""
		for the largeParsimony problem: generate a random binary tree and then apply greedyNearestNeighborInterchange
		"""
		T = cls()
		
		# generate a random binary tree with leaves from array of given strings
		if len(leaves) == 1:
			return T

		T.addEdge("0","1")
		T.addEdge("1","2")
		T.addEdge("1","3")
		T.addEdge("0","4")
		T.addEdge("0","5")
		n=4
		todo = ["2","3","4","5"]

		if bigPictureMode:
			bigPictureMode_data = [{"T":T.getDeepCopy(),"title":"start with (()())"}]

		while n < len(leaves):
			shuffle(todo)
			c = todo.pop()
			new_a=str(len(T.adjacencyList_d))
			new_b=str(len(T.adjacencyList_d)+1)
			T.addEdge(c,new_a)
			T.addEdge(c,new_b)
			todo += [new_a,new_b]
			n += 1
			if bigPictureMode:
				bigPictureMode_data += [{"T":T.getDeepCopy(),"title":f"add ({new_a},{new_b}) to {c}"}]

		maping = {todo[i]:leaves[i] for i in range(n)}
		T.adjacencyList_d = {(maping[i] if i in todo else i):{(maping[j] if j in todo else j):T.adjacencyList_d[i][j] for j in T.adjacencyList_d[i]} for i in T.adjacencyList_d}

		T.label={v:v for v in T.adjacencyList_d}

		for i in range(n):
			#print(maping[todo[i]],leave_labels[i],i,i in leave_labels)
			T.label[maping[todo[i]]] = leave_labels[i] if i < len(leave_labels) else maping[todo[i]]

		T.housekeeping()

		if bigPictureMode:
			bigPictureMode_data += [{"T":T.getDeepCopy(),"title":f"relabeling leaves"}]
			nxDrawStuff.doBigPicture(bigPictureMode_data,"generateRandomBinaryTree",withLabelsInsteafOfIds=bigPictureMode_withLabelsInsteafOfIds)

		return T

	def greedyNearestNeighborInterchange(self,verbose=False,bigPictureMode=False,bigPictureMode_withLabelsInsteafOfIds=True): 
		"""
		simple greedy heuristic to minimize parsimony score:
		swap inner nodes with _NearestNeighborsTreeProblem and take solutions that reduce the parsimony score (parsimonyScore)
		"""

		# deep-copy to T
		T=tree()
		T.adjacencyList_d = {i:{j:self.adjacencyList_d[i][j] for j in self.adjacencyList_d[i]} for i in self.adjacencyList_d}
		T.label = self.label
		T.housekeeping()
		#print(T.nodes)
		#print("ini",T)

		if bigPictureMode:
			bigPictureMode_data = []
			bigPictureMode_data += [{"T":T.getDeepCopy(),"title":f"start"}]

		T.SmallParsimony_Sankoff()

		score = float('inf')
		newscore = T.parsimonyScore()
		newT = T

		if bigPictureMode:
			bigPictureMode_data += [{"T":newT.getDeepCopy(),"title":f"do SmallParsimony_Sankoff, score:{newscore}"}]

		#print("A",len(T.nodes))
		
		while score > newscore:

			score = newscore
			T = newT
			if verbose:
				print("newscore:",newscore)
				print(newT)

			for i in range(len(T.nodes)):
				v=T.nodes[i]
				if v in T.leaves:
					continue
				for j in range(i,len(T.nodes)):
					w=T.nodes[j]
					if w in T.leaves:
						continue
					if w in T.adjacencyList_d[v]:

						A,B = T._NearestNeighborsTreeProblem(v,w)
						if bigPictureMode:
							A_unsankoffed = A.getDeepCopy()
							B_unsankoffed = B.getDeepCopy()
						A.SmallParsimony_Sankoff()
						A_score = A.parsimonyScore()
						B.SmallParsimony_Sankoff()
						B_score = B.parsimonyScore()
						
						if A_score < newscore:
							if bigPictureMode:
								bigPictureMode_data += [{"T":A_unsankoffed.getDeepCopy(),"title":f"at ({T.label[v]}-{T.label[w]}) interchange NN" if bigPictureMode_withLabelsInsteafOfIds else f"at ({v}-{w}) interchange NN"}]
								bigPictureMode_data += [{"T":A.getDeepCopy(),"title":f"do SmallParsimony_Sankoff, score:{A_score}"}]

							newscore = A_score
							newT = A

						if B_score < newscore:
							if bigPictureMode:
								bigPictureMode_data += [{"T":B_unsankoffed.getDeepCopy(),"title":f"at ({T.label[v]}-{T.label[w]}) interchange NN" if bigPictureMode_withLabelsInsteafOfIds else f"at ({v}-{w}) interchange NN"}]
								bigPictureMode_data += [{"T":B.getDeepCopy(),"title":f"do SmallParsimony_Sankoff, score:{B_score}"}]

							newscore = B_score
							newT = B
		if bigPictureMode:
			nxDrawStuff.doBigPicture(bigPictureMode_data,"greedyNearestNeighborInterchange",withLabelsInsteafOfIds=bigPictureMode_withLabelsInsteafOfIds)

		if verbose:
			print("newscore",newscore)
			print(newT)

		# depp-copy back from T
		self.adjacencyList_d = {i:{j:newT.adjacencyList_d[i][j] for j in newT.adjacencyList_d[i]} for i in newT.adjacencyList_d}
		self.label = newT.label
		self.housekeeping()

	def toNewick(self,cur_v=None):
		if self.root is None:
			self.root = [v for v in self.nodes if v not in self.leaves][0]
			self.housekeeping()
		if cur_v is None:
			return self.toNewick(self.root)
		else:
			inner=[]
			if cur_v in self.leaves:
				return f"{cur_v}"
			#print(cur_v,self.children[cur_v])
			for c in self.children[cur_v]:
				inner+=[f"{self.toNewick(c)}:{self.adjacencyList_d[c][cur_v]}"]
			return f"({','.join(inner)}){cur_v}"

	def _isInt(self):
		result = True 
		for v in self.nodes:
			for w in self.adjacencyList_d[v]:
				result = result and ( self.adjacencyList_d[v][w] is None or float(self.adjacencyList_d[v][w]) == float(int(self.adjacencyList_d[v][w])) )
		return result

	def _raw(self):
		if len(self.nodes)>1 and self.adjacencyList_d[self.nodes[0]][list(self.adjacencyList_d[self.nodes[0]].keys())[0]] is None:
			return "\n".join([ "\n".join([ f"{a}->{b}" for b in self.adjacencyList_d[a] ]) for a in self.adjacencyList_d ])
		if self._isInt():
			return "\n".join([ "\n".join([ f"{a}->{b}:"+"{}".format(self.adjacencyList_d[a][b]) for b in self.adjacencyList_d[a] ]) for a in self.adjacencyList_d ])
		return "\n".join([ "\n".join([ f"{a}->{b}:"+"{:.3f}".format(self.adjacencyList_d[a][b]) for b in self.adjacencyList_d[a] ]) for a in self.adjacencyList_d ])

	def __str__(self):
		if len(self.nodes)>1 and self.adjacencyList_d[self.nodes[0]][list(self.adjacencyList_d[self.nodes[0]].keys())[0]] is None:
			return "\n".join([ "\n".join([ f"{self.label[a] if a in self.label else a}->{self.label[b] if b in self.label else b}" for b in self.adjacencyList_d[a] ]) for a in self.adjacencyList_d ])
		if self._isInt():
			return "\n".join([ "\n".join([ f"{self.label[a] if a in self.label else a}->{self.label[b] if b in self.label else b}:"+"{}".format(self.adjacencyList_d[a][b]) for b in self.adjacencyList_d[a] ]) for a in self.adjacencyList_d ])
		return "\n".join([ "\n".join([ f"{self.label[a] if a in self.label else a}->{self.label[b] if b in self.label else b}:"+"{:.3f}".format(self.adjacencyList_d[a][b]) for b in self.adjacencyList_d[a] ]) for a in self.adjacencyList_d ])

	def __repr__(self):
		return str(self)

class LeafDistanceMatrix:

	def __init__(self,inputString:str=""):
		self.leafNames=[]
		D={}
		i=0
		for s in inputString.split("\n"):
			if s == "":
				continue
			j=0
			for x in re.split(r"\t| ",s):
				if x == "":
					continue
				try:
					x=float(x) # isnumeric does not work for floats idk ...
					if i not in D:
						D[i]={}
					D[i][j]=x
					j=j+1
				except ValueError:
					if i==0:
						self.leafNames=self.leafNames+[x]
			i=i+1
		if len(self.leafNames) < len(D):
			self.leafNames = [ str(x) for x in list(range(len(D))) ]
		minV = min([ D[i][j] for i in D for j in D[i] ])

		self.D = [ [ D[i][j]-minV for j in D[i] ] for i in D ]

		#print(minV,[ [ D[i][j]-minV for j in D[i] ] for i in D ])

	@classmethod
	def fromtree(cls,T:tree):
		D = cls()
		D.D = T.toLeaveDistMatrix()
		return D

	def LimbLength(self,j,max_n=-1):
		"""
		Input j integer (it of self.leaves)
		LimbLength(j) = minimum value of (D_{i,j} + D_{j,k} - D_{i,k})/2 over all pairs of leaves i and k
		"""
		LimbLength = -1
		one_i=-1
		if max_n==-1:
			max_n=len(self.D)
		for i in range(max_n):
			if i != j:
				one_i = i 
				break
		for k in range(max_n):
			if j==k:
				continue
			#print(i,j,k)
			val = (self.D[i][j] + self.D[j][k] - self.D[i][k])/2
			if LimbLength==-1 or LimbLength>val:
				LimbLength=val
		return LimbLength

	def AdditivePhylogeny(self,n=-1):
		"""
		exact tree reconstruction
		assumes that the Distance Matrix is additive (there exists a tree with Discrepancy 0)
		! fails if the input is non-additive !
		"""
		numberOfLeaves = len(self.D)
		if n == -1:
			n = len(self.D)-1
		if n == 1:
			T = tree(self.leafNames[0]+"->"+self.leafNames[1]+":"+str(self.D[0][1]))
			#print("return",T)
			return T
		ll = self.LimbLength(n,n)
		if ll < 1:
			return None
		for j in range(n):
			self.D[j][n] = self.D[j][n] - ll
			self.D[n][j] = self.D[j][n]
		i=-1
		k=-1
		breakFree = False
		for ii in range(n+1):
			for kk in range(ii+1,n+1):
				if self.D[ii][kk] == self.D[ii][n] + self.D[n][kk]:
					i = ii
					k = kk
					breakFree = True
					break
			if breakFree:
				break
		x = self.D[i][n]
		T = self.AdditivePhylogeny(n-1)
		v = T.getNodeAtDistance(self.leafNames[i],self.leafNames[k],x,numberOfLeaves)
		T.addLeaf(v,ll,self.leafNames[len(T.leaves)])
		return T

	def UPGMA(self,displayInnerNodesAsJoinedLeaves=True, bigPictureMode=False,verbose=False):
		"""
		if displayInnerNodesAsJoinedLeaves=False -> then inner nodes are just numbers from len(leaves)...numberOfNodes
		heuristic to reconstruct a rooted binary ultrametric tree
		1. n trivial cluster for each leaf
		2. join the clostest ones
			D=distance between clusters C1 and C2 as the average pairwise distance between elements of C1 and C2
		3. the age of the new cluster is age of C is D_C1_C2 /2
		! works for non-additive input but is kinda bad for additive input !
		returns a ROOTED tree
		assumption of a constant rate of evolution
		"""
		clusters = [ [i] for i in range(len(self.D)) ]
		T=tree("")

		age = { self.leafNames[x]:0 for x in range(len(self.D)) } # -> the constant evolution rate assumption
		
		cluster_D = { i:{ j:self.D[i][j] for j in range(len(self.D)) if i+1 <= j } for i in range(len(self.D)) }
		n=len(clusters)

		node2id = {}
		#print(cluster_D)
		node_c_str=""

		if bigPictureMode:
			bigPictureMode_data=[]

		while n > 1:
			if verbose:
				print("## n",n)
				print("\n".join([" ".join([ str(round(cluster_D[i][j],2)).rjust(4) if i+1 <= j else "-".rjust(4) for j in range(len(self.D))]) for i in range(len(self.D))]))
				print("clusters",clusters)
				print("D",cluster_D)

			min_D = None
			min_i = 0
			min_j = 0
			for i in range(len(clusters)):
				for j in range(i+1,len(clusters)):
					if cluster_D[i][j] > 0 and ( min_D is None or cluster_D[i][j] < min_D ):
						min_D = cluster_D[i][j]
						min_i = i
						min_j = j

			#print("min_D",min_D,"min_i",min_i,clusters[min_i],"min_j",min_j,clusters[min_j])

			node_a_str = "-".join([ "".join([ self.leafNames[int(k)] for k in str(x).split("-") ]) for x in clusters[min_i] ])
			node_b_str = "-".join([ "".join([ self.leafNames[int(k)] for k in str(x).split("-") ]) for x in clusters[min_j] ])
			node_c_str = "-".join([ "".join([ self.leafNames[int(k)] for k in str(x).split("-") ]) for x in clusters[min_i]+clusters[min_j] ])

			if not displayInnerNodesAsJoinedLeaves and node_a_str.find("-") != -1:
				if node_a_str not in node2id:
					node2id[node_a_str] = "v"+str(len(node2id)+len(clusters))
				node_a_str = node2id[node_a_str]
				
			if not displayInnerNodesAsJoinedLeaves and node_b_str.find("-") != -1:
				if node_b_str not in node2id:
					node2id[node_b_str] = "v"+str(len(node2id)+len(clusters))
				node_b_str = node2id[node_b_str]
				
			if not displayInnerNodesAsJoinedLeaves and node_c_str.find("-") != -1:
				if node_c_str not in node2id:
					node2id[node_c_str] = "v"+str(len(node2id)+len(clusters))
				node_c_str = node2id[node_c_str]
				
			clusters[min_i] = clusters[min_i]+clusters[min_j] 
			clusters[min_j] = []

			if verbose:
				print("a",node_a_str,"b",node_b_str,"c",node_c_str)

			for i in range(min_i):
				if i == min_i or i == min_j or len(clusters[i])==0: 
					continue
				sum_val = 0
				for k in clusters[i]:
					for l in clusters[min_i]:
						sum_val = sum_val + self.D[k][l]
				cluster_D[i][min_i] = sum_val / (len(clusters[i])*len(clusters[min_i]))
			for j in range(min_i+1,len(clusters)):
				if j == min_j or j == min_i or len(clusters[j])==0: 
					continue
				sum_val = 0
				for k in clusters[min_i]:
					for l in clusters[j]:
						sum_val = sum_val + self.D[k][l]
				cluster_D[min_i][j] = sum_val / (len(clusters[min_i])*len(clusters[j]))

			for i in range(len(self.D)):
				cluster_D[i][min_j] = -1
				cluster_D[min_j][i] = -1

			age[node_c_str] = min_D/2

			T.addEdge(node_a_str,node_c_str,min_D/2-age[node_a_str])
			T.addEdge(node_b_str,node_c_str,min_D/2-age[node_b_str])

			if verbose:
				print("newly added cluster:", clusters[min_i])

			n=0
			for c in clusters:
				if len(c) > 0:
					n = n + 1

			if bigPictureMode:
				bigPictureMode_data += [{"T":T.getDeepCopy(),"title":f"{node_c_str}"}]

		T.root = node_c_str
		T.housekeeping()

		if bigPictureMode:
			bigPictureMode_data += [{"T":T.getDeepCopy(),"title":f"done"}]

		if bigPictureMode:
			nxDrawStuff.doBigPicture(bigPictureMode_data,"UPGMA")

		return T

	def get_DStar(self):
		cluster_D = { i:{ j:self.D[i][j] for j in range(len(self.D)) if i+1 <= j } for i in range(len(self.D)) }

		TotalDistance = {i:0 for i in range(len(cluster_D))}
		for i in range(len(cluster_D)):
			for j in range(len(cluster_D)):
				if i==j:
					continue
				cur_d = cluster_D[i][j] if i < j else cluster_D[j][i] 
				if cur_d is None:
					continue
				TotalDistance[i] = TotalDistance[i] + cur_d

		cluster_DSTAR = {i:{} for i in range(len(cluster_D))}
		for i in range(len(cluster_D)):
			for j in range(i+1,len(cluster_D)):
				if i==j and cluster_D[i][j] > 0:
					continue
				if cluster_D[i][j] is None:
					cluster_DSTAR[i][j]=0
					continue
				cluster_DSTAR[i][j] = ( len(cluster_D) - 2 ) * cluster_D[i][j]  - TotalDistance[i] - TotalDistance[j]
		return cluster_DSTAR

	def NJ(self,displayInnerNodesAsJoinedLeaves=True,bigPictureMode=False,verbose=False):
		"""
		heuristic to reconstruct a rooted binary ultrametric tree
		refinment of the UPGMA -> solves bettwer for additive distance matrices D
		! works for non-additive input and is also good additive input !
		returns a UNrooted tree since it does not require the constant evolution rate assumption (if you want a root use a outgroup or UPGMA)
		"""
		clusters = [ [i] for i in range(len(self.D)) ]
		T=tree("")
		
		cluster_D = { i:{ j:self.D[i][j] for j in range(len(self.D)) if i+1 <= j } for i in range(len(self.D)) }
		n=len(clusters)

		#print("D",cluster_D)
		node2id = {}

		if bigPictureMode:
			bigPictureMode_data=[]

		while n > 2:
			#print("WHILE")
			if verbose:
				print("## n",n)
				print("D",cluster_D)
				#print("\n".join([" ".join([ str(round(cluster_D[i][j],2)).rjust(4) if i+1 <= j else "-".rjust(4) for j in range(len(self.D))]) for i in range(len(self.D))]))
				print("clusters",clusters)

			TotalDistance = {i:0 for i in range(len(clusters))}
			for i in range(len(clusters)):
				for j in range(len(clusters)):
					if i==j:
						continue
					cur_d = cluster_D[i][j] if i < j else cluster_D[j][i] 
					if cur_d is None:
						continue
					TotalDistance[i] = TotalDistance[i] + cur_d

			cluster_DSTAR = {i:{} for i in range(len(clusters))}
			for i in range(len(clusters)):
				for j in range(i+1,len(clusters)):
					if i==j and cluster_D[i][j] > 0:
						continue
					if cluster_D[i][j] is None:
						cluster_DSTAR[i][j]=0
						continue
					cluster_DSTAR[i][j] = ( n - 2 ) * cluster_D[i][j]  - TotalDistance[i] - TotalDistance[j]

			#print("TotalDistance: ",TotalDistance)
			if verbose:
				print("cluster_DSTAR\n","\n".join([" ".join([ str(round(cluster_DSTAR[i][j],2)).rjust(4) if i+1 <= j else "-".rjust(4) for j in range(len(clusters))]) for i in range(len(clusters))]))

			# find elements i and j such that D*i,j is a minimum non-diagonal element of

			min_DELTA = None
			min_D = None
			min_i = 0
			min_j = 0
			for i in range(len(clusters)):
				for j in range(i+1,len(clusters)):
					if ( min_D is None or cluster_DSTAR[i][j] < min_DELTA ):
						min_DELTA = cluster_DSTAR[i][j]
						min_D = cluster_D[i][j]
						min_i = i
						min_j = j

			if verbose:
				print("min_D",min_D,"min_DELTA",min_DELTA,"min_i",min_i,clusters[min_i],"min_j",min_j,clusters[min_j])

			node_a_str = "-".join([ "".join([self.leafNames[int(k)] for k in str(x).split("-")]) for x in clusters[min_i] ])
			node_b_str = "-".join([ "".join([self.leafNames[int(k)] for k in str(x).split("-")]) for x in clusters[min_j] ])
			node_c_str = "-".join([ "".join([self.leafNames[int(k)] for k in str(x).split("-")]) for x in clusters[min_i]+clusters[min_j] ])

			if not displayInnerNodesAsJoinedLeaves and node_a_str.find("-") != -1:
				if node_a_str not in node2id:
					node2id[node_a_str] = "v"+str(len(node2id)+len(clusters))
				node_a_str = node2id[node_a_str]
				
			if not displayInnerNodesAsJoinedLeaves and node_b_str.find("-") != -1:
				if node_b_str not in node2id:
					node2id[node_b_str] = "v"+str(len(node2id)+len(clusters))
				node_b_str = node2id[node_b_str]
				
			if not displayInnerNodesAsJoinedLeaves and node_c_str.find("-") != -1:
				if node_c_str not in node2id:
					node2id[node_c_str] = "v"+str(len(node2id)+len(clusters))
				node_c_str = node2id[node_c_str]
				
			clusters[min_i] = clusters[min_i]+clusters[min_j] 
			clusters[min_j] = []

			delta_t = (TotalDistance[min_i] - TotalDistance[min_j]) / ( n -2 )
			#print(min_i,min_j,cluster_D[min_i][min_j])
			limbLengthi = (1/2)*( cluster_D[min_i][min_j] + delta_t )
			limbLengthj = cluster_D[min_i][min_j] - limbLengthi
			#print("limbLengthi",limbLengthi)
			#print("limbLengthj",limbLengthj)

			#limbLengthi = self.LimbLength(min_i,len(clusters))
			#limbLengthj = self.LimbLength(min_j,len(clusters))

			for k in range(len(clusters)):
				if k == min_i or k == min_j or len(clusters[k])==0: 
					continue
				a = cluster_D[min_i][k] if min_i<k else cluster_D[k][min_i]
				b = cluster_D[min_j][k] if min_j<k else cluster_D[k][min_j]
				if k < min_i:
					cluster_D[k][min_i] = 0.5 * ( a + b - cluster_D[min_i][min_j] )
				else:
					cluster_D[min_i][k] = 0.5 * ( a + b - cluster_D[min_i][min_j] )

			for i in range(len(clusters)):
				cluster_D[i][min_j] = None
				cluster_D[min_j][i] = None

			T.addEdge(node_a_str,node_c_str,limbLengthi)
			T.addEdge(node_b_str,node_c_str,limbLengthj)

			n=0
			for c in clusters:
				if len(c) > 0:
					n = n + 1

			if bigPictureMode:
				bigPictureMode_data += [{"T":T.getDeepCopy(),"title":f"{node_c_str}"}]

			#print("DONE:")
			if verbose:
				#print(T)
				print(cluster_D)
			#print(self)
			#print("")

		if verbose:
			print("done with while loop")
		# add last edge
		min_i = 0
		min_j = 0
		min_D = None
		for i in range(len(clusters)):
			for j in range(i+1,len(clusters)):
				if ( min_D is None or cluster_D[i][j] is not None ):
					min_D = cluster_D[i][j]
					min_i = i
					min_j = j

		node_a_str = "-".join([ "".join([self.leafNames[int(k)] for k in str(x).split("-")]) for x in clusters[min_i] ])
		node_b_str = "-".join([ "".join([self.leafNames[int(k)] for k in str(x).split("-")]) for x in clusters[min_j] ])

		if not displayInnerNodesAsJoinedLeaves and node_a_str.find("-") != -1:
			if node_a_str not in node2id:
				node2id[node_a_str] = str(len(node2id)+len(clusters))
			node_a_str = node2id[node_a_str]
			
		if not displayInnerNodesAsJoinedLeaves and node_b_str.find("-") != -1:
			if node_b_str not in node2id:
				node2id[node_b_str] = str(len(node2id)+len(clusters))
			node_b_str = node2id[node_b_str]
			
		if verbose:
			print(f"adding {node_a_str}-{node_b_str} with {min_D}")
		T.addEdge(node_a_str,node_b_str,min_D)
		# housekeeping
		T.housekeeping()

		if bigPictureMode:
			bigPictureMode_data += [{"T":T.getDeepCopy(),"title":f"{node_c_str}"}]

		if bigPictureMode:
			nxDrawStuff.doBigPicture(bigPictureMode_data,"NJ")

		return T

	def Discrepancy(self,T:tree):
		"""
		Discrepancy(T, D)=sum_ij(d(T)_ij - D_ij)^2
		"""
		res = 0
		for i in range(len(self.D)):
			for j in range(i+1,len(self.D)):
				#print(i,j,self.leafNames[i], self.leafNames[j],self.D[i][j])
				#print(self.leafNames[i], self.leafNames[j], T.pathDist(self.leafNames[i],self.leafNames[j]), self.D[i][j])
				res = res + ( T.pathDist(self.leafNames[i],self.leafNames[j]) - self.D[i][j] )**2
		return res

	def __str__(self):
		return "\n".join([" ".join([str(self.D[v][w]) for w in range(len(self.D))]) for v in range(len(self.D))])
	def __repr__(self):
		return str(self)

class nxDrawStuff:

	@staticmethod
	def draw(T:tree,withLabelsInsteafOfIds=True,show=True,pos=None,output_file_name=""):

		G = extra_packages["networkx"].DiGraph()
		G.add_edges_from([(a,b) for a in T.adjacencyList_d for b in T.adjacencyList_d[a] ])

		node_labels = { v:(T.label[v][1:min([20,len(T.label[v])]) ] if v in T.label and withLabelsInsteafOfIds else str(v)) for v in T.nodes }
		edge_labels = dict([((a,b,),round(T.adjacencyList_d[a][b],3) if T.adjacencyList_d[a][b] is not None else "" ) for a,b,k in G.edges(data=True)])
		
		if pos is None:
			if len(T.adjacencyList_d)<300:
				pos = extra_packages["networkx"].drawing.nx_agraph.graphviz_layout(G,prog="dot")
			else:
				pos = extra_packages["networkx"].drawing.nx_agraph.graphviz_layout(G,prog="fdp")
		
		extra_packages["networkx"].draw_networkx_edge_labels(G,pos,edge_labels=edge_labels,font_size=5)
		extra_packages["networkx"].draw(G, pos, 
			node_size=150, 
			edge_cmap=extra_packages["matplotlib.pyplot"].pyplot.cm.Reds,
			arrows=None, 
			node_color=["#aba96a" if f == T.root else "#FFFFFF" for f in list(G)] if T.root is not None else "#FFFFFF")		

		extra_packages["networkx"].draw_networkx_labels(G, pos,labels=node_labels,font_size=8)

		if show:
			extra_packages["matplotlib.pyplot"].pyplot.show()
		else:
			if output_file_name != "" or output_file_name == None:
				output_file_name = "output_draw"
			extra_packages["matplotlib.pyplot"].pyplot.savefig(f"{output_file_name}.pdf")
			print("saved to output_draw.pdf")

		return pos,G,node_labels,edge_labels

	@staticmethod
	def doBigPicture(bigPictureMode_data,title="",colorDifferences=True,withLabelsInsteafOfIds=True):
		rows = math.floor( math.sqrt( len(bigPictureMode_data) ) )
		cols = rows
		while rows*cols < len(bigPictureMode_data):
			rows+=1
		print(f"n:{len(bigPictureMode_data)} -> rows:{rows} cols:{cols}")

		if len(bigPictureMode_data) == 1:
			nxDrawStuff.draw(bigPictureMode_data[idx]["T"])
			return;

		# adapted from: https://stackoverflow.com/questions/43646550/how-to-use-an-update-function-to-animate-a-networkx-graph-in-matplotlib-2-0-0
		last_node_labels={}
		last_edge_labels={}
		fig, axes = extra_packages["matplotlib.pyplot"].pyplot.subplots(ncols=cols, nrows=rows, figsize=(10,5))
		fig.suptitle(title, fontsize=20)
		for idx in range(rows*cols):
			if idx >= len(bigPictureMode_data):
				ax.set_xticks([])
				ax.set_yticks([])
				ax.set_xticks([])
				ax.set_yticks([])
				continue
			print(f"{idx}: "+bigPictureMode_data[idx]["title"])
			row_idx = math.floor( idx / cols ) 
			col_idx = idx % cols # {True: (0,idx), False: (1,idx - cols)}[idx < cols]
			T = bigPictureMode_data[idx]["T"]
			font_size_fct = 1
			if "font_size_fct" in bigPictureMode_data[idx]:
				font_size_fct=bigPictureMode_data[idx]["font_size_fct"]
			G = extra_packages["networkx"].DiGraph()
			G.add_edges_from([(a,b) for a in T.adjacencyList_d for b in T.adjacencyList_d[a] ])
			node_labels = { v:(T.label[v] if v in T.label and withLabelsInsteafOfIds else str(v)) for v in T.nodes }
			edge_labels = dict([((a,b,),round(T.adjacencyList_d[a][b],1) if T.adjacencyList_d[a][b] is not None else "" ) for a,b,k in G.edges(data=True)])
			pos = extra_packages["networkx"].drawing.nx_agraph.graphviz_layout(G, prog="dot")
			if cols==1:
				ax = axes[row_idx]
			else:
				ax = axes[row_idx][col_idx]
			extra_packages["networkx"].draw_networkx_edge_labels(G, pos, ax=ax, edge_labels=edge_labels, font_size=5*font_size_fct)
			extra_packages["networkx"].draw(G, pos, ax=ax,  node_size=150, edge_color=["black" if len(last_edge_labels)==0 or ( (a,b) in last_edge_labels and edge_labels[(a,b)] == last_edge_labels[(a,b)]) or not colorDifferences else "red" for a,b in G.edges()], edge_cmap=extra_packages["matplotlib.pyplot"].pyplot.cm.Reds, arrows=None, node_color=["#FFFFFF" if str(v) != str(T.root) else "#aba96a" for v in list(G)] )
			if len(last_node_labels)==0 or colorDifferences:
				extra_packages["networkx"].draw_networkx_labels(G, pos, ax=ax, font_color="red", labels={v:node_labels[v] for v in list(G) if v not in last_node_labels or last_node_labels[v]!=node_labels[v]}, font_size=8*font_size_fct)
			extra_packages["networkx"].draw_networkx_labels(G, pos, ax=ax, labels={v:node_labels[v] for v in list(G) if len(last_node_labels)==0 or ( v in last_node_labels and last_node_labels[v]==node_labels[v]) or not colorDifferences}, font_size=8*font_size_fct)
			ax.set_title(f"({idx}) "+bigPictureMode_data[idx]["title"], fontweight="bold", fontsize=10)
			ax.set_xticks([])
			ax.set_yticks([])
			last_node_labels = copy.deepcopy(node_labels)
			last_edge_labels = copy.deepcopy(edge_labels)
		extra_packages["matplotlib.pyplot"].pyplot.show()


def test():
	inputString="""0->4:11
1->4:2
2->5:6
3->5:7
4->0:11
4->1:2
4->5:4
5->4:4
5->3:7
5->2:6"""
	T=tree(inputString)
	print("len(T.leaves)==4 :",len(T.leaves)==4 or exit("test failed"))
	
	inputString="""Cow	Pig	Horse	Mouse	Dog	Cat	Turkey	Civet	Human
Cow	0	295	306	497	1081	1091	1003	956	954
Pig	295	0	309	500	1084	1094	1006	959	957
Horse	306	309     0	489	1073	1083	995	948	946
Mouse	497	500	489	0	1092	1102	1014	967	965
Dog	1081	1084	1073	1092	0	818	1056	1053	1051
Cat	1091	1094	1083	1102	818	0	1066	1063	1061
Turkey	1003	1006	995	1014	1056	1066	0	975	973
Civet	956	959	948	967	1053	1063	975	0	16
Human	954	957	946	965	1051	1061	973	16	0"""
	D=LeafDistanceMatrix(inputString)
	D_ini=LeafDistanceMatrix(inputString)

	print("Corona.LimbLength(1) == 149 :",D.LimbLength(1)==149 or exit("test failed") )

	T = D.AdditivePhylogeny()
	print("Discrepancy (should be 0, since the matrix was additive) :", D_ini.Discrepancy(T) == 0 or exit("test failed"))

	inputString="""Cow	Pig	Horse	Mouse	Dog	Cat	Turkey	Civet	Human
Cow	0	295	300	524	1077	1080	978	941	940
Pig	295	0	314	487	1071	1088	1010	963	966
Horse	300	314	0	472	1085	1088	1025	965	956
Mouse	524	487	472	0	1101	1099	1021	962	965
Dog	1076	1070	1085	1101	0	818	1053	1057	1054
Cat	1082	1088	1088	1098	818	0	1070	1085	1080
Turkey	976	1011	1025	1021	1053	1070	0	963	961
Civet	941	963	965	962	1057	1085	963	0	16
Human	940	966	956	965	1054	1080	961	16	0
"""
	D=LeafDistanceMatrix(inputString)

	T=D.UPGMA()
	print("Corona.UPGMA.Discrepancy >9000 :",D.Discrepancy(T)>9000 or exit("test failed"))
	T=D.NJ()
	print("Corona.NJ.Discrepancy >5000 :",D.Discrepancy(T)>5000 or exit("test failed"))

	inputStringtestA="""0 13 21 22
13 0 12 13
21 12 0 13
22 13 13 0
"""
	D=LeafDistanceMatrix(inputStringtestA)
	T=D.UPGMA()
	print("UPGMA Discrepancy of testA >0 :",D.Discrepancy(T)>0 or exit("test failed"))

	D=LeafDistanceMatrix(inputStringtestA)
	T=D.NJ()
	print("NJ Discrepancy of testA =0 :",D.Discrepancy(T)==0 or exit("test failed"))

	inputString="""i j k l
i 0 14 17 17
j 14 0 7 13
k 17 7 0 16
l 17 13 16 0"""
	D=LeafDistanceMatrix(inputString)

	print("LimbLength(1)==2 :",D.LimbLength(1)==2 or exit("test failed"))

	inputString="""i j k l
i 0 13 16 10
j 13 0 21 15
k 16 21 0 18
l 10 15 18 0"""
	D=LeafDistanceMatrix(inputString)
	print("get_DStar of 2<->3 == -62 :", D.get_DStar()[2][3] == -62 or exit("test failed"))


	inputString="""i j k l
i 0 14 17 17
j 14 0 13 7
k 17 13 0 20
l 17 7 20 0"""
	D=LeafDistanceMatrix(inputString)
	T=tree("")
	T.addEdge("j","1",2)
	T.addEdge("l","1",6)
	T.addEdge("k","2",8)
	T.addEdge("i","2",9)
	T.addEdge("1","2",4)
	T.housekeeping()
	print("Discrepancy of test mat == 11 :", D.Discrepancy(T) == 11 or exit("test failed"))


	inputString="""4->CAAATCCC
4->ATTGCGAC
5->CTGCGCTG
5->ATGGACGA
6->4
6->5"""
	T=tree(inputString,root=6)
	T.SmallParsimony_Sankoff()
	print("SmallParsimony_Sankoff(rooted tree) -> T.parsimonyScore() == 16: ",T.parsimonyScore() == 16 or exit("test failed"))
	
	inputString="""TCGGCCAA->4
4->TCGGCCAA
CCTGGCTG->4
4->CCTGGCTG
CACAGGAT->5
5->CACAGGAT
TGAGTACC->5
5->TGAGTACC
4->5
5->4"""
	T=tree(inputString)
	T.SmallParsimony_Sankoff()
	print("SmallParsimony_Sankoff(unrooted tree) ->T.parsimonyScore() == 17: ",T.parsimonyScore() == 17 or exit("test failed"))

	inputString="""GCAGGGTA->5
TTTACGCG->5
CGACCTGA->6
GATTCCAC->6
5->TTTACGCG
5->GCAGGGTA
5->7
TCCGTAGT->7
7->5
7->6
7->TCCGTAGT
6->GATTCCAC
6->CGACCTGA
6->7"""
	T=tree(inputString)
	T.greedyNearestNeighborInterchange()
	print("given tree + greedy minParsimony == 21 :", T.parsimonyScore() == 21 or exit("test failed"))

	TB = tree.largeParsimony(["GCAGGGTA","TTTACGCG","CGACCTGA","GATTCCAC","TCCGTAGT"])
	print("largeParsimony == 21 :", TB.parsimonyScore() == 21 or exit("test failed"))

	TB = tree.largeParsimony(["test","lul","123","okok","number5"],["GCAGGGTA","TTTACGCG","CGACCTGA","GATTCCAC","TCCGTAGT"])
	print("largeParsimony with species names == 21 :", TB.parsimonyScore() == 21 or exit("test failed"))

	exit(0)

def main():
	parser = OptionParser(usage="usage: %prog [options] 'INPUTFILE'\nyou can use 'cat << EOF | phylogeny.py' to directly read from terminal", version=f"%prog {version}")
	parser.add_option("-t", "--type", default="dist", help="(input file type) dist : leaf distance matrix. adj : adjacency list of a tree (each row represents an edge in the form a->b if unweighted or a->b|10 if weighted). aln: fasta-alignment (used for --work largeParsimony) [default: %default]")
	parser.add_option("-w", "--work", default="NJ", help="work of type=dist: 'NJ','UPGMA': clustering algorithms. 'AdditivePhylogeny': input matrix needs to be additive. 'generateRandomBinaryTree','generateRandomRootedBinaryTree': tree generation (needs --leaves and maybe --labels). 'smallParsimony': generate edge weights with minimal parsimony score. 'greedyNearestNeighborInterchange': change nearest neighbors and apply smallParsimony. 'largeParsimony': build a random binary tree and perform greedyNearestNeighborInterchange. [default: %default]")
	parser.add_option("--leaves", default="", help="mandatory for --work largeParsimony or generateRandomBinaryTree. The leave node ids (species names)")
	parser.add_option("--labels", default="", help="optional for --work largeParsimony or generateRandomBinaryTree. The leave node labels (sequences)")
	parser.add_option("-d", "--draw", action="store_true", default=False, help="generate a nx plot of the output tree")
	parser.add_option("--largeParsimonyRestarts", default=10, help="the number of initially randomly sampled random binary trees")
	parser.add_option("--drawIDs", action="store_true", default=False, help="draw node ids instead of labels (usually species names instead of sequences)")
	parser.add_option("--verbose", action="store_true", dest="verbose")
	parser.add_option("-v", action="store_true", dest="version")
	parser.add_option("-x", "--test", default=False, action="store_true")
	parser.add_option("--bigpicture", default=False, action="store_true",help="outputs a step-by-step picture for the algorithm")
	parser.add_option("--innerNodesJoin", default=False, action="store_true",help="if set inner nodes are displayed as the joined names from the leaves (for NJ and UPGMA)")
	parser.add_option("--example", default="",help="perform various examples (combine this with --draw and/or --bigpicture): 'corona': do a NJ of a distance matrix based on the corona virus genome found in 9 animals. 'HIV': calculate the largeParsimony of another HIV protein of the 20 patients of 'State of Louisiana vs. Richard Schmidt' trial of 1998 (~10min). 'adh': generate largeParsimony of the alcohol dehydrogenase (adh) protein of 12 yeast species (~3min).")
	parser.add_option("--discrepancy", default=False, action="store_true",help="calculate discrepancy between the generated tree and the input distance matrix for work 'NJ','UPGMA','AdditivePhylogeny'")
	parser.add_option("--parsimony", default=False, action="store_true",help="calculate parsimony score for the generated tree (sum of all edge weights)")
	(options, args) = parser.parse_args()

	if options.version:
		print(f"{version}")
		exit(0)

	if options.test:
		test()
		exit(0)

	if options.bigpicture or options.draw:	
		for cur_pks in list(extra_packages.keys()):
			try:
				extra_packages[cur_pks] = __import__(cur_pks)
			except ImportError:
				parser.error(f"Did not found '{cur_pks}', this package is essential for --bigpicture and --draw...")

	if options.example in ["HIV","corona","adh"]:
		if options.example == "HIV":
			"""
			the HIV crime
			"""
			hiv_prot = ["PISPIETVPVKLKPGMDGPKVKQWPLTEEKIKALVEICTEMEKEGKISKIGPENPYNTPVFAIKKKDSTKWRKLVDFRELNKRTQDFWEVQLGIPHPAGLKKKKSVTVLDVGDAYFSVPLDKEFRKYTAFTIPSINNETPGIRYQYNVLPQGWKGSPAIFQSSMTKILEPFRKQNPDIVIYQYMDDLYVGSDLEIGQHRIKIEELRQHLLKWGLTTPDKKHKKEPPFLW","PISPIETVPVKLKPGMDGPKVKQWPLTEEKIKALVEICTEMEKEGKISKIGPENPYNTPVFAIKKKDSTKWRKLVDFRELNKRTQDFWEVQLGIPHPAGLKKKKSVTVLDVGDAYFSVPLDKEFRKYTAFTIPSINNETPGIRYQYNVLPQGWKGSPAIFQSSMTKILEPFRKQNPDIVIYQYMDDLYVGSDLEIGQHRIKIEELRQHLLKWGLTTPDKKHQKEPPFLW","PISPIETVPVKLKPGMDGPKVKQWPLTEEKIKALVEICTEMEKEGKISKIGPENPYNTPVFAIKKKNSTRWRKLVDFRELNKRTQDFWEVQLGIPHPAGLKKKKSVTVLDVGDAYFSVPLDKEFRKYTAFTIPSINNETPGIRYQYNVLPQGWKGSPAIFQSSMTKILEPFRKQNPDIVIYQYMDDLYVGSDLEIGQHRIKIEELRQHLLKWGFITPDEKHQKEPPFRW","PISPIETVPVKLKPGMDGPKVKQWPLTEEKIKALVEICTEMEKEGKISKIGPENPYNTPVFAIKKKNSTRWRKLVDFRELNKRTQDFWEVQLGIPHPAGLKKKKSVTVLDVGDAYFSVPLDKEFRKYTAFTIPSINNETPGIRYQYNVLPQGWKGSPAIFQSSMTKILEPFRKQNPDIVIYQYMDDLYVGSDLEIGQHRIKTEELRQHLLKWGFFTPDEKHQKEPPFRW","PISPIETVPVKLKPGMDGPKVKQWPLTEEKIKALVEICTEMEKEGKISKIGPENPYNTPVFAIKKKNSTRWRKLVDFRELNKRTQDFWEVQLGIPHPAGLKKKKSVTVLDVGDAYFSVPLDKEFRKYTAFTIPSINNETPGIRYQYNVLPQGWKGSPAIFQSSMTKILEPFRKQNPDIVIYQYMDDLYVGSDLEIGQHRIKTEELRQHLLKWGFFTPDEKHQKEPPFRW","PISPIETVPVKLKPGMDGPRVKQWPLTEEKIKALVEICTELEQXGKISKIGPENPYNTPVFAIKKKNSDKWRKLVDFRELNKRTQDFXEVQLGIPHPGGLKKKKSVTVLDVGDAYFSIPLDEDFRKYTAFTIPSINNETPGIRYQYNVLPQGWKGSPAIFQSSMTKILEPFRKQNPDIVIYQYVDDLYVGSDLEIEQHRTKIXEFRQYLYKWGFYTPDRKYQKEPPFLW","PISPIETVPVKLKPGMDGPKVKQWPLTEEKIKALVEICTELEKXGKISKIGPENPYNTPVFAIKKKDSTKWRKLVDFRELNKRTQDFWEVQLGIPHPAGLKKKKSVTVLDVGDAYFSXPLDEXFRKYTAFTIPSINNETPGIRYQYNVLPQGWKGSPAIFQSSMTXILEPFRXQNPDIVIYQYMDDLYVGSDLEIXQHRXKIEELRQHLWXWGFYTPDKKHQKEPPFLW","PISPIETVPVKLKPGMDGPKVKQWPLTEEKIKALVEICTEMEKEGKISKIGPENPYNTPVFAIKKKDSTKWRKLVDFRELNKKTQDFWEVQLGIPHPAGLKKKKSVTVLDVGDAYFSVPLDKDFRKYTAFTIPSINNETPGIRYQYNVLPQGWKGSPAIFQSSMTKILEPFRKQNPDIVIYQYMDDLYVGSDLEIGQHRTKIEELRQHLLRWGFTTPDKKHQKEPPFLW","PISPIDTVPVKLKPGMDGPKVKQWPLTEEKIKALVEICAELEKXGKISKIGPENPYNTPVFAIKKKDXTKWRKLVDFRELNKRTQDFWEVQLGIPHPAGLKKKKSVTVLDVGDAYFSVPLXEDFRKYTAFTIPSTNNETPGIRYQYNVLPQGWKGSPAIFQCSMTKILEPFRKQNPDIVIYQYVDDLYVGSDLEIEQHRTKIEELRQHLWRWGFYTPDKKHQKEPPFLW","PISPIETVPVKLKPGMDGPKVKQWPLTEEKIKALVEICTEMEKEGKISKIXPENPYNTPVFAIKKKDSTKWRKLVDFRELNKRTQDFXEVQLXIPHPAGLKKKKSVTVLDVGDAYFSVPLDEDFRKYTAFTIPSTNNETPGVRYQYNVLPQGWKGSPAIFQSSMTKILEPFRKQHPDXVIYQYMDDLYVGSDLEIEQHRTKIEELRQHLLRWGFTTPDKKHQKXPPFLW","PISPIETVPVKLKPGMDGPKVKQWPLTEEKIKALVEICTEMEKEGKISKIGPENPYNTPVFAIKKKDSTKWRKLVDFRELNKRTQDFWEVQLGIPHPAGLKKKKSVTVLDVGDAYFSVPLDXDFRKYTAFTIPSINNETPGIRYQYNVLPQGWKGSPAIFQSSMTKILEPFRKHNPEIVIYQYVDDLYVGSDLEIGQHRTKIEELRQHLLRWGFXTPDKKHQKEPPFLW","PISPIETVPVKLKPGMDGPRVKQWPLTEEKIKALVEICXELEKEGKISKIGPENPYNTPVFAIKKKDSTXWRKLVDFRELNKRTQDFWEVQLGIPHPAGLKKKXSVTVLDVGDAYFSVPLDKDFRKYTAFTIPSINNETPGIRYQYNVLPQGWKGSPAIFQSSMTKILEPFRKQNPDIIIYQYMDDLYVGSDLDIGQHRTKXEELRQHLLRWGFYTPDKKHQKEPPFLW","PISPIETVPVKLKPGMDGPKVKQWPLTEEKIKALVEICTEMEKEGKISKIGPENPYNTPVFAIKKKXSTKWRKLVDFRELNKRTQDFWEVQLGIPHPAGLKKKKSVTVLDVGDAYFSVPLDKDFRKYTAFTIPSINNETPGIRYQYNVLPQGWKGSPAIFQSSMTKILEPFRKQNPDIVIYQYMDDLYVGSDLEIGQHRTKIEELRQHLLKWGFTTPDKKHQKEPPFLW","PISPIETVPVKLKPGMDGPRVKQWPLTEEKIKALVEICTEMEKEGKISKIGPENPYNTPVFAIKKKDSTKWRKLVDFRELNKRTQDFWEVQLGIPHPAGLKKKKSVTVLDVGDAYFSVPLDEDFRKYTAFTIPSVNNETPGIRYQYNVLPQGWKGSPAIFQCSMTKILEPFRKQNPDIVIYQYMDDLYVGSDLEIGQHRTKIEELRQHLLKWGFXTPDKKHQKEPPFLW","PISPIETVPVKLKPGMDGPKVKQWPLTEEKIKALVEICTEMEKEGKISKIGPENPYNTPIFAIKKKDSTKWRKLVDFRELNKKTQDFWEVQLGIPHPAGLKKKKSVTVLDVGDAYFSVPLDKDFRKYTAFTIPSINNETPGIRYQYNVLPQGWKGSPAIFQSSMTKILEPFRKQNPDIVIYQYMDDLYVGSDLEIGQHRTKIEELRQHLLKWGFTTPDKKHQKEPPFLW","PISPIETVPVKLKPGMDGPKVKQWPLTEEKIRALVEICTELEKEGKISKIGPENPYNTPVFAIKKKNSNRWRKLVDFRELNKRTQDFWEVQLGIPHPAGLKKKKSVTVLDVGDAYFSVPLDKDFRKYTAFTIPSVNNETPGIRYQYNVLPQGWKGSPALFQSSMTKILEPFRKQNPDIVIYQYVDDLYVGSDLEIGQHRTKTEELRQHLLRWGFFTPDEKHQKEPPFRW","PISPIETVPVKLKPGMDGPKVKQWPLTEEKIKALVEICTEMEKEGKISKIGPENPYNTPVFAIKKKDSTKWRKLVDFRELNKRTQDFWEVQLGIPHPAGLKKKKSVTVLDVGDAYFSVPLDKDFRKYTAFTIPSTNNETPGIRYQYNVLPQGWKGSPAIFQSSMTRILEPFRKQNPDIVIYQYMDDLYVGSDLEIGQHRTKIEELRQHLLRWGXTTPDXKHQKEPPFLW","PISPIXTVPVKLKPGMDGPKVKQWPLTEEKIKALVEICTEMEKEGKISKIGPENPYNTPIFAIKKKDSSKWRKLVDFRELNKKTQDFWEVQLGIPHPAGLKKKKSVTVLDVGDAYFSVPLDKDFRKYTAFTIPSINNETPGIRYQYNVLPQGWKGSPAIFQSSMTKILEPFRKQNPDIVIYQYMDDLYVGSDLEIGQHRTKIEELRQHLLKWGFTTPDKKHQKEPPFLW","PISPIETVPVKLKPGMDGPKVKQWPLTEEKIKALVEICAELEQXGKISKIGPENPYNTPVFAIKKKDSTKWRKLXDFRELNKRTQDFWEVQLGIPHPAGLKKKKSVTVLDVGDAYFSXPLXEDFRKYTAFTIPSXNNATPGIRYQYNVLPQGWKGSPAIFQSSMTKILEPFRKQNPXXXIYQYXDDLYVGSDLEIXQHRTKIXXLREHLWKWGFYTPDKKHQKEPPFLW","PISPIETVPVKLKPGMDGPKVKQWPLTEEKIKALVEICTEMEKEGKISKIGPENPYNTPVFAIKKKDSTKWRKLVDFRELNKKTQDFWEVQLGIPHPAGLKKKKSVTVLDVGDAYFSVPLDKDFRKYTAFTIPSINNETPGIRYQYNVLPQGWKGSPAIFQSSMIKILEPFRKQNPDIVIYQYMDDLYVGSDLEIEXHRXKIEELRQHLLRWGFTTPDKKHQKEPPFLW"]
			hiv_prot_patients = ["DM1","DM2","DM3","JT1","JT2","P1","P2","P3","P4","P5","P6","P7","P8","P9","P10","P11","P12","P13","P14","P15"]
			for i in range(len(hiv_prot)):
				print(">"+hiv_prot_patients[i]+"\n"+hiv_prot[i])
			T = tree.largeParsimony(hiv_prot_patients,hiv_prot,bigPictureMode=options.bigpicture,bigPictureMode_withLabelsInsteafOfIds=False,restarts=options.largeParsimonyRestarts)
			print("T.parsimonyScore():",T.parsimonyScore())
			if options.draw:
				nxDrawStuff.draw(T,show=True,withLabelsInsteafOfIds = not options.drawIDs)
			print(T.toNewick())
			exit(0)

		elif options.example == "adh":
			"""
			comparison of the alcohol dehydrogenase of different yeast species
			"""
			adh_aln = ["IPKTQKAMVFYKNGGPLKYEDIPVPKPKPSEILINVRYSGVCHTDLHAWKGDWPLPTKLPLVGGHEGAGVVVACGSEVKNFKVGDYAGIKWLNGSCMGCEYCMQGAEPNCPKADLSGYTHDGSFQQYATADAVQAAHIPQGTDLAAAAPILCAGVTVYKALKTADLRPGQWVAISGAGGGLGSLAVQYAKAMGLRVVGIDGGSEKKELATKLGAEEFIDFTQVSDVVKVMQNVTNGGPHGVINVSVSPRAMSQSVEYVRTLGKVVLVGLPADAVVQTKVFDHVIKSIQIRGSYVGNREDTAEALDFFERGLVHSPIKVVGLSDLPKVFSLMEKGKIAGRYVLDTSK","IPTTQKAIVFESNNGKLEYKDIPVPKPKPNELLINVKYSGVCHSDLHIWKGDLPLPYKFPLVGGHEGAGVVVGMGENVKDWKIGDYAGIKWLNSSCMNCEYCQQGAESVCDTLDLSGCTHDGTFQQYATADATQAARIPAGTDLASVAPILCAGITTYKALITAELMPGQWVAVSGAGGGLGSLAIQYAVAMGYRVLAIDGGEDKGKFVKSLGAEEYIDFTKEKDIVGAVQKITNGGPHGVINVSTSEAAINQSAQYIRKLGKVVLVGLPGGSKITAPVFTMVGKSFQIRGTNVGNRKDTAEAIDFFTRGLIKCPIKVVGLSELPEVFKLIEAGKILGRYVVDSSK","IPTTQKAIIFDKNNGPLEYKDIPVPKPKPNEILINVKYSGVCHTDLHAWKGDWPLPTKLPLVGGHEGAGVVVALGDEVKNFKIGDLAGIKWLNGSCQSCEYCVNGFESNCAQADLSGYTHDGSFQQYATADAVQAAKIPQGTDLAGVSPILCAGITVYKALKTAHLKAGDIVAISGAGGGLGSLAVQYAKAMGYRVLAIDGGKEKGELVKSLGAEFYVDYQSSPDLIEEIQSKTKGGPHGVINVSVSDKAISQSTKFVRSTGSVVLVGLPPNAVVHSDVFNHVIKSISIKGSYVGNRADTREAIEFFTRGLVKSPIKVVGLSELPGVYELMEQGKILGRYVVDTSK","IPETQKAIIFYESHGKLEHKDIPVPKPKANELLINVKYSGVCHTDLHAWHGDWPLPTKLPLVGGHEGAGVVVGMGENVKGWKIGDYAGIKWLNGSCMACEYCELGNESNCPHADLSGYTHDGSFQEYATADAVQAAHIPQGTDLAEVAPILCAGITVYKALKSANLRAGHWAAISGAAGGLGSLAVQYAKAMGYRVLGIDGGPGKEELFTSLGGEVFIDFTKEKDIVSAVVKATNGGAHGIINVSVSEAAIEASTRYCRANGTVVLVGLPAGAKCSSDVFNHVVKSISIVGSYVGNRADTREALDFFARGLVKSPIKVVGLSSLPEIYEKMEKGQIAGRYVVDTSK","IPETQKGVIFYESHGKLEYKDIPVPKPKANELLINVKYSGVCHTDLHAWHGDWPLPTKLPLVGGHEGAGVVVGMGENVKGWKIGDYAGIKWLNGSCMACEYCELGNESNCPHADLSGYTHDGSFQEYATADAVQAAHIPQGTDLAEVAPVLCAGITVYKALKSANLMAGHWVAISGAAGGLGSLAVQYAKAMGYRVLGIDGGEGKEELFRSIGGEVFIDFTKEKDIVGAVLKATDGGAHGVINVSVSEAAIEASTRYVRANGTTVLVGMPAGAKCCSDVFNQVVKSISIVGSYVGNRADTREALDFFARGLVKSPIKVVGLSTLPEIYEKMEKGQIVGRYVVDTSK","IPETQKGVIFYESHGKLEYKDIPVPKPKANELLINVKYSGVCHTDLHAWHGDWPLPVKLPLVGGHEGAGVVVGMGENVKGWKIGDYAGIKWLNGSCMACEYCELGNESNCPHADLSGYTHDGSFQEYATADAVQAAHIPQGTDLAEVAPVLCAGITVYKALKSANLMAGHWVAISGAAGGLGSLAVQYAKAMGYRVLGIDGGEGKEELFRSIGGEVFIDFTKEKDIVGAVLKATDGGAHGVINVSVSEAAIEASTRYVRANGTTVLVGMPAGAKCCSDVFNQVVKSISIVGSYVGNRADTREALDFFARGLIKSPIKVVGLSTLPEIYEKMEKGQIVGRYVVDTSK","IPEKQMAVIFETNGGPLEYKEIPVPKPKPTEILINVKYSGVCHTDLHAWKGDWPLATKLPLVGGHEGAGVVVAKGESVTNFEIGDYAGIKWLNGSCMSCEYCEQGHESNCPDADLSGYTHDGSFQQYATADAIQAAKIPKEADLAEVAPILCAGVTVYKALKTANLQAGQWVAISGAGGGLGSLAVQYAKAMGYRVVGIDGGADKGELVKSLGGEVFIDFTKEKDLTKAIQDATNGGPHGVINVSVSEAAISQSCDYVRSTGKVVLVGLPAGAVCHSPVFQHVVKSIEIKGSYVGNRADTREAIDFFSRGLVRSPIKVVGLSELGDVYEKMEKGAILGRYVVDTSY","IPETQKGVIFYENGGKLEYKDLPVPKPKANEILINVKYSGVCHTDLHAWKGDWPLPVKLPLVGGHEGAGIVVAKGENVKNFEIGDYAGIKWLNGSCMSCELCEQGYESNCLQADLSGYTHDGSFQQYRTADAVQAAQIPKGTDLAEIAPILCAGVTVYKALKTADLKPGQWVAISGAAGGLGSLAVQYRKAMGLRVLGIDGGDGKEELFKQCGGEVFIDFRKSKDMVADIQEATNGGPHGVINVSVSEAAVSMSTEYLRPTGLVVLVGLPADAYVKSEVFSHVVKSISIKGSYVGNRADTREATDLFTRGLVKSPIKIIGLSELPEAYALMEQGKILGRFVVDTYK","IPKTQKGVIFYESGGELQYKDIPVPTPKANEILINVKYSGVCHTDLHAWKGDWPLPTKLPLVGGHEGAGVVVAKGANVTNFEIGDYAGIKWLNGSCQSCEYCEKSFESNCPKADLSGYTHDGSFQQYATADAVQAAKIPKDADLAEVAPILCAGVTVYKALKTADLDAGQWVAVSGAGGGLGTLAIQYAKALGLRVLAIDGGDEKGELVKSLGAEVYIDFTKTKDIVKDIIEATNGGPHGVINVSVSEKAIEQSTEYVRPCGTVVLVGLPAGAVARAQVFGAVVKSVSIKGSYVGNRADTREAVDFFTRGLVKSPIKIIGLSELPKVYQLMAEGKILGRYVLDTSK","IPKTQKAVVFDTNGGQLVYKDYPVPTPKPNELLIHVKYSGVCHTDLHARKGDWPLATKLPLVGGHEGAGVVVGMGENVKGWKIGDFAGIKWLNGSCMSCEFCQQGAEPNCGEADLSGYTHDGSFEQYATADAVQAAKIPAGTDLANVAPILCAGVTVYKALKTADLAAGQWVAISGAGGGLGSLAVQYARAMGLRVVAIDGGDEKGEFVKSLGAEAYVDFTKDKDIVEAVKKATDGGPHGAINVSVSEKAIDQSVEYVRPLGKVVLVGLPAHAKVTAPVFDAVVKSIEIKGSYVGNRKDTAEAIDFFSRGLIKCPIKIVGLSDLPEVFKLMEEGKILGRYVLDTSK","VPTTQKAVVFESNGGPLLYKDIPVPTPKPNEILINVKYSGVCHTDLHAWKGDWPLDTKLPLVGGHEGAGVVVGIGSNVTGWELGDYAGIKWLNGSCLNCEFCQHSDEPNCAKADLSGYTHDGSFQQYATADAVQAARLPKGTDLAQAAPILCAGITVYKALKTAQIQPGNWVCISGAGGGLGSLAIQYAKAMGFRVIAIDGGEEKGEFVKSLGAEAYVDFTVSKDIVKDIQTATDGGPHAAINVSVSEKAIAQSCQYVRSTGTVVLVGLPAGAKVVAPVFDAVVKSISIRGSYVGNRADSAEAIDFFTRGLIKCPIKVVGLSELPKVYELMEAGKVIGRYVVDTSK","IPTTQKAVIFETNGGPLLYKDIPVPKPKPNELLINVKYSGVCHTDLHAWKGDWPLDTKLPLVGGHEGAGVVVALGENVTGWEIGDYAGIKWINGSCLQCEYCVTAHESNCPDADLSGYTHDGSFQQYATADAIQAARIPKGTDLALIAPILCAGITVYKALKTAQLQAGQWVAVSGAAGGLGSLAIQYAKAMGYRVVGIDGGADKGEFAKSLGAEVFVDFLSSKDVVADVLKATNGGAHGVINVSVSERAMQQSVDYVRPTGTVVLVGLPAGAKVSASVFSSVVRTIQIKGSYVGNRADSAEAIDFFTRGLIKCPIKIVGLSELASVYELMEQGKILGRYVVDTSK"]
			adh_aln_species = ["B.bruxellensis.AWRI1499","C.maltosa","W.ciferrii","S.cerevisiae","S.cerevisiae.YJM789","S.cerevisiae.RM11-1a","C.boidinii","K.lactis","W.anomalus","C.albicans","S.stipitis","S.stipitis.CBS.6054"]
			for i in range(len(hiv_prot)):
				print(">"+adh_aln_species[i]+"\n"+adh_aln[i])
			T = tree.largeParsimony(adh_aln_species,adh_aln,bigPictureMode=options.bigpicture,bigPictureMode_withLabelsInsteafOfIds=False,restarts=options.largeParsimonyRestarts)
			print("T.parsimonyScore():",T.parsimonyScore())
			if options.draw:
				nxDrawStuff.draw(T,show=True,withLabelsInsteafOfIds = not options.drawIDs)
			print(T.toNewick())
			exit(0)

		elif options.example == "corona":

			inputString="""Cow	Pig	Horse	Mouse	Dog	Cat	Turkey	Civet	Human
Cow	0	295	300	524	1077	1080	978	941	940
Pig	295	0	314	487	1071	1088	1010	963	966
Horse	300	314	0	472	1085	1088	1025	965	956
Mouse	524	487	472	0	1101	1099	1021	962	965
Dog	1076	1070	1085	1101	0	818	1053	1057	1054
Cat	1082	1088	1088	1098	818	0	1070	1085	1080
Turkey	976	1011	1025	1021	1053	1070	0	963	961
Civet	941	963	965	962	1057	1085	963	0	16
Human	940	966	956	965	1054	1080	961	16	0
"""
			print(inputString)
			D=LeafDistanceMatrix(inputString)
			T=D.NJ(bigPictureMode=options.bigpicture)
			print("D.Discrepancy(T):",D.Discrepancy(T))
			if options.draw:
				nxDrawStuff.draw(T,show=True,withLabelsInsteafOfIds = not options.drawIDs)
			print(T.toNewick())
			exit(0)

	if options.work not in ["NJ","UPGMA","AdditivePhylogeny","smallParsimony","greedyNearestNeighborInterchange","largeParsimony","generateRandomBinaryTree","generateRandomRootedBinaryTree"]:
		parser.error("invalid --type (valid are: NJ,UPGMA,AdditivePhylogeny)")
	
	if options.labels is None and options.leaves is not None:
		options.labels = options.leaves

	if options.work in ["generateRandomBinaryTree","generateRandomRootedBinaryTree"]:
		options.type=""

	if (options.type == "dist" and options.work not in ["NJ","UPGMA","AdditivePhylogeny"]) or \
		(options.type == "adj" and options.work not in ["smallParsimony","greedyNearestNeighborInterchange"]) or \
		(options.type == "aln" and options.work not in ["largeParsimony"]):
		parser.error("invalid --type and --work combination (type dist is needed for work NJ,UPGMA,AdditivePhylogeny and type adj is needed for work largeParsimony,smallParsimony,greedyNearestNeighborInterchange)")

	if options.type == "dist":
		r=""
		for line in fileinput.input(args):
			r+=line
		D=LeafDistanceMatrix(r)
		#print(len(D.leafNames),len(D.D),len(D.D[1]))
		#print(D.D)
	elif options.type == "adj":
		r=""
		for line in fileinput.input(args):
			r+=line
		T=tree(r)
	elif options.type == "aln":
		options.labels=[]
		options.leaves=[]
		r=""
		last_seq=""
		for line in fileinput.input(args):
			if len(line)==0:
				continue
			if line[0] == ">":
				options.labels+=[last_seq]
				options.leaves+=[line]
				last_seq=""
			else:
				last_seq+=line
		options.labels+=[last_seq]

	if options.work == "NJ":
		T=D.NJ(bigPictureMode=options.bigpicture,verbose=options.verbose,displayInnerNodesAsJoinedLeaves=options.innerNodesJoin)
	elif options.work == "UPGMA":
		T=D.UPGMA(bigPictureMode=options.bigpicture,verbose=options.verbose,displayInnerNodesAsJoinedLeaves=options.innerNodesJoin)
	elif options.work == "AdditivePhylogeny":
		T=D.AdditivePhylogeny()
	elif options.work == "generateRandomBinaryTree":
		T=tree.generateRandomBinaryTree(options.labels,options.leaves,bigPictureMode=options.bigpicture)
	elif options.work == "generateRandomRootedBinaryTree":
		T=tree.generateRandomBinaryTree(options.labels,options.leaves,bigPictureMode=options.bigpicture,rooted=True)
	elif options.work == "largeParsimony":
		T=tree.largeParsimony(options.labels,options.leaves,verbose=options.verbose,restarts=options.largeParsimonyRestarts,bigPictureMode=options.bigpicture)
	elif options.work == "smallParsimony":
		T.SmallParsimony_Sankoff(bigPictureMode=options.bigpicture)
	elif options.work == "greedyNearestNeighborInterchange":
		T.greedyNearestNeighborInterchange(bigPictureMode=options.bigpicture)

	if options.discrepancy:
		print("D.Discrepancy(T):",D.Discrepancy(T))
	if options.parsimony:
		print("T.parsimonyScore():",T.parsimonyScore())
	if options.draw:
		nxDrawStuff.draw(T,show=True,withLabelsInsteafOfIds = not options.drawIDs,output_file_name=args)
	print(T.toNewick())

def doSomeExampleBigPictures():

	inputString="""4->CAAATCCC
4->ATTGCGAC
5->CTGCGCTG
5->ATGGACGA
6->4
6->5"""
	T=tree(inputString,root=6)
	T.SmallParsimony_Sankoff(bigPictureMode=True)

	inputString="""TCGGCCAA->4
4->TCGGCCAA
CCTGGCTG->4
4->CCTGGCTG
CACAGGAT->5
5->CACAGGAT
TGAGTACC->5
5->TGAGTACC
4->5
5->4"""
	T=tree(inputString)
	T.SmallParsimony_Sankoff(bigPictureMode=True)

	inputString="""GCAGGGTA->5
TTTACGCG->5
CGACCTGA->6
GATTCCAC->6
5->TTTACGCG
5->GCAGGGTA
5->7
TCCGTAGT->7
7->5
7->6
7->TCCGTAGT
6->GATTCCAC
6->CGACCTGA
6->7"""
	T=tree(inputString)
	T.greedyNearestNeighborInterchange(bigPictureMode=True)

	TB = tree.largeParsimony(["GCAGGGTA","TTTACGCG","CGACCTGA","GATTCCAC","TCCGTAGT"],restarts=1,bigPictureMode=True)

if __name__ == '__main__':
	main()


