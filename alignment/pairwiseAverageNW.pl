#!/usr/bin/env perl
# pk

use strict;
use warnings "all";
use Getopt::Long;
use File::Basename;
use Parallel::ForkManager;
use Text::LevenshteinXS qw(distance);

my $version = "0.1.8"; # last updated on 2025-02-05 11:31:10

my $usage = <<ENDUSAGE;
pairwiseAverageNW.pl performs an all versus all pairwise averaged length-normalized needlemanwunsch
 
SYNOPSIS
 
pairwiseAverageNW.pl (-help|-h) (-noselfhit) (-notlengthnormed) (-verbose) (-j THREADS) FASTA1 (FASTA2 ...)

    FASTA   Fasta file containing genes/proteins
    if one file is given, all sequences are compared
    if multiple files are given, the average score is calculated between the files

    optional:
        -noself    do not compare the same file/sequence
        -nonorm    do not normalize the scores to the maximal length
        -verbose   display progress (STDERR)
        -all       print the list of all scores instead of the mean (STDERR)
        -wide      outputs a matrix format instead of a long list of pairs
        -j         number of threads for parallel processing (default: 1)
        -help      displays this.

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES Getopt::Long, File::Basename, easyalignment.py, Parallel::ForkManager
ENDUSAGE

# Parameters
my $help;
my $noselfhit;
my $notlengthnormed;
my $verbose;
my $wide = 0;
my $printAllScores;
my $threads = 1; # Default single thread

GetOptions(
    'help!'       => \$help,
    'noself!'     => \$noselfhit,
    'nonorm!'     => \$notlengthnormed,
    'verbose!'    => \$verbose,
    'all!'        => \$printAllScores,
    'wide!'       => \$wide,
    'j=i'         => \$threads,
    'version!'    => sub { print "$0\tv$version\n"; exit 0; },
);

if ($help || scalar(@ARGV) < 1) {
    print $usage;
    exit(1);
}

# Parse input files
my @files;
my @headers;

for my $arg (@ARGV) {
    open(my $fh, '<', $arg) or die "Cannot open file $arg: $!";
    my $cur_file = "";
    while (<$fh>) {
        chomp;
        if (length($_) < 1) { next; }
        if (substr($_, 0, 1) ne ">") {
            $cur_file .= $_;
        } else {
            s/^>//g;
            push(@headers, $_);
            if ($cur_file ne "") {
                $cur_file .= "\n";
            }
        }
    }
    push(@files, $cur_file);
    close($fh);
}

if (scalar @files == 1) {
    @files = split(/\n/, $files[0]);
} else {
    @headers = @ARGV;
}

# Prepare for matrix output
my @wide_mat;
if ($wide) {
    @wide_mat = map { [(0) x scalar @files] } (0 .. $#files);
}

# Parallel processing
my $pm = Parallel::ForkManager->new($threads);

for my $file_i (0 .. $#files) {
    for my $file_j ($file_i .. $#files) {
        next if $noselfhit and $file_i == $file_j;

        $pm->start and next; # Start a new thread

        my @lines_i = split(/\n/, $files[$file_i]);
        my @lines_j = split(/\n/, $files[$file_j]);

        my $sum = 0;
        my $n = 0;

        for my $i (0 .. $#lines_i) {
            next if length($lines_i[$i]) < 1;
            for my $j (0 .. $#lines_j) {
                next if length($lines_j[$j]) < 1;
                next if $file_i == $file_j && $j <= $i;

                print STDERR "$lines_i[$i] vs $lines_j[$j]\n" if $verbose;

				my $score = distance($lines_i[$i], $lines_j[$j]);
                # Normalize score if required
                my $max_len = length($lines_i[$i]) > length($lines_j[$j]) ? length($lines_i[$i]) : length($lines_j[$j]);
                my $score_raw = $score;
                $score = $notlengthnormed ? $score : $score / $max_len;

                # my $max = length($lines_i[$i]) > length($lines_j[$j]) ? length($lines_i[$i]) : length($lines_j[$j]);
                # my $out = `easyalignment.py -e 0 '$lines_i[$i]' '$lines_j[$j]' 2>/dev/null`;
                # my $score = $out =~ /score: ?([+\-0-9]+)/i ? $1 : 0;

                # print STDERR "$out = $score (" . ($score / $max) . ")\n" if $verbose;

                # my $score_raw = $score;
                # $score /= $max unless $notlengthnormed;

                if ($printAllScores) {
                    print "$headers[$file_i]\t$headers[$file_j]\t$score_raw\t$max_len\t" . (int(10000 * $score) / 10000) . "\n";
                }

                $sum += $score;
                $n++;
            }
        }

        if ($n > 0) {
            my $average = int(10000 * $sum / $n) / 10000;
            if ($wide) {
                $wide_mat[$file_i][$file_j] = $average;
                $wide_mat[$file_j][$file_i] = $average;
            } elsif (!$printAllScores) {
                print "$headers[$file_i]\t$headers[$file_j]\t$average\n";
            }
        }

        $pm->finish; # End the thread
    }
}

$pm->wait_all_children; # Wait for all threads to finish

# Print wide matrix
if ($wide) {
    print join("\t", "", @headers) . "\n";
    for my $i (0 .. $#files) {
        print $headers[$i], "\t", join("\t", @{$wide_mat[$i]}), "\n";
    }
}
