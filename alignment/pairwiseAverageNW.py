import argparse
from concurrent.futures import ProcessPoolExecutor
from rapidfuzz.distance import Levenshtein


def parse_fasta(file_path):
    with open(file_path, 'r') as file:
        sequences = []
        headers = []
        current_seq = []

        for line in file:
            line = line.strip()
            if line.startswith(">"):
                if current_seq:
                    sequences.append("".join(current_seq))
                    current_seq = []
                headers.append(line[1:])  # Strip '>'
            else:
                current_seq.append(line)

        if current_seq:
            sequences.append("".join(current_seq))

    return headers, sequences


def compute_score(seq1, seq2, normalize=True):
    dist = Levenshtein.distance(seq1, seq2)
    if normalize:
        max_len = max(len(seq1), len(seq2))
        return dist / max_len if max_len > 0 else 0
    return dist


def compare_seq_pair(i, seq1, header1, seqs2, headers2, noselfhit, normalize, verbose):
    results = []
    for j, (seq2, header2) in enumerate(zip(seqs2, headers2)):
        if noselfhit and i == j:
            continue
        score = compute_score(seq1, seq2, normalize=normalize)
        results.append((header1, header2, round(score, 4)))
        if verbose:
            print(f"{header1} vs {header2}: {score}")
    return results


def main(args):
    fasta_data = [parse_fasta(file) for file in args.files]
    file_headers = [headers for headers, _ in fasta_data]
    file_sequences = [seqs for _, seqs in fasta_data]

    results = []
    with ProcessPoolExecutor(max_workers=args.threads) as executor:
        for i in range(len(fasta_data)):
            headers1, seqs1 = file_headers[i], file_sequences[i]
            for j in range(i if args.noself else 0, len(fasta_data)):
                headers2, seqs2 = file_headers[j], file_sequences[j]
                futures = [
                    executor.submit(
                        compare_seq_pair, k, seq1, headers1[k], seqs2, headers2,
                        args.noself, not args.nonorm, args.verbose
                    )
                    for k, seq1 in enumerate(seqs1)
                ]
                for future in futures:
                    results.extend(future.result())

    if args.wide:
        headers = [header for headers, _ in fasta_data for header in headers]
        header_to_index = {header: i for i, header in enumerate(headers)}
        matrix = [[0] * len(headers) for _ in range(len(headers))]

        for header1, header2, score in results:
            i, j = header_to_index[header1], header_to_index[header2]
            matrix[i][j] = matrix[j][i] = score

        print("\t" + "\t".join(headers))
        for i, row in enumerate(matrix):
            print(headers[i] + "\t" + "\t".join(map(str, row)))
    else:
        for header1, header2, score in results:
            print(f"{header1}\t{header2}\t{score}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Pairwise comparison of sequences using Levenshtein distance.")
    parser.add_argument("files", nargs="+", help="Input FASTA files.")
    parser.add_argument("-noself", action="store_true", help="Exclude self-hits.")
    parser.add_argument("-nonorm", action="store_true", help="Disable length normalization.")
    parser.add_argument("-verbose", action="store_true", help="Display progress.")
    parser.add_argument("-wide", action="store_true", help="Output results in matrix format.")
    parser.add_argument("-j", "--threads", type=int, default=1, help="Number of threads for parallel processing.")

    args = parser.parse_args()
    main(args)
