#!/usr/bin/env perl
# pk

use strict;
use warnings "all";
# RUN : perl MSAtrimmer.pl (options) somealn.aln

# given an alignment in aln format, this script trimmes the aln

our $RED="\033[0;41m";
our $PURPLE="\033[0;45m";
our $YELLOW="\033[0;43m";
our $ORANGE="\033[1;33m";
our $NC="\033[0m"; # No Color
my $version="0.0.39"; # last updated on 2023-02-17 20:16:22

my $usage = "MSAtrimmer.pl        trimmes a given alignment (column- and row-wise) and realigns it. By default only gaprich (>0.5) columns are trimmed.
 
SYNOPSIS
 
MSAtrimmer.pl (options) ALIGNMENTFILE 

	ALIGNMENTFILE	alignment file (clustaw or fasta format) or - for STDIN

		e.g. CLUSTAL W formated input:
		...
		Dolosicoccus_paucivorans__GCF_      ATGT-TATGGTATA-CTTATGATGCAA----------
		Lactobacillus_hayakitensis_DSM      CTAAT-ATGGTATT-ATAATTAT--------------
		Lactobacillus_salivarius__GCF_      ATGATTATGCTATG-ATTGATTT--------------
		Leuconostoc_garlicum__GCF_0019      ATTTTCGCGGTATA-ATAATTGATG------------
		...

	options 

		-p=(clustalw|musle) : the algorithm that is called after trimming to realign [default:clustalw]. This also defines the output format!
		-realn      :	this option enables a re-alignment call on the results (using clustal)
		-fasta      :	outputs fasta format (is the default now)
		-clustal    :	outputs clustalw format


	options VERTICAL (columns)

		-VmaxGap=number, -g=number :	the maximum number/percentage (with a % suffix) of gap characters in each column [default:50%]
		-VminId=number,  -t=number :	the minimum number/percentage (with a % suffix) of the dominant nongap-characters in each column [default:0 <=> disabled]
		
		percentage numbers [0\% - 100\%]
		integer numbers [1 - 999999]
		NOTE: If you want to specify a percentage number the \% symbol is neccesary 

	options HORIZONTAL (rows)

		-HminPairId=percentage, -h=percentage:	the minimum average pairwise identity to all other rows. Scoring gap-gap=0 [default:0\% <=> disabled]
		-HmaxGap=number, -j=number:	the maximum number/percentage (with a % suffix, relative to the number of columns) of gaps per row. [default:0\% <=> disabled]

		percentage numbers [0\% - 100\%] (this option needs the \% symbol !)

	options display

		-color      :	showing original aln with all removed rows/cols colored (+ this option disables the realn call on the results)

EXAMPLE

	MSAtrimmer.pl -VmaxGap=25% -VminId=3 test.aln

    Dolosicoccus_paucivorans__GCF_      AT${RED}GT${NC}-${RED}T${NC}ATGGTAT${RED}A${NC}-CT${RED}T${NC}ATG${RED}A${NC}T${RED}GCAA----------${NC}
    Lactobacillus_hayakitensis_DSM      CT${RED}AA${NC}T${RED}-${NC}ATGGTAT${RED}T${NC}-AT${RED}A${NC}ATT${RED}A${NC}T${RED}--------------${NC}
    Lactobacillus_salivarius__GCF_      AT${RED}GA${NC}T${RED}T${NC}ATGCTAT${RED}G${NC}-AT${RED}T${NC}GAT${RED}T${NC}T${RED}--------------${NC}
    Leuconostoc_garlicum__GCF_0019      AT${RED}TT${NC}T${RED}C${NC}GCGGTAT${RED}A${NC}-AT${RED}A${NC}ATT${RED}G${NC}A${RED}TG------------${NC}
                                        ${RED}  ^^ ^        ^  ^ ^ ^ ^^^^^^^^^^^^^^${NC}

    --> VmaxGap=25\% -> a maximum of 1 gap per column, VminId=3 -> each column should now have at least 3 common non-gap characters
    --> Trimmed coloumns are marked red

OUTPUT
	alingment file

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES clustalw (optional for re-alignment)
";

#my $noInput=1;
#for($v = 0 ; $v < scalar @ARGV ; $v++){
#	if($ARGV[$v] ne "-" && $ARGV[$v] =~ m/^-/){ next;}
#	$noInput=0;
#}
#if($noInput){push(@ARGV,"-")}

if(scalar @ARGV == 0){print $usage; exit 1;}
if(scalar @ARGV != 0 && $ARGV[0] =~ /-?-version|-v$/ ){ print "$0\tv$version\n"; exit 0; }

if($ARGV[0] eq "-help" || $ARGV[0] eq "-h" || $ARGV[0] eq "--help" ){print $usage; exit(0);}

my $maxnumberofgaps=0.5;
my $minDominantChar=0;
my $minrowid=0;
my $maxrowgap=-1;
my $maxrowgap_is_p=0;
my $realn=0;
my $showtrimmedred=0;
my $p="clustalw";
my $outputfasta=1;

my $printINFO=0;

my $argv_str=join(" ",@ARGV);
$argv_str=~s/(-[^ =]+) (muscle|clustalw|[0-9]+\b)/$1=$2/g;
@ARGV=split(" ",$argv_str);

for(my $v = 0 ; $v < scalar @ARGV ; $v++){
	if($ARGV[$v] =~ m/--?VmaxGap=([0-9.]+)%/ || $ARGV[$v] =~ m/--?g=([0-9.]+)%/){ if($1<0 || $1>100){print STDERR $usage."\n"."[STDERR] ERROR: invalid range of -g ...\n";exit(1);};$maxnumberofgaps=$1/100; if($maxnumberofgaps==1){$maxnumberofgaps=-1;} next;}
	if($ARGV[$v] =~ m/--?VmaxGap=([0-9.]+)$/ || $ARGV[$v] =~ m/--?g=([0-9.]+)$/){ if($1<0){print STDERR $usage."\n"."[STDERR] ERROR: invalid range of -g ...\n";exit(1);};$maxnumberofgaps=$1; next;}
	if($ARGV[$v] =~ m/--?VminId=([0-9.]+)%/ || $ARGV[$v] =~ m/--?t=([0-9.]+)%/){ if($1<0 || $1>100){print STDERR $usage."\n"."[STDERR] ERROR: invalid range of -t ...\n";exit(1);};$minDominantChar=$1/100; if($minDominantChar==1){$minDominantChar=-1;} next;}
	if($ARGV[$v] =~ m/--?VminId=([0-9.]+)$/ || $ARGV[$v] =~ m/--?t=([0-9.]+)$/){ if($1<0){print STDERR $usage."\n"."[STDERR] ERROR: invalid range of -t ...\n";exit(1);};$minDominantChar=$1; next;}
	if($ARGV[$v] =~ m/--?HminPairId=([0-9.]+)%?/ || $ARGV[$v] =~ m/--?h=([0-9.]+)%?/){ if($1<0 || $1>100){print STDERR $usage."\n"."[STDERR] ERROR: invalid range of -h ...\n";exit(1);}; $minrowid=$1/100; next;}	
	if($ARGV[$v] =~ m/--?HmaxGap=([0-9.]+)%/ || $ARGV[$v] =~ m/--?j=([0-9.]+)%/){ if($1<0 || $1>100){print STDERR $usage."\n"."[STDERR] ERROR: invalid range of -j ...\n";exit(1);}; $maxrowgap=$1/100; $maxrowgap_is_p=1; next;}
	if($ARGV[$v] =~ m/--?HmaxGap=([0-9]+)$/ || $ARGV[$v] =~ m/--?j=([0-9]+)$/){ if($1<0){print STDERR $usage."\n"."[STDERR] ERROR: invalid range of -j ...\n";exit(1);}; $maxrowgap=$1; next;}
	if($ARGV[$v] =~ m/--?no[ _]?re[ _]?ali?n/){ $realn=0; next;} # but now is realn=0 default, option stays for legacy
	if($ARGV[$v] =~ m/--?[ _]?re[ _]?ali?n/){ $realn=1; next;}
	if($ARGV[$v] =~ m/--?p=(clustalw|muscle)/){ $p=$1; next;}
	if($ARGV[$v] =~ m/--?color/){ $showtrimmedred=1; next;}
	if($ARGV[$v] =~ m/--?fasta/){ $outputfasta=1; next;}
	if($ARGV[$v] =~ m/--?clustal/){ $outputfasta=0; next;}

	if(-e $ARGV[$v] || $ARGV[$v] eq "-"){next;}
	print $usage."\n";
	print STDERR "[STDERR] ERROR : I dont understand the argument '".$ARGV[$v]."' ! Aborting...\n";exit(1);
}

sub padRight{
	my $s = shift;
	my $l = shift;
	return $s . (" " x ($l-length($s)));
}

if($showtrimmedred){print STDERR "[STDERR] WARNING : If -color is set -norealn is forced.\n";$realn=0;}

for(my $v = 0 ; $v < scalar @ARGV ; $v++){

	if($ARGV[$v] ne "-" && $ARGV[$v] =~ m/^-/){next;}

	my %data;
	my $cur_geneName="";
	
	my $cur_header="";
	
	if($ARGV[$v] eq "-"){
		while(<STDIN>){
			chomp($_);
			my $entry=$_;
			$entry=~s/^[ \t]+//g;
			my @arr=split(/[ \t][ \t]+/,$entry);
			if( scalar(@arr) == 2){
				if(!exists($data{$arr[0]})){$data{$arr[0]}="";}
				$data{$arr[0]}.=$arr[1];
			}else{
				if($entry=~m/>(.+)/){
					$cur_header=$1;
				}elsif($cur_header ne "" && $entry=~m/^[^ ]+$/){
					if(!exists($data{$cur_header})){$data{$cur_header}="";}
					$data{$cur_header}.=$entry;
				}
			}
		}
	}else{
			
		open (FH,'<',$ARGV[$v]);
		#1. load in the alignment
		while(<FH>){
			chomp($_);
			my $entry=$_;
			$entry=~s/^[ \t]+//g;
			my @arr=split(/[ \t][ \t]+/,$entry);
			if( scalar(@arr) == 2){
				if(!exists($data{$arr[0]})){$data{$arr[0]}="";}
				$data{$arr[0]}.=$arr[1];
			}else{
				if($entry=~m/>(.+)/){
					$cur_header=$1;
				}elsif($cur_header ne "" && $entry=~m/^[^ ]+$/){
					if(!exists($data{$cur_header})){$data{$cur_header}="";}
					$data{$cur_header}.=$entry;
				}
			}
		}
		close(FH);
	}

	#2. compute the score
	my @rownames = keys %data;

	if($minDominantChar==-1){$minDominantChar=scalar @rownames;}
	if($maxnumberofgaps==-1){$maxnumberofgaps=scalar @rownames;}

	if($printINFO == 0){$printINFO=1;
		print STDERR "[STDERR] Selecting columns with:\n        (1) number of gaps <= $maxnumberofgaps.\n        (2) a dominant (non-gap) char that occures > $minDominantChar.\n";
		print STDERR "[STDERR] Selecting rows with:\n        (1) average pairwise identity > $minrowid && gaps < $maxrowgap (-1 = disabled)\n";
	}

	my @colinfo;
	my @rowinfo;
	my $longestRowname=0;

	print STDERR "[STDERR] APPLY HORIZONTAL OPTIONS...\n";

	for (my $i=0 ; $i<scalar @rownames ; $i++){
		
		if(length $rownames[$i] > $longestRowname){$longestRowname=length $rownames[$i]}

		if(length $rownames[$i] < 1 or length $data{$rownames[$i]} < 1 ){next;}
		for(my $k=0 ; $k < length $data{$rownames[$i]} ; $k++){

			if(!exists($colinfo[$k])){$colinfo[$k] = {};}
			if(!exists($colinfo[$k]{substr($data{$rownames[$i]},$k,1)})){$colinfo[$k]{substr($data{$rownames[$i]},$k,1)} = 0;}

			$colinfo[$k]{substr($data{$rownames[$i]},$k,1)}++;
		}

		if(!exists($rowinfo[$i])){$rowinfo[$i]="OK";}
		
		if($minrowid != 0){
			my $cur_row_id=0;
			my $unalignedA= () = $data{$rownames[$i]} =~ /[^-]/g;

			for (my $j=0 ; $j<scalar @rownames ; $j++){
				if($i==$j or length $rownames[$j] < 1 or length $data{$rownames[$j]} < 1 ){next;}
				
				my $cur_score_api = 0; 
				for(my $k=0 ; $k < length $data{$rownames[$i]} ; $k++){
					if(substr($data{$rownames[$i]},$k,1) eq substr($data{$rownames[$j]},$k,1) && substr($data{$rownames[$i]},$k,1) ne "-" && substr($data{$rownames[$j]},$k,1) ne "-" ){
						$cur_score_api++;
					}
				}

				my $minLengthWithoutGaps=0;
				my $unalignedB= () = $data{$rownames[$j]} =~ /[^-]/g;
				# print $unalignedA;
				if( $unalignedA > $unalignedB  ){
					$minLengthWithoutGaps = $unalignedB;
				}else{
					$minLengthWithoutGaps = $unalignedA;
				}

				$cur_row_id += ($cur_score_api/($minLengthWithoutGaps)); #relative identity 
			}


			if ( $minrowid > ( $cur_row_id / ((scalar @rownames) -1)) ) {
				$rowinfo[$i]="minrowid";
			}
		}
		if($maxrowgap != -1){
		
			my $cur_gap_num=0;
			for(my $k=0 ; $k < length $data{$rownames[$i]} ; $k++){
				if(substr($data{$rownames[$i]},$k,1) eq "-" ){
					$cur_gap_num++;
				}
			}

			if ( ( $maxrowgap_is_p && $maxrowgap < ( $cur_gap_num / (length $data{$rownames[$i]}) ) ) || 
				 ( !$maxrowgap_is_p && $maxrowgap < $cur_gap_num  ) ) {
				$rowinfo[$i]="maxrowgap";
			}
		}
	}

	my @colisvalid;

	print STDERR "[STDERR] APPLY VERTICAL OPTIONS...\n";
	for(my $k=0 ; $k < scalar @colinfo ; $k++){

		if(!exists($colisvalid[$k])){$colisvalid[$k] = 1;}

		if(exists $colinfo[$k] && exists($colinfo[$k]{"-"})){
			if( ($maxnumberofgaps>=1 && $colinfo[$k]{"-"} > $maxnumberofgaps) || 
				($maxnumberofgaps<1 && $maxnumberofgaps>=0 && $colinfo[$k]{"-"} > $maxnumberofgaps*(scalar @rownames)) ){
				$colisvalid[$k]=0;
			}
		}

		my $dominantNumberOfOccurencesInThisColumn=0;
		my $numOfGapsInThisCol=0;
		foreach my $key (keys %{$colinfo[$k]}){
			if($key ne "-"){
				if($dominantNumberOfOccurencesInThisColumn<$colinfo[$k]{$key}){ $dominantNumberOfOccurencesInThisColumn=$colinfo[$k]{$key}; }
			}else{
				$numOfGapsInThisCol=$colinfo[$k]{$key};
			}
		}
		if( ($minDominantChar>=1 && $dominantNumberOfOccurencesInThisColumn < $minDominantChar) || 
			($minDominantChar<1 && $minDominantChar>=0 && $dominantNumberOfOccurencesInThisColumn < $minDominantChar*((scalar @rownames))) ){
			if($colisvalid[$k]==0){
				$colisvalid[$k]=-2;
			}else{
				$colisvalid[$k]=-1;
			}
		}
	}

	my $didremovedrows_minrowid=0;
	my $didremovedrows_maxrowgap=0;
	my $didremovedcols=0;
	my $didremovedcolsi=0;

	if($realn){
		print STDERR "[STDERR] DOING THE REALIGNMENT...\n";
		my $seed=rand();
		open(FILE,'>>',"/tmp/MSAtrim".$seed);
		print FILE "CLUSTAL 2.1 multiple sequence alignment\n\n\n";
		for (my $i=0 ; $i<scalar @rownames ; $i++){
			if($rowinfo[$i] eq "minrowid"){$didremovedrows_minrowid++;next;}
			if($rowinfo[$i] eq "maxrowgap"){$didremovedrows_maxrowgap++;next;}
			print FILE $rownames[$i]."      ";
			for(my $k=0 ; $k < length $data{$rownames[$i]} ; $k++){
				if($colisvalid[$k]==1){
					print FILE substr($data{$rownames[$i]},$k,1);
				}else{
					if($didremovedcolsi==0 || $didremovedcolsi == $i){
						$didremovedcolsi=$i;
						$didremovedcols++;
					}
				}
			}
			print FILE "\n";
		}
		print FILE "\n";
		close(FILE);

		if($didremovedrows_minrowid + $didremovedrows_maxrowgap >= (scalar @rownames) -1){
			print STDERR "[STDERR] nothing to align...\n";
			system("clustal2fasta.pl /tmp/MSAtrim".$seed." >/tmp/MSAtrim".$seed.".aln;");
		}else{
			if($p eq "clustalw"){
				system("clustalw -INFILE=/tmp/MSAtrim".$seed." -OUTFILE=/tmp/MSAtrim".$seed.".aln.clustalw >/dev/null && clustal2fasta.pl /tmp/MSAtrim".$seed.".aln.clustalw>/tmp/MSAtrim".$seed.".aln; >/dev/null");
			}elsif($p eq "muscle"){
				system("clustal2fasta.pl /tmp/MSAtrim".$seed." | muscle -in - -out /tmp/MSAtrim".$seed.".aln >/dev/null 2>&1");
			}else{
				print STDERR "[STDERR] ERROR : -p=$p unknown option...";exit 1;
			}
		}
		system("cat /tmp/MSAtrim".$seed.".aln");
		unlink("/tmp/MSAtrim".$seed.".aln");
		unlink("/tmp/MSAtrim".$seed);
	}else{
		if(!$outputfasta){ print "CLUSTAL 2.1 multiple sequence alignment\n\n\n" }

		for (my $i=0 ; $i<scalar @rownames ; $i++){
			if($rowinfo[$i] eq "minrowid"){
				$didremovedrows_minrowid++;
				if($showtrimmedred){
					if($outputfasta){
						print ">".$rownames[$i]."\n";
					}else{
						print $RED.padRight($rownames[$i],$longestRowname)."      ";
					}
				}else{ 
					next; 
				}
			}elsif($rowinfo[$i] eq "maxrowgap"){
				$didremovedrows_maxrowgap++;
				if($showtrimmedred){
					if($outputfasta){
						print ">".$rownames[$i]."\n";
					}else{
						print $RED.padRight($rownames[$i],$longestRowname)."      ";
					}
				}else{ 
					next; 
				}
			}else{
				if($outputfasta){
					print ">".$rownames[$i]."\n";
				}else{
					print padRight($rownames[$i],$longestRowname)."      ";
				}
			}
			for(my $k=0 ; $k < length $data{$rownames[$i]} ; $k++){
				if($colisvalid[$k]==1){
					print substr($data{$rownames[$i]},$k,1);
				}else{
					if($didremovedcolsi==0 || $didremovedcolsi == $i){
						$didremovedcolsi=$i;
						$didremovedcols++;
					}
					if($showtrimmedred){
						if($rowinfo[$i] eq "OK"){
							if($colisvalid[$k]==-2){
								print $RED.substr($data{$rownames[$i]},$k,1).$NC;
							}elsif($colisvalid[$k]==-1){
								print $YELLOW.substr($data{$rownames[$i]},$k,1).$NC;
							}else{
								print $PURPLE.substr($data{$rownames[$i]},$k,1).$NC;
							}
						}else{
							print substr($data{$rownames[$i]},$k,1);
						}
					}
				}
			}
			
			if( $rowinfo[$i] ne "OK" && $showtrimmedred){
				print $NC;
			}
			print "\n";
		}
		#if(!$outputfasta){print "\n"}
	}

	if($showtrimmedred){
		print STDERR "[STDERR] Colorcode of removed columns/rows : ${PURPLE}-VmaxGap$NC, ${YELLOW}-VminId$NC, ${RED}-VmaxGap and -VminId$NC and ${RED}-HminPairId$NC\n";
	}

	print STDERR "[STDERR] PRINT STATISTICS...\n";
	if($didremovedcols>0){
		my $lastk=-10;
		my $lastkiseq=0;
		print STDERR "[STDERR]$ORANGE Removed $didremovedcols column(s)$NC with "; if($maxnumberofgaps!=-1){print STDERR "#gaps > $maxnumberofgaps";if($minDominantChar!=0){print STDERR " and ";}}if($minDominantChar!=0){print STDERR "#dominant nongap-characters < $minDominantChar ";} print STDERR ": \n";
		for(my $k=0 ; $k < length $data{$rownames[0]} ; $k++){
			if($colisvalid[$k]==1){if($lastkiseq==1){print STDERR ($k-1);$lastkiseq=0;}next;}

			if($lastk==$k-1){if($lastkiseq==0){print STDERR "-";$lastkiseq=1;}}
			else{
				$lastkiseq=0;
				print STDERR " ".$k;
			}
			$lastk=$k;
		}
		if($lastkiseq==1){print STDERR ($lastk);}
		print STDERR "\n";
	}
	if($didremovedrows_minrowid>0){
		print STDERR "[STDERR]$ORANGE Removed $didremovedrows_minrowid row(s) cause of -h$NC with a average row identity of $minrowid or lower (no order) : \n";
		for (my $i=0 ; $i<scalar @rownames ; $i++){
			if($rowinfo[$i] ne "minrowid"){next;}
			print STDERR $rownames[$i].",";
		}
		print STDERR "\n";
	}
	if($didremovedrows_maxrowgap>0){
		print STDERR "[STDERR]$ORANGE Removed $didremovedrows_maxrowgap row(s) cause of -j$NC with $maxrowgap gaps or more\n";
		#for (my $i=0 ; $i<scalar @rownames ; $i++){
		#	if($rowinfo[$i] ne "maxrowgap"){next;}
		#	print STDERR $rownames[$i].",";
		#}
		print STDERR "\n";
	}
	
}

