#!/usr/bin/env perl
# pk

use strict;
use warnings "all";
# RUN : perl MSAconsensus.pl (options) somealn.aln

# given an alignment in aln format, this script makes a consensus

my $dominant_cutoff=0;

our $RED="\033[0;41m";
our $PURPLE="\033[0;45m";
our $YELLOW="\033[0;43m";
our $ORANGE="\033[1;33m";
our $NC="\033[0m"; # No Color
my $version="0.0.11"; # last updated on 2022-06-27 15:45:30

my $usage = "MSAconsensus.pl        calculates a consensus of a given alignment (columnwise).
 
SYNOPSIS
 
MSAconsensus.pl (options) ALIGNMENTFILE 

	ALIGNMENTFILE	alignment file (clustaw or fasta format) or - for STDIN

		e.g. CLUSTAL W formated input:

		...
		Dolosicoccus_paucivorans__GCF_      ATGT-TATGGTATA-CTTATGATGCAA----------
		Lactobacillus_hayakitensis_DSM      CTAAT-ATGGTATT-ATAATTAT--------------
		Lactobacillus_salivarius__GCF_      ATGATTATGCTATG-ATTGATTT--------------
		Leuconostoc_garlicum__GCF_0019      ATTTTCGCGGTATA-ATAATTGATG------------
		...

	options

		-includeGaps, -g : include gaps in the consensus
		-ambiguous, -a   : if there are MULTIPLE dominant characters, then print ONE at random.
		-dominant=, -d=  : if the frequency of the most dominant character is below this threshold (given as percent), then omit this character. (default:0 = disabled)
							e.g. use 100 to only print the characters that are 100\% conserved in all sequences.
		-multiple, -m    : if there are MULTIPLE dominant characters, then print ALL if the format '{TA}' if T and A are the dominat characters. 
		-plain, -p       : remove no-domiant/ambiguous characters '#?' entirely.
		-c       : outputs tsv positionwise conservation to STDERR (0-1).
		-varna   : outputs varna style conservation scores to STDERR (usable with varnas -colorMap option).

		# = no domiant character 
			- if all chars are gaps and -g is NOT set 
			- if the most dominant character is below the -d threshold
		? = there are multiple dominant characters found (set -a to display any)

EXAMPLES

	MSAconsensus.pl test.aln 

		ATG?TTATGGTATA#AT?ATTAT??AA##########

	MSAconsensus.pl test.aln -p

		ATGATTTATGGTATAATATATTAT

	MSAconsensus.pl test.aln -a
	
		ATGTTTATGGTATA#ATAATTATTCAA##########
		---^-------------^-----^^------------ the '?' position can vary each call

	MSAconsensus.pl test.aln -a
	
		ATGATTATGGTATA#ATTATTATGGAA##########
		---^-------------^-----^^------------ the '?' position can vary each call

	MSAconsensus.pl test.aln -a -g

		ATGTTTATGGTATA-ATAATTAT--------------

	MSAconsensus.pl test.aln -m

		ATG{AT}TTATGGTATA#AT{AT}ATTAT{GT}{CG}AA##########

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES -
";

#my $noInput=1;
#for($v = 0 ; $v < scalar @ARGV ; $v++){
#	if($ARGV[$v] ne "-" && $ARGV[$v] =~ m/^-/){ next;}
#	$noInput=0;
#}
#if($noInput){push(@ARGV,"-")}

if(scalar @ARGV == 0){print $usage; exit 0;}
if(scalar @ARGV != 0 && $ARGV[0] =~ /-?-version|-v$/ ){ print "$0\tv$version\n"; exit 0; }

if( $ARGV[0] eq "-help" ||  $ARGV[0] eq "-h" ||  $ARGV[0] eq "--help" ){print $usage; exit 0;}

my $includeGaps=0;
my $ambiguous=0;
my $plain=0;
my $multiple=0;
my $conservOut=0;
my $varnaOut=0;

for(my $v = 0 ; $v < scalar @ARGV ; $v++){
	if($ARGV[$v] =~ m/--?includeGaps/i || $ARGV[$v] =~ m/--?g/i){ $includeGaps=1; next;}
	if($ARGV[$v] =~ m/--?ambiguous/i || $ARGV[$v] =~ m/--?a/i){ $ambiguous=1; next;}
	if($ARGV[$v] =~ m/--?d(?:ominant)?=([0-9]+)/i ){ $dominant_cutoff=$1; if($dominant_cutoff<1 || $dominant_cutoff>100){die "[ERROR] invalid -d please specify a percent number (1-100)\n"} next;}
	if($ARGV[$v] =~ m/--?plain/i || $ARGV[$v] =~ m/--?p/i){ $plain=1; next;}
	if($ARGV[$v] =~ m/--?multiple/i || $ARGV[$v] =~ m/--?m/i){ $multiple=1; next;}
	if($ARGV[$v] =~ m/--?c/i ){ $conservOut=1; next;}
	if($ARGV[$v] =~ m/--?varna/i ){ $varnaOut=1; next;}

	if(-e $ARGV[$v] || $ARGV[$v] eq "-"){next;}
	print $usage."\n";
	print STDERR "ERROR : I dont understand the argument '".$ARGV[$v]."' ! Aborting...\n";exit 0;
}

if($multiple && $ambiguous){print STDERR "[ERROR] cannot set -m AND -a...\n";exit 1;}

for(my $v = 0 ; $v < scalar @ARGV ; $v++){

	if($ARGV[$v] ne "-" && $ARGV[$v] =~ m/^-/){ next;}

	my %data;
	my $cur_geneName="";
	
	my $cur_header="";
	
	if($ARGV[$v] eq "-"){
		while(<>){
			chomp($_);
			my $entry=$_;
			my @arr=split(/  +/,$entry);
			if( scalar(@arr) == 2){
				if(!exists($data{$arr[0]})){$data{$arr[0]}="";}
				$data{$arr[0]}.=$arr[1];
			}else{
				if($entry=~m/>([^ ]+)/){
					$cur_header=$1;
				}elsif($cur_header ne "" && $entry=~m/^[^ ]+$/){
					if(!exists($data{$cur_header})){$data{$cur_header}="";}
					$data{$cur_header}.=$entry;
				}
			}
		}
	}else{
		open (FH,'<',$ARGV[$v]);
		#1. load in the alignment
		while(<FH>){
			chomp($_);
			my $entry=$_;
			my @arr=split(/  +/,$entry);
			if( scalar(@arr) == 2){
				if(!exists($data{$arr[0]})){$data{$arr[0]}="";}
				$data{$arr[0]}.=$arr[1];
			}else{
				if($entry=~m/>([^ ]+)/){
					$cur_header=$1;
				}elsif($cur_header ne "" && $entry=~m/^[^ ]+$/){
					if(!exists($data{$cur_header})){$data{$cur_header}="";}
					$data{$cur_header}.=$entry;
				}
			}
		}
		close(FH);
	}

	#2. compute the score
	my @rownames = keys %data;
	my @colinfo;
	my %symbols;
	
	for (my $i=0 ; $i<scalar @rownames ; $i++){
		if(length $rownames[$i] < 1 or length $data{$rownames[$i]} < 1 ){next;}
		for(my $k=0 ; $k < length $data{$rownames[$i]} ; $k++){

			$symbols{substr($data{$rownames[$i]},$k,1)}=1;

			if(!exists($colinfo[$k])){$colinfo[$k] = {};}
			if(!exists($colinfo[$k]{substr($data{$rownames[$i]},$k,1)})){$colinfo[$k]{substr($data{$rownames[$i]},$k,1)} = 0;}

			$colinfo[$k]{substr($data{$rownames[$i]},$k,1)}++;
		}
	}

	if($conservOut){
		print STDERR "# i";
		foreach my $sym (sort keys %symbols) {
			print STDERR "\t$sym";
		}
		print STDERR "\n";
	}
	for(my $k=0 ; $k < scalar @colinfo ; $k++){
		my $domNum=0;
		my %domChars;

		foreach my $key (keys %{$colinfo[$k]}){
			if($includeGaps || $key ne "-"){
				if($dominant_cutoff > 0 && $colinfo[$k]{$key} <= $dominant_cutoff/100 * (scalar @rownames)){next}
				if($domNum<$colinfo[$k]{$key}){ $domNum=$colinfo[$k]{$key};}
			}
		}
		if($domNum>0){
			foreach my $key (keys %{$colinfo[$k]}){
				if($includeGaps || $key ne "-"){
					if($domNum==$colinfo[$k]{$key} && !exists $domChars{$key}){ $domChars{$key}=1; }
				}
			}	
		}

		if($varnaOut){
			#print STDERR ($k+1)."-".($k+1).":".sprintf ("fill=#%2.2X%2.2X%2.2X",255*($domNum/scalar @rownames),0,255*(1-($domNum/scalar @rownames))).";";
			print STDERR ($domNum/scalar @rownames).";";
		}elsif($conservOut){
			print STDERR ($k);
			foreach my $sym (sort keys %symbols) {
				print STDERR "\t".(exists $colinfo[$k]{$sym} ? $colinfo[$k]{$sym} : 0);
			}
			print STDERR "\n";
		}

		if(scalar keys %domChars == 0 && !$plain){
			print "#";
		}elsif($ambiguous){
			my @arr=keys %domChars;
			print $arr[0];
		}elsif($multiple && scalar keys %domChars >1){
			my @arr=keys %domChars;
			print "{".join("",sort keys %domChars)."}";
		}elsif(!$ambiguous && scalar keys %domChars == 1){
			my @arr=keys %domChars;
			print $arr[0];
		}elsif(!$plain){
			print "?";
		}
	}
	print "\n";
}

