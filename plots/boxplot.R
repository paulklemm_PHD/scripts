#!/usr/bin/env Rscript
#pk 

version="0.1.15"; # last updated on 2023-03-04 23:43:49

for ( f in c("patchwork","ggplot2","readr","optparse","reshape2","stringr") ){
  if(! f %in% rownames(installed.packages())){ install.packages(f, lib = Sys.getenv("R_LIBS_USER"), repos='https://cloud.r-project.org/'); }; suppressMessages(require(f,character.only = TRUE))
}

args = commandArgs(trailingOnly=TRUE)

usage=paste0("EXAMPLES

boxplot.R -F='\t' counts.tsv

boxplot.R - << EOF
1;a
1;a
1;a
2;a
2;a
3;a
10;a
4;b
3;b
3;b
EOF

or simply

boxplot.R - << EOF
1
1
1
2
2
3
10
4
3
3
EOF

VERSION v",version,"
AUTHOR Paul Klemm
DEPENDENCIES ggplot2");
             
op<-OptionParser(usage=usage,
     prog="boxplot.R",
     option_list=list(
       make_option(c("--log"), action="store_true", default=FALSE,
                   help="(optional) plot log10 +1 transformed values", metavar="boolean"),
       make_option(c("--col_names"), action="store_true", default=FALSE,
                   help="(optional) enable if table contains column names", metavar="boolean"),
       make_option(c("--longinput","-l"), type="character", default="",
                   help="input file in long format, (- = STDIN)", metavar="character"),
       make_option(c("--wideinput","-w"), type="character", default="",
                   help="input file in wide format", metavar="character"),
       make_option(c("--delim","-F"), type="character", default=";",
                   help="field delimiter", metavar="character"),
       make_option(c("--comment"), type="character", default="#",
                   help="comment field character", metavar="character"),
       make_option(c("--wideid"), type="character", default="",
                   help="id field for wide tables (comma separated list of header names, use e.g. 'X1' in lists without names)", metavar="character"),
       make_option(c("--name","-n"), type="character", default="INPUTFILENAME",
                   help="output name", metavar="character")
))
opt = parse_args(op)

if( (length(args) > 0 && ( args[1] == "-v"|| args[1] == "-version"|| args[1] == "--version" )) ){
  message(paste0("boxplot.R\t",version));
  stop("", call.=FALSE);
}

# global: set that selecting a single column of a data.frame will keep the rownames 
`[` <- function(...) base::`[`(...,drop=F)

give.n <- function(x){return(c(y = min(x), label = length(x) ))};
rounduptob <- function(x,b) b*ceiling(x/b)
rounddowntob <- function(x,b) b*floor(x/b)
findNiceLabelRange<-function(from,to,isInt=0){
  stepsize = 10^(round(log10(to-from))-1)
  if(isInt){stepsize=round(stepsize)}
  if(stepsize==0){stepsize=1}
  laststepsize = 0
  while( round(from+stepsize) > round(to-stepsize) ){
    stepsize=stepsize/2
  }
  nice_range=unique(c(from,seq(rounduptob(from,stepsize) , rounddowntob(to,stepsize) , stepsize),to))
  iii=0
  while( (length(nice_range) < 6 || length(nice_range) > 15) && stepsize != laststepsize && iii<10 ){
    laststepsize=stepsize
    iii=iii+1
    if( length(nice_range) >15 ){
      stepsize=stepsize*10
    }
    if( length(nice_range) < 6 ){
      stepsize=stepsize/2
    }
    while( rounduptob(from,stepsize) > rounddowntob(to,stepsize) ){
      stepsize=stepsize/2
    }
    if(isInt){stepsize=round(stepsize)}
    if(stepsize==0){stepsize=1; stepsize = laststepsize}
    nice_range=unique(c(from,seq(rounduptob(from,stepsize) , rounddowntob(to,stepsize) , stepsize),to))
  }
  return(nice_range);
}

# opt$comment="#"; opt$wideinput="/archiv/READS/Arabidopsis_thaliana_Batschauer_2023/COUNTED/counts.tsv"; opt$col_names=F; opt$wideid="X1"; opt$delim="\t"

input=ifelse(opt$longinput=="-" | opt$wideinput=="-","STDIN",ifelse(opt$longinput!="",opt$longinput,opt$wideinput))
name=ifelse(opt$name=="INPUTFILENAME",input,opt$name)

if( opt$longinput=="-" | opt$wideinput=="-" ){
	dat <- read.table(file("stdin"),sep=opt$delim);
	if(opt$col_names){
	  if(nrow(dat)<=1){message("no data found in input ...");stop("",call.=FALSE);}
	  names(dat)=dat[1,]
	  dat=dat[2:nrow(dat),]
	}
}else{
  dat <- read_delim(ifelse(opt$longinput!="",opt$longinput,opt$wideinput), comment = opt$comment,
                  delim=opt$delim,escape_double = FALSE, col_names = opt$col_names, 
                  trim_ws = TRUE)
}
if(nrow(dat)==0){message("no data found in input ...");stop("",call.=FALSE);}

if(opt$longinput!=""){
  # input is long
  if(ncol(dat) == 2){ names(dat)=c("value","variable") }
  if(ncol(dat) == 1){
    names(dat)=c("value")
    dat$variable = "";
  }
  if(ncol(dat)!=2){ message("invalid input format");stop("",call.=FALSE); }
}else{
  # input is wide
  dat=melt(dat, id.vars=unlist(str_split(opt$wideid,pattern = ",")))
  dat$variable=gsub("\\.{3}[0-9]+$","",dat$variable)
  #dat$id=dat[,unlist(str_split(opt$wideid,pattern = ","))]#
}

dat$variable = factor(dat$variable);
dat$value = as.numeric(dat$value);
dat = dat[is.numeric(dat$value),];
dat = dat[!is.na(dat$value),];

if(opt$log){dat$value=log10(dat$value)}

p1=ggplot(dat,aes(x=variable,y=value,color=variable))+
  geom_boxplot()+
  theme_bw()+theme(axis.title.x=element_blank(),axis.title.y=element_blank(),panel.spacing = unit(0, "lines"), panel.grid.minor = element_blank(),legend.position="none")+
  ylab("")+xlab("")+
  facet_grid(variable~"", switch = "y",scales = "free_y")+
  scale_y_continuous(limits = c(min(dat$value),max(dat$value)))+
  coord_flip()+
  theme(strip.text.y.left = element_text(angle = 0))

if(!opt$log){p1=p1+scale_y_continuous(breaks=findNiceLabelRange(min(dat$value),max(dat$value),1), labels=findNiceLabelRange(min(dat$value),max(dat$value),1))}

p2=ggplot(dat,aes(y=value))+
  stat_bin(aes(fill=variable))+
  stat_summary(aes(x=0),vjust = 0, fun.data = give.n, geom = "text", fun = median)+ #, position = position_dodge(width = 0.75)
  theme_bw()+theme(axis.title.x=element_blank(),axis.title.y=element_blank(),panel.spacing = unit(0, "lines"), panel.grid.minor = element_blank(),legend.position="none")+
  ylab("")+xlab("")+
  scale_y_continuous(limits = c(min(dat$value),max(dat$value)))+
  facet_grid(variable~"",switch = "y",scales="free_y")+
  coord_flip()+
  theme(strip.text.y.left = element_text(angle = 0))

if(!opt$log){p2=p2+scale_y_continuous(breaks=findNiceLabelRange(min(dat$value),max(dat$value),1), labels=findNiceLabelRange(min(dat$value),max(dat$value),1))}

p=p1/p2
ggsave(filename = paste0(name,".svg"),plot=p,height=5+length(unique(dat$variable))/5,width=ifelse(length(unique(dat$variable))>1,12,8))
#ggsave(filename = paste0(opt$input,".svg"),plot=p1/p2)
