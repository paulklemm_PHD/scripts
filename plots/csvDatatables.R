#!/usr/bin/env Rscript

pks=c("optparse","crosstalk", "DT","plotly","htmlwidgets","readr","optparse")
for ( f in pks ){ 
  if(! f %in% rownames(installed.packages())){ install.packages(f, lib = Sys.getenv("R_LIBS_USER"), repos='https://cloud.r-project.org/'); }; suppressMessages(require(f,character.only = TRUE))
}

# global: set that selecting a single column of a data.frame will keep the rownames 
`[` <- function(...) base::`[`(...,drop=F)

version="0.0.20"; # last updated on 2021-10-02 23:08:46

op<-OptionParser(usage=paste0("this program converts a given csv/tsv table to DT:tatables (html). 

AUTHOR Paul Klemm
VERSION v",version,"
DEPENDENCIES ",paste0(pks,collapse=",")),
                 prog="csvDatatables.R",
                 option_list=list(
                   make_option(c("-i", "--input"), type="character", default=NULL,
                               help="input table (csv or tsv). E.g. '--input test.csv'. Use - for STDIN", metavar="character"),
                   make_option(c("-f", "--firstRowIsNotHeader"), action="store_true", default=FALSE,
                               help="sets the header to X1, X2, ... and enables automatic column namings for files ending in .gff, .sam and .bla format"),
                   make_option(c("-t", "--type"), type="character", default="",
                               help="sets the header to 'gff', 'sam' and 'blast'. If the header contains the first row, the first row is appended to the table.", metavar="character"),
                   make_option(c("-s", "--sep"), type="character", default="\t|;| +",
                               help="sets delimiter", metavar="character"),
                   make_option(c("-o", "--open"), type="character", default="",
                               help="open directly with [firefox,safari,chrome,...]", metavar="character")
                 )
)
opt = parse_args(op);

if(is.null(opt$input)){
  print_help(op)
  stop("ERROR, no inputs", call.=FALSE)
}

options(warn = 1);

message("parsing input")

input_name = opt$input;
if(opt$input == "-"){ opt$input <- file("stdin"); input_name="STDIN" }

df = read_delim(opt$input,opt$sep, escape_double = FALSE, trim_ws = TRUE, col_names=!opt$firstRowIsNotHeader);

if(ncol(df) < 2){
  df = read_delim(opt$input,";", escape_double = FALSE, trim_ws = TRUE, col_names=!opt$firstRowIsNotHeader, comment=ifelse(grepl("\\.sam$",opt$input),"@",ifelse(grepl("\\.gff$",opt$input),"#","")));
}else if(ncol(df) < 2){
  df = read_delim(opt$input,"\t", escape_double = FALSE, trim_ws = TRUE, col_names=!opt$firstRowIsNotHeader, comment=ifelse(grepl("\\.sam$",opt$input),"@",ifelse(grepl("\\.gff$",opt$input),"#","")));
}

if(is.null(df) || ncol(df) < 2){
  stop("ERROR, input table contains < 2 columns, this seems odd...", call.=FALSE)
}

if(opt$type == "gff" | (opt$firstRowIsNotHeader && grepl("\\.gff$",opt$input) && ncol(df)==9)){
  if(!all(grepl("^X[0-9]+$",names(df)))){ df = rbind(names(df),df) }
	names(df) = c("seqname","source","feature","start","end","score","strand","frame","attribute")
}else if(opt$type == "sam" | (opt$firstRowIsNotHeader && grepl("\\.sam$",opt$input) && ncol(df)>=11)){
  if(!all(grepl("^X[0-9]+$",names(df)))){ df = rbind(names(df),df) }
  names(df)[1:11] = c("QNAME","FLAG","RNAME","POS","MAPQ","CIGAR","RNEXT","PNEXT","TLEN","SEQ","QUAL")
}else if(opt$type == "blast" | (opt$firstRowIsNotHeader && grepl("\\.bla$",opt$input) && ncol(df)==12)){
  if(!all(grepl("^X[0-9]+$",names(df)))){ df = rbind(names(df),df) }
  names(df)[1:12] = c("qseqid","sseqid","pident","length","mismatch","gapopen","qstart","qend","sstart","send","evalue","bitscore")
}

# numeric_cols = apply(df,2, function(x){is.numeric(x[!is.na(x)])}) & apply(df,2, function(x){min(x, na.rm=T)!=max(x, na.rm=T)})

p_datatable <- datatable(df,rownames=F,
     style = "bootstrap", class = "compact",
     extensions = c('Scroller', 'Select', 'Buttons'), #'Responsive',
     filter = 'top',width = "100%",
     options = list(
       colReorder = TRUE,
       scroller = TRUE,
       scrollCollapse= TRUE, autoWidth=TRUE,
       select = list(style = "multi", items = "row"),
       columnDefs = list(
       		list(
           	render = JS( # meta.col
          	"function(data, type, row, meta) { if(data === null){data=''} ",
          	"return type === 'display' && data !== null && data !== '' && data && data.length > 30 ?",
            	"'<span style=\"font-size: 10px;max-width:100em;overflow-wrap: break-word;display:block;white-space: normal\">' + data + '</span>' : '<center>'+ data+ '</center>';",
          	"}"), 
           	targets = "_all")
       ),
       language = list(
         info = 'Showing _START_ to _END_ of _TOTAL_ entries'),
       deferRender = TRUE,
       scrollY= '70vh',
       scrollX = TRUE,
       dom = "Blrtip",
       buttons = list(I("colvis"),
                      c('copy', 'csv', 'excel', 'pdf'),
                      list(extend='selectAll',className='selectAll',text="select all",action=DT::JS("function () { var table = $('#DataTables_Table_0').DataTable();table.rows({ search: 'applied'}).select();}")), 
                      list(extend='selectNone',text="deselect",action=DT::JS("function () {var table = $('#DataTables_Table_0').DataTable();table.rows({ search: 'applied'}).deselect();}")
                      ))), selection="none" )
bs=bscols(widths = c(12), list( p_datatable) )
htmltools::save_html(bs, paste(input_name,".html",sep=""))

if(opt$open!=""){ system(paste0( ifelse(Sys.info()["sysname"] == "Darwin" , "open -a Firefox" , "firefox -new-tab" ), " '", getwd(),"/", input_name , ".html'")) }

message(paste0("DONE : output written to ",input_name,".html"))
