#!/usr/bin/perl
#pk
use strict;

use List::Util qw(shuffle); 

srand(42);

my $number_replicates=10;
my $min_species=100;
my $max_species=1000;
my $species_inc=100;

if(scalar @ARGV == 0 || $ARGV[0] =~ /^--?h/){
	print "USAGE $0 BLAST-GRAPH (BLAST-GRAPH,...)\ngenerate number_replicates=$number_replicates times randomly subgraphs between min_species=$min_species and max_species=$max_species (increment by $species_inc)."
	exit;
}

my %blastgraph;
if(!-d "parts"){
	mkdir "parts";
	for (my $i = 0; $i < scalar @ARGV; $i++) {
		my $f="$ARGV[$i]";
		print STDERR "reading $f\n";
		open(my $FH,"<$f");
		my $header="";
		my $FHo;
		my $is_open=0;
		while(<$FH>){
			chomp;
			if(/^#/){
				if(/^# file_a\tfile_b/){if($is_open){ close($FHo)}; $header="$_\n";next}
				if(/^# ([^\t]*fasta|[^\t]*faa)\t([^\t]*fasta|[^\t]*faa)/ && !-e "$1.$2.part"){ $is_open=1; $header.="$_\n"; open($FHo,">parts/$1.$2.part") or die "$? $!\n";  }
				else{$header.="$_\n"}
			}elsif($is_open){
				if($header ne ""){ print $FHo "$header\n"; $header=""; }
				print $FHo "$_\n";
			}
		}
		close($FH);
	}
}

if(!-e "all_files.list"){system("find parts | grep part | sed -E 's/(parts.)|(.part)//g' | sed -E 's/(faa|fasta)\\./\\1\\n/g' | sort | uniq > all_files.list")}

for (my $rep = 0; $rep < $number_replicates; $rep++) {
	my @all_files = shuffle map {chomp;$_} `cat all_files.list`;
	for(my $k=$min_species; $k<$max_species; $k+=$species_inc){

		my $it=0; while(-e "subset_large_k$k.id$it.blast-graph"){$it+=1;}
		my $fn="subset_large_k$k.id$it.blast-graph";

		my @parts;
		system("echo ''>$fn");
		for(my $i=0; $i<$k; $i++){
			for(my $j=0; $j<$k; $j++){
				if(-e "parts/$all_files[$i].$all_files[$j].part"){push(@parts,"parts/$all_files[$i].$all_files[$j].part");}
				if(-e "parts/$all_files[$j].$all_files[$i].part"){push(@parts,"parts/$all_files[$j].$all_files[$i].part");}
				if(scalar @parts > 100){ system("cat ".join(" ",@parts)." >>$fn"); @parts=(); }
			}
		}
		if(scalar @parts > 0){ system("cat ".join(" ",@parts)." >>$fn"); @parts=(); }
	}
}
