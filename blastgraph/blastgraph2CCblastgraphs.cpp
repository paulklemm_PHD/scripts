/*
compile: g++ -std=c++11 -O3 blastgraph2CCblastgraphs.cpp -o blastgraph2CCblastgraphs

Usage: blastgraph2CCblastgraphs (options) BLASTGRAPH (BLASTGRAPH2,BLASTGRAPH3,...)

Detail: paritions the input blastgraph(s) into all the connected components and produces a file in the blast-graph style for each of them. The output files are named 'BLASTGRAPH_x.cc' where x is the current CC index. The output files are
generated in the current directory (the files getting tar-ed to fight io overload). The size of the current CC is printed to STDOUT in the form n,m with n=number of nodes and m=number of edges.
 Example Output:
    | 1295527.5 Enterococcus_faecium    1158601.5 Enterococcus_malodoratus      0       187     0       187
    | 1295527.5 Enterococcus_faecium    1330525.4 Enterococcus_raffinosus       0       186     0       186
    | ...
    (1295527.5 is a protein of the species Enterococcus_faecium)
Options:
-s , --statonly : print just the statistics in stdout (number of nodes, edges of each CC)
-id , -idonly : each node (protein/gene) is replaced with an id (saves alot of memory)!
    | 0 1       0       187     0       187
    | 0 2       0       186     0       186
    | ...
    (the ID 0,1,... are unique for each protein+species)
-notar : Do not tar the results!
-species : print the species header separately (this will result in alot of comment lines)
    | # Enterococcus_faecium    Enterococcus_malodoratus
    | # 300    400
    | 1295527.5 1158601.5       0       187     0       187
    | # Enterococcus_faecium    Enterococcus_raffinosus
    | # 300    999
    | 1295527.5 1330525.4       0       186     0       186
    | ...
    the 300, 400 and 999 are the species specific median bitscores
*/
//pk
#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <string>
#include <map>
#include <cstdlib>
#include <algorithm>
#include <cmath>
#include <list>
#include <unordered_set>
#include <cstring>
#include <string.h>

using namespace std;

struct wedge {unsigned int edge;};
struct protein {vector<wedge> edges;};

// Globals
unsigned int species_counter = 0;	// Species
unsigned int protein_counter = 0;	// Proteins
vector<string> species;			// Number -> Name
vector<protein> graph;			// Graph containing all protein data

// TMP Globals
map<string,unsigned int> species2id;		// Name -> Number
map<string,unsigned int> protein2id;		// Name -> Number

bool fullinformation=true;
bool printspecies=false;
int minnode=0;
bool notar=false;

///////////////////////////////////////////////////////////
// Misc functions
///////////////////////////////////////////////////////////
// Convert string to double
double string2double(string str) {
	istringstream buffer(str);
	double value;
	buffer >> value;
	return value;
}
// Convert string to float
float string2float(string str) {
	istringstream buffer(str);
	float value;
	buffer >> value;
	return value;
}


pair<unsigned int, unsigned int> ordPair(unsigned int a,unsigned int b){if(a<b){return make_pair(a,b);}else{return make_pair(b,a);}}

// Split a string at a certain delim
void tokenize(const string& str, vector<string>& tokens, const string& delimiters = "\t") {

	// Skip delimiters at beginning.
	string::size_type lastPos = str.find_first_not_of(delimiters, 0);
	// Find first "non-delimiter".
	string::size_type pos = str.find_first_of(delimiters, lastPos);

	while (string::npos != pos || string::npos != lastPos) {
		// Found a token, add it to the vector.
		tokens.push_back(str.substr(lastPos, pos - lastPos));
		// Skip delimiters.  Note the "not_of"
		lastPos = str.find_first_not_of(delimiters, pos);
		// Find next "non-delimiter"
		pos = str.find_first_of(delimiters, lastPos);
	}

}


struct lineinfo {unsigned int bitscore; float eval; map<string,unsigned int>::iterator a_name_it;map<string,unsigned int>::iterator b_name_it; unsigned int a_species_name_it;unsigned int b_species_name_it; };
map<pair<unsigned int, unsigned int>, lineinfo > edge2line;
struct pair_hash {
    inline std::size_t operator()(const std::pair<int,int> & v) const {
        return v.first*31+v.second;
    }
};
unordered_set<pair<unsigned int,unsigned int> , pair_hash> edge_is_done;
map<pair<unsigned int,unsigned int>,string> speciesPair2Median;

///////////////////////////////////////////////////////////
// File parser
///////////////////////////////////////////////////////////
void parse_file(string file) {

	string line;
	ifstream graph_file(file.c_str());
	if (graph_file.is_open()) {
		// For each line
		string file_a = "";	unsigned int file_a_id = 0;
		string file_b = "";	unsigned int file_b_id = 0;
		while (!graph_file.eof()) {
			getline(graph_file, line);
			vector<string> fields;
			tokenize(line, fields, "\t");
			// Header line
			if (fields.size() == 2 && fields[0].substr(0, 1) == "#") {
				file_a = fields[0].substr(2, fields[0].size()-2);
				file_b = fields[1];

				if (file_a == "file_a" && file_b == "file_b") continue;	// Init Header

				// Map species a
				if (species2id.find(file_a) == species2id.end())	{
						species.push_back(file_a);
						species2id[file_a] = species_counter++;
				}
				// Map species b
				if (species2id.find(file_b) == species2id.end())	{
						species.push_back(file_b);
						species2id[file_b] = species_counter++;
				}

				file_a_id = species2id[file_a];
				file_b_id = species2id[file_b];
			}else if (fields.size() == 4 && fields[0].substr(0, 1) == "#") {
				speciesPair2Median[make_pair(file_a_id,file_b_id)]=line;
			}
			// Data line
			else if ((fields.size() >1) && fields[0].substr(0, 1) != "#") {
				// a b e1 b1 e2 b2 score

				if(fields.size() < 6){
					fullinformation=false;
				}

				// 5.16 deal with duplicated IDs by adding file ID to protein ID
				string ida = fields[0];
				string idb = fields[1];
				fields[0] += " "; fields[0] += file_a;
				fields[1] += " "; fields[1] += file_b;

				// 5.16 do not point to yourself
				if (!fields[0].compare(fields[1])) {continue;}

				// A new protein
				map<string,unsigned int>::iterator a_name_it=protein2id.find(fields[0]);
				map<string,unsigned int>::iterator b_name_it=protein2id.find(fields[1]);
				if (a_name_it == protein2id.end())	{
					protein a;
					protein2id[fields[0]] = protein_counter++;
					graph.push_back(a);
					a_name_it=protein2id.find(fields[0]);
				}
				if (b_name_it == protein2id.end())	{
					protein b;
					protein2id[fields[1]] = protein_counter++;
					graph.push_back(b);
					b_name_it=protein2id.find(fields[1]);
				}

				// Add link to graph (reciprocal)
				unsigned int a_id = protein2id[fields[0]];
				unsigned int b_id = protein2id[fields[1]];

				unsigned short bitscore_avg=0;
				float eval_avg=0;
				if(fields.size() > 5){
					float bit_a = string2float(fields[3]);
					float bit_b = string2float(fields[5]);
					bitscore_avg = (bit_a+bit_b)/2;
					float eval_a = string2float(fields[2]);
					float eval_b = string2float(fields[4]);
					eval_avg = (eval_a+eval_b)/2;
				}

				// 5.17, add weight
				wedge w;
				w.edge=b_id;
				graph[a_id].edges.push_back(w);
				w.edge=a_id;
				graph[b_id].edges.push_back(w);

				edge2line[ordPair(a_id,b_id)].bitscore = bitscore_avg;
				edge2line[ordPair(a_id,b_id)].eval = eval_avg;

				if(fullinformation && fields.size() > 5){
					edge2line[ordPair(a_id,b_id)].a_species_name_it=file_a_id;
					edge2line[ordPair(a_id,b_id)].b_species_name_it=file_b_id;
					edge2line[ordPair(a_id,b_id)].a_name_it = a_name_it;
					edge2line[ordPair(a_id,b_id)].b_name_it = b_name_it;
					//edge2line[ordPair(a_id,b_id)]=fields[0]+"\t"+fields[1]+"\t"+fields[2]+"\t"+fields[3]+"\t"+fields[4]+"\t"+fields[5];
				}		
				edge_is_done.insert(ordPair(b_id,a_id));
			}
		}
		graph_file.close();
	}
	else {
		throw string("Could not open file " + file);
	}

}
bool statonly=false;
ofstream out;
unsigned int n=0;

string last_species_header="";
string last_median_header="";

void printToOut(pair<unsigned int,unsigned int>e){
	unsigned int cur_node = e.first;
	unsigned int adjacency_node = e.second;
	if(!statonly){
		lineinfo li = edge2line[ordPair(cur_node,adjacency_node)];
		if(fullinformation){
			if( printspecies ){
				vector<string> fieldsA;
				tokenize(li.a_name_it->first, fieldsA, " ");
				vector<string> fieldsB;
				tokenize(li.b_name_it->first, fieldsB, " ");

				if ( last_species_header=="" || last_species_header !=  "# "+fieldsA[1] + "\t" + fieldsB[1] ){
					out << "# "+fieldsA[1] + "\t" + fieldsB[1]<< endl;
					last_species_header="# "+fieldsA[1] + "\t" + fieldsB[1];
					if(speciesPair2Median.count(make_pair(li.a_species_name_it,li.b_species_name_it))){
						out << speciesPair2Median[make_pair(li.a_species_name_it,li.b_species_name_it)]<< endl;
					}
				}else{

				}

				out << fieldsA[0] << "\t" << fieldsB[0] << "\t" ;
			}else{

				out << li.a_name_it->first << "\t" << li.b_name_it->first << "\t" ;
			}
			
		}else{
			out << cur_node << "\t" << adjacency_node << "\t";
		}
		out << li.eval << "\t" << li.bitscore << "\t"<< li.eval << "\t" << li.bitscore << endl ;
	}
}

bool BFS(vector<bool> * done, unsigned int cur_node, string filename ){

	list<unsigned int> q;
	q.push_back(cur_node);
	(*done)[cur_node]=true;

	list<pair<unsigned int,unsigned int> > cache;
	bool didprintcc=false;

	while(q.size()>0){

		list<unsigned int> q_new;

		for(list<unsigned int>::iterator it = q.begin() ; it != q.end() ; ++it){
			cur_node = *it;

			for (unsigned int i = 0; i < graph[cur_node].edges.size(); i++) {

				unsigned int adjacency_node = graph[cur_node].edges[i].edge;

				if(adjacency_node > graph.size()){
					cerr << string("[ERROR] : Input graph is invalid. The node is reporting an edge/adjacent node, that is not present in the graph.").c_str() << endl;throw;
				}
				if(adjacency_node > (*done).size()){
					cerr << string("[ERROR] : Input graph is invalid. The node is not present in done vector.").c_str() << endl;throw;
				}

				if( !(*done)[adjacency_node] ){
					n++;
					(*done)[adjacency_node] = true;
					q_new.push_back(adjacency_node);
				}

				if(edge_is_done.count( ordPair(cur_node,adjacency_node) ) ){

					//cerr << edge_is_done[ordPair(cur_node,adjacency_node)] << endl;

					if(didprintcc){
						printToOut(make_pair(cur_node,adjacency_node));
					}else if(cache.size()>minnode){
						didprintcc=true;
						if(!statonly){
							out.open(filename);
							if(fullinformation){
								out << "# file_a	file_b"<< endl<< "# a     b       evalue_ab       bitscore_ab     evalue_ba       bitscore_ba"<< endl;
							}
						}
						for(list<pair<unsigned int,unsigned int> >::iterator it = cache.begin() ; it != cache.end() ; ++it){
							printToOut(*it); 
						}
					}else{
						cache.push_back(make_pair(cur_node,adjacency_node));
					}

  					unordered_set<pair<unsigned int, unsigned int >,pair_hash>::iterator iter = edge_is_done.find(ordPair(cur_node,adjacency_node) ) ;
				  	if( iter != edge_is_done.end() )
				    	edge_is_done.erase( iter ); // removes this edge from edge_is_done -> (for counting the number of edges in the CC)
				}
			}
		}
		q=q_new;
	}

	if(didprintcc && !statonly){ out.close(); }
	return didprintcc;
}

int main(int argc, char *argv[]) {
	// check for an argument
	
	if(argc < 2 || argv[1] == "-h" || argv[1] == "--h" || argv[1] == "-help" || argv[1] == "--help" || argv[1] == "help" || argv[1] == "h"){
		PRINTHELP:
		cerr << endl << "Usage: " << argv[0] << " (options) BLASTGRAPH (BLASTGRAPH2,BLASTGRAPH3,...)" << endl  << endl 
		<< "Detail: paritions the input blastgraph(s) into all the connected components and produces a file in the blast-graph style for each of them. The output files are named 'BLASTGRAPH_x.cc' where x is the current CC index. The output files are generated in the current directory (the files getting tar-ed to fight io overload). The size of the current CC is printed to STDOUT in the form n,m with n=number of nodes and m=number of edges."<< endl
		<< " Example Output:" << endl
		<< "    | 1295527.5 Enterococcus_faecium	1158601.5 Enterococcus_malodoratus	0	187	0	187" << endl
		<< "    | 1295527.5 Enterococcus_faecium	1330525.4 Enterococcus_raffinosus	0	186	0	186" << endl
		<< "    | ..." << endl << "    (1295527.5 is a protein of the species Enterococcus_faecium)" << endl << "Options:"<< endl
		<< "-s , --statonly : print just the statistics in stdout (number of nodes, edges of each CC)" << endl
		<< "-id , -idonly : each node (protein/gene) is replaced with an id (saves alot of memory)!" << endl
		<< "    | 0	1	0	187	0	187" << endl
		<< "    | 0	2	0	186	0	186" << endl
		<< "    | ..." << endl
		<< "    (the ID 0,1,... are unique for each protein+species)" << endl
		<< "-notar : Do not tar the results!" << endl 
		<< "-species : print the species header separately (this will result in alot of comment lines)" << endl
		<< "    | # Enterococcus_faecium	Enterococcus_malodoratus" << endl
		<< "    | 1295527.5	1158601.5	0	187	0	187" << endl
		<< "    | # Enterococcus_faecium	Enterococcus_raffinosus" << endl
		<< "    | 1295527.5	1330525.4	0	186	0	186" << endl
		<< "    | ..." << endl;
		return -1;
	}

	try{
		// read in a text file that contains a real matrix stored in column major format
		// but read it into row major format

		string name="";

		for(unsigned int argi=1;argi<argc;argi++){
			if(strcmp(argv[argi],"-s")==0 || strcmp(argv[argi],"--statonly")==0 || strcmp(argv[argi],"--onlystat")==0 ){
				statonly=true;
			}else if(strcmp(argv[argi],"--idonly")==0 || strcmp(argv[argi],"--onlyid")==0 || strcmp(argv[argi],"-id")==0 ){
				fullinformation=false;
			}else if(strcmp(argv[argi],"--species")==0 || strcmp(argv[argi],"-species")==0 ){
				printspecies=true;
			}else if(strcmp(argv[argi],"--minnode")==0 || strcmp(argv[argi],"-minnode")==0 ){
				argi++;
				minnode=string2float(string(argv[argi]));
			}else if(strcmp(argv[argi],"--notar")==0 || strcmp(argv[argi],"-notar")==0 ){
				notar=true;
			}else{
				 if(argv[argi][0] == '-'){
					cout << "[ERROR] : unknown option '" << argv[argi] << "'"<< endl;
					goto PRINTHELP;
				}
				cerr << "[STDERR] Parsing "<< argv[argi] << endl;
				name=argv[argi];
				parse_file(argv[argi]);
			}
		}

		cerr << "[STDERR] I know " << graph.size() << " proteins with " << edge_is_done.size() << " connections of " << species_counter << " species."<< endl;

		std::size_t found = string(name).find_last_of("/");
		name=string(name).substr(found+1);

		species2id.clear();
		cerr << "[STDERR] Parsing done ..." << endl;

		unsigned int cur_CC_indx=0; // index of the current CC (e.g. 5 => all edge of the 5. CC have the index 5)

		vector<bool> done = vector<bool> (graph.size(), false);	// Keep track on what was done (for each node)
		bool allNodesAreDone = false;
		unsigned int last_protein_id=0;
		unsigned int tictoc=0;
		string cur_arg="";
		unsigned int total_num_edges=edge_is_done.size();


		while( !allNodesAreDone ){

			allNodesAreDone=true;

			for (unsigned int protein_id = last_protein_id ; protein_id < graph.size() ; protein_id++) {
				//last_protein_id=protein_id;

				if (done[protein_id]){continue;}// We were here already

				done[protein_id]=true; // mark this node

				cur_CC_indx++;
				n=1;

				stringstream ss;
				ss<<name;
				ss<<"_"<<cur_CC_indx;
				ss<<".cc";
				

				size_t prev_s = edge_is_done.size();

				bool didprintcc=BFS(&done,protein_id,ss.str().c_str()); // get the CC of the current node (protein_id);
				last_species_header="";

				size_t new_s = edge_is_done.size();

				if(!statonly && didprintcc && !notar){
					cur_arg+=ss.str()+" ";
					tictoc++;
					if(tictoc==1000 && !notar){
						cerr << "[STDERR] Tar-ing 1000 connected components. " << edge_is_done.size() << " edges left (=" << (1.0-(double)edge_is_done.size()/(double)total_num_edges)*100.0 << "\% done)"<< endl;
						tictoc=0;
						int i = system(("tar -rf "+name+"_CC.tar "+cur_arg).c_str());
						i=system(("rm "+cur_arg).c_str());
						cur_arg="";
					}
				}
				cout << n << "," << (prev_s-new_s) << endl;	 //prev_s = the number of edges before running BFS (removes all edges of the given CC) 

				last_protein_id=protein_id;
				allNodesAreDone=false;
			}
		}
		if(!statonly && !notar){
			std::size_t found = string(argv[1]).find_last_of("/");
			cerr << "[STDERR] Tar-ing last connected components into "<< name << "_CC.tar. " << edge_is_done.size() << " edges left."<< endl;
			int i = system(("tar -rf "+name+"_CC.tar "+cur_arg).c_str());
			i=system(("rm "+cur_arg).c_str());
		}
		
	}catch(string& error) {
		cerr << "[STDERR] [ERROR] catched " << error << endl;

		goto PRINTHELP;
		return EXIT_FAILURE;
	}
}
