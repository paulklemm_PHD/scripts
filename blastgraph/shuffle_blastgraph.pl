#!/usr/bin/perl
#pk

use strict;
use List::Util qw(shuffle);

my $cur_header="";
my $cur_header_appendix="";
my %data;
while(<>){
	chomp;
	if(/^# file/ || /# a/){print "$_\n";}
	elsif(/^#[^\t]+\t[^\t]+$/){ $cur_header=$_;$cur_header_appendix="" }
	elsif(/^#/){$cur_header_appendix=$_}
	else{
		if(!exists $data{$cur_header}){$data{$cur_header}{"appendix"}=$cur_header_appendix;$data{$cur_header}{"data"}=()}
		push(@{$data{$cur_header}{"data"}},$_);
	}
}

foreach my $h (shuffle keys %data) {
	print "$h\n".$data{$h}{"appendix"}."\n";
	foreach my $x (shuffle @{$data{$h}{"data"}}) { print "$x\n"; }
}
