#!/usr/bin/env perl
#pk

use strict;
use warnings "all";

our $COLOR="\033[33m"; #yellowish
our $COLOR1="\033[32m"; #green
our $COLOR2="\033[31m"; #red
our $NC ="\033[0m"; # No Color
my $version="0.0.5"; # last updated on 2021-10-05 11:53:44

my $tput=`tput color 2>/dev/null`; # test if the shell supports colors
$tput=~s/[\r\n]+$//;
if ($tput=~m/[^0-9]|^&/ && $tput <16) {
  $COLOR="";$NC="";$COLOR1="";$COLOR2="";
}

use Cwd qw(abs_path cwd);
use File::Basename; # basename()

my $help=0;
my $infile="";
my $threads=4;
local $ENV{PATH} = "$ENV{PATH}:".abs_path(dirname(__FILE__)); # add the current directory of the script astarget.pl to $PATH enviroment variable

my $usage = "
gridRNAcofold.pl        This program executes RNAcofold on a grid of parameters (different monomer concentrations and temperatures). 

SYNOPSIS

gridRNAcofold.pl (options) FASTA

	FASTA : input with 2 sequences as one entry in the RNAcofold format (ACACA...&...ACACA)

DESCRIPTION

	temperature grid: 
		from 0°C to 100°C in 5°C steps

	concentration grid:
		input sequence from 1e-06 to 0.2 increased by the factor 1.5 in each step
		rev.comp.(input) sequence from 1e-06 to 0.2 increased by the factor 1.5 in each step

EXAMPLES

	input.fna file with:
	>test
	ACAGCGTTGTGTAAGA&AAAAGTGTGTGTGAAA

	produces a output file input.fna.out : 

	# T	concA	concB	AB, AA, BB, A, B
	0 1e-06 1e-06 0.00000 0.24983 0.00001 0.00033 0.49998
	0 1e-06 1.5e-06 0.00000 0.19987 0.00002 0.00027 0.59996
	0 1e-06 2.25e-06 0.00000 0.15374 0.00004 0.00021 0.69223
	0 1e-06 3.375e-06 0.00000 0.11421 0.00006 0.00015 0.77131
	(...)

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES perl : Cwd qw(abs_path cwd), perl : File::Basename, RNAcofold (viennarna package)
";

sub parseARGV{ # call with "parseARGV(\@ARGV)" -> replaces spaces of the @ARGV values with "=" (if needed)
	my $argv=shift;my @argv_new; # argv is a reference to the ARGV
	for (my $argv_i = 0; $argv_i < scalar @{$argv} ; $argv_i++) {
		if($argv_i+1 < scalar @{$argv} && $argv->[$argv_i] =~ m/^-/ && $argv->[$argv_i] !~ m/=/ && $argv->[$argv_i+1] !~ m/^-/){
			push(@argv_new,$argv->[$argv_i]."=".$argv->[$argv_i+1]);$argv_i++; # found a case where a "=" is needed
		}else{
			push(@argv_new,$argv->[$argv_i]);
		}
	}@{$argv}=@argv_new; # save 
}

parseARGV(\@ARGV);

foreach (@ARGV) {
	if ($_ =~ /^--?(help?|h)/) {$help = 1;}
	elsif ($_ =~ /^--?(version?|v)/) {print "$0\tv$version\n"; exit 0}
	elsif($_ =~ /^-/){
		print $usage."\n[ERROR] unknown option '$_'!?\n";exit 1;
	}else{
		$infile=$_;
	}
}

if($help){print $usage; exit 0}
if($infile eq ""){print $usage."\n[ERROR] no input file\n";exit 1;}

system("RNAcofold -h >.RNAcofold.err 2>&1");
if($? != 0){print STDERR `cat .RNAcofold.err`."\n[ERROR] RNAcofold (from viennarna) is missing or is not executable (please make sure that RNAcofold is installed correctly).\n";unlink(".RNAcofold.err");exit 1;}
unlink(".RNAcofold.err");
print STDERR "RNAcofold ${COLOR1}ok${NC}, ";

system("threaded.pl -h >.threaded.pl.err 2>&1");
if($? != 0){print STDERR `cat .threaded.pl.err`."\n[ERROR] threaded.pl (from astarget) is missing or is not executable (please make sure that threaded.pl is installed correctly).\n";unlink(".threaded.pl.err");exit 1;}
unlink(".threaded.pl.err");
print STDERR "threaded.pl ${COLOR1}ok${NC}, ";

open(FHOUT,">.concfile");
my $c=1e-06; do {my $c2=1e-06; do {print FHOUT "$c\t$c2\n"; $c2*=1.5;} while $c2<0.2; $c*=1.5; } while $c<0.2;
close(FHOUT);

my $command="";
for (my $temp = 0; $temp < 100; $temp+=5) {
	$command .= "RNAcofold --noPS -f .concfile -T $temp <$infile | tail -n +11 | awk '{\\\$1=\\\"$temp \\\"\\\$1}1' >".basename($infile).".t.$temp.out; \n";
}
print STDERR $command;
system("echo \"$command\" | threaded.pl -threads=$threads");
unlink(".concfile");

system("cat ".basename($infile).".t.*.out >".basename($infile).".out;");
system("rm ".basename($infile).".t.*.out");
