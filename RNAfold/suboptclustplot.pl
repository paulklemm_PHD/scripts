#!/usr/bin/env perl
#pk 

use strict;
use warnings "all";
use threads;
use threads::shared;
use Thread::Queue;
my $q = Thread::Queue->new();

our $COLOR="\033[33m"; #yellowish
our $COLOR1="\033[32m"; #green
our $COLOR2="\033[31m"; #red
our $NC ="\033[0m"; # No Color

my $tput=`tput color 2>/dev/null`; # test if the shell supports colors
$tput=~s/[\r\n]+$//;
if ($tput=~m/[^0-9]|^&/ && $tput <16) {
  $COLOR="";$NC="";$COLOR1="";$COLOR2="";
}

use Cwd qw(abs_path cwd);
use File::Basename; # basename()

my $version="0.1";

my $shuffle_input="";
our @input_input_files;
our @input_files;
my @input_files_argv;
my %is_target_or_othertarget;
my $help=0;
my $tmp=".";
my $command="";
our $debug=0;
my $cthreshold=10000;
my $project="myproject";
my $highlight="";

my $type="rna";
my $subopte=5;
my $max_updateDisplay=60 ; # number of seconds until terminal is updated for parallel execution
my $counter_updateDisplay=0;
my $cpus=1;
my $maxsuboptrows=750;

our %result_table; 
our %result_table_subseq2seqname; # converts the actual sequence to the seqname (>...) 
my %posANDk2key;
my %input_data; 
my %nameclashcheck;
my $FH_HTML;

local $ENV{PATH} = abs_path(dirname(__FILE__)).":$ENV{PATH}"; # add the current directory of the script suboptclustplot.pl to $PATH enviroment variable

print STDERR "┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬┬
  ${COLOR2}┴┴┴${NC} suboptclustplot (v$version) ${COLOR2}┴┴┴${NC}
";

my $usage = "
suboptclustplot.pl        ${COLOR2}Subopt${NC}imal folding structures of a given input sequence are ${COLOR2}cluster${NC}ed and ${COLOR2}plot${NC}ted in this program. 

SYNOPSIS

suboptclustplot.pl (options) --input FASTA

	--input FASTA
		The input RNA (/DNA) fasta file with the input that should be evaluated. 

	OPTIONAL

	--subopte,-e INTEGER
		the max. allowed difference to the mfe [default:$subopte (in kcal/mol)]

	--maxsuboptrows INTEGER
		the max. allowed number of suboptimal structures. The --subopte,-e is reduced until there this requirement is fulfilled ! [default:$maxsuboptrows]

	--cwd DIRECTORY    The current working directory. [default:the current directory]
	--project STRING   The general prefix for all generated files (this should prevent name clashes) [default:$project]	
	--cpus,-t INTEGER  number of threads [default:$cpus]
		
DESCRIPTION

	The following external programs are needed for execution:

		RNAsubopt,RNAfold,RNAplot   : For the general fold energy of all target files 
		clusterStrings.pl, splitfasta.pl (https://gitlab.com/paulklemm_PHD/scripts)

EXAMPLES

	perl suboptclustplot.pl --input file.fna

AUTHOR
	Paul Klemm

DEPENDENCIES
	perl : Getopt::Long, File::Basename, List::Util qw( max ), threads, Threads::Queue
	vienna rna: RNAsubopt, RNAfold, RNAplot
	clusterStrings.pl, splitfasta.pl
";
#--r  If set, then the reverse complement of the input is searched too.

sub parseARGV{ # call with "parseARGV(\@ARGV)" -> replaces spaces of the @ARGV values with "=" (if needed)
	my $argv=shift;my @argv_new; # argv is a reference to the ARGV
	for (my $argv_i = 0; $argv_i < scalar @{$argv} ; $argv_i++) {
		if($argv_i+1 < scalar @{$argv} && $argv->[$argv_i] =~ m/^-/ && $argv->[$argv_i] !~ m/=/ && $argv->[$argv_i+1] !~ m/^-/){
			push(@argv_new,$argv->[$argv_i]."=".$argv->[$argv_i+1]);$argv_i++; # found a case where a "=" is needed
		}else{
			push(@argv_new,$argv->[$argv_i]);
		}
	}@{$argv}=@argv_new; # save 
}
parseARGV(\@ARGV);

foreach (@ARGV) {
	if ($_ =~ /^--?(inputs?)=(.+)$/) { push(@input_files_argv,split(/[, ]/,$2)); }
	elsif ($_ =~ /^--?(cwd|tmp)=(.+)$/) {$tmp = $2;}
	elsif ($_ =~ /^--?(project)=(.+)$/) {$project = $2;$project=~s/[ .]/_/g;}
	elsif ($_ =~ /^--?(subopte?|e)=([0-9]+)$/) {$subopte = $2;}
	elsif ($_ =~ /^--?(maxsuboptrows?)=([0-9]+)$/) {$maxsuboptrows = $2;}
	elsif ($_ =~ /^--?(type?)=(rna|dna)$/) {$type = $2;}
	elsif ($_ =~ /^--?(cpus?|t)=([0-9]+)$/) {$cpus=$2;}
	elsif ($_ =~ /^--?(c)=(.+)$/) {$cthreshold = $2;}
	elsif ($_ =~ /^--?(x)/) {$debug = 1;}
	elsif ($_ =~ /^--?(help?|h)/) {$help = 1;}
	else {
		print $usage."\n[ERROR] unknown option '$_' !?\n";exit 1;
	}
}

my $cwd=cwd;

if( $help ){print $usage; exit 0;}
if( ( scalar @input_files_argv ) == 0){print $usage."[ERROR] One target (--input) is required!\n"; exit 1;}

if($shuffle_input ne ""){ $project.="_shuffled$shuffle_input"; }

################################################
# threads stuff
################################################

sub worker{
	local $SIG{KILL} = sub { threads->exit };
	my $tid = threads->tid;

	# fetch jobs until you recive an undef (there is an undef for each thread at the end of the queue, so all threads allways terminate)
	while (defined(my $sys = $q->dequeue())) { # they terminate for a undef job 
		
		my $errfile = "";
		while($sys =~ /2>>? ?([^ ;)]+)/g){ $errfile = $1 }

		if($debug){print STDERR "tid=$tid starting '$sys'\n"}

		system($sys);

		if($debug){print STDERR "tid=$tid done with '$sys'\n"}
		
		if($? != 0){
			if($errfile ne "" && $errfile ne "/dev/null" && -e $errfile){ print STDERR `cat '$errfile'` }
			print STDERR "\n[ERROR] '$sys' failed.\n";exit 1;
		}
	}
	return;
}

sub spawnWorkers{
	for (my $i = 0; $i < $cpus; $i++) { threads->create("worker") } # spawn worker
}

# handle a SIG/INT terminating gracefully
$SIG{TERM} = sub {
 	foreach (threads->list()) {$_->kill('KILL')->detach}
 	exit 1;
};
$SIG{INT} = $SIG{TERM};
################################################
# temporary directory / cwd 
################################################

print STDERR "${COLOR}[suboptclustplot]$NC Starting suboptclustplot workflow of project '$project' with $cpus threads...\n";

# temporary directory stuff
if(!-d $tmp){print STDERR "$usage\n[ERROR] The current working directory '$tmp' (-cwd) does not exists.\n";exit 1;}
my $cur_rnd_dir="$tmp/${project}_suboptclustplot_cwd";#.rand();
#if(-d $cur_rnd_dir && !$debug){print STDERR "!! WARNING !! The current working directory '$cur_rnd_dir' exists allready ! (you can specify the project name with -project=...)\n\n I will wait 10 seconds and then continue anyway (overwriting '$project')...\n\n";sleep 10;}
if(!-d $cur_rnd_dir){system("mkdir -p '$cur_rnd_dir'");};
if($?!=0){print STDERR "$usage\n[ERROR] The current working directory '$tmp' could not be used to create a working directory (please choose a directory with writing permissions with -cwd).\n";exit 1;}
print STDERR "${COLOR}[suboptclustplot]$NC Creating current working working directory $cur_rnd_dir ...\n";

my $cur_rnd_dir_MASTER=abs_path($cur_rnd_dir);

################################################
# test if all programs are executable
################################################

print STDERR "${COLOR}[suboptclustplot]$NC Checking if all suboptclustplot programs are executable ...\n";

print STDERR "${COLOR}[suboptclustplot]$NC ";

system("mkdir -p '$cur_rnd_dir_MASTER/test_all_programs'");
$cur_rnd_dir=$cur_rnd_dir_MASTER."/test_all_programs";

sub test_program{

	my $program=shift;

	system("$program -h >'$cur_rnd_dir/$program.err' 2>&1");
	if($? != 0){print STDERR `cat '$cur_rnd_dir/$program.err'`."\n[ERROR] $program missing or is not executable (please make sure that $program is installed correctly).\n";return 0;}
	print STDERR "$program ${COLOR1}ok${NC}, ";
	return 1;
}

if($debug){goto SKIP_TEST;}

test_program("splitfasta.pl")   || exit 1;
test_program("clusterStrings.pl")   || exit 1;
#test_program("RNAclust")   || exit 1;
test_program("RNAsubopt")     || exit 1;
test_program("RNAfold")     || exit 1;

print STDERR "\n";

SKIP_TEST:

open($FH_HTML,">","$cwd/$project.html") || die($!);

print $FH_HTML "
<!DOCTYPE html>
<head>
<meta content='text/html;charset=utf-8' http-equiv='Content-Type'>
<meta content='utf-8' http-equiv='encoding'>
<style> table{ border-collapse: collapse;
	border-spacing: 0;
	empty-cells: show;
	border: 1px solid #cbcbcb;
	font-family: monospace;
	font-weight: normal;
	width:100%; }
td, th { border-left: 1px solid #cbcbcb;/*  inner column border */
	border-width: 0 0 0 1px;
	font-size: inherit;
	margin: 0;
	overflow: visible; 
	padding: 0.5em 1em; }
.clickable{cursor: pointer; -webkit-touch-callout: none; -webkit-user-select: none; -khtml-user-select: none; -moz-user-select: none; -ms-user-select: none;user-select: none;}
.clickable:hover{text-decoration: underline}
tr > td:nth-of-type(1) { max-width: 10%; }
thead { background-color: #e0e0e0;
	color: #000;
	text-align: left;
	vertical-align: bottom; }
#errorbox { 
  width: 90%; 
  max-width: 800px;
  margin: 0 auto; 
  background-color:#FFA500 ;    
  background: repeating-linear-gradient(
    45deg,
    rgba(255,0,0,.05),
    rgba(255,0,0,.05) 10px,
    rgba(0,0,0,0) 10px,
    rgba(0,0,0,0) 20px
  );
  margin-bottom:5px;
  padding: 10px;
  border-style:solid;
  border-width:5px;
  border-color:rgba(255,0,0,.2);
    text-align: left; }
body{margin-bottom:260px;
  margin-left: 0px;
  font-family: 'Lato', sans-serif;}
h1{position:relative;
  background-color: white;
  z-index:1;
  margin: 50px;}
#main { 
  width: 90%; 
  max-width: 1200px;
  margin: 0 auto;    
  padding: 10px;
  border-style:solid;
  border-width:5px;
  border-color:#a7b6d4;
    text-align: left; }
body{margin-bottom:260px;
  margin-left: 0px;
  font-family: 'Lato', sans-serif;}
h1{position:relative;
  background-color: white;
  z-index:1;
  margin: 50px;}
.control{position:fixed;
	background-color:#dedede;
	border:1px solid #000000;
  top:0;right:0;}
h2{color:#2b384f; padding-left:0;margin-left:5px}
h2 + div{margin-left:5px;border-left:2px solid #a7b6d4;}
h2 + div *{margin-left:5px}
h3{color:#2b384f; margin-left:10px}
h3 + div{margin-left:10px;border-left:1px solid #a7b6d4;}
h3 + div *{margin-left:10px}
h4{margin:1px;margin:1px;margin-left:15px}
h4 + div *{margin:1px;margin:1px;margin-left:15px}
h5{margin:1px;margin:1px;margin-left:20px}
h5 + div *{margin:1px;margin:1px;margin-left:20px}
  </style>
<script>
scaleimg = (factor) => { var imgs = document.getElementsByTagName('img');
	for (i = 0; i < imgs.length; i++) {
	  var s = parseInt(imgs[i].style.width.replace('%',''));
	  if(factor === 1 && s < 100){ s+=10 }
	  if(factor === -1 && s > 10){ s-=10 }
	  imgs[i].style.width=s+'%';
	} 
}
toggle = (id) => { var x = document.getElementById(id); if (x.style.display === 'none') { x.style.display = 'block'; } else { x.style.display = 'none'; } };
</script></head>
<body>
<div class='control'>resize images: <button onclick='scaleimg(1)'>+</button><button onclick='scaleimg(-1)'>-</button></div>
<div id='errorbox'><h2>Work in progress</h2><br>suboptclustplot.pl is currently working on this project, intermediate results are displayed below.<br>Refresh the page to check for updates.<br></div>
<div id='main'>";

################################################
# test if input exists and write first log outs. Rename input files (remove all but one dot)
################################################

# test if input_files exists

print $FH_HTML "<h2>Input</h2><ul>";

for(my $input_i = 0 ; $input_i < scalar @input_files_argv ; $input_i++){

	if(! -e $input_files_argv[$input_i]){print STDERR "$usage\n[ERROR] The input '".$input_files_argv[$input_i]."' does not exists.\n";exit 1;}
	system("echo 'input	$input_files_argv[$input_i]	".abs_path($input_files_argv[$input_i])."' >> '$cwd/$project.log'"); #log

	if(exists $nameclashcheck{basename($input_files_argv[$input_i])}){print STDERR "$usage\n[ERROR] the file name '".basename($input_files_argv[$input_i])."' clashes with other filenames, please make sure each input file has a different file name.\n";exit 1;}else{$nameclashcheck{basename($input_files_argv[$input_i])}=1;}

	my $new_filename=basename($input_files_argv[$input_i]);
	while($new_filename=~m/\.([^.]*\.[^.]*)$/){$new_filename=~s/\.([^.]*\.[^.]*)$/_$1/g;} # remove all but one dot

	system("ln -s '".abs_path($input_files_argv[$input_i])."' '$cur_rnd_dir_MASTER/$new_filename'");
	system("cd '$cur_rnd_dir_MASTER/'; splitfasta.pl '$new_filename'");

	foreach my $f (glob "$cur_rnd_dir_MASTER/${new_filename}_fastaFiles/*"){
		
		my $f_base = basename($f);

		print $FH_HTML "<li>$f_base (".`grep -v ">" $f | tr -d '\\n' | wc -c | awk '{print \$1}'`."nt)</li>";

		push(@input_files,"$cur_rnd_dir_MASTER/${new_filename}_fastaFiles/$f_base");
	}
}

print $FH_HTML "</ul>";

################################################
# Calculate regular fold of all inputs
################################################

my %mfe;

print STDERR "${COLOR}[suboptclustplot]$NC Calculate regular fold (RNAfold) of inputs ...\n";

system("mkdir -p '$cur_rnd_dir_MASTER/RNAfold'");
$cur_rnd_dir=$cur_rnd_dir_MASTER."/RNAfold";

print $FH_HTML "<div><h2 class='clickable' onclick='toggle(11)'>Secondary structure prediction + dotplot (RNAfold)</h2><div style='display:none' id='11'>

<p>The dot plot shows a matrix of squares with area proportional to the pairing probability in the upper left half (red and gray), and one square for each pair in the minimum free energy structure in the lower right half (blue).<br>
Only base pairings with a probability p larger than 10E\-6 are displayed.<br></p>

<table>";

print $FH_HTML "<tbody>";

for(my $input_i = 0 ; $input_i < scalar @input_files ; $input_i++){

	my $f_base = basename($input_files[$input_i]);
	
	my $chars = `grep -v ">" $input_files[$input_i] | tr -d "\\n" | wc -m | awk '{print \$1}'`;
	chomp($chars);

	if($chars > $cthreshold){
		print STDERR "${COLOR}[suboptclustplot]$NC The input file '$input_files[$input_i]' contains more than $cthreshold characters, therefore RNAfold is skipped (change this behavior with -c)\n";
		print $FH_HTML "<tr><td>$f_base<br>skipped</td></tr>";
		next;
	}elsif($chars > $cthreshold/2){
		print STDERR "${COLOR}[suboptclustplot]$NC WARNING: The input file '$input_files[$input_i]' contains more than ".($cthreshold/2)." characters, this can take a long time (change this behavior with -c)\n";
	}

	if(-e "$cur_rnd_dir/$f_base.RNAfold"){print STDERR "${COLOR}[suboptclustplot]$NC Is allready present skipping ...\n";}
	else{

		system("mkdir -p '$cur_rnd_dir/$f_base.RNAfold'");

		$command="cd '$cur_rnd_dir/$f_base.RNAfold'; RNAfold -p <'".$input_files[$input_i]."' >'../$f_base.RNAfold.out' 2>'$cur_rnd_dir/$f_base.RNAfold.err'";
		
		$q->enqueue($command);
	}
}

$q->enqueue(undef) for 1..$cpus; # enqueue the termination signal
spawnWorkers(); # spawn worker threads
while( $q->pending() > 0 ){ if($debug){print STDERR "zzz pending=".$q->pending()."\n"} sleep 1 } # await 
$_->join() for threads->list();

for(my $input_i = 0 ; $input_i < scalar @input_files ; $input_i++){

	my $f_base = basename($input_files[$input_i]);
	
	system("echo 'RNAfold of inputs	OK	$command' >> '$cwd/$project.log'"); #log

	open(my $FH,"<","$cur_rnd_dir/$f_base.RNAfold.out") || die($!);
	my $isFirst=1;
	my $name="";
	my $seq="";
	my $ss="";
	while(<$FH>){
		$_=~s/[\n\r]+$//g;
		if(substr($_,0,1) eq ">"){
			$name=$_;
			$name=~s/^>//g;
			chomp($name);
		}
		elsif(length($_)>0 && substr($_,0,1) ne ">" && $isFirst){
			$seq = $_; $isFirst=0;
		}
		elsif(length($_)>0 && substr($_,0,1) ne ">" && !$isFirst && $_=~m/([().]+) \(([^()]+)\)/){ 

			$ss = $1;
			$mfe{$f_base}=$2;
		}
		elsif(length($_)>0 && substr($_,0,1) ne ">" && !$isFirst && $_=~m/frequency of mfe structure in ensemble *([^;]+);/){ 
			
			my $filepath=`ls $cur_rnd_dir/$f_base.RNAfold/*ss.ps | head -n1 | tr -d '\n'`;
			if(substr($filepath,0,length $cwd) eq $cwd){$filepath=substr($filepath,length $cwd); $filepath=~s/^\///}
			
			my $filepath2=`ls $cur_rnd_dir/$f_base.RNAfold/*dp.ps | head -n1 | tr -d '\n'`;
			if(substr($filepath2,0,length $cwd) eq $cwd){$filepath2=substr($filepath2,length $cwd); $filepath2=~s/^\///}

			print $FH_HTML "<tr><td>$name<br>$seq<br>$ss<br>MFE = $mfe{$f_base} kcal/mol<br>frequency of MFE =  ".(int($1*100*100)/100)."%<br>
			</td></tr><tr><td><img style='width:30%' src='$filepath.svg' alt='(program is still working / project directory is missing)' />
			<img style='width:30%' src='$filepath2.svg' alt='(program is still working / project directory is missing)' /></td></tr>";
		}
	}
	close($FH);
}
print $FH_HTML "<tbody></table></div>";

################################################
# Calculate regular suboptfold of all input
################################################

print STDERR "${COLOR}[suboptclustplot]$NC Calculate suboptimal folds (RNAsubopt) of inputs + generating clusters...\n";

system("mkdir -p '$cur_rnd_dir_MASTER/'");
$cur_rnd_dir=$cur_rnd_dir_MASTER."/";

print $FH_HTML "</div>\n<div><h2 class='clickable' onclick='toggle(1)'>Suboptimal secondary structure prediction (RNAsubopt)</h2><div style='display:none' id='1'>

<p>Suboptimal secondary structures that are withing $subopte kcal/mol from the optimal mfe structure (but at most $maxsuboptrows entries are generated).<br>
Structures that are similar enough are clustered together using a simple iterative heuristic.<br>
The structures in each cluster/group are ordered by mfe.</p>";

our %cur_subopte;

for(my $input_i = 0 ; $input_i < scalar @input_files ; $input_i++){

	my $f_base = basename($input_files[$input_i]);
	my $f_base_display = $f_base; $f_base_display =~s/_*\.fasta$//;
	$cur_subopte{$f_base}=$subopte;

	if(-e "$cur_rnd_dir/$f_base/$f_base.RNAsubopt.out"){print STDERR "${COLOR}[suboptclustplot]$NC $f_base RNAsubopt (1) is allready present skipping ...\n";}
	else{

		system("mkdir -p '$cur_rnd_dir/$f_base'");

		DOSUBOPT:

		$command="cd '$cur_rnd_dir/$f_base'; RNAsubopt −−nonRedundant -e $cur_subopte{$f_base} -s --noLP  <'".$input_files[$input_i]."' >'$f_base.RNAsubopt.out' 2>'$f_base.RNAsubopt.err'";
		
		system($command);

		while(`wc -l '$cur_rnd_dir/$f_base/$f_base.RNAsubopt.out' | awk '{print \$1}' | tr -d '\n'` > $maxsuboptrows && $cur_subopte{$f_base}>1){
			$cur_subopte{$f_base}-=.1;
			goto DOSUBOPT;
		}
	}
}

for(my $input_i = 0 ; $input_i < scalar @input_files ; $input_i++){

	my $f_base = basename($input_files[$input_i]);
	my $f_base_display = $f_base; $f_base_display =~s/_*\.fasta$//;

	if(-e "$cur_rnd_dir/$f_base/$f_base.clusterStrings.err"){print STDERR "${COLOR}[suboptclustplot]$NC $f_base clusterStrings.pl is allready present skipping ...\n";}
	else{

		$command="cd '$cur_rnd_dir/$f_base'; cat '$f_base.RNAsubopt.out' | tail -n +3 | awk '!(\$1 in d){i_d=sprintf(\"\%04d\", i++); print \">id\"i_d\"_\"\$2\"".'\n'."\"\$1; d[\$1]=1;}' | clusterStrings.pl -fasta -all - -tofiles 2>'$cur_rnd_dir/$f_base/$f_base.clusterStrings.err'";

		$q->enqueue($command);
	}
}

$q->enqueue(undef) for 1..$cpus; # enqueue the termination signal
spawnWorkers(); # spawn worker threads
while( $q->pending() > 0 ){ if($debug){print STDERR "zzz pending=".$q->pending()."\n"} sleep 1 } # await 
$_->join() for threads->list();

my $ii = 0;
my $ki = 0;
for(my $input_i = 0 ; $input_i < scalar @input_files ; $input_i++){

	$ii++;
	my $f_base = basename($input_files[$input_i]);
	my $f_base_display = $f_base; $f_base_display =~s/_*\.fasta$//;

	my $head = `head -n2 '$cur_rnd_dir/$f_base/$f_base.RNAsubopt.out' | tail -n1 | awk '{print \$1}' | tr -d '\n'`;

	$command="(
		cd '$cur_rnd_dir/$f_base';".' 
		for f in STDIN.group.*; do 
			perl -lne \'print $_; if(/^>/){print "'.$head.'"}\' "$f" >tmp; mv tmp "'."$f_base".'.$f"; rm "$f"; 
		done; 
		cat "'.$f_base.'".* >"'.$f_base.'.clust"
	) 2>"'."$cur_rnd_dir/$f_base/$f_base".'.rename.err"';

	system($command);
	
	if($? != 0){
		my $errfile="$cur_rnd_dir/$f_base/$f_base.rename.err";
		print STDERR `cat '$errfile'`."\n[ERROR] rename failed.\n";exit 1;
	}

	my $generatedrows = `wc -l '$cur_rnd_dir/$f_base/$f_base.RNAsubopt.out' | awk '{print \$1}' | tr -d '\n'`;

	print $FH_HTML "\n<h3 class='clickable' onclick='toggle(\"f_$ii\")'>$f_base_display</h3><div style='display:none' id='f_$ii'><p>generated $generatedrows suboptimal structures within $cur_subopte{$f_base} kcal/mol of the optimal mfe (-subopte).</p><hr>";

	my $FH_HTML_MORE;

	foreach my $k (reverse sort { -s $a <=> -s $b } glob "$cur_rnd_dir/$f_base/*.group.*"){
		
		my $k_base = basename($k);
		my $k_base_display=$k_base;
		$k_base_display=~s/\.RNAsubopt\.out\./ RNAsubopt group id:/;
		$k_base_display=~s/.*\.STDIN\.group\./RNAsubopt group id:/;
		my $numElements = `grep '>' $k | wc -l | tr -d '\n'`;
		$k_base_display.=". ".( int(10000*$numElements/$generatedrows)/100 )."% ($numElements members)";

		if(-e "$cur_rnd_dir/$k_base.RNAplot"){print STDERR "${COLOR}[suboptclustplot]$NC $k_base RNAplot is allready present skipping ...\n";}
		else{

			system("mkdir -p '$cur_rnd_dir/$f_base/$k_base.RNAplot'");

			$command="(cd '$cur_rnd_dir/$f_base/'; tr '".'\n'."' '\%' <'$k_base' | sed 's/\%\$//' | sed 's/\%>/".'\n>'."/g' | sort -t '_' -k1,1 | tr '\%' '".'\n'."' >tmp; mv tmp '$k_base') 2>'$cur_rnd_dir/$f_base/$k_base.RNAplot/$k_base.sorting.err'";
			
			system($command);
			
			if($? != 0){
				my $errfile="$cur_rnd_dir/$f_base/$k_base.RNAplot/$k_base.sorting.err";
				print STDERR `cat '$errfile'`."\n[ERROR] sorting failed.\n";exit 1;
			}

			$command="cd '$cur_rnd_dir/$f_base/$k_base.RNAplot'; splitfasta.pl '../$k_base'; for f in ${k_base}_fastaFiles/*; do RNAplot <\"\$f\" >'$k_base.RNAplot.out' 2>>'$k_base.splitfasta.err'; done";
			
			system($command);
			
			if($? != 0){
				my $errfile="$cur_rnd_dir/$f_base/$k_base.RNAplot/$k_base.splitfasta.err";
				print STDERR `cat '$errfile'`."\n[ERROR] RNAplot of subopt structs failed.\n";exit 1;
			}

			my $seq="";
			my $name="";
			my $mfe="";

			my $groupNumber = 0;
			if($k_base =~ /\.group\.([0-9]+)/){$groupNumber=$1}

			print $FH_HTML "<h4 class='clickable' onclick='toggle(\"group_${k_base}_$groupNumber\")'>$k_base_display</h4><div style='display:block' id='group_${k_base}_$groupNumber'>";

			my $linecounter = 0;
			open(my $FH, "<","$cur_rnd_dir/$f_base/$k_base");
			while(<$FH>){
				$_=~s/[\n\r]+$//g;
				if(/>([^_]+)_([^_ ]+)/){
					$name=$1;
					$mfe=$2;
				}elsif(/^([ACGTUacgtu]+)$/){
					$seq=$1;
				}elsif(/^([().]+)$/){

					my $filepath=`ls $cur_rnd_dir/$f_base/$k_base.RNAplot/${name}*ps | head -n1 | tr -d '\n'`;
					if(substr($filepath,0,length $cwd) eq $cwd){$filepath=substr($filepath,length $cwd); $filepath=~s/^\/+//}
					$filepath=~s/\/+/\//g;

					my $filepathmorehtmlsvg=`cd $cur_rnd_dir/$f_base/; ls $k_base.RNAplot/${name}*ps | head -n1 | tr -d '\n'`;
					if(substr($filepathmorehtmlsvg,0,length $cwd) eq $cwd){$filepathmorehtmlsvg=substr($filepathmorehtmlsvg,length $cwd); $filepathmorehtmlsvg=~s/^\/+//}
					$filepathmorehtmlsvg=~s/\/+/\//g;

					if($linecounter==1){
						my $filepathmorehtml="$cur_rnd_dir/$f_base/$k_base.more.html";
						if(substr($filepathmorehtml,0,length $cwd) eq $cwd){$filepathmorehtml=substr($filepathmorehtml,length $cwd); $filepathmorehtml=~s/^\///}
			
						print $FH_HTML "<h5><a href='$filepathmorehtml'>more</a></h5>";
						open($FH_HTML_MORE,">","$cur_rnd_dir/$f_base/$k_base.more.html");
					}

					if($linecounter>0){ # print to the "more" html file
						print $FH_HTML_MORE "<b>$name ($mfe kcal/mol)</b><br>";
						print $FH_HTML_MORE "$seq<br>$_<br>MFE = $mfe kcal/mol<br><img style='width:20%' src='$filepathmorehtmlsvg.svg' alt='(program is still working / project directory is missing)' /><br><hr><br>"; # h5 
					}else{
						print $FH_HTML "<p>best member: $name ($mfe kcal/mol)</p>";
						print $FH_HTML "<table><tbody>";
						print $FH_HTML "<tr><td>$seq<br>$_<br>MFE = $mfe kcal/mol<br><img style='width:20%' src='$filepath.svg' alt='(program is still working / project directory is missing)' /></td></tr>";
						print $FH_HTML "</tbody></table>"; # h5 
					}

					$ki++;
					$linecounter++;
				}
			}
			close($FH);

			if($linecounter>1){close($FH_HTML_MORE)} #h5 more
			print $FH_HTML "</div><hr>"; #h4

		}
	}
	print $FH_HTML "</div>\n"; # h3
}

print $FH_HTML "\n</div>"; # h2
print $FH_HTML "\n</div>"; # div of second content
print $FH_HTML "\n</body>";

renderPS();

sub renderPS{

	print STDERR "${COLOR}[suboptclustplot]$NC Rendering postscripts files to vector images (svg files are needed for the html output) ...\n";

	system("mkdir -p '$cur_rnd_dir_MASTER/ssps2svg/'");

	my $allcommands=`find '$cur_rnd_dir_MASTER' -type d `;

	for my $c (split("\n",$allcommands)){
		foreach my $ps (glob(qq("${c}/*ps"))) {
			if(-e $ps){
				$q->enqueue("ssps2svg.R '$ps' 2>'$cur_rnd_dir_MASTER/ssps2svg/".basename($ps).".err' && rm '$ps'");
			}
		}
	}

	$q->enqueue(undef) for 1..$cpus; # enqueue the termination signal
	spawnWorkers(); # spawn worker threads
	while( $q->pending() > 0 ){ if($debug){print STDERR "zzz pending=".$q->pending()."\n"} sleep 1 } # await 
	$_->join() for threads->list();
}

write__result_table__toFile("$cwd/$project.result");

print $FH_HTML "<script> (function() {document.getElementById('errorbox').style.display = 'none'; })();</script>";

system("touch '$cwd/done'");

sub write__result_table__toFile{

	my $filename=shift;

	open(my $FH,">",$filename) || die($!);

		my $isFirst=1;

		foreach my $key (sort keys %{$result_table{basename($input_files[0])}} ){

			my $key2 = (keys %{$result_table{basename($input_files[0])}{$key}})[0];

			print $FH "".($isFirst ? "":"\t").$key2;
			$isFirst=0;
		}
		print $FH "\n";

		for(my $input_i = 0 ; $input_i < scalar @input_files ; $input_i++){ # go over all target files 
			$isFirst=1;

			foreach my $key (sort keys %{$result_table{basename($input_files[$input_i])}} ){
				if($key eq "-" || $key eq ""){next;} 

				my $key2 = (keys %{$result_table{basename($input_files[$input_i])}{$key}})[0];

				print $FH "".($isFirst ? "":"\t").$result_table{basename($input_files[$input_i])}{$key}{$key2};
				$isFirst=0;

			}
			print $FH "\n";
		}
	close($FH);
}

sub namebr{
	my $in = shift;
	my $ret="";
	for (my $ii = 0; $ii < length($in); $ii++) {
		$ret.=substr($in,$ii,1);
		if($ii > 1 && $ii % 20 == 0){$ret.="<br>"}
	}
	$ret=~s/\.(fasta|fna|fa|f)$//g;
	return $ret;
}

print STDERR "${COLOR}[suboptclustplot]$NC Done ...\n";
foreach (threads->list()) {$_->kill('KILL')->detach}

exit 0;

