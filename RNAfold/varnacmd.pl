#!/usr/bin/env perl
#pk
use strict;
use warnings "all";

use File::Basename;

my $version="0.1.6"; # last updated on 2021-10-05 11:53:44

my $fold="";
my $seq="";
my $highlight="";
my $anno="";
my $ignore=0;
my $output="varna.svg";
my $varnadir=dirname(__FILE__);
my $resolution=10;
my $result = "";
my $usage="varnacmd.pl  a simple varna interface

SYNOPSIS
    varnacmd.pl (options)

    -seq SEQ             : the sequence (varnas -sequenceDBN)
    -fold FOLD           : the fold information (varnas -structureDBN)
    -highlight HIGHLIGHT : varnas -highlightRegion. 
                           e.g. 1-15:fill=#cfcfcf;5-20:outline=#cfcfcf
                           -> highlights pos 1-16 (1-based index!)
                           or you can supply one subsequence that you want to highlight (or multiple delimitted by ;)
    -output OUTPUTNAME   : the output name (default:$output). Needs a suffix like png,jpg,svg,... !
    -resolution R        : resolution of the output image (default:resolution)
    -varnadir DIR        : varna directory (default:cwd and /usr/local/bin/)
    --ignore NUM         : ignore the first NUM positions (secondary structure)
                           if NUM is negative: ignore the left most NUM positions
    --anno STR           : annotation vector (;-separated)
                           e.g. ⬀:type=B,anchor=2;texthere:type=B,anchor=3;...
                           (1-based index)

    instead of seq and fold you can also provide a fold file as given by RNAfold through the STDIN (RNAfold myseq.fasta | varnacmd.pl)

    NOTE : svg images can be easily converted to pdf using 'inkscape --export-pdf=output.pdf input.svg'

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES java : VARNA*.jar (http://varna.lri.fr/index.php?lang=en&css=varna&page=downloads), perl : File::Basename
";

if (scalar @ARGV == 1 && $ARGV[0] =~ m/^--?(version|v)$/)        { print "$0\tv$version\n"; exit 0;   }
if( scalar @ARGV == 0 || $ARGV[0] =~ m/^--?h/ ){
    print STDERR $usage;
    exit scalar @ARGV == 0 || $ARGV[0] =~ m/^--?h/ ? 0 : 1;
}

my $argv_str=join(" ",@ARGV);
$argv_str=~s/-([^ =]+) ([^ =-]+)/-$1=$2/g;
@ARGV=split(" ",$argv_str);

my $output_is_set=0;
if( scalar @ARGV != 0 ) {
    for(my $i = 0 ; $i < scalar @ARGV; $i++){
        if($ARGV[$i]=~m/^--?seq=([^ ]+)/){$seq=$1}
        elsif($ARGV[$i]=~m/^--?fold=([^ ]+)/){$fold=$1}
        elsif($ARGV[$i]=~m/^--?highlight=([^ ]+)/){$highlight=$1}
        elsif($ARGV[$i]=~m/^--?resolution=([^ ]+)/){$resolution=$1}
        elsif($ARGV[$i]=~m/^--?varnadir=([^ ]+)/){$varnadir=$1}
        elsif($ARGV[$i]=~m/^--?ignore=([0-9-]+)/){$ignore=$1}
        elsif($ARGV[$i]=~m/^--?anno=([^ ]+)/){$anno=$1}
        elsif($ARGV[$i]=~m/^--?output=([^ ]+)/ || $ARGV[$i]=~m/^--?o=([^ ]+)/){$output=$1; $output_is_set=1;}
        elsif($ARGV[$i]!~m/^-/ && !$output_is_set){$output=$ARGV[$i].".svg"}
    }
}

$result = `java -cp $varnadir/VARNA*.jar fr.orsay.lri.varna.applications.VARNAcmd -h 2>&1 | head -n1`;
if($result =~ /Error/){
    $varnadir = "/usr/local/bin/";
    $result = `java -cp $varnadir/VARNA*.jar fr.orsay.lri.varna.applications.VARNAcmd -h 2>&1 | head -n1`;
    if($result =~ /Error/){
        print STDERR `java -cp $varnadir/VARNA*.jar fr.orsay.lri.varna.applications.VARNAcmd -h`."\n[ERROR] VARNA missing or is not executable ($?) (please make sure that VARNA*.jar is executable and is inside --varnadir).\n";exit 1;
    }
}
print STDERR "[varnacmd.pl] found $varnadir/VARNA*.jar '$result': OK\n";

if($seq eq ""){
    system("RNAfold -h >/dev/null 2>&1");
    if($? != 0){print STDERR "[ERROR] RNAfold missing or is not executable ($?).\n";exit 1;}
    print STDERR "[varnacmd.pl] found RNAfold : OK\n";
}

if($seq eq "-" || $seq eq "" || -e $seq){
    while(<>){
        if($_=~m/([().]{3,})/){$fold=$1}
        if($_=~m/\b([ACGTUNacgtun]{3,})\b/){$seq=$1}
    }
}

my $command="";
if($fold eq ""){
    $command = "echo '$seq' | RNAfold | tail -n1 | sed 's/ .*//g'";
    print STDERR "$command\n";
    $fold = `$command`;
    if($? != 0){print STDERR "[ERROR] RNAfold failed ($?).\n";exit 1;}
    chomp($fold);
}

if($ignore != 0){
    if($ignore>0){
        for (my $i = 0; $i < $ignore; $i++) { substr($fold,$i,1) = "X" }
    }else{
        for (my $i = 0; $i < -$ignore; $i++) { substr($fold,length($fold)-$i-1,1) = "X" }
    }
}

if($highlight =~ m/^[ACGTUNacgtun;]+$/){
    my $newhighlight="";
    foreach my $hi (split(";",$highlight)){
        while($seq=~m/$hi/g){ if($newhighlight ne ""){$newhighlight.=";"} $newhighlight .= ($-[0]+1)."-".($-[0]+length $hi).":#ff0000" };
    }
    $highlight=$newhighlight;print $newhighlight."\n";
}

if(length $fold != length $seq){
    print STDERR $usage;
    print STDERR "[ERROR] length of sequence and fold does not agree (seq=$seq ".(length $seq)." and fold=$fold ".(length $fold).")";
    exit 1;
}

$command = "java -cp $varnadir/VARNA*.jar fr.orsay.lri.varna.applications.VARNAcmd -sequenceDBN '$seq' -structureDBN '$fold' -o '$output' -resolution '$resolution' -highlightRegion '$highlight'";
if($anno ne ""){$command.=" -annotations '$anno'"}
print STDERR "$command\n";

$result = `$command`;
if($result =~ /Error/){print STDERR "[ERROR] fr.orsay.lri.varna.applications.VARNAcmd failed with:\n$result.\n";exit 1;}

exit 0;
