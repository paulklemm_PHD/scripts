#!/usr/bin/env perl
#pk

use threads;
use threads::shared;
use Thread::Queue;
my $q = Thread::Queue->new();
my @ret :shared;

use strict;
use warnings "all";

my $version="0.0.22"; # last updated on 2022-02-18 12:51:44

our $COLOR="\033[33m"; #yellowish
our $COLOR1="\033[32m"; #green
our $COLOR2="\033[31m"; #red
our $NC ="\033[0m"; # No Color

my $tput=`tput color 2>/dev/null`; # test if the shell supports colors
$tput=~s/[\r\n]+$//;
if ($tput=~m/[^0-9]|^&/ && $tput <16) {
  $COLOR="";$NC="";$COLOR1="";$COLOR2="";
}

use Cwd qw(abs_path cwd);
use List::Util qw(max);
use File::Basename; # dirname

my $help=0;
our $INPUT="";
our $arg="";
our $w=1500;
our $s=int($w/5);
our $cpus=6;
our $debug=0;
local $ENV{PATH} = "$ENV{PATH}:".abs_path(dirname(__FILE__));

my $usage = "
RNAup_slidingWindow.pl     performes RNAup in k sized windows with step size s.

SYNOPSIS

RNAup_slidingWindow.pl (options) FASTA/-

	FASTA file or - for STDIN 
		The input should have at least 2 sequences. The first sequence is used as the (longer) target to 
		compare to all other (shorter) sequences.

	-w : window size (default $w), needs to be smaller than the long target
	-s : step size (default $s = 20\% of the window size), needs to be smaller than the window size (at most 0.5*w)
	--cpus, -t : number of threads (default: $cpus)
	--arg, -a : additional RNAup parameters. The parameters are substituted for ARG in: 
		RNAup ARG --interaction_first --no_output_file << INPUP'

DESCRIPTION

	A w sized window slides in s steps across the input sequence and RNAup is perfomed and
	the optimal/minimal binding energy is printed. Bindings to the edges of the window 
	(first and last area of size s) are omitted.
	The result should correspond to a full RNAup call if there are no long-range 
	interaction (longer than window size $w).

	!! Either the first input or the other input sequences needs to be longer than the window size w !!
	
	The RNAup modus is '--interaction_first --no_output_file'.

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES perl : Cwd qw(abs_path cwd), List::Util qw(max), File::Basename, Thread::Queue, RNAup (viennarna package)
";

foreach my $a (@ARGV) {
	if ($a =~ m/^--?(help?|h)/) {$help = 1;}
	elsif ($a =~ m/^--?(s)[= ]([0-9]+)/) {$s = $2;}
	elsif ($a =~ m/^--?(w)[= ]([0-9]+)/) {$w = $2;}
	elsif ($a =~ m/^--?(debug|x)/) {$debug = 1;}
	elsif ($a =~ m/^--?(version|v)$/) {print "$0\tv$version\n"; exit 0;}
	elsif ($a =~ m/^--?(cpus?|t)[= ]([0-9]+)/) {$cpus = $2;}
	elsif ($a =~ m/^--?(arg?|a)[= ](.+)/) {$arg = $2;}
	elsif($a =~ m/^--?./){
		print $usage."\n[ERROR] unknown option '$_'!?\n";exit 1;
	}else{
		$INPUT=$a;
	}
}

if($help){print $usage; exit 0}

my %data;
our $name="";
our $name_first="";
our $seq_first="";
our $total_qs=1;
sub processLine{
	$_=~s/[\n\r]+$//g;
	if($_=~m/^>/){
		if($name_first eq ""){ $name_first = $_ }
		else{ $name=$_ }
	}else{
		if($name eq ""){
			$seq_first.=$_
		}else{
			if(!exists $data{$name}){ $data{$name}="" }
			$data{$name}.=$_;
		}
	}
}

# read input file or from STDIN or as string from first argument
if(-e $INPUT){ # if file
	print STDERR "reading $INPUT\n";
	open(my $FH_in,"<$INPUT") || die($!);
	while(<$FH_in>){ processLine($_) }
	close($FH_in);
}elsif($INPUT eq "-"){ # STDIN
	print STDERR "reading STDIN\n";
	while(<STDIN>){ processLine($_) }
}

if( length $seq_first == 0){
	print $usage;
	print STDERR "[ERROR] !! The input is empty !!\n"; exit(1);
}
if( length $cpus == 1){
	print STDERR "[WARNING] !! Please use more than one cpu core. With the current window and step size you could use up to ".int((length($seq_first)-$w+$s)/$s)." cores (or any number below) !!\n"; 
}
if( length $seq_first < $w){
	print STDERR "[WARNING] !! The first input is smaller than the window size '$w', this will result in a single window ... (please decrease the window size with -w, e.g. 10\% = ".int((length $seq_first)/10).") !!\n"; 
	$w=length $seq_first;
}
if( (max map { length $_ } values %data) > length $seq_first ){
	print STDERR "[ERROR] !! One of the input sequences is longer than target (first sequence), make sure that the first sequence is the longes (target) otherwise the indices are wrong !!\n"; exit(1);
}
if( $s > $w/2 ){
	print STDERR "[ERROR] !! the step size is too large, please use a value smaller than 0.5*w (=".int(0.5*$w).") !!\n"; exit(1);
}


print $name_first."\n";

sub worker{
	local $SIG{KILL} = sub { threads->exit };

	my $tid = threads->tid;

	while (defined(my $job = $q->dequeue())) { # they terminate for a undef job 
		
		print STDERR "\033[J[tid=$tid] ".(int(10000*(1-$q->pending()/$total_qs))/100)."%\033[G";

		my $res = `$job`;

		if($debug){ print STDERR "\n$job\n$res\n\n\n"; }

		$res =~ s/[\n\r]/#/g;
		
		if(!defined $ret[$tid]){
			$ret[$tid] = "$res";
		}else{
			$ret[$tid].="$;$res";
		}
	}

	return; # return the results + join does not work ...
}

# handle a SIG/INT terminating gracefully
$SIG{TERM} = sub {
 	foreach (threads->list()) {$_->kill('KILL')->detach}
 	exit 1;
};
$SIG{INT} = $SIG{TERM};

foreach my $key (sort keys %data) {

	print $key."\n";

	# enqueue all RNAup calls
	
	my $lastj=(((length $seq_first) - $w) < 1 ? 1 : ((length $seq_first) - $w));
	my $lasti=(((length $data{$key}) - $w) < 1 ? 1 : ((length $data{$key}) - $w));

	for (my $j = 0; $j < $lastj; $j+=$s) {
		my $cur_seq_first = ($j+$s>=$lastj ? substr($seq_first,$j) : substr($seq_first,$j,$w));
		my $i = 0;
		for (my $i = 0; $i < $lasti; $i+=$s) {
			my $cur_seq = ($i+$s>=$lasti ? substr($data{$key},$i) : substr($data{$key},$i,$w));
			$q->enqueue("echo 'OFFSETi=$i;OFFSETj=$j;LENi=".(length $data{$key}).";LENj=".(length $seq_first)."'; RNAup $arg −−no_output_file --interaction_first 2>/dev/null << EOF\n$name_first\n$cur_seq_first\n$key\n$cur_seq\nEOF");
		}
	}

	$total_qs = $q->pending();

	# stop signal for each worker
	$q->enqueue(undef) for 1..$cpus; 

	# spawn $cpus worker
	for (my $i = 0; $i < $cpus; $i++) { threads->create("worker") }

	# wait for + collect results from worker 
	my $mindg=0;
	my $minLine="";
	my $minLine_full="";
	foreach my $thr ( threads->list() ) {

		$thr->join; # wait on finish

		my $joined = $ret[$thr->tid];
		if(!defined $joined || $joined eq "" ){next}

		foreach my $result (split $;,$joined) {	
			if(!defined $result || $result eq "" ){next}

			my $result_full = $result;

			my $offseti=0; if($result =~ /OFFSETi=([^ ().;#]+)/){ $offseti=$1 }
			my $offsetj=0; if($result =~ /OFFSETj=([^ ().;#]+)/){ $offsetj=$1 }
			my $leni=0; if($result =~ /LENi=([^ ().;#]+)/){ $leni=$1 }
			my $lenj=0; if($result =~ /LENj=([^ ().;#]+)/){ $lenj=$1 }
			
			my @result_arr = split("#",$result);
			my @result_arr2 = split(/[ :,\t]+/,$result_arr[3],6);

			if( # only need to check j here since by design the first sequence is the longes (sliding window stuff)
					($result_arr2[2]-$result_arr2[1]<($w-2*$s)) && # match is significant smaller than the window size
					( ($offsetj > 0 && $result_arr2[1] < $s/2) || # match start in the very beginning and it is not the first frame
					  ($offsetj+$w < $lenj && $result_arr2[2] > $w-$s/2) ) # same for end
			 	){
				print STDERR "".($result_arr2[2]-$result_arr2[1]<($w-2*$s))." \n";
			 	# the current bind is at the very start or end of a window, there are other that overlap windows this position at a more central position -> dissmiss
			 	next;
			}

			$result_arr2[1]+=$offsetj;
			$result_arr2[2]+=$offsetj;
			$result_arr2[3]+=$offseti;
			$result_arr2[4]+=$offseti;
			
			$result = $result_arr2[0]." ".$result_arr2[1].",".$result_arr2[2]." : ".$result_arr2[3].",".$result_arr2[4]." ".$result_arr2[5]."\n".$result_arr[4];

			my $dg = 0;
			if($result =~ /\( ?([^ =]+) ?=/){ $dg=$1+0 }
			if($dg <= $mindg){$mindg = $dg; $minLine = $result; $minLine_full=$result_full}
		}
	}
	print "$minLine\n";
	if($debug){$minLine_full=~s/#/\n/g; print STDERR "$minLine_full\n"}
}

