SRC_FILES_PL := $(wildcard *.pl) $(wildcard */*.pl)
SRC_FILES_SH := $(wildcard *.sh) $(wildcard */*.sh)
SRC_FILES_R := $(wildcard *.R) $(wildcard */*.R)
SRC_FILES_PY := $(wildcard *.py) $(wildcard */*.py)
SRC_CPP := $(wildcard *.cpp) $(wildcard */*.cpp)
OBJ_FILES := $(patsubst %.cpp,%,$(SRC_CPP))

# ALIAS for PREFIX
INSTALLDIR=/usr/local/bin

ifdef PREFIX
INSTALLDIR=$(PREFIX)
endif

$(OBJ_FILES): $(SRC_CPP)
	g++ -fopenmp -o $(INSTALLDIR)/$$(basename $@) $<

all: $(SRC_FILES_PL) $(SRC_FILES_SH) $(SRC_FILES_R) $(SRC_FILES_PY)
	@echo "INSTALLING everything to $(INSTALLDIR)"
	install -v $^ $(INSTALLDIR);

install: $(SRC_FILES_PL) $(SRC_FILES_SH) $(SRC_FILES_R) $(SRC_FILES_PY)
	@echo "INSTALLING everything to $(INSTALLDIR)"
	@for f in $^; do \
		bn=$$(basename $$f); \
		install -v $$f $(INSTALLDIR)/pmjk_$${bn}; \
		ln -sf pmjk_$${bn} $(INSTALLDIR)/$${bn}; \
	done
	ln -sf $(INSTALLDIR)/pmjk_simple_msg.py $(INSTALLDIR)/msg;

uninstall: $(SRC_FILES_PL) $(SRC_FILES_SH) $(SRC_FILES_R) $(SRC_FILES_PY)
	@for f in $^; do \
		bn=$$(basename $$f); \
		echo "rm $(INSTALLDIR)/pmjk_$${bn} $(INSTALLDIR)/$${bn}"; \
	done
	@echo "# please run 'make uninstall | sh' to execute the commands of above!";
