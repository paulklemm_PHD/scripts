#!/usr/bin/env perl
#pk 

use strict;
use warnings "all";

use POSIX;
use Getopt::Long; # for parameter specification on the command line

my $help;
my $version="0.0.35"; # last updated on 2022-03-11 09:42:02

our $RED="\033[0;41m";
our $PURPLE="\033[0;45m";
our $H2="\033[30;47m";
our $H1="\033[30;41m";
our $BOLD="\033[1m";
our @COLORPAL = ("\033[7;32m","\033[7;31m","\033[7;33m","\033[2;1m") ;
our $NC="\033[0m"; # No Color

our $maxNumOfCharsInOneLine=`tput cols`; # get the linewith
chomp($maxNumOfCharsInOneLine);
our $noheader=0; # if set to 1 then each row scales according to content
our $columnsrange=""; # only dicurRowArrayay columns from a to b, e.g. 0-3 dicurRowArrayays the first 4 columns (0,1,2,3). a,b are 0-index numbers.
our $split_delim=""; # auto detect
our $numColumns=0;
our $isHeaderLine=1; # current row is a header line 
our $last_isHeaderLine=0;
our @curRowArray_HEADER; # holds the current header for sizes
our %seenHeaderLine; # exclude headers that are repetitive (head = # prefix)
our $plain=0; # exclude headers that are repetitive (head = # prefix)
our $html=0;
our $opendirectly=0;
our $skip=0;
our $header="";

our $debug=0; # exclude headers that are repetitive (head = # prefix)
GetOptions('help!'=>\$help,'h!'=>\$help,
	'width=i'=>\$maxNumOfCharsInOneLine,
	'skip=i'=>\$skip,
	'noheader!'=>\$noheader,
	'h!'=>\$noheader,
	'w=f'=>\$maxNumOfCharsInOneLine,
	'k=s'=>\$columnsrange,
	'd=s'=>\$split_delim,
	'header=s'=>\$header,
	'x!'=>\$debug,
	'p!'=>\$plain,
	'version!'=>sub { print "$0\tv$version\n";exit 0; },
	'html!'=>\$html,
	'plain!'=>\$plain);

if($opendirectly){$html=1}

if ($help || scalar @ARGV != 1 ){
	print "printCSV.pl	prints a given tab/comma/semicolon separated file to the terminal (maximal width is automatically adjusted).

SYNOPSIS

	printCSV.pl (options) FILENAME
	printCSV.pl (options) -

OPTIONS

	FILENAME 			can also be - to idicate STDIN.

	-width NUMBER, -w NUMBER	the maximal width of the table, can be absolute values or relative values (0.5 = half of the maximal window width) [default:auto detect maximal window width]
	-noheader, -h			if set, every row is treated independently (otherwise the first row determines the with of each column)
	-k a-b				only show columns from a to b, e.g. 0-3 shows the first 4 columns (0,1,2,3). a,b are 0-index numbers.
	-k a,b,c...			only show specific columns from a,b,c,.. a,b are 0-index numbers OR column names (specified in first line)
	-skip				skip first x lines
	-d delim			specify the delimiter of the input table [default:auto detect]
	-plain,-p			output plain csv table (only useful if you select some columns with -k as well)
	-html			generate a html table (-plain suppresses the head-boilercode)
	-header				generate headers from typical formats (blast6,gff,vcf,sam)

	NOTE: reoccuring header lines are ignored.

EXAMPLE

	\$ cat input.csv

# Species       Genes   Alg.-Conn.      C.faa   C2.faa  E.faa   L.faa   M.faa
2       5       0.16    *       *       *       L_641,L_643     M_640,M_642,M_649
4       6       0.115   C_12,C_21       *       E_313,E_315     L_313   M_313
3       6       0.301   C_164,C_166,C_167,C_2   *       *       L_2     M_2
2       4       0.489   *       *       *       L_645,L_647     M_644,M_646
3       3       0.312   *       *       E_367   L_319   M_319
4       5       0.165   C_63,C_22       *       E_19    L_19    M_19


	\$ printCSV.pl input.csv
	\$ cat input.csv | printCSV.pl -

-------------+------------+------------+------------+------------+------------+------------+------------
   Species   |   Genes    | Alg.-Conn. |   C.faa    |   C2.faa   |   E.faa    |   L.faa    |   M.faa    
-------------+------------+------------+------------+------------+------------+------------+------------
      2      |     5      |    0.16    |     *      |     *      |     *      |L_641,L_643 |M_640,M_642#
      4      |     6      |   0.115    | C_12,C_21  |     *      |E_313,E_315 |   L_313    |   M_313    
      3      |     6      |   0.301    |C_164,C_166#|     *      |     *      |    L_2     |    M_2     
      2      |     4      |   0.489    |     *      |     *      |     *      |L_645,L_647 |M_644,M_646 
      3      |     3      |   0.312    |     *      |     *      |   E_367    |   L_319    |   M_319    
      4      |     5      |   0.165    | C_63,C_22  |     *      |    E_19    |    L_19    |    M_19    

      The '#' indicates that the cell is cut off due to size limitations

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES perl : POSIX, Getopt::Long
";
	exit 1;
}
if($maxNumOfCharsInOneLine<1){$maxNumOfCharsInOneLine=$maxNumOfCharsInOneLine * `tput cols | tr -d '\n'`;}
if($maxNumOfCharsInOneLine<1){$maxNumOfCharsInOneLine=`tput cols | tr -d '\n'`;}
if($noheader){$isHeaderLine=0;}

our $len_curRowArray=0;
our $didprintbody=0;

if($html && !$plain){htmlHeader();print "<body>"}
if($html){print "<table>"}

if($header eq "blast6"){
	my $prev_split_delim=$split_delim;
	printLineAsTable("qseqid,sseqid,pident,length,mismatch,gapopen,qstart,qend,sstart,send,evalue,bitscore");
	$split_delim=$prev_split_delim;
}elsif($header eq "gff"){
	my $prev_split_delim=$split_delim;
	printLineAsTable("seqname,source,feature,start,end,score,strand,frame,attribute");
	$split_delim=$prev_split_delim;
}elsif($header eq "sam"){
	my $prev_split_delim=$split_delim;
	printLineAsTable("QNAME,FLAG,RNAME,POS,MAPQ,CIGAR,RNEXT,PNEXT,TLEN,SEQ,QUAL");
	$split_delim=$prev_split_delim;
}elsif($header eq "vcf"){
	my $prev_split_delim=$split_delim;
	printLineAsTable("CHROM,POS,ID,REF,ALT,QUAL,FILTER,INFO,FORMAT");
	$split_delim=$prev_split_delim;
}

my $cur_i=0;
my @whole_table;
if($ARGV[0] eq "-" ){
	#while(<STDIN>){ if($cur_i++ < $skip){next}; printLineAsTable($_) }
	@whole_table = split "\n", do { local $/; <STDIN> };
}else{
	#while(<>){ if($cur_i++ < $skip){next}; printLineAsTable($_) }
	@whole_table = split "\n", do { local $/; <> };
}

@whole_table = @whole_table[$skip..$#whole_table];

# determineNumColumns
my @numColumns_list=();

foreach my $line (@whole_table[int($#whole_table*5/10)..int($#whole_table*6/10+1)]){ # peak into the data to get a good split_delim

	my @curRowArray;
	my @curRowArray_t=split("\t",$line);
	my @curRowArray_c=split(",",$line);
	my @curRowArray_s=split(";",$line);

	if(scalar @curRowArray_t < 2 && scalar @curRowArray_c < 2 && scalar @curRowArray_s < 2){next;}

	if(scalar @curRowArray_t > scalar @curRowArray_c && scalar @curRowArray_t > scalar @curRowArray_s ){ @curRowArray = @curRowArray_t; $split_delim='\t';}
	elsif(scalar @curRowArray_c > scalar @curRowArray_t && scalar @curRowArray_c > scalar @curRowArray_s ){ @curRowArray = @curRowArray_c; $split_delim=",";}
	elsif(scalar @curRowArray_s > scalar @curRowArray_c && scalar @curRowArray_s > scalar @curRowArray_t ){ @curRowArray = @curRowArray_s; $split_delim=";";}

	$numColumns = scalar @curRowArray;
	@{$numColumns_list[$#numColumns_list+1]} = ($numColumns,$split_delim);
}
# get the best delim (maximal number of occurences of the number of columns produced)
($numColumns,$split_delim) = @{(sort {$b->[0] <=> $a->[0]} @numColumns_list)[0]};

# main loop
foreach my $line (@whole_table){ printLineAsTable($line) }
# main

if($html){print "</tbody></table>"}
if($html && !$plain){print "</body>"}

sub pad {
    my ($str, $padding, $length) = @_;
    if($length<length $str){ return(substr($str,0,$length)) }
    my $tictoc=0;
    my $len = length $str;
    while($len < $length){
    	if($tictoc){
    		$str.=$padding;
		}else{
    		$str=$padding.$str;
		}
    	$tictoc=1-$tictoc;
    	$len+=1;
    }
    return($str);
}

sub printLineAsTable{

	my $line=shift;
	$line=~s/[\n\r]+$//g;

	my @curRowArray; # the current row
	if( length($line)<1 || (substr($line,0,1) eq "#" && $seenHeaderLine{$line}) || (substr($line,0,1) eq "#" && $seenHeaderLine{$line}) ){if( $debug ){print STDERR "[DEBUG] found duplicated line -> skipping.\n"} return } # if empty line || comment line that is allready printed -> skip
	if( !$html && ($split_delim ne "" && scalar split($split_delim,$line) <2 ) ){if( $debug ){print STDERR "[DEBUG] just a comment.\n"} @curRowArray=();push(@curRowArray,$line); $isHeaderLine=1; goto doprinting;} # if just a comment -> print as it is

	while($line=~m/(^|\t)(\t|$)/){$line=~s/(^|\t)(\t|$)/$1 $2/g;} # solve the issue if there are empty columns (split removes these...)

	# auto detect the delimiter
	if($split_delim eq ""){
		my @curRowArray_t=split("\t",$line);
		my @curRowArray_c=split(",",$line);
		my @curRowArray_s=split(";",$line);

		if(scalar @curRowArray_t < 2 && scalar @curRowArray_c < 2 && scalar @curRowArray_s < 2){next;}

		if(scalar @curRowArray_t > scalar @curRowArray_c && scalar @curRowArray_t > scalar @curRowArray_s ){ @curRowArray = @curRowArray_t; $split_delim='\t';}
		elsif(scalar @curRowArray_c > scalar @curRowArray_t && scalar @curRowArray_c > scalar @curRowArray_s ){ @curRowArray = @curRowArray_c; $split_delim=",";}
		elsif(scalar @curRowArray_s > scalar @curRowArray_c && scalar @curRowArray_s > scalar @curRowArray_t ){ @curRowArray = @curRowArray_s; $split_delim=";";}

		if( $debug ){print STDERR "[DEBUG] detected '$split_delim' delimiter.\n";}

		# $numColumns = scalar @curRowArray;

	}else{
		@curRowArray=split($split_delim,$line);
	}

	$len_curRowArray = scalar @curRowArray;

	if( $debug ){print STDERR "[DEBUG] numColumns=$numColumns curRowArray=".(scalar @curRowArray).".\n";}
	# if( $numColumns != scalar @curRowArray ){if( $debug ){print STDERR "[DEBUG] a comment header.\n"} @curRowArray=();push(@curRowArray,$line); $isHeaderLine=1; goto doprinting;} # if just a comment -> print as it is

	if($html){goto doprinting}
	
	# only print certain columns if $columnsrange is set
	redoSelectColumns:
	if($columnsrange ne "" && $numColumns == scalar @curRowArray){
		if($columnsrange=~m/(\d+)-(\d+)/){
			my $a=$1;
			my $b=$2;
			if( $b < scalar @curRowArray && $a <= $b){
				while(scalar @curRowArray > $b+1){pop @curRowArray;}
			}
			if($a < scalar @curRowArray){
				while($a-- > 0){shift @curRowArray;}
			}
		}elsif($columnsrange=~m/^[0-9,]+$/){

			my @tmparr;
			while($columnsrange=~m/(\d+)/g){
				if($1<scalar @curRowArray && $1>=0){
		        	push(@tmparr, $curRowArray[int($1)]);
				}
			}
			@curRowArray=@tmparr;
		}else{
			if($isHeaderLine){
				my $newcolumnsrange;
				while($columnsrange=~m/([^,]+)/g){
					my $key = $1;
					my $res = join ",", grep { $curRowArray[$_] =~ $key } 0..$#curRowArray; 
					if($res ne ""){ if($newcolumnsrange ne ""){$newcolumnsrange.=","} $newcolumnsrange.="$res" }
				}
				$columnsrange = $newcolumnsrange;
				goto redoSelectColumns;
			}
		}
	}

	if($plain){if($isHeaderLine){print "# ";}$isHeaderLine=0;$last_isHeaderLine=0;  goto doprinting;} # if -plain is set, then print as \t sep file 

	if( $debug ){print STDERR "[DEBUG] header got ".(scalar @curRowArray_HEADER)." and the current line has ".(scalar @curRowArray)." columns\n";}

	if(scalar @curRowArray < 2){if( $debug ){print STDERR "[DEBUG A] this line got no delims -> probably a header line\n";} $isHeaderLine=1; goto doprinting;}
	if(scalar @curRowArray_HEADER > 0 && scalar @curRowArray != scalar @curRowArray_HEADER){$isHeaderLine=1;}
	if(substr($line,0,1) eq "#"){$isHeaderLine=1; $seenHeaderLine{$line}=1;$curRowArray[0]=~s/^# ?//g;}
	if(scalar(@curRowArray)*2-1>$maxNumOfCharsInOneLine){$maxNumOfCharsInOneLine= -1+2*scalar @curRowArray;print STDERR "Corrected minimum table width: -w=$maxNumOfCharsInOneLine such that at least 1 character per column is dicurRowArrayayed.\n";}

	my $sumOfCharsLine=length(join("",@curRowArray));

	if( $isHeaderLine || $noheader){ # is a header row 

		while(($sumOfCharsLine + scalar @curRowArray-1) > $maxNumOfCharsInOneLine){ # shave of chars from widest cell
			my $max_l=0;
			my @max_l_is;
			for (my $i = 0; $i < scalar @curRowArray; $i++) {
				if($max_l < length $curRowArray[$i]){$max_l=length $curRowArray[$i];@max_l_is=();push(@max_l_is,$i)}elsif($max_l == length $curRowArray[$i]){push(@max_l_is,$i)}
			}
			for (my $i = 0; $i < scalar @max_l_is; $i++) {
				$curRowArray[$max_l_is[$i]]=substr($curRowArray[$max_l_is[$i]],0,length($curRowArray[$max_l_is[$i]])-1)
			}
			$sumOfCharsLine=length(join("",@curRowArray));
		}

		while(($sumOfCharsLine + scalar @curRowArray-1) < $maxNumOfCharsInOneLine ){ # add of chars to smallest cell
			my $min_l=$maxNumOfCharsInOneLine*10;
			my @min_l_is;
			for (my $i = 0; $i < scalar @curRowArray; $i++) {
				if($min_l > length $curRowArray[$i]){$min_l=length $curRowArray[$i];@min_l_is=();push(@min_l_is,$i)}
			}
			for (my $i = 0; $i < scalar @min_l_is; $i++) {

				my $leftPad=0;
				my $rightPad=0;
				if($curRowArray[$min_l_is[$i]]=~m/( +)$/){$rightPad=length $1}
				if($curRowArray[$min_l_is[$i]]=~m/^( +)/){$leftPad=length $1}

				if( $leftPad < $rightPad ){
					$curRowArray[$min_l_is[$i]]=" ".$curRowArray[$min_l_is[$i]];
				}else{
					$curRowArray[$min_l_is[$i]]=$curRowArray[$min_l_is[$i]]." ";
				}
				
			}
			$sumOfCharsLine=length(join("",@curRowArray));
		}

		@curRowArray_HEADER=@curRowArray;

	}else{ # is not headerline -> do the same as in headerline
		
		while(scalar @curRowArray > scalar @curRowArray_HEADER){pop @curRowArray;}

		for (my $i = 0; $i < scalar @curRowArray; $i++) {
			while(length $curRowArray[$i]< length $curRowArray_HEADER[$i]){ # add pads
				my $leftPad=0;
				my $rightPad=0;
				if($curRowArray[$i]=~m/( +)$/){$rightPad=length $1}
				if($curRowArray[$i]=~m/^( +)/){$leftPad=length $1}

				if( $leftPad < $rightPad ){
					$curRowArray[$i]=" ".$curRowArray[$i];
				}else{
					$curRowArray[$i]=$curRowArray[$i]." ";
				}
			}
			while(length $curRowArray[$i]>length $curRowArray_HEADER[$i]){ # trim
				$curRowArray[$i]=substr($curRowArray[$i],0,length($curRowArray[$i])-2)."#"
			}
		}
	}

	doprinting:
	if( $debug ){print STDERR "[DEBUG A] headerline=".$isHeaderLine." lastheader=".$last_isHeaderLine."\n";}

	if($isHeaderLine && substr($line,0,1) ne "#"){$last_isHeaderLine=1; $isHeaderLine=0}

	if( ($isHeaderLine && !$last_isHeaderLine) ){
		if($html){
			print "<thead>";
		}else{
			print "$H2";
			my @tmp;
			my $tmp_i=0;
			foreach(@curRowArray_HEADER){push(@tmp,pad($tmp_i++,"─",(length $_)))};
			print join("┼",@tmp)."\n";
			print "$NC";
		}
	}
	if( (!$isHeaderLine && $last_isHeaderLine) || ($isHeaderLine && $last_isHeaderLine && $numColumns != $len_curRowArray) ){
		if($html){
			print "</thead>";
		}else{
			print "$H2";
			my @tmp;
			foreach(@curRowArray_HEADER){push(@tmp,"─"x(length $_));};
			print join("┼",@tmp)."\n";
			print "$NC";
		}
	}

	if($html){
		if(!$isHeaderLine && !$didprintbody){$didprintbody=1;print "<tbody>\n"}
		if($numColumns != $len_curRowArray){
			print "<tr><th colspan='".(scalar @curRowArray)."'>".(join("\t",@curRowArray))."</th></tr>\n";
		}else{
			print "<tr><th>".join("</th><th>",@curRowArray)."</th></tr>\n";
		}
	}else{
		#if($isHeaderLine && $numColumns == $len_curRowArray){ @curRowArray = map{ "$H1$_$NC" } @curRowArray }
		#if($isHeaderLine && $numColumns != $len_curRowArray){ @curRowArray = map{ "$H2$_$NC" } @curRowArray }

		if($isHeaderLine){ print $numColumns == $len_curRowArray ? "$H1" : "$H2" ; }
		if($plain){
			print join("\t",@curRowArray)."\n";
		}else{
			print join("│",@curRowArray)."\n";
		}
		if($isHeaderLine){ print "$NC"; }
	}

	if( $numColumns != $len_curRowArray ){$isHeaderLine=0;}
	
	$last_isHeaderLine=$isHeaderLine;
	$isHeaderLine=0;

	if( $debug ){print STDERR "[DEBUG B] headerline=".$isHeaderLine." lastheader=".$last_isHeaderLine."\n\n";}
}



sub htmlHeader{
	print "<!DOCTYPE html>
<head>
<meta content='text/html;charset=utf-8' http-equiv='Content-Type'>
<meta content='utf-8' http-equiv='encoding'>
<style> 
table{ border-collapse: collapse;
	border-spacing: 0;
	empty-cells: show;
	border: 1px solid #cbcbcb;
	font-family: monospace;
	font-weight: normal;
	width:100%; }
td, th { border-left: 1px solid #cbcbcb;/*  inner column border */
	border-width: 0 0 0 1px;
	font-size: inherit;
	margin: 0;
	overflow: visible; 
	padding: 0.5em 1em; }
thead { background-color: #e0e0e0;
	color: #000;
	text-align: left;
	vertical-align: bottom; }
</style>
</head>";
}
