#!/usr/bin/perl
#pk

use strict;
use warnings "all";

use Getopt::Long; # for parameter specification on the command line
use File::Basename; # basename()
my $version="0.0.25"; # last updated on 2023-01-10 13:02:07

my $help;
my $argument;
my $operation;
my $verbose="2>/dev/null";
my $max_dist=150;
my $filter="CDS";
my $disable_detect_operons;
my $gff;
my $all;
my $promotech;

my $usage = <<ENDUSAGE;
$0        Filter output of PromoTech search by gff distance to annotated genes.
 
SYNOPSIS
 
$0 (options) --promotech=PROMOTECHFILE --gff=GFFFILE

	options
		--gff=F : input gff
		--promotech=F : input promotech prediction file (genome_prediction.tsv)
		--filter=s : filter the gff by this tag => gff column 3 (input is regex-able, e.g. "CDS|ECF", default:$filter)
		--all : output not only the best scoring promoter for each gene but simply all
		--max_dist=i : maximal upstream distance, including the 40mere of the promoter (default:$max_dist)
		--disable_detect_operons : if set, the automatic operon detection is disabled

EXAMPLE/DESCRIPTION

	Input:
		the pDM598_pSRKGm_ecf02.gff describes the features of pDM598_pSRKGm_ecf02.fna vector:
		Lac-Operon regulating target gene=ECF02, Promoters=lacp,lacIp,T5p

	Exemplary PromoTech commands to produce the input for this script:

	python promotech.py -m RF-HOT -pg -f pDM598_pSRKGm_ecf02.fna -o results_pDM598_pSRKGm_ecf02_HOT
	python promotech.py -m RF-HOT -g -t 0.5 -i results_pDM598_pSRKGm_ecf02_HOT -o results_pDM598_pSRKGm_ecf02_HOT

	$0 --promotech=results_pDM598_pSRKGm_ecf02_HOT/genome_predictions.csv --gff=pDM598_pSRKGm_ecf02.gff --filter="CDS|ECF"

	pDM597_pSRKGm_ecf02     1319    1358    0.51177 +       AATGGCGCAAAACCTTTCGCGGTATGGCATGATAGCGCCC        pDM597_pSRKGm_ecf02     GenBank CDS     1388    2470    .       +       1       ID=GenBank-CDS-pDM597_pSRKGm_ecf02-1388-2470;product=lacI;vntifkey=4
	pDM597_pSRKGm_ecf02     2629    2668    0.83978 +       TTATTTGCTTTGTGAGCGGATAACAATTATAATAGATTCA        pDM597_pSRKGm_ecf02     GenBank ECF     2725    3300    .       +       1       ID=GenBank-region-pDM597_pSRKGm_ecf02-2725-3300;product=ECF02_2817;vntifkey=21

	The first 6 columns are the promotech output (seqname,start,end,predictionscore,strand,sequence) 
	and the following 9 are the matching gff entry for this promoter. Only the best promoter for any
	gff entry is printed (if any).
	The first identified promoter is the lacIp promoter matching the lacI gene.
	The second one is the lacZp promoter matching the target gene ECF02. 
	
VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES perl : Getopt::Long, File::Basename
ENDUSAGE

GetOptions('help!'=>\$help,'h!'=>\$help,
	'version!'=>sub { print "$0\tv$version\n";exit 0; },
	'max_dist=i'=>\$max_dist,
	'disable_detect_operons!'=>\$disable_detect_operons,
	'all!'=>\$all,
	'filter=s'=>\$filter,
	'gff=s'=>\$gff,
	'promotech=s'=>\$promotech,
);

if($help){print $usage; exit 0}
if(!defined $gff){print $usage."\nERROR: -gff missing\n"; exit 1}
if(!defined $promotech){print $usage."\nERROR: -promotech missing\n"; exit 1}

open(my $FH_GFF,"<$gff") or die "ERROR: $? $!\n";
my $gff_num=0;
my %gff_data;
# gff_data contains a 3 level hash:
# 1. seqname
# 2. strand
# start => 3. start position for + stranded genes and end position for - stranded genes
# 			value = the other position of the gene (e.g. for + strand this is end position)
# end => the end position (just yes no)
while(<$FH_GFF>){
	chomp;
	my @arr = split("\t",$_);
	if(scalar @arr != 9){next}
	if(defined $filter && $arr[2] !~ $filter){next}
	if(!exists $gff_data{$arr[0]}){$gff_data{$arr[0]}=()}
	if(!exists $gff_data{$arr[0]}{$arr[6]}){$gff_data{$arr[0]}{$arr[6]}{"start"}=();$gff_data{$arr[0]}{$arr[6]}{"end"}=();}
	$gff_data{$arr[0]}{$arr[6]}{"start"}{int($arr[6] eq "+" ? $arr[3] : $arr[4])} = {
		"end"=>int($arr[6] eq "+" ? $arr[4] : $arr[3]),
		"full"=>$_
	};
	$gff_data{$arr[0]}{$arr[6]}{"end"}{int($arr[6] eq "+" ? $arr[4] : $arr[3])} = 1;
	$gff_num++;
}
close($FH_GFF);

if(!$disable_detect_operons){
	my $deleted_operons=0;
	# the idea here is to walk through the gff data (3. key)
	# and check if the start value (3. key) is very close to any end key (value)
	foreach my $seqname (keys %gff_data) {
		foreach my $strand (keys %{$gff_data{$seqname}}) {
			my @keys=keys %{$gff_data{$seqname}{$strand}{"start"}};
			foreach my $start (@keys) {	
				if( exists $gff_data{$seqname}{$strand}{"end"}{$start}   ||
					exists $gff_data{$seqname}{$strand}{"end"}{$start-1} ||
					exists $gff_data{$seqname}{$strand}{"end"}{$start-2} ||
					exists $gff_data{$seqname}{$strand}{"end"}{$start-3} ||
					exists $gff_data{$seqname}{$strand}{"end"}{$start-4} ||
					exists $gff_data{$seqname}{$strand}{"end"}{$start-5} ){
					#print STDERR "FOUND OPERON $gff_data{$seqname}{$strand}{$start}{'full'}\n";
					$deleted_operons++;
					$gff_num--;
					delete $gff_data{$seqname}{$strand}{$start}{"start"}; # keep the ends
				}
			}
		}
	}
	print STDERR "[INFO] Found and removed $deleted_operons genes that are part of an operon (but not the first one).\n";
}

print "[INFO] Loaded a total of $gff_num gff entries (filter=$filter)\n";

if(scalar keys %gff_data == 0){
	print STDERR "[ERROR] gff is empty\n";
	exit 1;
}

open(my $FH_PROMO,"<$promotech") or die "ERROR: $? $!\n";
my %gene2promoter;
while(<$FH_PROMO>){
	chomp;
	my @arr = split("\t",$_);
	if(scalar @arr != 6){next}
	my $seqname = $arr[0];
	if($seqname eq "chrom"){next}
	if(!exists $gff_data{$seqname}){print STDERR "ERROR: the promotech file seqname does not match the gff file (entry=$_)\n"; exit 1;}

	my $strand=$arr[4];
	my $start = $strand eq "+" ? $arr[1] : $arr[2];
	my $end = $strand eq "+" ? $arr[2] : $arr[1];
	for (my $i = abs($end-$start); $i < $max_dist; $i++) {
		# $start + $i*($strand eq "-" ? -1:1) = is the relative position to the start of the promoter
		# for + stranded genes: go to right and for - go to the left
		if(exists $gff_data{$seqname}{$strand}{"end"}{ $start + $i*($strand eq "-" ? -1:1) }){
			# the current promoter does overlap a gene -> abort
			last;
		}elsif(exists $gff_data{$seqname}{$strand}{"start"}{ $start + $i*($strand eq "-" ? -1:1)}){
			if(!exists $gene2promoter{$gff_data{$seqname}{$strand}{"start"}{ $start + $i*($strand eq "-" ? -1:1)}{'full'}}){$gene2promoter{$gff_data{$seqname}{$strand}{"start"}{ $start + $i*($strand eq "-" ? -1:1)}{'full'}}=()}
			push(@{$gene2promoter{$gff_data{$seqname}{$strand}{"start"}{ $start + $i*($strand eq "-" ? -1:1)}{'full'}}},\@arr);
			last;
		}
	}
}
close($FH_PROMO);

if(scalar keys %gene2promoter == 0){
	print STDERR "[ERROR] no promoter found matching any gff entry, maybe try a lower -t threshold with promotech\n";
	exit 1;
}

foreach my $gene (keys %gene2promoter) {
	my @best = reverse sort { $a->[3] <=> $b->[3] } @{$gene2promoter{$gene}};
	if($all){
		foreach my $x (@best) {
			print join("\t",@{$x})."\t$gene"."\n";
		}
	}else{
		# first entry now contains highest prediction value
		print join("\t",@{$best[0]})."\t$gene"."\n";
	}
}

# the difference is ignored in the calculation of the distance between promoter and gene start as it only makes for a difference of 1 ...
print STDERR "[INFO] Warning the promotech start and end positions are 0-based while the gff is 1-based\n";
