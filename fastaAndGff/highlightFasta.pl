#!/usr/bin/env perl
#pk

use strict;
use warnings "all";

my $fasta ="";

use Cwd 'abs_path';
use File::Basename;

my $version="0.3.2"; # last updated on 2021-10-02 23:08:46

if(scalar(@ARGV) == 5 && $ARGV[2] > $ARGV[3]){
  my $tmp=$ARGV[2];
  $ARGV[2]=$ARGV[3];
  $ARGV[3]=$tmp;
}

our $RED="\033[0;41m";
our $PURPLE="\033[0;45m";
our $YELLOW="\033[0;43m";
our $ORANGE="\033[1;33m";
our $NC="\033[0m"; # No Color

if(scalar @ARGV != 0 && $ARGV[0] =~ /-?-version|-v$/ ){ print "$0\tv$version\n"; exit 0; }

if(! (scalar(@ARGV) == 2 || ( scalar(@ARGV) == 5 && $ARGV[2] <= $ARGV[3]))){
  print "".("USAGE : highlightFasta.pl   marks genomic positions in a fasta file

SYNOPSIS 
  highlightFasta.pl <FASTAFILE> chromosom start end strand
  chromosom can be - for the first entry of the fasta file

  or

  highlightFasta.pl <FASTAFILE> <GFFFILE>
  start and end are 1-based indices/coordinates

  You can use 'aha' to convert the output to html (conda install aha)

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES perl : Cwd 'abs_path', perl : File::Basename
"); exit 0;
}

our %stats; # for outputting log information

$stats{"gff entries"}=0;
$stats{"extracted"}=0;
#
# Load data
#

  my $isOnlyOneGffentry=0;
  my @gff;
  my %orginalGFFstring;

  if(scalar(@ARGV)==2){ # OPEN GFF FILE

    open (GFF,"<",$ARGV[1]) or die "[ERROR] error thrown when try to open the gff file : \n$!";
    while (my $entry = <GFF>){
      $entry=~ s/[\r\n]+$//; # better than chomp

      if(length($entry)>3 && substr($entry,0,1)ne "#"){

        my @a=split("\t",$entry);
        if(scalar(@a)<9){die "[ERROR] gff is invalid (trying to split by '\\t' resulted in less than 7 columns) : \n$entry"}
        $a[3]=int($a[3]);$a[4]=int($a[4]);
        if($a[3]>$a[4]){die "[ERROR] gff is invalid: start>end of the entry : \n$_"}
        if($a[3]<1 || $a[4]<1){die "[ERROR] start,end needs to be 1-indices : \n$entry"}
        $orginalGFFstring{join(" ",@a)}=$entry;

        $stats{"gff entries"}++;

        push(@gff,\@a); #[$a[0],$a[1],$a[2],$a[3],$a[4],$a[5],$a[6],$a[7],$a[8]]
      }
    }
    close (GFF);

  }else{ # GFF FROM TERMINAL LINE

    $isOnlyOneGffentry=1;
    
    if($ARGV[1] eq "-"){$ARGV[1]=">";}

    my @a=($ARGV[1],"getgff","-",int($ARGV[2]),int($ARGV[3]),".",$ARGV[4],".","-");
    if($a[3]<1 || $a[4]<1){die "[ERROR] start,end needs to be 1-indices !"}
    if($a[3]>$a[4]){die "[ERROR] start needs to be smaller than end (positions are relative to the + strand) !"}
    $stats{"gff entries"}++;
    push(@gff,\@a);
    $orginalGFFstring{join(" ",@a)}=join(" ",@a);

  }

print STDERR "[getgff] I loaded ".$stats{"gff entries"}." gff entries.\n";

  # sort by CHR first and then the start value -> for a given chr, just go to the first entry of that name -> iterate over the start positions 
  sub sortByStart {if($a->[0] eq $b->[0]){return (int($a->[3]) <=> int($b->[3]))}else{return ($a->[0] cmp $b->[0])};}   
  @gff = (sort sortByStart @gff);

  #for(my $i=0;$i<scalar(@gff);$i++){print STDERR $gff[$i][0]." ".$gff[$i][3]."\n"}
  #die;

#
# Iterate over fasta and return for each entry of the gff the corresponding sequence
#

  my %is_done_gff; # marking gff entries as done so we dont need to compare them again

  open (FASTA,"<",$ARGV[0]) or die "[ERROR] error thrown when try to open the gff file : \n$!";
    my $cur_position=1;
    my %result;
    my $cur_header="";
    my $cur_lowestStart=-1;

    my $gff_start_i_of_current_CHR=0;
    my $gff_end_i=-1;
    my $current_CHR="";
    our $cur_gene;
    my $is_active=0;

    while (my $entry = <FASTA>){ # iterate over the fasta file

      if( $entry =~ m/^>/){ # header line

        $entry=~ s/[\r\n]+$//; # better than chomp

        $current_CHR="";
        $cur_header=$entry;
        $cur_position=1;

        my $last_CHR="";
        $cur_header=~s/\.//g;
        $cur_header=~s/_//g;

        for(my $i=0;$i<scalar(@gff);$i++){ # find the smallest start value for this CHR (gff is sorted)
          
          if(exists $is_done_gff{$i}){next;}

          my $chr=$gff[$i][0];
          $chr=~s/\.//g;
          $chr=~s/_//g;

          if( $last_CHR ne $chr && $cur_header =~ qr/$chr/ ){

            $cur_lowestStart=int($gff[$i][3]);
            $gff_start_i_of_current_CHR=$i;
            $current_CHR=$gff[$i][0];
            last;
          }
          $last_CHR=$gff[$i][0]; # just to speed up this search
        }

      }elsif ( $current_CHR ne "" && $cur_header ne "") { # not a header line AND a header line was allready present

        $entry=~ s/[\r\n]+$//; # better than chomp

        my $new_end_position=$cur_position+length($entry)-1; # end position of the current line

        my %currentylActive; # keeps trac of all gff entries that are currently active (could be multiple if gff entries overlap)

        if($cur_lowestStart==-1 || ( $cur_lowestStart >= $cur_position && $cur_lowestStart <= $new_end_position)){

          $cur_lowestStart=-1;

          for(my $i=$gff_start_i_of_current_CHR;$i<scalar(@gff);$i++){

            if(exists $is_done_gff{$i}){next;}

            my $chr   = $gff[$i][0];
            my $start = $gff[$i][3]; 
            my $end   = $gff[$i][4];
            my $strand= $gff[$i][6];

            if($chr ne $current_CHR){next;}

            if( $cur_position <= $start && $start <= $new_end_position ){ # a gff entry starts in this line
              
              if( $end <= $new_end_position ) { # and also ends there

                # $cur_gene=substr($entry , $start-$cur_position , $end-$start+1);

                substr($entry, $start-$cur_position+$end-$start+1, 0) = "$NC";
                substr($entry, $start-$cur_position, 0) = "$RED";
                $stats{"extracted"}++;

                $is_done_gff{$i}=1;
                $is_active=0;
                delete $currentylActive{$i};

              }else{ # goes to the next line

                # $cur_gene=substr($entry , $start-$cur_position);

                substr($entry, $start-$cur_position, 0) = "$RED";
                $stats{"extracted"}++;
                $is_active=1;

                $currentylActive{$i}=0;
              }

            }elsif( $start <= $new_end_position ){ # entry extends into this line

              if( $end <= $new_end_position ) { # and also ends there

                # $cur_gene=$result{join(" ",@{$gff[$i]})} . substr($entry , 0 , $end-$cur_position+1); # take the string of the current line AND everything that was present allready
                
                substr($entry, $end-$cur_position+1 , 0) = "$NC";
                $is_active=0;

                $is_done_gff{$i}=1;
                delete $currentylActive{$i};

              }else{ # goes to the next line

                $currentylActive{$i}=0;
              }
            }
          }
        }
        $cur_position = $new_end_position+1; # +1 pushed the position to the next line since new_end_position is still inside the line (last position)
      }
      print "$entry$NC\n";if($is_active){print $RED}
    }
  close(FASTA);

print STDERR "[getgff] I extracted ".$stats{"extracted"}." out of the given ".$stats{"gff entries"}." gff entries.\n";
