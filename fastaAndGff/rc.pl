#!/usr/bin/env perl
#pk

use strict;
use warnings "all";
use Cwd 'abs_path';
use File::Basename;

my $fasta ="";
my $useSTDIN=0;
my $doComplement=1;
my $doReverse=1;
my $all=0;
my $rnadna="";
my $version="0.0.2"; # last updated on 2021-10-02 23:08:46

my $usage = "USAGE : $0  generates the reverse and or complement of a given input sequence

SYNOPSIS
  $0 (options) FASTAFILE/-

  FASTAFILE = nucleotide (fna) file OR '-' then it takes the input from STDIN (no header is needed).
  
  --complement, --comp, -c : only do the complement
  --reverse, --rev, -r 	: only reverse
  --rna                 : force to use Uracil instead of Thymin (otherwise it is set automatically)
  --dna                 : force to use Thymin instead of Uracil 
  --all, -a             : print all all variations (reversed, complemented, rc and the original sequence). Variations are separated by |
                          you should use the --dna or --rna option along with this or this can produce unwanted results! 
EXAMPLES

  $0 - <<< 'ACGCAGCGAC'

  # search all variations of a string in a fasta file:

  grep -E \"\$($0 -a - <<< 'ACGCAGCGAC')\" fasta.fna

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES perl : Cwd 'abs_path', perl : File::Basename
";

# load argv
foreach (@ARGV) {
  if ($_ =~ /^[^-]/) {$fasta = $_;}
	elsif ($_ eq "-"){
		$useSTDIN=1;
	}elsif ($_ =~ m/--?(complement|comp|c)$/ ){
		$doReverse=0;
	}elsif ($_ =~ m/--?(reverse|rev|r)$/ ){
		$doComplement=0;
	}elsif ($_ =~ m/--?(dna|rna)$/ ){
		$rnadna=$1;
	}elsif ($_ =~ m/--?(all|a)$/ ){
		$all=1;
	}elsif ($_ =~ m/--?(version|v)$/ ){
		print "$0\tv$version\n"; exit 0;
	}else {
		print $usage; exit 0;
	}
}

if($all){
	if($rnadna eq ""){print STDERR "[STDERR] Warning: you should use the --dna or --rna option along with the --all !\n";}
	$doReverse=0;
	$doComplement=0;
}

if($fasta eq "" && !$useSTDIN){ $useSTDIN=1; }

our $cur_gene="";
our $cur_header="";
our %data;
our @data_keys;

# process linewise
sub processLine{
	my $entry = shift;
	my $prefix = shift;

	if( $entry =~ m/^>/){
		if($cur_header ne ""){ $data{$cur_header}=$cur_gene; push(@data_keys,$cur_header) }
		$cur_gene="";
		$cur_header=$prefix.$entry;
	}elsif ($entry =~ m/^[A-Za-z\n\r]+$/) {
		$entry=~s/[\n\r]+$//g;
		$cur_gene.=$entry;
	}
}

# load in data from various inputs
if($useSTDIN){
  while (my $entry = <STDIN>){ &processLine($entry,"STDIN") }
}else{
  open (FASTA,"<",$fasta) or die "error $!";
  while (my $entry = <FASTA>){ &processLine($entry,"") }
  close(FASTA);
}
# save last entry
if($cur_header eq ""){$cur_header="STDIN"}
$data{$cur_header}=$cur_gene; push(@data_keys,$cur_header);

# go over data and print out the wanted variation (rc, c, r)
foreach my $key (@data_keys){

	# restart for --all option (do all variations)
	RESTART:

	$cur_gene = $data{$key};
	$cur_header = $key;
	if($cur_header eq "STDIN"){$cur_header="";}

	if($rnadna eq "dna"){
		$cur_gene=~s/[Uu]/T/g;
	}elsif($rnadna eq "rna"){
		$cur_gene=~s/[Tt]/U/g;	
	}

	if($doComplement){
		if($rnadna eq "rna"){
			$cur_gene =~ tr/ACGUacgu/UGCAugca/;
		}elsif($rnadna eq "dna"){
			$cur_gene =~ tr/ACGTacgt/TGCAtgca/;
		}else{
			if($cur_gene =~ m/[Uu]/ && $cur_gene !~ m/[Tt]/){$cur_gene =~ tr/ACGUacgu/UGCAugca/;} # if there are Us and no Ts
			else{$cur_gene =~ tr/ACGTacgt/TGCAtgca/;}
		}
	}
	if($doReverse){$cur_gene = reverse $cur_gene;}

	print $cur_header.$cur_gene.($all && $doReverse * $doComplement == 0 ? "|" : "\n");

	# restart for -all
	if($all){
		if($doReverse * $doComplement != 1){
			if(!$doReverse && !$doComplement){
				$doReverse=1;
				$doComplement=0;
			}elsif($doReverse && !$doComplement){
				$doReverse=0;
				$doComplement=1;
			}else{
				$doReverse=1;
				$doComplement=1;
			}
			goto RESTART;
		}else{
			$doReverse=0;
			$doComplement=0;
		}
	}
}
