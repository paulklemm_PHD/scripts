#!/usr/bin/perl
#pk
# adpted from https://stackoverflow.com/questions/17563032/calculating-molecular-weight-in-perl

use strict;
use warnings;
use Encode;

if(scalar @ARGV == 0 || $ARGV[0] =~ /^--?h/){
    print STDERR "USAGE $0

";
}

for my $file (@ARGV) {
    open my $fh, '<:encoding(UTF-8)', $file or die "$? $!\n";
    my $input = join q{}, <$fh>; 
    close $fh;
    while ( $input =~ /^(>.*?)$([^>]*)/smxg ) {
        my $name = $1;
        my $seq = $2;
        $seq =~ s/\n//smxg;
        my $mass = calc_mass($seq)/1e+3;
        print "$name\t$mass\n";
    }
}

sub calc_mass {
    my $a = shift;
    my @a = ();
    my $x = length $a;
    @a = split q{}, $a;
    my $b = 0;
    my %data = (
        #A=>71.09,R=>16.19,D=>114.11,N=>115.09,
        #C=>103.15,E=>129.12,Q=>128.14,G=>57.05,
        #H=>137.14,I=>113.16,L=>113.16,K=>128.17,
        #M=>131.19,F=>147.18,P=>97.12,S=>87.08,
        #T=>101.11,W=>186.12,Y=>163.18,V=>99.14
        # 'A'=>   89.1,
        # 'R'=>   174.2,
        # 'N'=>   132.1,
        # 'D'=>   133.1,
        # 'C'=>   121.2,
        # 'E'=>   147.1,
        # 'Q'=>   146.2,
        # 'G'=>   75.1,
        # 'H'=>   155.2,
        # 'I'=>   131.2,
        # 'L'=>   131.2,
        # 'K'=>   146.2,
        # 'M'=>   149.2,
        # 'F'=>   165.2,
        # 'P'=>   115.1,
        # 'S'=>   105.1,
        # 'T'=>   119.1,
        # 'W'=>   204.2,
        # 'Y'=>   181.2,
        # 'V'=>   117.1
    
    # monoisotopic_protein_weights https://github.com/biopython/biopython/blob/master/Bio/Data/IUPACData.py
    "A"=> 89.047678,
    "C"=> 121.019749,
    "D"=> 133.037508,
    "E"=> 147.053158,
    "F"=> 165.078979,
    "G"=> 75.032028,
    "H"=> 155.069477,
    "I"=> 131.094629,
    "K"=> 146.105528,
    "L"=> 131.094629,
    "M"=> 149.051049,
    "N"=> 132.053492,
    "O"=> 255.158292,
    "P"=> 115.063329,
    "Q"=> 146.069142,
    "R"=> 174.111676,
    "S"=> 105.042593,
    "T"=> 119.058243,
    "U"=> 168.964203,
    "V"=> 117.078979,
    "W"=> 204.089878,
    "Y"=> 181.073893
    );
    for my $i( @a ) { $b += $data{$i}; }
    my $c = $b - (18.010565 * ($x - 1)); # add the molecular weight of water for every bond
    return $c;
}
