#!/usr/bin/env perl
# pk

use strict;
use warnings "all";

use Getopt::Long; 
use File::Basename; # basename()

my $version="0.5.19"; # last updated on 2024-12-15 22:27:04

my $GREPFASTADIR = $ENV{'GREPFASTADIR'};
if(!defined $GREPFASTADIR && -d $ENV{"HOME"}."/grepfastadir"){ $GREPFASTADIR=$ENV{"HOME"}."/grepfastadir/"; }
elsif(!defined $GREPFASTADIR || !-d $GREPFASTADIR){ $GREPFASTADIR="./"; }
else{$GREPFASTADIR.="/"}

my $usage = <<ENDUSAGE;
grepfasta.pl        greps all genes/proteins of a given fasta file
 
SYNOPSIS
 
grepfasta.pl (options) QUERY INFILE1 (INFILE2 ...)

	QUERY	identifier FILE or search query STRING:
		a)	string of one identifier e.g. 'tr|asd3|asd' OR multiple identifier separated by ',' OR '-' for STDIN
		b)	(-file QUERY) file with the ids. The file contains either just 
			identifier or tabseparated a mapping:
			e.g. POP1 to the 3 identifier 
			tr|asd|asd, tr|asd2|asd and tr|asd3|asd.
	INFILE	file containing the query ids (database)

	optional:
		-tofiles, -t 	print everything to files instead of files 
		-tofilesSource, -ts  same as -tofiles but output files have the source file name in their file names
		-v 	    negates the search
		-E    	enables regex matching otherwise the string is escaped (e.g. | -> \|)
		-i    	enables case insensitive search
		-source, -s    	adds the file name to the found gene name
		-F=s 	char delimiter for multiple identifier (default: ',')
		-tag=s 	search by fasta tag, e.g. -tag='GN' will search for ... GN=PRPSAP2 ...
		-seq    search in the sequence space instead of in the header
		-acc    enables accession number search instead of a full id search (use A0A2R6RCR6 instead of tr|A0A2R6RCR6|A0A2R6RCR6_ACTCC),
				(does not work together with -E)
		-end    query and target string are matching if they are equal up to a dangling end.
		        e.g. query="test42" matches "test42 and some more info" with -end.

DESCRIPTION
 
	This script finds all ids of a list of fasta files with identifier
	provided in a different file and saves the output to seperate files named
	ID_FASTA where ID is the found identifier and FASTA the 
	name of the fasta file that contains this identifier.
	STDERR contains ids and the number of times they are found.
	Use GREPFASTADIR environment variable to store fa-idx files (otherwisse they are written to input files or current directory)
       
EXAMPLES

 	# 1. most simple call:
	perl grepfasta.pl 'tr|asd|asd' *.faa
		STDOUT:
			>tr|asd|asd Cell pattern formation-associated protein stuA OS=Acremon(...)
			MNNGGPTEMYYQQHMQSAGQPQQPQTVTSGPMSHYPPAQPPLLQPGQPYSHGAPSPYQYG

 	# 2. regex search:
	perl grepfasta.pl -E '.*a?sd[0-3].*' *.faa

 	# 3. multiple ids:
	perl grepfasta.pl 'tr|asd|asd,tr|asd2|asd' *.faa

 	# 4. id file and write output to files:
	cat test.idlist | grepfasta.pl - test.faa
		-----test.idlist:---------- (does not need the first column)
		POP1	tr|asd|asd
		POP1	tr|asd1|asd
		POP1	tr|asd2|asd
		POP2	tr|asd3|asd
		
 	# 5. id file and write output to files:
	grepfasta.pl -tofiles test.idlist test.faa
		-----test.idlist:---------- (does not need the first column)
		POP1	tr|asd|asd
		POP1	tr|asd1|asd
		POP1	tr|asd2|asd
		POP2	tr|asd3|asd
		
		------test.faa:------------
		>tr|asd|asd Cell pattern formation-associated protein stuA OS=Acremon(...)
		MNNGGPTEMYYQQHMQSAGQPQQPQTVTSGPMSHYPPAQPPLLQPGQPYSHGAPSPYQYG
		>tr|asd3|asd Histone H4 OS=Acremonium chrysogenum (strain ATCC 11550 (...)
		MTGRGKGGKGLGKGGAKRHRKILKDNIQGITKPAIRRLARRGGVKRISAMIYEETRGVLK
		>tr|A0A086SUI7|A0A086SUI7_ACRC1 Uncharacterized protein OS=Acremonium(...)
		MAFPLHFSREPAHAIPSMKAPFSRHEVPFGRSPSMAIPNSETHDDVPPPLPPPRHPPCTN
		>tr|asd2|asd 1-phosphatidylinositol 3-phosphate 5-(...)
		MAFPLHFSREPAHAIPSMKAPFSRHEVPFGRSPSMAIPNSETHDDVPPPLPPPRHPPCTN

		OUTPUT (2 files)

		------POP1_test:------------
		>tr|asd|asd Cell pattern formation-associated protein stuA OS=Acremon(...)
		MNNGGPTEMYYQQHMQSAGQPQQPQTVTSGPMSHYPPAQPPLLQPGQPYSHGAPSPYQYG
		>tr|asd2|asd 1-phosphatidylinositol 3-phosphate 5-(...)
		MAFPLHFSREPAHAIPSMKAPFSRHEVPFGRSPSMAIPNSETHDDVPPPLPPPRHPPCTN
		
		------POP2_test:------------
		>tr|asd3|asd Histone H4 OS=Acremonium chrysogenum (strain ATCC 11550 (...)
		MTGRGKGGKGLGKGGAKRHRKILKDNIQGITKPAIRRLARRGGVKRISAMIYEETRGVLK

		STDERR: 	
		tr|asd|asd	1
		tr|asd1|asd	1
		tr|asd2|asd	0
		tr|asd3|asd	1

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES perl : Getopt::Long, perl : File::Basename
ENDUSAGE

our $has_samtools=1;
# system("grep --help >/dev/null 2>&1"); if($?!=0){$has_grep=0;print STDERR "[WARNING] Please consider installing 'grep', to speed up the search process for large fasta files !\n"}

our $has_gzip=1;
system("gzip --help >/dev/null 2>&1"); if($?!=0){$has_gzip=0}

my $query;
my $help;
my $tofiles=0;
my $sourceFN=0;
my $justid;
my $prefix=">";
my $tag="";
my $doregex=0;
my $source=0;
my $del=',';
my $stat=0;
my $slow=1;
my $end=0;
my $caseinsensitive=0;
my $acc=0;
my $anyway=0;
my $debug=0;
my $normalsearch=1;
my $seq=0;
GetOptions('help!'=>\$help,'h!'=>\$help,
	'tofiles!'=>\$tofiles,
	'tofilesSource!'=>\$sourceFN,
	'ts!'=>\$sourceFN,
	'sourceFN!'=>\$sourceFN,
	't!'=>\$tofiles,
#	'prefix=s'=>\$prefix,
	'source'=>\$source,
	'end'=>\$end,
	'tag=s'=>\$tag,
	'stat'=>\$stat,
	'acc'=>\$acc,
	'samtools'=>sub { $slow=0; }, # \$slow,
	'version!'=>sub { print "$0\tv$version\n";exit 0; },
	's'=>\$source,
	'seq'=>\$seq,
	'v'=>sub { $normalsearch=0 },
	'F=s'=>\$del,
	'x'=>\$debug,
	'i'=>\$caseinsensitive,
	'anyway'=>\$anyway,
	'E'=>\$doregex);

$query = $ARGV[0];
shift(@ARGV);

if($sourceFN){$tofiles=1}

if(!$normalsearch && ($tofiles || $seq)){print STDERR "[ERROR] this combination of parameters does not work...\n"; exit 1;}
if($doregex && $acc){print STDERR "[ERROR] the combination of -E and -acc is not supported...\n"; exit 1;}
if($doregex && !$slow){print STDERR "[WARNING] -E is not compatible with the fast default search (samtools fa-idx only creates an index of the gene name), setting -slow and continuing...\n"; $slow=1;}

if(!$slow){
	system("samtools 2>/dev/null"); if($?!=256){$has_samtools=0;print STDERR "[WARNING] Please consider installing 'samtools' for the fast search (fa-idx), to speed up the search process for large fasta files!\n"}
}else{$has_samtools=0}

if($tag ne ""){$tag="$tag=([^ ]+)"}

if ($help || scalar(@ARGV) == 0 || !$query ){
    print $usage;
    exit(1);
}

sub makePercent{
	my $a = shift;
	return( (int($a*10000)/100)."%" );
}
sub approxProgress{
	my $in=shift;
	if($in > 0.999){$in=0.999}
	return $in;
}

my %qdata;
my %qdata_count;
my $input_query_is_file_with_group=0;
our $input_query_is_file_with_group_tmpfile="";

$SIG{INT} = sub { if(-e $input_query_is_file_with_group_tmpfile){unlink($input_query_is_file_with_group_tmpfile)} };
$SIG{TERM} = sub { if(-e $input_query_is_file_with_group_tmpfile){unlink($input_query_is_file_with_group_tmpfile)} };

if(open(FH,'<',$query)) {
	while(<FH>){
		if($_=~/^#/){next}
		chomp($_);
		my @sp = split("\t",$_);
		if(scalar(@sp)==2){
			if($sp[1]=~/^#/){next}
			$input_query_is_file_with_group=1;
			if(!exists $qdata{$sp[1]}){$qdata{$sp[1]}=()}
			push(@{$qdata{$sp[1]}},$sp[0]);
			$qdata_count{$sp[1]}=0;
		}elsif(scalar(@sp)==1 && $_ ne ""){
			if(!exists $qdata{$sp[0]}){$qdata{$sp[0]}=()}
			push(@{$qdata{$sp[0]}},basename($query));
			$qdata_count{$sp[0]}=0;
		}
	}
	close(FH);
}else{
	if($query eq "-"){
		foreach my $line (<STDIN>){
			if($line=~/^#/){next}
			chomp($line);
			if($line eq ""){next}
			if(!exists $qdata{$line}){$qdata{$line}=()}
			push(@{$qdata{$line}},$line);
			$qdata_count{$line}=0;
		}
	}else{
		my @sp = split($del,$query);
		for(my $v = 0 ; $v < scalar @sp ; $v++){
			chomp($sp[$v]);
			if($sp[$v] eq ""){next}
			if(!exists $qdata{$sp[$v]}){$qdata{$sp[$v]}=()}
			push(@{$qdata{$sp[$v]}},$sp[$v]);
			$qdata_count{$sp[$v]}=0;
		}
	}
}


if(scalar keys %qdata_count == 0){
	print STDERR "[ERROR] nothing to search ...\n";
	exit 1;
}

my $FHout;
my %output_filename_count;

for(my $v = 0 ; $v < scalar @ARGV ; $v++){

	my $wcl=0;
	my $is_large = 0;
	my $samtools_fai = "";
	my $filesize = 0;
	my $FH;

	if($ARGV[$v] eq "-"){
		$filesize = 0; # unknown for STDIN
	}else{
		$filesize = (-s $ARGV[$v]);
	}

	if($ARGV[$v] ne "-" && -e $ARGV[$v] && !$slow && ( $debug || (!$seq && $filesize > 2e+8)) ){
		$is_large = 1;
		$wcl = $filesize * 0.15 * ($ARGV[$v]=~/\.gz$/ ? 10 : 1); # approx
	
		# generate a md5 checksum to test against a local fai file
		my $md5="";
		if(!-e "".$ARGV[$v].".fai" && !-e "".$ARGV[$v].".fai.gz"){
			$md5=`md5sum '$ARGV[$v]' | awk '{print \$1}'`;
			if($? != 0){print STDERR "$!\n";exit 1}
			chomp($md5);
		}
		# if there is no local fai index present -> generate one using samtools fai-idx
		if( !-e "".$ARGV[$v].".fai" && 
			!-e "$GREPFASTADIR".(basename $ARGV[$v]).".$md5.fai" && 
			!-e "".$ARGV[$v].".fai.gz" && 
			!-e "$GREPFASTADIR".(basename $ARGV[$v]).".$md5.fai.gz" ){
			print STDERR "Generating fasta index $ARGV[$v].fai ...\n";
			
			if($has_samtools){
				system("touch '".$ARGV[$v].".fai.tmp' && samtools faidx '".$ARGV[$v]."' --fai-idx '".$ARGV[$v].".fai.tmp' && mv '".($ARGV[$v]).".fai.tmp' '".($ARGV[$v]).".fai'");
				if($? != 0){
					print STDERR "Generating local fasta index $GREPFASTADIR".(basename $ARGV[$v]).".$md5.fai ...\n";
					system("samtools faidx '".$ARGV[$v]."' --fai-idx '$GREPFASTADIR".(basename $ARGV[$v]).".$md5.fai.tmp' && mv '$GREPFASTADIR".(basename $ARGV[$v]).".$md5.fai.tmp' '$GREPFASTADIR".(basename $ARGV[$v]).".$md5.fai'");	
					if($? != 0){print STDERR "[ERROR] samtools faidx : $?";exit 1}
				}
			}else{ print STDERR "[ERROR] input file '$ARGV[$v]' large enough to use samtools but I cannot find it, please install samtools or use -slow to use the slow algorithm...\n"; exit 1; }

			$samtools_fai = -e "".$ARGV[$v].".fai" ? $ARGV[$v].".fai" : (-e "$GREPFASTADIR".(basename $ARGV[$v]).".$md5.fai" ? "$GREPFASTADIR".(basename $ARGV[$v]).".$md5.fai" : "");
			if($samtools_fai eq ""){$samtools_fai = -e "".$ARGV[$v].".fai.gz" ? $ARGV[$v].".fai.gz" : (-e "$GREPFASTADIR".(basename $ARGV[$v]).".$md5.fai.gz" ? "$GREPFASTADIR".(basename $ARGV[$v]).".$md5.fai.gz" : "")}
			if($has_gzip && $samtools_fai ne "" && $samtools_fai !~ /\.gz$/){print STDERR "Compressing fasta index $samtools_fai...\n"; system("gzip '$samtools_fai'")}
		}else{
			print STDERR "Found fasta index...\n";
			$samtools_fai = -e "".$ARGV[$v].".fai" ? $ARGV[$v].".fai" : (-e "$GREPFASTADIR".(basename $ARGV[$v]).".$md5.fai" ? "$GREPFASTADIR".(basename $ARGV[$v]).".$md5.fai" : "");
			if($samtools_fai eq ""){$samtools_fai = -e "".$ARGV[$v].".fai.gz" ? $ARGV[$v].".fai.gz" : (-e "$GREPFASTADIR".(basename $ARGV[$v]).".$md5.fai.gz" ? "$GREPFASTADIR".(basename $ARGV[$v]).".$md5.fai.gz" : "")}
			if($has_gzip && $samtools_fai ne "" && $samtools_fai !~ /\.gz$/){print STDERR "Compressing fasta index $samtools_fai...\n"; system("gzip '$samtools_fai'")}
		}
		$samtools_fai = -e "".$ARGV[$v].".fai" ? $ARGV[$v].".fai" : (-e "$GREPFASTADIR".(basename $ARGV[$v]).".$md5.fai" ? "$GREPFASTADIR".(basename $ARGV[$v]).".$md5.fai" : "");
		if($samtools_fai eq ""){$samtools_fai = -e "".$ARGV[$v].".fai.gz" ? $ARGV[$v].".fai.gz" : (-e "$GREPFASTADIR".(basename $ARGV[$v]).".$md5.fai.gz" ? "$GREPFASTADIR".(basename $ARGV[$v]).".$md5.fai.gz" : "")}

		# check modified timestamp for sanity (fasta should be newer than the index)
		my $modtime_samtools_fai = (stat($samtools_fai))[9];
		my $modtime_ARGV = (stat($ARGV[$v]))[9];

		if($modtime_samtools_fai < $modtime_ARGV){print STDERR "[ERROR] the fasta index file is older than the source fasta file, this does not make sense, please remove the fasta index '$samtools_fai' and try again\n"; exit 1}
	}

	# check input file and open stream
	if($samtools_fai eq "" || !-e $samtools_fai){
		print STDERR "searching '".($ARGV[$v] eq "-" ? "STDIN" : $ARGV[$v])."' ...\n";
		if($ARGV[$v] eq "-"){
			$FH="STDIN";
		}elsif(!-e $ARGV[$v]){
			print STDERR "[WARNING] cannot read file '$ARGV[$v]': file not found...\n";
			#exit 1;
		}elsif($ARGV[$v]=~/\.gz$/){
			open($FH,"zcat ".$ARGV[$v]." |") || die "Can't open file: $!";
			$wcl = `zcat '$ARGV[$v]' | wc -l`;
			$wcl =~s/^([0-9]+) .*/$1/g;
			$wcl = int($wcl);
		}else{
			open($FH,'<',$ARGV[$v]) || die "Can't open file: $!";
			$wcl = `wc -l '$ARGV[$v]'`;
			$wcl =~s/^([0-9]+) .*/$1/g;
			$wcl = int($wcl);
		}
	}


	# if there is an index present -> use that !
	if($samtools_fai ne "" && !$slow && -e $samtools_fai){
		my $FAI;
		if(-e $query){
			if($input_query_is_file_with_group && !-e $input_query_is_file_with_group_tmpfile ){
				$input_query_is_file_with_group_tmpfile=".grepfasta.searchids.tmp";
				while(-e $input_query_is_file_with_group_tmpfile){$input_query_is_file_with_group_tmpfile.=".tmp"}
				system("touch '$input_query_is_file_with_group_tmpfile'");
				open(FHoutIDS,">".$input_query_is_file_with_group_tmpfile) || die "Can't open file: $!";
				foreach my $key (keys %qdata) { print FHoutIDS "$key\n"; }
				close(FHoutIDS);
				$query=$input_query_is_file_with_group_tmpfile;
			}
		}

		open($FAI,($samtools_fai=~/\.gz$/?"z":"")."grep ".(!$doregex||-e $query ? "-":"").($doregex ? "":"F").(-e $query ? "f":"")." '$query' '".$samtools_fai."' |") || die "$!\n";

		my $cur_offset=0;
		open(my $FA,"<".$ARGV[$v]) or die "Can't open file: $!";
		my $output_filename = "";

		while(<$FAI>){
			chomp;
			my @fai_arr = split("\t",$_);
			if(scalar @fai_arr<5){next}
			my ($cur_header,$len,$offset,$linebases,$linewidth) = @fai_arr;

			my $clean_cur_header="$cur_header";
			$clean_cur_header =~ s/ .*//;

			my $clean_cur_header_acc=$clean_cur_header;
			if($acc){$clean_cur_header_acc=~s/^[^|_]+\|([^|_]+)\|[^|_]+_[^|_]+$/$1/g;}
			
			if($doregex){
				if($normalsearch){
					foreach my $key (keys %qdata) { 
						my $regexv = $key;
						my $cleankey = $key; $cleankey =~s/^["'>]+//g; $cleankey =~s/["']$//g; 
						my $nodescr = $cur_header; $nodescr =~s/[ \t]+.*$//g;

						my $min_len = length($cleankey)<length($cur_header) ? length($cleankey):length($cur_header);

						my $found_tag="";
						if($tag ne "" && ($caseinsensitive ? $cur_header =~ m/$tag/i : $cur_header =~ $tag) ){ $found_tag = $1 }

						if( (($tag ne "" && $found_tag ne "" && 
								(( $doregex && ($caseinsensitive ? $found_tag =~ m/$regexv/i: $found_tag =~ $regexv)) || $found_tag eq $key )
							) ||
							( $tag eq "" && 
								(
									($doregex && ($caseinsensitive ? $cur_header =~ m/$regexv/i : $cur_header =~ $regexv)) || 
									$cur_header eq $key || 
						 			(!$doregex && $end && substr($cur_header,0,$min_len) eq substr($cleankey,0,$min_len) ) || 
						 			(!$doregex && $cur_header eq $cleankey) || 
						 			(!$doregex && $nodescr eq $key)|| 
						 			(!$doregex && $nodescr eq $cleankey)
						 		)
							)) == $normalsearch
						){
							foreach my $output_filename (@{$qdata{$key}}) {
								
								if(!exists $qdata_count{$key}){$qdata_count{$key}=0}
								$qdata_count{$key}+=1;
							
								if($tofiles){
									my $nam=basename($ARGV[$v]); $nam=~s/\.(f[na]?a|fasta)$//ig; $nam=~s/[^a-zA-Z0-9_.-]/_/g;
									$output_filename =~ s/[^a-zA-z0-9._]/_/g;
									open($FHout,(!exists $output_filename_count{$output_filename} ? ">": ">>").$output_filename.(!$sourceFN?"":"_$nam").".fasta");
									my $result=""; 
									$offset-=length($clean_cur_header);
									while( $result eq "" && $offset > -2 * length($clean_cur_header) ){
										$result="";
										my $header_count=0;
										seek $FA, $offset<0?0:$offset,0; 
										while(<$FA>){
											if($header_count!=-1 && (rindex $_,">$clean_cur_header",0) == 0 ){$header_count=-1;$result.=$_;$result=~s/>//g;$result=">$output_filename--$result"}
											elsif($header_count==-1 && !/^>/){$result.="$_"}
											elsif($header_count==-1 &&  /^>/){last}
											elsif(/^>/){$header_count++}
											elsif($header_count>=5){$result="";last}
										}
										$offset-=length($clean_cur_header);
									}
									print $FHout $result;
									close($FHout);
									$output_filename_count{$output_filename}=1;
								}else{
									my $result=""; 
									$offset-=length($clean_cur_header);
									while( $result eq "" && $offset > -2 * length($clean_cur_header) ){
										$result="";
										my $header_count=0;
										seek $FA, $offset<0?0:$offset,0; 
										while(<$FA>){
											if($header_count!=-1 && (rindex $_,">$clean_cur_header",0) == 0 ){$header_count=-1;$result.=$_}
											elsif($header_count==-1 && !/^>/){$result.="$_"}
											elsif($header_count==-1 &&  /^>/){last}
											elsif(/^>/){$header_count++}
											elsif($header_count>=5){$result="";last}
										}
										$offset-=length($clean_cur_header);
									}
									print $result;
								}
							}
						}
					}
				}else{
					my $result=1;
					foreach my $key (keys %qdata) { 
						my $regexv = $key;
						my $cleankey = $key; $cleankey =~s/^["'>]+//g; $cleankey =~s/["']$//g; 
						my $nodescr = $cur_header; $nodescr =~s/[ \t]+.*$//g;

						my $min_len = length($cleankey)<length($cur_header) ? length($cleankey):length($cur_header);

						my $found_tag="";
						if($tag ne "" && $cur_header =~ $tag){ $found_tag = $1 }

						if( (($tag ne "" && $found_tag ne "" && 
								(( $doregex && $found_tag =~ $regexv) || $found_tag eq $key )
							) ||
							( $tag eq "" && 
								(
									($doregex && $cur_header =~ $regexv) || 
									$cur_header eq $key || 
						 			(!$doregex && $end && substr($cur_header,0,$min_len) eq substr($cleankey,0,$min_len) ) || 
						 			(!$doregex && $cur_header eq $cleankey) || 
						 			(!$doregex && $nodescr eq $key)|| 
						 			(!$doregex && $nodescr eq $cleankey)
						 		)
							))
						){
							$result *= 0
						}
					}
					if($result == 1){ # none of the qdata are found -> -v makes it that it is print
						$output_filename = "input";
						my $result=""; 
						$offset-=length($clean_cur_header);
						while( $result eq "" && $offset > -2 * length($clean_cur_header) ){
							$result="";
							my $header_count=0;
							seek $FA, $offset<0?0:$offset,0; 
							while(<$FA>){
								if($header_count!=-1 && (rindex $_,">$clean_cur_header",0) == 0 ){$header_count=-1;$result.=$_}
								elsif($header_count==-1 && !/^>/){$result.="$_"}
								elsif($header_count==-1 &&  /^>/){last}
								elsif(/^>/){$header_count++}
								elsif($header_count>=5){$result="";last}
							}
							$offset-=length($clean_cur_header);
						}
						print $result;
					}
				}
			}else{
				if( ($acc && (exists $qdata{$clean_cur_header_acc}) == $normalsearch ) || 
					(!$acc && (exists $qdata{$clean_cur_header}) == $normalsearch) ){

					foreach my $output_filename (@{$qdata{$clean_cur_header}}) {
						
						if(!exists $qdata_count{$acc ? $clean_cur_header_acc : $clean_cur_header}){$qdata_count{$acc ? $clean_cur_header_acc : $clean_cur_header}=0}
						$qdata_count{$acc ? $clean_cur_header_acc : $clean_cur_header}+=1;
					
						if($tofiles){
							my $nam=basename($ARGV[$v]); $nam=~s/\.(f[na]?a|fasta)$//ig; $nam=~s/[^a-zA-Z0-9_.-]/_/g;
							$output_filename =~ s/[^a-zA-z0-9._]/_/g;

							my $result=""; 
							$offset-=length($clean_cur_header);
							while( $result eq "" && $offset > -2 * length($clean_cur_header) ){
								$result="";
								my $header_count=0;
								seek $FA, $offset<0?0:$offset,0; 
								while(<$FA>){
									if($header_count!=-1 && (rindex $_,">$clean_cur_header",0) == 0 ){$header_count=-1;$result.=$_;$result=~s/>//g;$result=">$output_filename--$result"}
									elsif($header_count==-1 && !/^>/){$result.="$_"}
									elsif($header_count==-1 &&  /^>/){last}
									elsif(/^>/){$header_count++}
									elsif($header_count>=5){$result="";last}
								}
								$offset-=length($clean_cur_header);
							}

							open($FHout,(!exists $output_filename_count{$output_filename} ? ">": ">>").$output_filename.(!$sourceFN?"":"_$nam").".fasta");
							print $FHout $result;
							close($FHout);
							$output_filename_count{$output_filename}=1;
						}else{
							my $result=""; 
							$offset-=length($clean_cur_header);
							while( $result eq "" && $offset > -2 * length($clean_cur_header) ){
								$result="";
								my $header_count=0;
								seek $FA, $offset<0?0:$offset,0; 
								while(<$FA>){
									if($header_count!=-1 && (rindex $_,">$clean_cur_header",0) == 0 ){$header_count=-1;$result.=$_}
									elsif($header_count==-1 && !/^>/){$result.="$_"}
									elsif($header_count==-1 &&  /^>/){last}
									elsif(/^>/){$header_count++}
									elsif($header_count>=5){$result="";last}
								}
								$offset-=length($clean_cur_header);
							}
							if((rindex $result,">",0)!=0){print STDERR "[ERROR] was not able to find '$cur_header' although it was present in the fasta index, please re-calculate the index file...";exit 1}
							print $result;
						}
					}
				}
			}
		}
		close($FAI);
		close($FA);

	}elsif($ARGV[$v] eq "-" || -e $ARGV[$v]){

		my @output_filenames;

		my $cur_header="";
		my $cur_seq="";
		my $linei=0;
		my $print_current_entry=0;

		while(<$FH>){
			chomp($_);
			if($_ eq ""){ next }
			$linei++;

			if( substr($_,0,1) eq $prefix ){
				$print_current_entry = 0;

				if($seq){
					foreach my $key (keys %qdata) { 
						if($cur_seq =~ $key){
							foreach my $output_filename (@{$qdata{$key}}){
							
								if(!exists $qdata_count{$key}){$qdata_count{$key}=0}
								$qdata_count{$key}+=1;
							
								if($tofiles){
									my $nam=$ARGV[$v] eq "-" ? "STDIN" : basename($ARGV[$v]); $nam=~s/\.(f[na]?a|fasta)$//ig; $nam=~s/[^a-zA-Z0-9_.-]/_/g;
									$output_filename =~ s/[^a-zA-z0-9._]/_/g;
									open($FHout,(!exists $output_filename_count{$output_filename} ? ">": ">>").$output_filename.(!$sourceFN?"":"_$nam").".fasta");
									print $FHout ">$output_filename--$cur_header\n$cur_seq";
									close($FHout);
									$output_filename_count{$output_filename}=1;
								}else{
									if($input_query_is_file_with_group){$cur_header="$cur_header $output_filename"}
									print ">$cur_header\n$cur_seq\n";
								}
							}
						}
					}
				}

				$cur_seq = "";
				$cur_header = $_;

				@output_filenames = ();
				$cur_header=substr($cur_header,1); # remove >
				my $clean_cur_header=$cur_header;
				$clean_cur_header =~ s/ .*//;

				my $clean_cur_header_acc=$clean_cur_header;
				if($acc){$clean_cur_header_acc=~s/^[^|_]+\|([^|_]+)\|[^|_]+_[^|_]+$/$1/g;}

				if($seq){next}

				if($doregex){
					if($normalsearch){
						foreach my $key (keys %qdata) { 
							my $regexv = $key;
							my $cleankey = $key; $cleankey =~s/^["'>]+//g; $cleankey =~s/["']$//g; 
							my $nodescr = $cur_header; $nodescr =~s/[ \t]+.*$//g;

							my $min_len = length($cleankey)<length($cur_header) ? length($cleankey):length($cur_header);

							my $found_tag="";
							if( $tag ne "" && $cur_header =~ $tag ){ $found_tag = $1 }

							if( (($tag ne "" && $found_tag ne "" && 
									(( $doregex && $found_tag =~ $regexv) || $found_tag eq $key )
								) ||
								( $tag eq "" && 
									(
										($doregex && $cur_header =~ $regexv) || 
										$cur_header eq $key || 
							 			(!$doregex && $end && substr($cur_header,0,$min_len) eq substr($cleankey,0,$min_len) ) || 
							 			(!$doregex && $cur_header eq $cleankey) || 
							 			(!$doregex && $nodescr eq $key)|| 
							 			(!$doregex && $nodescr eq $cleankey)
							 		)
								)) == $normalsearch
							){
								$print_current_entry=1;

								foreach my $output_filename (@{$qdata{$key}}) {
									if(!exists $qdata_count{$key}){$qdata_count{$key}=0}
									$qdata_count{$key}+=1;
								
									if($tofiles){
										my $nam=$ARGV[$v] eq "-" ? "STDIN" :basename($ARGV[$v]); $nam=~s/\.(f[na]?a|fasta)$//ig; $nam=~s/[^a-zA-Z0-9_.-]/_/g;
										$output_filename =~ s/[^a-zA-z0-9._]/_/g;
										open($FHout,(!exists $output_filename_count{$output_filename} ? ">": ">>").$output_filename.(!$sourceFN?"":"_$nam").".fasta");
										print $FHout ">$output_filename--$cur_header\n";
										close($FHout);
										$output_filename_count{$output_filename}=1;
									}else{
										if($input_query_is_file_with_group){$cur_header="$cur_header $output_filename"}
										if($source){ print ">$cur_header ".($ARGV[$v] eq "-" ? "STDIN" :basename($ARGV[$v]))."\n"; }else{ print ">$cur_header\n"; }
									}
								}
							}
						}
					}else{
						my $result=1;
						foreach my $key (keys %qdata) { 
							my $regexv = $key;
							my $cleankey = $key; $cleankey =~s/^["'>]+//g; $cleankey =~s/["']$//g; 
							my $nodescr = $cur_header; $nodescr =~s/[ \t]+.*$//g;

							my $min_len = length($cleankey)<length($cur_header) ? length($cleankey):length($cur_header);

							my $found_tag="";
							if($tag ne "" && $cur_header =~ $tag){ $found_tag = $1 }

							if( (($tag ne "" && $found_tag ne "" && 
									(( $doregex && $found_tag =~ $regexv) || $found_tag eq $key )
								) ||
								( $tag eq "" && 
									(
										($doregex && $cur_header =~ $regexv) || 
										$cur_header eq $key || 
							 			(!$doregex && $end && substr($cur_header,0,$min_len) eq substr($cleankey,0,$min_len) ) || 
							 			(!$doregex && $cur_header eq $cleankey) || 
							 			(!$doregex && $nodescr eq $key)|| 
							 			(!$doregex && $nodescr eq $cleankey)
							 		)
								))
							){
								$result *= 0
							}
						}
						if($result == 1){ # none of the qdata are found -> -v makes it that it is print
							$print_current_entry=1;
							if($input_query_is_file_with_group){$cur_header="$cur_header data"}
							if($source){ print ">$cur_header ".($ARGV[$v] eq "-" ? "STDIN" :basename($ARGV[$v]))."\n"; }else{ print ">$cur_header\n"; }
						}
					}
				}else{
					if( ($acc && (exists $qdata{$clean_cur_header_acc}) == $normalsearch ) || 
						(!$acc && (exists $qdata{$clean_cur_header}) == $normalsearch) ){
						$print_current_entry=1;
						@output_filenames = @{$qdata{$clean_cur_header}};
						foreach my $output_filename (@{$qdata{$clean_cur_header}}){

							if(!exists $qdata_count{$acc ? $clean_cur_header_acc : $clean_cur_header}){$qdata_count{$acc ? $clean_cur_header_acc : $clean_cur_header}=0}
							$qdata_count{$acc ? $clean_cur_header_acc : $clean_cur_header}+=1;
						
							if($tofiles){
								my $nam=$ARGV[$v] eq "-" ? "STDIN" :basename($ARGV[$v]); $nam=~s/\.(f[na]?a|fasta)$//ig; $nam=~s/[^a-zA-Z0-9_.-]/_/g;
								$output_filename =~ s/[^a-zA-z0-9._]/_/g;
								open($FHout,(!exists $output_filename_count{$output_filename} ? ">": ">>").$output_filename.(!$sourceFN?"":"_$nam").".fasta");
								print $FHout ">$output_filename--$cur_header\n";
								close($FHout);
								$output_filename_count{$output_filename}=1;
							}else{
								if($input_query_is_file_with_group){$cur_header="$cur_header $output_filename"}
								if($source){ print ">$cur_header ".($ARGV[$v] eq "-" ? "STDIN" :basename($ARGV[$v]))."\n"; }else{ print ">$cur_header\n"; }
							}
						}
					}
				}
			}elsif($print_current_entry){
				if(!$tofiles && scalar @output_filenames > 1){@output_filenames = ($output_filenames[0])}
				if($tofiles){
					foreach my $output_filename (@output_filenames){
						if(defined $output_filename && $output_filename ne "" && $tofiles){
							my $nam=$ARGV[$v] eq "-" ? "STDIN" :basename($ARGV[$v]); $nam=~s/\.(f[na]?a|fasta)$//ig; $nam=~s/[^a-zA-Z0-9_.-]/_/g;
							system("echo '$_' >>".$output_filename.(!$sourceFN?"":"_$nam").".fasta");
						}
					}
				}else{
					print "$_\n";	
					$cur_seq.=$_;
				}
			}
		}
		close($FH);

		if($wcl > 10000){ print STDERR "\033[J"." done with ".($ARGV[$v] eq "-" ? "STDIN" :basename($ARGV[$v]))."..."."\033[G"; }

		if($seq){
			foreach my $key (keys %qdata) { 
				if($cur_seq =~ $key){
					foreach my $output_filename (@{$qdata{$key}}) {
							
						if(!exists $qdata_count{$key}){$qdata_count{$key}=0}
						$qdata_count{$key}+=1;
					
						if($tofiles){
							my $nam=$ARGV[$v] eq "-" ? "STDIN" :basename($ARGV[$v]); $nam=~s/\.(f[na]?a|fasta)$//ig; $nam=~s/[^a-zA-Z0-9_.-]/_/g;
							$output_filename =~ s/[^a-zA-z0-9._]/_/g;
							open(my $FHout,(!exists $output_filename_count{$output_filename} ? ">": ">>".$output_filename.(!$sourceFN?"":"_$nam").".fasta"));
							print $FHout ">$output_filename--$cur_header\n$cur_seq";
							close($FHout);
							$output_filename_count{$output_filename}=1;
						}else{
							if($input_query_is_file_with_group){$cur_header="$cur_header $output_filename"}
							print ">$cur_header\n$cur_seq\n";
						}
					}
				}
			}
		}
	}
}

if($stat == 0){
	my $foundNum = 0;
	my $foundMultiNum = 0;
	my $foundTotal = 0;
	foreach my $key (keys %qdata_count) { 
		$foundTotal+=$qdata_count{$key};
		if($qdata_count{$key} == 1){$foundNum++}
		elsif($qdata_count{$key} > 1){$foundMultiNum++}
	}
	my $notfound=0;
	foreach my $key (keys %qdata_count) { if($qdata_count{$key}==0){ $notfound++ }}

	print STDERR "I found ".$foundNum." (".( int(10000*($foundNum/scalar keys %qdata_count))/100 )."%) uniquely and ".$foundMultiNum." (".( int(10000*($foundMultiNum/scalar keys %qdata_count))/100 )."%) multiple times out of the given ".(scalar keys %qdata_count)." input queries".($notfound>0 ? " (missing $notfound)":"")." and a total of $foundTotal entries were outputted. (Use --stat to recive a full table of all entries).\n";
	if((scalar keys %qdata_count)>1 && (scalar keys %qdata_count) - $foundNum == 1){
		if($normalsearch){
			print STDERR "Only one entry is missing, this could be a header line? The entry is: '";
			foreach my $key (keys %qdata_count) { if($qdata_count{$key}==0){ print STDERR $key."'\n"; last; }}
		}else{
			print STDERR "Only one entry is missing, the entry is: '";
			foreach my $key (keys %qdata_count) { if($qdata_count{$key}==0){ print STDERR $key."'\n"; last; }}
		}
	}
	
}else{
	print STDERR "# key\toccurences\n";
	foreach my $key (keys %qdata_count) { 
		print STDERR $key."\t".$qdata_count{$key}."\n";
	}
}

if($input_query_is_file_with_group_tmpfile ne "" && -e $input_query_is_file_with_group_tmpfile){unlink $input_query_is_file_with_group_tmpfile}
