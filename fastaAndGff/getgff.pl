#!/usr/bin/env perl
#pk

use strict;
use warnings "all";

my $fasta ="";

use Cwd 'abs_path';
use File::Basename;

my $version="0.2.5"; # last updated on 2024-01-10 15:53:41

my $id="";
my @newARGV;
my $do_format=0;

foreach my $arg (@ARGV) {
  if($arg =~ m/^-?-id=(.+)/){
    $id=$1;
  }elsif($arg =~ m/^-?-format\b/){
    $do_format=1
  }else{
    push(@newARGV,$arg)
  }
}
@ARGV=@newARGV;

if(scalar(@ARGV) == 5 && !-e $ARGV[2] && !-e $ARGV[3] && $ARGV[2] > $ARGV[3]){
  my $tmp=$ARGV[2];
  $ARGV[2]=$ARGV[3];
  $ARGV[3]=$tmp;
}

if(scalar @ARGV != 0 && $ARGV[0] =~ /-?-version|-v$/ ){ print "$0\tv$version\n"; exit 0; }

if( scalar(@ARGV) < 2 ){
  print "".("USAGE : getgff.pl   extracts genomic positions of a given gff file OR from command line

SYNOPSIS 
  getgff.pl (options) FASTA(S) chromosom start end strand
    chromosom: can be - for the first entry of the fasta file
    start, end: are 1-based indices/coordinates
    FASTA(S) : one or multiple fasta file

  or

  getgff.pl (options) FASTA(S) GFF
    FASTA(S) : one or multiple fasta file (FASTA1 FASTA2 ...)
    GFF : one gff file

OPTIONAL options

  -id=STRING 
    set the name of the output fasta entries to the gff attribute with this name 
    STRING: product, id, name, ...
      multiple fields can be specified by | (e.g. -id='ID|product|Name')
      the gff column names can be used here too:
    seqname source feature start end score strand frame attribute 

  -format
    standard 80 characters per line output

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES perl: Cwd, File::Basename
"); exit 0;
}

our %stats; # for outputting log information
my %id_clashes;

$stats{"gff entries"}=0;
$stats{"extracted"}=0;
$stats{"clashes"}=0;
#
# Load data
#

  my $isOnlyOneGffentry=0;
  my @gff;
  my %orginalGFFstring;

  my $chr_i = scalar(@ARGV)-4;
  my $start_i = scalar(@ARGV)-3;
  my $end_i = scalar(@ARGV)-2;
  my $strand_i = scalar(@ARGV)-1;
  my $load_gff_from_terminal=0;

  my $gff_file_name = $ARGV[(scalar @ARGV)-1];

  if(scalar(@ARGV) >= 5 && !-e $ARGV[$start_i] && !-e $ARGV[$end_i]){ # GFF FROM TERMINAL LINE

    $load_gff_from_terminal=1;
    $isOnlyOneGffentry=1;
    $gff_file_name="COMMANDLINE";
    
    if($ARGV[$chr_i] eq "-"){$ARGV[$chr_i]=">";}

    $stats{"gff entries"}++;
    my @a=($ARGV[$chr_i],"getgff","-",int($ARGV[$start_i]),int($ARGV[$end_i]),".",$ARGV[$strand_i],".","-");

    if( int($a[3]) ne $a[3] ){print STDERR "[WARNING] 'start' needs to be an integer ('@a') !\n"; next;}
    if( int($a[4]) ne $a[4] ){print STDERR  "[WARNING] 'end' needs to be an integer ('@a') !\n"; next;}
    $a[3]=int($a[3]);$a[4]=int($a[4]);
    if( $a[3]<1 || $a[4]<1 ){print STDERR  "[WARNING] 'start', 'end' needs to be 1-indices ('@a') !\n"; next;}
    if(  $a[3] > $a[4] ){print STDERR  "[WARNING] 'start' needs to be smaller than 'end' (positions are relative to the + strand) ('@a') !\n"; next;}
    
    push(@gff,\@a);
    $orginalGFFstring{join(" ",@a)}=join(" ",@a);

  }else{ # OPEN GFF FILE

    open (GFF,"<",$ARGV[(scalar @ARGV)-1]) or die "[ERROR] error thrown when try to open the gff file : \n$!";
    while (my $entry = <GFF>){
      $entry=~ s/[\r\n]+$//; # better than chomp

      if(length($entry)<=3 || substr($entry,0,1) eq "#"){ next }

      my @a=split("\t",$entry);
      if(scalar(@a)!=9){die "[ERROR] gff is invalid (splitting by '\\t' did not result in 9 columns) : \n$entry\n"}
      
      $stats{"gff entries"}++;

      if( int($a[3]) ne $a[3] ){print STDERR "[WARNING] 'start' needs to be an integer ('@a') !\n"; next;}
      if( int($a[4]) ne $a[4] ){print STDERR  "[WARNING] 'end' needs to be an integer ('@a') !\n"; next;}
      $a[3]=int($a[3]);$a[4]=int($a[4]);
      if( $a[3]<1 || $a[4]<1 ){print STDERR  "[WARNING] 'start', 'end' needs to be 1-indices ('@a') !\n"; next;}
      if(  $a[3] > $a[4] ){print STDERR  "[WARNING] 'start' needs to be smaller than 'end' (positions are relative to the + strand) ('@a') !\n"; next;}
      
      $orginalGFFstring{join(" ",@a)}=$entry;

      if($id ne ""){
        my @cur_id;
        while( $entry =~ /(${id})=([^;]+)/g ){ push(@cur_id, $2); }
        if( scalar @cur_id == 0 && ( $entry =~ /Note=([^;]+)/i || $entry=~ /gene=([^;]+)/i ) ){ push(@cur_id, $1); }

        if(scalar @cur_id == 0 && $entry=~ /id=([^;]+)/i){ push(@cur_id, $1); }
        while($id =~ /(seqname|source|feature|start|end|score|strand|frame|attribute)/g){
          if($1 eq "seqname"){
            push(@cur_id,$a[0]);
          }elsif($1 eq "source"){
            push(@cur_id,$a[1]);
          }elsif($1 eq "feature"){
            push(@cur_id,$a[2]);
          }elsif($1 eq "start"){
            push(@cur_id,$a[3]);
          }elsif($1 eq "end"){
            push(@cur_id,$a[4]);
          }elsif($1 eq "score"){
            push(@cur_id,$a[5]);
          }elsif($1 eq "strand"){
            push(@cur_id,$a[6]);
          }elsif($1 eq "attribute"){
            push(@cur_id,$a[7]);
          }
        }
        if(scalar @cur_id > 0){
          my $cur_id_str = join("_",@cur_id);
          $cur_id_str =~s/[ \/\\:*|]/_/g;
          if(exists $id_clashes{$cur_id_str}){
            $cur_id_str=~s/_[0-9]+$//g;
            my $i_id=0;
            while(exists $id_clashes{$cur_id_str."_$i_id"}){$i_id++; $stats{"clashes"}++}
            $cur_id_str.="_$i_id"; 
          }
          $orginalGFFstring{join(" ",@a)}=$cur_id_str;
          $id_clashes{$cur_id_str}=1;
        }

      }else{
        if(exists $id_clashes{$a[0]}){
          $a[0]=~s/_[0-9]+$//g;
          my $i_id=0;
          while(exists $id_clashes{$a[0]."_$i_id"}){$i_id++; $stats{"clashes"}++}
          $a[0].="_$i_id"; 
        }
      }

      push(@gff,\@a); #[$a[0],$a[1],$a[2],$a[3],$a[4],$a[5],$a[6],$a[7],$a[8]]
    }
    close (GFF);
  }

  print STDERR "[getgff] I loaded ".$stats{"gff entries"}." gff entries".($stats{"clashes"}>0 ? " (with ".$stats{"clashes"}." resolved id clashes)": "")." from '$gff_file_name'.\n";

  # sort by CHR first and then the start value -> for a given chr, just go to the first entry of that name -> iterate over the start positions 
  sub sortByStart {if($a->[0] eq $b->[0]){return (int($a->[3]) <=> int($b->[3]))}else{return ($a->[0] cmp $b->[0])};}   
  @gff = (sort sortByStart @gff);

  #for(my $i=0;$i<scalar(@gff);$i++){print STDERR $gff[$i][0]." ".$gff[$i][3]."\n"}
  #die;

#
# Iterate over fasta and return for each entry of the gff the corresponding sequence
#

my %is_done_gff; # marking gff entries as done so we dont need to compare them again
my %result;
for (my $fasta_i = 0; $fasta_i < ($load_gff_from_terminal ? (scalar @ARGV)-4 : (scalar @ARGV)-1) ; $fasta_i++) {

  if(! -e $ARGV[$fasta_i]){next}

  open (FASTA,"<",$ARGV[$fasta_i]) or die "[ERROR] error thrown when try to open the fasta file : \n$!";
    my $cur_position=1;
    my $cur_header="";
    my $cur_lowestStart=-1;

    my $gff_start_i_of_current_CHR=0;
    my $gff_end_i=-1;
    my $current_CHR="";
    our $cur_gene;

    while (my $entry = <FASTA>){ # iterate over the fasta file

      if( $entry =~ m/^>/){ # header line

        $entry=~ s/[\r\n]+$//; # better than chomp

        $current_CHR="";
        $cur_header=$entry;
        $cur_position=1;

        my $last_CHR="";
        $cur_header=~s/\.//g;
        $cur_header=~s/_//g;

        for(my $i=0;$i<scalar(@gff);$i++){ # find the smallest start value for this CHR (gff is sorted)
          
          if(exists $is_done_gff{$i}){next;}

          my $chr=$gff[$i][0];
          $chr=~s/\.//g;
          $chr=~s/_//g;

          if( $last_CHR ne $chr && $cur_header =~ qr/$chr/ ){

            $cur_lowestStart=int($gff[$i][3]);
            $gff_start_i_of_current_CHR=$i;
            $current_CHR=$gff[$i][0];
            last;
          }
          $last_CHR=$gff[$i][0]; # just to speed up this search
        }

      }elsif ( $current_CHR ne "" && $cur_header ne "") { # not a header line AND a header line was allready present

        $entry=~ s/[\r\n]+$//; # better than chomp

        my $new_end_position=$cur_position+length($entry)-1; # end position of the current line

        my @foundSomethingThisLine; # entries that are found this line(gff)
        my %currentylActive; # keeps trac of all gff entries that are currently active (could be multiple if gff entries overlap)

        if($cur_lowestStart==-1 || ( $cur_lowestStart >= $cur_position && $cur_lowestStart <= $new_end_position)){

          $cur_lowestStart=-1;

          for(my $i=$gff_start_i_of_current_CHR;$i<scalar(@gff);$i++){

            if(exists $is_done_gff{$i}){next;}

            my $chr   = $gff[$i][0];
            my $start = $gff[$i][3]; 
            my $end   = $gff[$i][4];
            my $strand= $gff[$i][6];

            if($chr ne $current_CHR){next;}

            if( $cur_position <= $start && $start <= $new_end_position ){ # a gff entry starts in this line
              $cur_gene="";

              if( $end <= $new_end_position ) { # and also ends there

                $cur_gene=substr($entry , $start-$cur_position , $end-$start+1);
                if( $strand eq "-" ){ 
                  $cur_gene =~ tr/ACGTacgt/TGCAtgca/; 
                  $cur_gene = reverse $cur_gene;
                } # if - = complement of the + gene string
                $result{join(" ",@{$gff[$i]})} = $cur_gene;
                push(@foundSomethingThisLine,$i); # this gff entry is done -> mark as found
                $is_done_gff{$i}=1;
                delete $currentylActive{$i};

              }else{ # goes to the next line

                $cur_gene=substr($entry , $start-$cur_position);
                $result{join(" ",@{$gff[$i]})} = $cur_gene; # new entry
                $currentylActive{$i}=0;

              }

            }elsif( $start <= $new_end_position ){ # entry extends into this line

              if( $end <= $new_end_position ) { # and also ends there

                $cur_gene=$result{join(" ",@{$gff[$i]})} . substr($entry , 0 , $end-$cur_position+1); # take the string of the current line AND everything that was present allready
                if( $strand eq "-" ){ 
                  $cur_gene =~ tr/ACGTacgt/TGCAtgca/; 
                  $cur_gene = reverse $cur_gene;
                } # if - = complement of the + gene string
                $result{join(" ",@{$gff[$i]})} = $cur_gene;
                push(@foundSomethingThisLine,$i); # this gff entry is done -> mark as found
                $is_done_gff{$i}=1;
                delete $currentylActive{$i};

              }else{ # goes to the next line

                $cur_gene=$entry; # whole line
                $result{join(" ",@{$gff[$i]})} .= $cur_gene; # append
                $currentylActive{$i}=0;

              }
            }
          }
        }

        if((scalar @foundSomethingThisLine)>0){
          if(scalar keys %currentylActive == 0){ # if there are overlapping gff entries, one entry could finish and another one is still going -> all must be done then update to the next cur_lowestStart position
            for(my $i=0;$i<scalar(@gff);$i++){ # update the cur_lowestStart to the next element of the sorted gff (only look at the first of each CHR entry since it is sorted)
              if(exists $is_done_gff{$i}){next;}
              my $chr=$gff[$i][0];
              if( $current_CHR eq $chr && $cur_header =~ qr/$chr/ ){
                $cur_lowestStart=int($gff[$i][3]);
                last;
              }
            }
          }
        }
        if(scalar keys %is_done_gff == scalar @gff){last;} # we are done
        $cur_position = $new_end_position+1; # +1 pushed the position to the next line since new_end_position is still inside the line (last position)

      }
    }
  close(FASTA);
}
#
# Print results
#

foreach my $key (sort keys %result){
  $stats{"extracted"}++;
  if($isOnlyOneGffentry){
    print "".($do_format ? join "\n", unpack "(A80)*", $result{$key} : $result{$key})."\n";
  }else{
    print ">".$orginalGFFstring{$key}."\n".( $do_format ? join "\n", unpack "(A80)*", $result{$key} : $result{$key} )."\n";
  }
}

print STDERR "[getgff] Extracted ".$stats{"extracted"}." sequence out of the given ".$stats{"gff entries"}." gff entries (".(int(10000*$stats{"extracted"}/$stats{"gff entries"})/100)."\%).\n";
