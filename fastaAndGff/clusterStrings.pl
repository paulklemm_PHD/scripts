#!/usr/bin/perl
#pk

use strict;
use warnings "all";

use Getopt::Long; # for parameter specification on the command line
use File::Basename; # basename()
use List::Util qw( max );

my $query = "";
my $help;
our $parsefasta = 0;
our $debug = 0;
our $consensus = 0;
our $all = 0;
our $tofiles = 0;
our $stat = 0;
our $max_it = 10000;
our $threshold = 0.45;

my $version="0.0.19"; # last updated on 2021-10-01 14:11:18

my $usage = <<ENDUSAGE;
clusterStrings.pl        cluster input strings of same lengths (sequences)
 
SYNOPSIS
 
clusterStrings.pl (options) INFILE

	INFILE  input file containing the strings (can be - for STDIN)

	optional:
		-fasta      parses fasta formats
		-consensus  prints out the consensus string (with maximal character probability)
					otherwise one member of the group is printed
		-all        print all entries with a group number
		-tofiles    split output in different files 
		-stat       emits for each symbol the number input strings that agree with that symbol. The numbers are separated by |
		-t FLOAT    cluster threshold: the maximal percentual similarity for merging 2 strings/groups (default:$threshold) 
		-maxit INT  maximal number of iterations (default:$max_it) 

DESCRIPTION
 
	TODO

EXAMPLES

	# use protein sequences

	clusterStrings.pl -fasta -consensus proteins.faa

	# use STDIN

	awk '{print \$1}' somefile | clusterStrings.pl -

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES perl : Getopt::Long, File::Basename, List::Util qw( max )
ENDUSAGE

GetOptions('help!'=>\$help,'h!'=>\$help,
	'fasta'=>\$parsefasta,
	'consensus'=>\$consensus,
	't=f'=>\$threshold,
	'maxit=i'=>\$max_it,
	'all'=>\$all,
	'tofiles'=>\$tofiles,
	'stat'=>\$stat,
	'version!'=>sub { print "$0\tv$version\n";exit 0; },
	'x'=>\$debug);

my $query_output ="";
if(scalar @ARGV > 0){
	$query = $ARGV[0];
	shift(@ARGV);
	$query_output = ( $query eq "-" ? "STDIN" : $query);
}

if ( $help || !$query ){
    print $usage;
    exit(0);
}

our @data;
our @header;

our $maximalLength=0;

sub parseLine{
	my $line = shift;
	$line=~s/[\r\n]+$//g;

	if(length $line == 0){return}

	if(!$parsefasta && $line=~/^>([^ ]+)/){print STDERR "[WARNING] the file looks like a fasta format but --fasta is not set...\n"}

	if($parsefasta){
		if($line=~/^>([^ ]+)/){push(@header,$1)}
		else{
			if(scalar @data == scalar @header){
				$data[scalar @data -1].=$line; # append
			}else{
				push(@data,$line) # create new entry
			}
			if(length $data[scalar @data -1] > $maximalLength){$maximalLength=length $data[scalar @data -1]}
		}
	}else{
		push(@data,$line)
	}
}

unless(open(FH,'<',$query)) {
	if($query eq "-"){
		while(<STDIN>){parseLine($_)}
	}
}else{
	while(<FH>){parseLine($_)}
	close(FH);
}

if(scalar @data != scalar @header && $parsefasta){print STDERR "ERROR: -fasta is set but the number of sequences are not matching the number of header (or the header names collide)..."; die;}

for (my $i = 0; $i < scalar @data; $i++) {
	if( length $data[$i] != $maximalLength){
		print STDERR "entry number $i is of different length -> I use empty character padding...\n";
		$data[$i] .= (" "x($maximalLength - length $data[$i]));
	}
}

sub log10 {
    my $n = shift;
    return log($n)/log(10);
}

our $num_entries = scalar @data;
our $padding_len = int(log10($num_entries))+1;

if(scalar @header >0){
	cluster(\@data,\@header)	
}else{
	cluster(\@data)
}

###################################################################################

sub dist {
	# distance between the 2 input strings
	# input strings are considered to be of same length

	my @a = @{$_[0]};
	my @b = @{$_[1]};

	# count the number of entries in each input
	my $a_n=0;
	my $b_n=0;
	my @ua = @{$a[0]};
	my @ub = @{$b[0]};
	for (my $j = 0; $j < scalar @ua; $j++) {
		$a_n+=$ua[$j];
		$b_n+=$ub[$j];
	}

	if($debug){print STDERR "a_n=$a_n b_n=$b_n\n"}

	my $s = 0;
	for (my $i = 0; $i < scalar @a; $i++) {
		my @ua = @{$a[$i]};
		my @ub = @{$b[$i]};
		my $us = 0;
		for (my $j = 0; $j < scalar @ua; $j++) {
			$us += ($ua[$j]/$a_n - $ub[$j]/$b_n)**2;
		}
		$s += sqrt($us);
	}

	return ($s/scalar @a);
}

sub sumvector {
	my @a = @{$_[0]};
	my @b = @{$_[1]};
	for (my $i = 0; $i < scalar @a; $i++) { $a[$i] += $b[$i] }
	return @a;
}

sub symbol2unitvector{
	# given a symbol e.g. A and an alphabet, e.g. [A,B,C], then the corresponding
	# unit/index vector is returned -> (1,0,0) 

	my $input = $_[0];
	my @alphabet = @{$_[1]};

	my @ret = split //, 0 x (scalar @alphabet);

	for (my $i = 0; $i < scalar @alphabet; $i++) {
		if($input eq $alphabet[$i]){$ret[$i]=1;}
	}

	return @ret;
}


sub vector2symbol{
	# reverses the symbol2unitvector operation
	# chooses one maximal entry and returns the corresponding symbol

	my @input = @{$_[0]};
	my @alphabet = @{$_[1]};

	my $max_i = 0;
	my $max = $input[0];
	for (my $i = 1; $i < scalar @input; $i++) {
		if($input[$i] > $max ){$max=$input[$i]; $max_i = $i}
	}

	return $alphabet[$max_i];
}

sub padLeftZero {
  my ($num, $len) = @_;

  if(($len - length $num) < 0){
  	return $num;
  }
  return '0' x ($len - length $num) . $num;
}

sub cluster{
	# 1. argument : array of strings -> data/sequences/...
	# 2. argument : array of names of each sequence of the data (optional). If unset: 1,2,3,4,...

	my @data = @{$_[0]};
	my @header;

	if(scalar @_ == 1){ @header = map { "id".padLeftZero($_,$padding_len) } keys @data }else{ @header = @{$_[1]} }

	# 1. build alphabet from all entries
	my %char_cnt;
	foreach my $s (@data){ ++$char_cnt{$_} for split (//, $s) }
	my @alphabet = sort keys %char_cnt;

	# 2. map input data to array representation (each char/symbol is replaced with the unit vector)
	my %data_vec;
	for (my $i = 0; $i < scalar @data; $i++) {
		my @chars = split //,$data[$i];
	 	for (my $j = 0; $j < scalar @chars; $j++) {
	 		my @u = symbol2unitvector($chars[$j],\@alphabet);
	 		$data_vec{$i}[$j] = \@u;
	 	}
	}
		
	if($debug){print STDERR "".(join ",",@alphabet). "\n" ;}
	if($debug){printmat(\%data_vec);}


	my %dists;

	# greedily merge the most similar inputs
	my $it = -1;
	while(++$it <= $max_it){

		#if($debug){printmat(\%data_vec);}

		my @min_d = (-1,0,0);
		my @keys = keys %data_vec;
		for (my $i = 0; $i < scalar @keys; $i++) {
			for (my $j = $i+1; $j < scalar @keys; $j++) {

				if(!exists $dists{$keys[$i]}{$keys[$j]}){ 
					$dists{$keys[$i]}{$keys[$j]} = dist( $data_vec{$keys[$i]}, $data_vec{$keys[$j]} );
				}

				my $dist = $dists{$keys[$i]}{$keys[$j]};
				
				#if($debug){print STDERR "$i vs $j -> d=$dist\n";}

				if($min_d[0] == -1 || $dist < $min_d[0]){
					@min_d = ($dist , $keys[$i] , $keys[$j]);
				}
			}
		}

		if($debug){print STDERR "\nit=$it -> min_d=$min_d[0] < t=$threshold\n";}

		if($min_d[0] > -1 && $min_d[0] <= $threshold){
			
			# pool the corresponding entries
			my $i = $min_d[1];
			my $j = $min_d[2];
			
			if($debug){print STDERR " merge '$i' and '$j'\n";}

			for (my $k = 0; $k < scalar @{$data_vec{$i}}; $k++) {
				my @u = sumvector( $data_vec{$i}[$k] , $data_vec{$j}[$k] );
				$data_vec{$i."_#MERGED#_".$j}[$k] = \@u;
			}
			delete $data_vec{$i};
			delete $data_vec{$j};
			delete $dists{$i}{$j};
		}else{ last }
	}

	if($debug){printmat(\%data_vec);}

	# print RESULTS
	my @keys = keys %data_vec;
	my $group_i = 0;
	foreach my $i (sort @keys) {
		if($all){
			if($tofiles){

				open(my $FH_out, ">",$query_output.".group.$group_i");
				if($consensus){
					print $FH_out ">group_${group_i}_consensus ".(join "_#MERGED#_", map { $header[$_] } sort split /_#MERGED#_/,$i)."\n".(join "", map {vector2symbol($_,\@alphabet)} @{$data_vec{$i}})."\n";
					if($stat){ print "".join("|",map { my @a = @{$_}; max @a } @{$data_vec{$i}})."\n" }
				}
				foreach my $x (sort split /_#MERGED#_/,$i) {
					print $FH_out ">$header[$x] (group #$group_i)\n"."$data[$x]\n";
					if($stat){ print $FH_out "".join("|",map { my @a = @{$_}; max @a } @{$data_vec{$i}})."\n" }
				}
				close($FH_out);

			}else{

				if($consensus){
					print ">group_${group_i}_consensus ".(join "_#MERGED#_", map { $header[$_] } sort split /_#MERGED#_/,$i)."\n".(join "", map {vector2symbol($_,\@alphabet)} @{$data_vec{$i}})."\n";
					if($stat){ print "".join("|",map { my @a = @{$_}; max @a } @{$data_vec{$i}})."\n" }
				}
				foreach my $x (sort split /_#MERGED#_/,$i) {
					print ">$header[$x] (group #$group_i)\n"."$data[$x]\n";
					if($stat){ print "".join("|",map { my @a = @{$_}; max @a } @{$data_vec{$i}})."\n" }
				}
			}
			
		}else{

			if($tofiles){

				open(my $FH_out, ">",$query_output.".group.$group_i");
				if($consensus){
					print $FH_out "".($parsefasta ? ">".(join "_#MERGED#_", sort map { $header[$_] } split /_#MERGED#_/,$i)."\n" : "").(join "", map {vector2symbol($_,\@alphabet)} @{$data_vec{$i}})."\n";
					if($stat){ print "".join("|",map { my @a = @{$_}; max @a } @{$data_vec{$i}})."\n" }
				}else{
					my $representative = $i; $representative =~s/_#MERGED#_.*$//;
					print $FH_out "".($parsefasta ? ">".(join "_#MERGED#_", sort map { $header[$_] } split /_#MERGED#_/,$i)."\n" : "")."$data[$representative]\n";
					if($stat){ print "".join("|",map { my @a = @{$_}; max @a } @{$data_vec{$i}})."\n" }
				}
				close($FH_out);

			}else{

				if($consensus){
					print "".($parsefasta ? ">".(join "_#MERGED#_", sort map { $header[$_] } split /_#MERGED#_/,$i)."\n" : "").(join "", map {vector2symbol($_,\@alphabet)} @{$data_vec{$i}})."\n";
					if($stat){ print "".join("|",map { my @a = @{$_}; max @a } @{$data_vec{$i}})."\n" }
				}else{
					my $representative = $i; $representative =~s/_#MERGED#_.*$//;
					print "".($parsefasta ? ">".(join "_#MERGED#_", sort map { $header[$_] } split /_#MERGED#_/,$i)."\n" : "")."$data[$representative]\n";
					if($stat){ print "".join("|",map { my @a = @{$_}; max @a } @{$data_vec{$i}})."\n" }
				}
			}
		}
		$group_i++;
	}
	
	return %data_vec;
}

sub printmat{
	print STDERR "\n";
	my %d=%{$_[0]};
	foreach my $i (sort keys %d) {
		print STDERR "$i -> ";
		for (my $j = 0; $j < scalar @{$d{$i}}; $j++) {
			print STDERR "|".(join ",", @{$d{$i}[$j]});
		}
		print STDERR "\n";
	}
	print STDERR "\n";
}
