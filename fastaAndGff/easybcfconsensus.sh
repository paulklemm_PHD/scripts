#!/usr/bin/env bash
#pk

RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
NC='\033[0m' # No Color

version="0.0.10"; # last updated on 2021-10-04 18:57:23

export LC_ALL=C

if [ "$#" -lt 2 ]; then
    echo -e "${ORANGE}USAGE$NC : easybcfconsensus.sh REFFASTA READS"
    echo " This scripts generate a consensus.fa from READS compared to the reference REFFASTA (missing regions are filled in with with reference sequences)"
    echo " Reads are filtered with default bcftools filter [UNMAP,SECONDARY,QCFAIL,DUP]"
    echo " Input READS can be in sam or bam format, REFFASTA is the mapping target of READS (matching QNAME of the READS)"
    echo " Input is expected to be haploidic"
    echo " adapted from https://samtools.github.io/bcftools/howtos/consensus-sequence.html"
    echo " You can use murmer and compareMummerFasta.pl for further analysis"
    echo ""
    echo ""
    echo -e "${ORANGE}VERSION$NC $version"
    echo -e "${ORANGE}AUTHOR$NC Paul Klemm"
    echo -e "${ORANGE}DEPENDANCIES$NC bcftools, samtools"
    exit
fi

# generates genotype likelihoods at each genomic position with coverage
bcftools mpileup -Ou -f $1 $2 >$(basename $2).mpileup 2>$(basename $2).mpileup.err
if [ "$?" != "0" ]; then cat $(basename $2).mpileup.err; exit 1; fi

# call variances (only report differences)
bcftools call --ploidy 1 -mv -Oz -o $(basename $2).call.gz <$(basename $2).mpileup 2>$(basename $2).call.err
if [ "$?" != "0" ]; then cat $(basename $2).call.err; exit 1; fi

# index
bcftools index $(basename $2).call.gz 2>$(basename $2).call.gz.err
if [ "$?" != "0" ]; then cat $(basename $2).call.gz.err; exit 1; fi

# normalize indels
bcftools norm -f $1 $(basename $2).call.gz -Ob -o $(basename $2).call.norm.bcf 2>$(basename $2).norm.err
if [ "$?" != "0" ]; then cat $(basename $2).norm.err; exit 1; fi

# filter adjacent indels within 5bp
bcftools filter --IndelGap 5 $(basename $2).call.norm.bcf -Ob -o $(basename $2).call.norm.flt-indels.bcf 2>$(basename $2).filter.err
if [ "$?" != "0" ]; then cat $(basename $2).filter.err; exit 1; fi

# apply variants to create consensus sequence
cat $1 | bcftools consensus $(basename $2).call.gz > $(basename $2).consensus.fa 2>$(basename $2).consensus.err
if [ "$?" != "0" ]; then cat $(basename $2).consensus.err; exit 1; fi

# generates a coverage
samtools coverage $2 -m -w 100 >$(basename $2).coverage.info 2>$(basename $2).coverage.err
if [ "$?" != "0" ]; then cat $(basename $2).coverage.err; exit 1; fi
