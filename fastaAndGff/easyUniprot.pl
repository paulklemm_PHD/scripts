#!/usr/bin/perl
#pk
use strict;
use warnings;
use LWP::UserAgent;
use Getopt::Long; # for parameter specification on the command line
# -> cpan install LMP::UserAgen Mozilla::CA

my $version="0.1.18"; # last updated on 2023-07-11 12:32:09

my $columnTable='
# Names & Taxonomy
Entry: id
Entry name: entry name
Gene names: genes
Gene names (primary): genes(PREFERRED)
Gene names (synonym): genes(ALTERNATIVE)
Gene names (ordered locus): genes(OLN)
Gene names (ORF): genes(ORF)
Organism: organism
Organism ID: organism-id
Protein names: protein names
Proteomes: proteome
Taxonomic lineage: lineage(ALL)
Virus hosts: virus hosts

# Sequences
Fragment: fragment
Gene encoded by: encodedon
Alternative products: comment(ALTERNATIVE PRODUCTS)
Erroneous gene model prediction: comment(ERRONEOUS GENE MODEL PREDICTION)
Erroneous initiation: comment(ERRONEOUS INITIATION)
Erroneous termination: comment(ERRONEOUS TERMINATION)
Erroneous translation: comment(ERRONEOUS TRANSLATION)
Frameshift: comment(FRAMESHIFT)
Mass spectrometry: comment(MASS SPECTROMETRY)
Polymorphism: comment(POLYMORPHISM)
RNA editing: comment(RNA EDITING)
Sequence caution: comment(SEQUENCE CAUTION)
Length: length
Mass: mass
Sequence: sequence
Alternative sequence: feature(ALTERNATIVE SEQUENCE)
Natural variant: feature(NATURAL VARIANT)
Non-adjacent residues: feature(NON ADJACENT RESIDUES)
Non-standard residue: feature(NON STANDARD RESIDUE)
Non-terminal residue: feature(NON TERMINAL RESIDUE)
Sequence conflict: feature(SEQUENCE CONFLICT)
Sequence uncertainty: feature(SEQUENCE UNCERTAINTY)
Sequence version: version(sequence)

# Function
Absorption: comment(ABSORPTION)
Active site: feature(ACTIVE SITE)
Binding site: feature(BINDING SITE)
Catalytic activity: comment(CATALYTIC ACTIVITY)
ChEBI: chebi
ChEBI (Catalytic activity): chebi(Catalytic activity)
ChEBI (Cofactor): chebi(Cofactor)
ChEBI IDs: chebi-id
Cofactor: comment(COFACTOR)
DNA binding: feature(DNA BINDING)
EC number: ec
Enzyme regulation: comment(ENZYME REGULATION)
Function [CC]: comment(FUNCTION)
Kinetics: comment(KINETICS)
Metal binding: feature(METAL BINDING)
Nucleotide binding: feature(NP BIND)
Pathway: comment(PATHWAY)
pH dependence: comment(PH DEPENDENCE)
Redox potential: comment(REDOX POTENTIAL)
Rhea ids: rhea-id
Site: feature(SITE)
Temperature dependence: comment(TEMPERATURE DEPENDENCE)

# Miscellaneous
Annotation score: annotation score
Features: features
Caution: comment(CAUTION)
Miscellaneous [CC]: comment(MISCELLANEOUS)
Keywords: keywords
Matched text: context
Protein existence: existence
Tools: tools
Reviewed: reviewed

# Interaction
Subunit structure [CC]: comment(SUBUNIT)
Interacts with: interactor

# Expression
Developmental stage: comment(DEVELOPMENTAL STAGE)
Induction: comment(INDUCTION)
Tissue specificity: comment(TISSUE SPECIFICITY)

# Gene Ontology (GO)
Gene ontology (GO): go
Gene ontology (biological process): go(biological process)
Gene ontology (molecular function): go(molecular function)
Gene ontology (cellular component): go(cellular component)
Gene ontology IDs: go-id

# Pathology & Biotech
Allergenic properties: comment(ALLERGEN)
Biotechnological use: comment(BIOTECHNOLOGY)
Disruption phenotype: comment(DISRUPTION PHENOTYPE)
Involvement in disease: comment(DISEASE)
Pharmaceutical use: comment(PHARMACEUTICAL)
Toxic dose: comment(TOXIC DOSE)

# Subcellular location
Subcellular location [CC]: comment(SUBCELLULAR LOCATION)
Intramembrane: feature(INTRAMEMBRANE)
Topological domain: feature(TOPOLOGICAL DOMAIN)
Transmembrane: feature(TRANSMEMBRANE)

# PTM / Processsing
Post-translational modification: comment(PTM)
Chain: feature(CHAIN)
Cross-link: feature(CROSS LINK)
Disulfide bond: feature(DISULFIDE BOND)
Glycosylation: feature(GLYCOSYLATION)
Initiator methionine: feature(INITIATOR METHIONINE)
Lipidation: feature(LIPIDATION)
Modified residue: feature(MODIFIED RESIDUE)
Peptide: feature(PEPTIDE)
Propeptide: feature(PROPEPTIDE)
Signal peptide: feature(SIGNAL)
Transit peptide: feature(TRANSIT)

# Structure
3D: 3d
Beta strand: feature(BETA STRAND)
Helix: feature(HELIX)
Turn: feature(TURN)

# Publications
Mapped PubMed ID: citationmapping
PubMed ID: citation

# Date of
Date of creation: created
Date of last modification: last-modified
Date of last sequence modification: sequence-modified
Entry version: version(entry)

# Family & Domains
Domain [CC]: comment(DOMAIN)
Sequence similarities: comment(SIMILARITY)
Protein families: families
Coiled coil: feature(COILED COIL)
Compositional bias: feature(COMPOSITIONAL BIAS)
Domain [FT]: feature(DOMAIN EXTENT)
Motif: feature(MOTIF)
Region: feature(REGION)
Repeat: feature(REPEAT)
Zinc finger: feature(ZINC FINGER)

# Taxonomic lineage
Taxonomic lineage (all): lineage(all)
Taxonomic lineage (SUPERKINGDOM): lineage(SUPERKINGDOM)
Taxonomic lineage (KINGDOM): lineage(KINGDOM)
Taxonomic lineage (SUBKINGDOM): lineage(SUBKINGDOM)
Taxonomic lineage (SUPERPHYLUM): lineage(SUPERPHYLUM)
Taxonomic lineage (PHYLUM): lineage(PHYLUM)
Taxonomic lineage (SUBPHYLUM): lineage(SUBPHYLUM)
Taxonomic lineage (SUPERCLASS): lineage(SUPERCLASS)
Taxonomic lineage (CLASS): lineage(CLASS)
Taxonomic lineage (SUBCLASS): lineage(SUBCLASS)
Taxonomic lineage (INFRACLASS): lineage(INFRACLASS)
Taxonomic lineage (SUPERORDER): lineage(SUPERORDER)
Taxonomic lineage (ORDER): lineage(ORDER)
Taxonomic lineage (SUBORDER): lineage(SUBORDER)
Taxonomic lineage (INFRAORDER): lineage(INFRAORDER)
Taxonomic lineage (PARVORDER): lineage(PARVORDER)
Taxonomic lineage (SUPERFAMILY): lineage(SUPERFAMILY)
Taxonomic lineage (FAMILY): lineage(FAMILY)
Taxonomic lineage (SUBFAMILY): lineage(SUBFAMILY)
Taxonomic lineage (TRIBE): lineage(TRIBE)
Taxonomic lineage (SUBTRIBE): lineage(SUBTRIBE)
Taxonomic lineage (GENUS): lineage(GENUS)
Taxonomic lineage (SUBGENUS): lineage(SUBGENUS)
Taxonomic lineage (SPECIES GROUP): lineage(SPECIES GROUP)
Taxonomic lineage (SPECIES SUBGROUP): lineage(SPECIES SUBGROUP)
Taxonomic lineage (SPECIES): lineage(SPECIES)
Taxonomic lineage (SUBSPECIES): lineage(SUBSPECIES)
Taxonomic lineage (VARIETAS): lineage(VARIETAS)
Taxonomic lineage (FORMA): lineage(FORMA)

# Taxonomic identifier
Taxonomic identifier (all): lineage-id(all)
Taxonomic identifier (SUPERKINGDOM): lineage-id(SUPERKINGDOM)
Taxonomic identifier (KINGDOM): lineage-id(KINGDOM)
Taxonomic identifier (SUBKINGDOM): lineage-id(SUBKINGDOM)
Taxonomic identifier (SUPERPHYLUM): lineage-id(SUPERPHYLUM)
Taxonomic identifier (PHYLUM): lineage-id(PHYLUM)
Taxonomic identifier (SUBPHYLUM): lineage-id(SUBPHYLUM)
Taxonomic identifier (SUPERCLASS): lineage-id(SUPERCLASS)
Taxonomic identifier (CLASS): lineage-id(CLASS)
Taxonomic identifier (SUBCLASS): lineage-id(SUBCLASS)
Taxonomic identifier (INFRACLASS): lineage-id(INFRACLASS)
Taxonomic identifier (SUPERORDER): lineage-id(SUPERORDER)
Taxonomic identifier (ORDER): lineage-id(ORDER)
Taxonomic identifier (SUBORDER): lineage-id(SUBORDER)
Taxonomic identifier (INFRAORDER): lineage-id(INFRAORDER)
Taxonomic identifier (PARVORDER): lineage-id(PARVORDER)
Taxonomic identifier (SUPERFAMILY): lineage-id(SUPERFAMILY)
Taxonomic identifier (FAMILY): lineage-id(FAMILY)
Taxonomic identifier (SUBFAMILY): lineage-id(SUBFAMILY)
Taxonomic identifier (TRIBE): lineage-id(TRIBE)
Taxonomic identifier (SUBTRIBE): lineage-id(SUBTRIBE)
Taxonomic identifier (GENUS): lineage-id(GENUS)
Taxonomic identifier (SUBGENUS): lineage-id(SUBGENUS)
Taxonomic identifier (SPECIES GROUP): lineage-id(SPECIES GROUP)
Taxonomic identifier (SPECIES SUBGROUP): lineage-id(SPECIES SUBGROUP)
Taxonomic identifier (SPECIES): lineage-id(SPECIES)
Taxonomic identifier (SUBSPECIES): lineage-id(SUBSPECIES)
Taxonomic identifier (VARIETAS): lineage-id(VARIETAS)
Taxonomic identifier (FORMA): lineage-id(FORMA)

# Cross-references
db_abbrev: database(db_abbrev)
e.g. EMBL: database(EMBL)';

my $conversionsTable='# Category: UniProt
     UniProtKB AC/ID      │         ACC+ID          │    from
       UniProtKB AC       │           ACC           │    both
 UniProtKB/SwissProt ACC  │        SWISSPROT        │     to 
       UniProtKB ID       │           ID            │    both
         UniParc          │          UPARC          │    both
         UniRef50         │          NF50           │    both
         UniRef90         │          NF90           │    both
        UniRef100         │          NF100          │    both
        Gene name         │        GENENAME         │    both
          64CRC           │          64CRC          │    both

# Category: Sequence databases
    EMBL/GenBank/DDBJ     │         EMBL_ID         │    both
  EMBL/GenBank/DDBJ CDS   │          EMBL           │    both
   Entrez Gene (GeneID)   │     P_ENTREZGENEID      │    both
        GI number         │          P_GI           │    both
           PIR            │           PIR           │    both
    RefSeq Nucleotide     │      REFSEQ_NT_ID       │    both
      RefSeq Protein      │       P_REFSEQ_AC       │    both

# Category: 3D structure databases
           PDB            │         PDB_ID          │    both

# Category: Protein-protein interaction databases
         BioGRID          │       BIOGRID_ID        │    both
      ComplexPortal       │    COMPLEXPORTAL_ID     │    both
           DIP            │         DIP_ID          │    both
          STRING          │        STRING_ID        │    both

# Category: Chemistry databases
          ChEMBL          │        CHEMBL_ID        │    both
         DrugBank         │       DRUGBANK_ID       │    both
   GuidetoPHARMACOLOGY    │ GUIDETOPHARMACOLOGY_ID  │    both
       SwissLipids        │     SWISSLIPIDS_ID      │    both

# Category: Protein family/group databases
        Allergome         │      ALLERGOME_ID       │    both
           CLAE           │         CLAE_ID         │    both
          ESTHER          │        ESTHER_ID        │    both
          MEROPS          │        MEROPS_ID        │    both
        PeroxiBase        │      PEROXIBASE_ID      │    both
          REBASE          │        REBASE_ID        │    both
           TCDB           │         TCDB_ID         │    both

# Category: PTM databases
        GlyConnect        │      GLYCONNECT_ID      │    both

# Category: Genetic variation databases
         BioMuta          │       BIOMUTA_ID        │    both
           DMDM           │         DMDM_ID         │    both

# Category: 2D gel databases
       World-2DPAGE       │     WORLD_2DPAGE_ID     │    both

# Category: Proteomic databases
          CPTAC           │        CPTAC_ID         │    both
       ProteomicsDB       │     PROTEOMICSDB_ID     │    both

# Category: Protocols and materials databases
          DNASU           │        DNASU_ID         │    both

# Category: Genome annotation databases
         Ensembl          │       ENSEMBL_ID        │    both
     Ensembl Protein      │     ENSEMBL_PRO_ID      │    both
    Ensembl Transcript    │     ENSEMBL_TRS_ID      │    both
     Ensembl Genomes      │    ENSEMBLGENOME_ID     │    both
 Ensembl Genomes Protein  │  ENSEMBLGENOME_PRO_ID   │    both
Ensembl Genomes Transcript│  ENSEMBLGENOME_TRS_ID   │    both
   GeneID (Entrez Gene)   │     P_ENTREZGENEID      │    both
           KEGG           │         KEGG_ID         │    both
          PATRIC          │        PATRIC_ID        │    both
           UCSC           │         UCSC_ID         │    both
        WBParaSite        │      WBPARASITE_ID      │    both
WBParaSite Transcript/Protein│ WBPARASITE_TRS_PRO_ID│    both   

# Category: Organism-specific databases
      ArachnoServer       │    ARACHNOSERVER_ID     │    both
         Araport          │       ARAPORT_ID        │    both
           CCDS           │         CCDS_ID         │    both
           CGD            │           CGD           │    both
        ConoServer        │      CONOSERVER_ID      │    both
        dictyBase         │      DICTYBASE_ID       │    both
         EchoBASE         │       ECHOBASE_ID       │    both
         euHCVdb          │       EUHCVDB_ID        │    both
         FlyBase          │       FLYBASE_ID        │    both
        GeneCards         │      GENECARDS_ID       │    both
       GeneReviews        │     GENEREVIEWS_ID      │    both
           HGNC           │         HGNC_ID         │    both
        LegioList         │      LEGIOLIST_ID       │    both
         Leproma          │       LEPROMA_ID        │    both
         MaizeGDB         │       MAIZEGDB_ID       │    both
           MGI            │         MGI_ID          │    both
           MIM            │         MIM_ID          │    both
         neXtProt         │       NEXTPROT_ID       │    both
         Orphanet         │       ORPHANET_ID       │    both
         PharmGKB         │       PHARMGKB_ID       │    both
         PomBase          │       POMBASE_ID        │    both
        PseudoCAP         │      PSEUDOCAP_ID       │    both
           RGD            │         RGD_ID          │    both
           SGD            │         SGD_ID          │    both
       TubercuList        │     TUBERCULIST_ID      │    both
        VEuPathDB         │      VEUPATHDB_ID       │    both
           VGNC           │         VGNC_ID         │    both
         WormBase         │       WORMBASE_ID       │    both
     WormBase Protein     │     WORMBASE_PRO_ID     │    both
   WormBase Transcript    │     WORMBASE_TRS_ID     │    both
         Xenbase          │       XENBASE_ID        │    both
           ZFIN           │         ZFIN_ID         │    both

# Category: Phylogenomic databases
          eggNOG          │        EGGNOG_ID        │    both
         GeneTree         │       GENETREE_ID       │    both
         HOGENOM          │       HOGENOM_ID        │    both
           OMA            │         OMA_ID          │    both
         OrthoDB          │       ORTHODB_ID        │    both
         TreeFam          │       TREEFAM_ID        │    both

# Category: Enzyme and pathway databases
          BioCyc          │        BIOCYC_ID        │    both
      PlantReactome       │    PLANT_REACTOME_ID    │    both
         Reactome         │       REACTOME_ID       │    both
        UniPathway        │      UNIPATHWAY_ID      │    both

# Category: Gene expression databases
         CollecTF         │       COLLECTF_ID       │    both

# Category: Family and domain databases
         DisProt          │       DISPROT_ID        │    both
          IDEAL           │        IDEAL_ID         │    both

# Category: Miscellaneous databases
         ChiTaRS          │       CHITARS_ID        │    both
         GeneWiki         │       GENEWIKI_ID       │    both
        GenomeRNAi        │      GENOMERNAI_ID      │    both
         PHI-base         │       PHI_BASE_ID       │    both
';

my $format = 'tab';
my $columns;
my $include = "no";
my $compress = "no";
my $limit;
my $offset;
my $from;
my $to;
my $helpMore="";
my $debug;
my $help;

my $usage = <<ENDUSAGE;

!!! DPRECATED: API did change, nothing works anymore, ... !!!
!!! DPRECATED: API did change, nothing works anymore, ... !!!

easyUniprot.pl        simple interface to uniprot API
 
SYNOPSIS

easyUniprot.pl options TOOL QUERY

  TOOL : either 'uniprot' for general queries (search all proteins of XYZ) or 'uploadlists' to convert ID or proteomes
  
  if TOOL=uploadlists :
    --from, --to the ID to convert e.g. P_REFSEQ_AC and ACC (see --table=ID) 
  if TOOL=uniprot:
    e.g. query='organism:9601+AND+antigen+OR+length:[100 TO *]'
    here you can use format="fasta"
    taxid can be found here : https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi
  if TOOL=proteomes:
    e.g. query='organism:1423'
    this returns a table of proteomes 

  to download a proteome use 
    tool='uniprot', 
    format='fasta',
    query='proteome:UP000295488' <- this is unqiue 
    or query='organism:224308' <- this downloads all sub-strains too
    ( format fasta with --limit reduces the number of protein sequences and not the number of proteomes )

  QUERY : input query (or file), no STDIN.

  --format       output formats are html, tab, xls, fasta, gff, txt, xml, rdf, list, rss
  --columns      comma separated list of columns to return (default:a simple from to list) for more details see --table=column or --table=columnfull
  --colcommon    short hand for --columns='id,entry name,sequence,reviewed,protein names,genes,organism,organism-id,length,database(RefSeq),go-id,go(biological process),go(molecular function)'
  --include      If set: Include isoform sequences when the format parameter is set to fasta. Include description of referenced data when the format parameter is set to rdf. This parameter is ignored for all other values of the format parameter.
  --compress,-g  If set: A gzipped archive is returned.
  --limit        Maximum number of results to retrieve.
  --offset       Offset of the first result, typically used together with the limit parameter.
  --table        displays various tables to use 
    'ID': identifier used for --from and --to (full table: IDfull)
    'column': identifier used for --column (full table: columnfull)
EXAMPLES
  \$ easyUniprot.pl 'uniprot' 'WP_003246159.1+AND+organism:224308' --colcommon
  find everything about WP_003246159.1 in B.sub. 168 (and all sub-strains)

  \$ easyUniprot.pl --from="P_REFSEQ_AC" --to="ACC" 'uploadlists' DEP_result_table.ids.csv
  converts ncbi REFSEQ to uniprot ACCESSION (2 columns table: from to)

  \$ easyUniprot.pl --from="P_REFSEQ_AC" --to="ACC" --colcommon 'uploadlists' DEP_result_table.ids.csv
  finds all REFSEQ entries of the input file and outputs a list with all sorts of information + sequences
  - cannot be combined with complex queries like ...+AND+organism:...
  - the from/query column is the last one (yourlist:...)

  \$ easyUniprot.pl 'proteomes' 'Bacillus subtilis 168'
  get a list of all B.subt. 168 strains -> e.g. UP000001570

  \$ easyUniprot.pl 'uniprot' 'proteome:UP000001570' --limit=10
  find the first 10 proteins of this strain

  \$ easyUniprot.pl 'uniprot' 'proteome:UP000001570' --format="fasta" -g >UP000001570.gz
  download the full proteome of this strain
  - use the uniprot UP-ID instead of the NCBI Organism-ID (organism:Organism ID) since it is unique

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES perl : Getopt::Long, LMP::UserAgen Mozilla::CA

!!! DPRECATED: API did change, nothing works anymore, ... !!!
!!! DPRECATED: API did change, nothing works anymore, ... !!!

ENDUSAGE

GetOptions(
  'from=s' => \$from,
  'to=s' => \$to,
  'format=s' => \$format,
  'columns=s' => \$columns,
  'include!' =>sub { $include="yes" },
  'compress!' =>sub { $compress="yes" },
  'g!' =>sub { $compress="yes" },
  'limit=i' => \$limit,
  'offset=i' => \$offset,
  'x!' => \$debug,
  'table=s'=>\$helpMore,
  'help!'=>\$help,'h!'=>\$help,
  'colcommon!'=>sub { $columns='id,entry name,sequence,reviewed,protein names,genes,organism,organism-id,length,database(RefSeq),go-id,go(biological process),go(molecular function)' },
  'version!'=>sub { print "$0\tv$version\n";exit 0; },
  'v!'=>sub { print "$0\tv$version\n";exit 0; }
);

my $tool;
my $query;
if(scalar @ARGV == 2){ $tool = $ARGV[0]; $query = $ARGV[1]; }

if($debug){
  $tool="proteomes";
  $query='organism:224308';
}

if ( $helpMore eq "columnfull" ){ print $columnTable; exit(0); }
if ( $helpMore eq "column" ){ my @a=split("\n", $columnTable); print join("\n",@a[0..29])."\n" ; exit(0); }
if ( $helpMore eq "IDfull" ){ print $conversionsTable; exit(0); }
if ( $helpMore eq "ID" ){ my @a=split("\n", $conversionsTable); print join("\n",@a[0..40])."\n" ; exit(0); }
if ( $help ){
    print $usage;
    exit(0);
}
if(!defined $query){print STDERR "ERROR: no query defined"; exit 1;}
if(!defined $tool || ($tool ne "uniprot" && $tool ne "uploadlists" && $tool ne "proteomes") ){print STDERR "ERROR: tool needs to be either uniprot, proteomes or uploadlists"; exit 1;}

my $base = 'https://www.uniprot.org';

my $params = {
  include => $include,
  compress => $compress,
  format => $format
};
if($tool eq "uploadlists"){ $params->{'from'} = $from; $params->{'to'} = $to }

if(-e $query && $tool ne "uploadlists"){ print STDERR "ERROR: input files are only supported by tool=uploadlists\n";exit 1;}
if(-e $query){ $params->{'file'} = [$query]; }else{ $params->{'query'} = $query; }

if(defined $offset){ $params->{'offset'} = $offset; }
if(defined $limit){ $params->{'limit'} = $limit; }
if(defined $columns){ $params->{"columns"}=$columns; }

my $contact = ''; # Please set a contact email address here to help us debug in case of problems (see https://www.uniprot.org/help/privacy).
my $agent = LWP::UserAgent->new(agent => "libwww-perl $contact");
push @{$agent->requests_redirectable}, 'POST';

my $response = $agent->post(
  "$base/$tool/", 
  $params, 
  'Content_Type' => 'form-data'
);

while (my $wait = $response->header('Retry-After')) {
  print STDERR "Waiting ($wait)...n";
  sleep $wait;
  $response = $agent->get($response->base);
}

if($response->is_success){
  print $response->content
}else{
  my $err=$response->status_line;
  print STDERR 'Failed, got ' . $err .
    ' for ' . $response->request->uri . "\n";

  if( $err =~ m/Can't verify SSL peers without knowing which Certificate Authorities to trust/ig ){
    print STDERR "you need to run: cpan install LMP::UserAgen Mozilla::CA\n";
  }
  die;
}
 
