#!/usr/bin/perl
#pk

use warnings "all";

use Getopt::Long; # for parameter specification on the command line
use File::Basename; # basename()
$|=1; #autoflush

my $version="0.0.10"; # last updated on 2022-04-11 15:03:00

my $me = basename($0);

my $help;
my $out;
my $debug;
my $verbose;
my $del=";";

my $usage = "$me        reduces a gff

SYNOPSIS

$me (options) INPUT.gff

	-d delimiter for aggregating source, feature, attribute [default:$del]
	
DESCRIPTION
	identical entries with same seqname, start, end, strand are combined into one entry with aggregated informations

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES Perl Getopt::Long, File::Basename
";


GetOptions('help!' =>\$help,'h!' =>\$help,
	'd=s' =>\$del,
	'version!' => sub { print "$0\tv$version\n";exit 0; },
	'v!' => sub { print "$0\tv$version\n";exit 0; },
	'verbose' => \$verbose,
	'x' => \$debug
);

if ($help){
    print $usage;
    exit(1);
}
my $header="";
my %gff;
while(<>){
	$_=~s/[\r\n]+$//g;
	if(/^#/ || $_ eq ""){$header.="$_\n";next}
	my @a = split("\t",$_);
	if(scalar @a < 9){next}
	my $id = $a[0]."#".$a[3]."#".$a[4]."#".$a[6];
	if(!exists $gff{$id}){
		$gff{$id} = \@a;
	}else{
		foreach my $i (1,2,8) {
			if($a[$i] ne "" && index($gff{$id}->[$i],$a[$i]) == -1 ){ 
				$gff{$id}->[$i] .= $del.$a[$i];
			}
		}
	}
}

print $header;
foreach my $id (sort {my @arra = split("#",$a);my @arrb = split("#",$b); return(($arra[0] cmp $arrb[0]) || ($arra[2] <=> $arrb[2]))} keys %gff) {
	print join("\t",@{$gff{$id}})."\n";
}
