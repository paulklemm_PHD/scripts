#!/usr/bin/env perl
#pk 

use strict;
use warnings "all";

use Getopt::Long; 

my $version="0.0.21"; # last updated on 2023-07-11 12:32:09


my $usage = <<ENDUSAGE;
	print "USAGE: splitfasta.pl <FASTAFILE>
Returns for each '>' entry of the fasta a single file inside a created *_fastaFiles/ directory (* = input fasta name)

OPTIONS
	-brackets split by name within the square brackets at the end of the header. 
		>AAM01497_1 Glutamate-1-semialdehyde aminotransferase [Methanopyrus kandleri AV19]
		results in the file: Methanopyrus_kandleri_AV19.fasta

	-OS -OX split by name given as OS/OX end of the header. 
		>AAM01497_1 Glutamate-1-semialdehyde aminotransferase OS=Methanopyrus kandleri AV19 OX=...
		results in the file: Methanopyrus_kandleri_AV19.fasta

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES -
ENDUSAGE

my $help;
my $brackets;
my $OS;
my $OX;
my $bin=0;

GetOptions('help!'=>\$help,'h!'=>\$help,
	'brackets!'=>\$brackets,
	'OS!'=>\$OS,
	'OX!'=>\$OX,
	'bin=i'=>\$bin,
	'version!'=>sub { print "$0\tv$version\n";exit 0; },
);

if ( $help || scalar @ARGV != 1){
    print $usage;
    exit(1);
}

my $gen="";
my $genname="";
my $tic=0;
my $genname_original;

my %data;

my $basefilename=$ARGV[0];
$basefilename=~s/^.*\/([^\/]+)$/$1/g;
$basefilename=~s/[^a-zA-Z0-9_.]/_/g;

#if(-d "${basefilename}_fastaFiles"){
#	print STDERR "ERROR : '${basefilename}_fastaFiles' directory exists \n";
#	exit 1;
#}

system("mkdir $basefilename"."_fastaFiles 2>/dev/null");
my $filename="";
while(<>){
	chomp;
	if(substr($_,0,1)eq">"){
		$genname=$_;
		$filename=$genname;
		$filename=~s/[>]//g;
		if($brackets){ $filename=~s/.*\[([^\[\]]+)\].*/$1/g; }
		elsif($OS){ $filename=~s/^.*OS=(.+) ($|OX=.*$)/$1/g; if(length($filename)>30){$filename=substr($filename,0,30)}; $filename=~s/[- _]+$//g; }
		elsif($OX){ $filename=~s/^.*OX=([0-9]+).*$/$1/g; if(length($filename)>30){$filename=substr($filename,0,30)}; $filename=~s/[- _]+$//g; }
		$filename=~s/[^a-zA-Z0-9_.]/_/g;
		$gen=~s/[^A-Za-z().\n]//g;
		
		$filename="$basefilename"."_fastaFiles/".$filename.".fasta";

		if(! exists $data{$filename}){ $data{$filename}="" }
		$data{$filename} .= "\n".$genname."\n";
	}else{ $data{$filename}.=$_; }
}

if($bin==0){
	foreach my $x (sort keys %data) {
		open(my $FH,">",$x) or die "$? $!\n";
		print $FH $data{$x}."\n";
		close $FH;
	}	
}else{
	my %fn;
	foreach my $x (sort keys %data) {
		my $len = () = $data{$x} =~ /[^\n]/g;
		$len -= $len % $bin;
		my $FH;
		if(!exists $fn{"${basefilename}_fastaFiles/$basefilename.$len"}){open($FH,">${basefilename}_fastaFiles/$basefilename.$len") or die "$? $!\n";$fn{"${basefilename}_fastaFiles/$basefilename.$len"}=$FH}
		else{$FH=$fn{"${basefilename}_fastaFiles/$basefilename.$len"};}
		print $FH $data{$x}."\n";
	}
	foreach my $FH (values %fn){
		close $FH;
	}
}
