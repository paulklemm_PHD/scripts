#!/usr/bin/python3
#pk
import argparse
import os
import requests
import re
from Bio.PDB import MMCIFParser, PDBIO, Select
from Bio.PDB.MMCIF2Dict import MMCIF2Dict

version="0.0.21" # last updated on 2024-11-13 13:19:56

PDB_URL = "https://files.rcsb.org/download/"
ALPHAFOLD_URL = "https://alphafold.ebi.ac.uk/files/"

def add_remark(pdb_file, remark):
    n=80-11 # 11 = "REMARK   2 ".length
    cur_str = remark.upper().rstrip()
    chunks = [ cur_str[i:i+n] for i in range(0,len(cur_str),n) ]
    chunks = [ f"REMARK   2 {chunks[i]}".ljust(80," ")+"\n" for i in range(0,len(chunks)) ]

    with open(pdb_file, "r") as file:
        pdb_content = file.readlines()

    i=-1
    for j in range(len(pdb_content)):
        if "REMARK " in pdb_content[j]:
            i=j
    if i == -1:
        i=1
    pdb_content.insert(i, "\n".join(chunks))
    with open(pdb_file, "w") as file:
        file.writelines(pdb_content)

def replacer(s, newstring, index, nofail=False):
    if not nofail and index not in range(len(s)):
        raise ValueError("index outside given string")
    if index < 0:  # add it to the beginning
        return newstring + s
    if index > len(s):  # add it to the end
        return s + newstring
    return s[:index] + newstring + s[index + 1:]

def copy_meta(pdb_source_file, pdb_file, select_chain=None, new_chain_name=None):
    with open(pdb_source_file, "r") as file:
        pdb_source_content = file.readlines()

    if select_chain is not None:
        # trim the meta data by selected chain id (and replace if there was a renaming), usually i rename them simply "A" if only one chain is extracted ...
        pdb_source_content_subset=[]
        prefix_indices = {
            "SHEET": 21, # this is the position where the chain id is stored (fixed with in PDB format jaja)
            "HELIX": 19,
            "SSBOND": 15,
            "CISPEP": 15,
            "LINK": 21,
            "SITE": 22,
            "HET ": 12,
            "MODRES": 16,
            "SEQRES": 11,
            "SEQADV": 16,
            "DBREF": 12,
        }
        for line in pdb_source_content:
            is_good=True
            for prefix, index in prefix_indices.items():
                if line.startswith(prefix):
                    if line[index] != f"{select_chain}":
                        is_good=False
                        continue
                    if new_chain_name is not None:
                        line = replacer(line, new_chain_name, index)
            if is_good:
                pdb_source_content_subset += [line]
        pdb_source_content = pdb_source_content_subset

    last_i=0
    for i in range(len(pdb_source_content)):
        if re.search("^ATOM",pdb_source_content[i]):
            break
        last_i=i

    pdb_source_content = pdb_source_content[0:last_i]

    with open(pdb_file, "r") as file:
        pdb_content = file.readlines()
    pdb_content = pdb_source_content+pdb_content

    with open(pdb_file, "w") as file:
        file.writelines(pdb_content)

def add_meta_information(pdb_file, cif_dict, select_chain=None, new_chain_name=None):
    metadata = {
        "HEADER": f"{cif_dict.get('_struct_keywords.pdbx_keywords', 'Unknown')[0]} {cif_dict.get('_entry.id', '')[0]}{f' CHAIN:{select_chain}' if select_chain is not None else ''}\n",
        "TITLE": f"{cif_dict.get('_struct.title', 'Unknown')[0]}\n",
        "COMPND": f"MOL_ID: 1;\nCOMPND   2 MOLECULE: {cif_dict.get('_entity.pdbx_description', ['Unknown'])[0]}\n",
        "SOURCE": f"ORGANISM_SCIENTIFIC: {cif_dict.get('_entity_src_gen.pdbx_gene_src_scientific_name', ['Unknown'])[0]}\n",
        "KEYWDS": f"{', '.join(cif_dict.get('_struct_keywords.pdbx_keywords', 'Unknown'))}\n",
        "EXPDTA": f"{cif_dict.get('_exptl.method', ['Unknown'])[0]}\n",
        "AUTHOR": f"{', '.join(cif_dict.get('_audit_author.name', ['Unknown']))}\n",
        "REVDAT": f"{cif_dict.get('_pdbx_database_status.recvd_initial_deposition_date', 'Unknown')[0]}\n",
        "JRNL": f"{cif_dict.get('_citation.title', 'Unknown')[0]} {', '.join(cif_dict.get('_citation_author.name', 'Unknown'))} {cif_dict.get('_citation.journal_abbrev', 'Unknown')[0]}\n",
    }

    with open(pdb_file, "r") as file:
        pdb_content = file.readlines()

    meta_content = []
    for key in metadata:
        n=70 # 70 is without the key field wich is 10 chars !!
        for cur_str in metadata[key].upper().rstrip().split("\n"):
            chunks = [ cur_str[i:i+n] for i in range(0,len(cur_str),n) ]
            chunks = [ f"{key}{f'{i+1}'.rjust(4,' ')} {chunks[i]}\n".ljust(80," ")+"\n" if i>0 else f"{key}    {chunks[i]}\n" for i in range(0,len(chunks)) ]
            meta_content += chunks

    if '_struct_sheet.id' in cif_dict:
        sheet_ids = cif_dict['_struct_sheet.id']
        sheet_beg_label_asym_id = cif_dict['_struct_sheet_range.beg_label_asym_id']
        sheet_beg_label_seq_id = cif_dict['_struct_sheet_range.beg_label_seq_id']
        sheet_end_label_asym_id = cif_dict['_struct_sheet_range.end_label_asym_id']
        sheet_end_label_seq_id = cif_dict['_struct_sheet_range.end_label_seq_id']
        sheet_num_strands = cif_dict['_struct_sheet.number_strands']
        beg_label_comp_ids = cif_dict['_struct_sheet_range.beg_label_comp_id'] 
        end_label_comp_ids = cif_dict['_struct_sheet_range.end_label_comp_id'] 

        for i, sheet_id in enumerate(sheet_ids):
            start_chain_id = sheet_beg_label_asym_id[i]
            if select_chain is not None:
                if start_chain_id != select_chain:
                    continue
                start_chain_id = new_chain_name if new_chain_name is not None else start_chain_id
            start_res_seq = int(sheet_beg_label_seq_id[i])
            end_chain_id = sheet_end_label_asym_id[i]
            start_aa=beg_label_comp_ids[i]
            end_aa=end_label_comp_ids[i]
            if select_chain is not None:
                if end_chain_id != select_chain:
                    continue
                end_chain_id = new_chain_name if new_chain_name is not None else end_chain_id
            end_res_seq = int(sheet_end_label_seq_id[i])
            num_strands = int(sheet_num_strands[i])
            meta_content+=[f"SHEET {i+1:4d} {sheet_id:>3}{num_strands:2d} {start_aa:3s} {start_chain_id:1s}{start_res_seq:4d}  {end_aa:3s} {end_chain_id:1s}{end_res_seq:4d}  0\n"]
    else:
        print(f"missing SHEET table for {pdb_file}")

    if '_struct_conf.conf_type_id' in cif_dict:
        helix_ids = cif_dict['_struct_conf.conf_type_id']
        helix_pdb_ids = cif_dict['_struct_conf.pdbx_PDB_helix_id']
        beg_label_comp_ids = cif_dict['_struct_conf.beg_label_comp_id']
        end_label_comp_ids = cif_dict['_struct_conf.end_label_comp_id'] 
        helix_beg_label_asym_id = cif_dict['_struct_conf.beg_label_asym_id']
        helix_beg_label_seq_id = cif_dict['_struct_conf.beg_label_seq_id']
        helix_end_label_asym_id = cif_dict['_struct_conf.end_label_asym_id']
        helix_end_label_seq_id = cif_dict['_struct_conf.end_label_seq_id']
        helix_details = cif_dict['_struct_conf.details']
        pdbx_PDB_helix_classes = cif_dict['_struct_conf.pdbx_PDB_helix_class']
        helix_index = 1  # Initialize helix index
        for i, helix_id in enumerate(helix_ids):
            if 'HELX_P' in helix_id:  # Filtering only HELIX records
                helix_pdb_id=helix_pdb_ids[i]
                beg_label_comp_id=beg_label_comp_ids[i]
                end_label_comp_id=end_label_comp_ids[i]
                start_chain_id = helix_beg_label_asym_id[i]
                if select_chain is not None:
                    if start_chain_id != select_chain:
                        continue
                    start_chain_id = new_chain_name if new_chain_name is not None else start_chain_id
                start_res_seq = int(helix_beg_label_seq_id[i])
                end_chain_id = helix_end_label_asym_id[i]
                if select_chain is not None:
                    if end_chain_id != select_chain:
                        continue
                    end_chain_id = new_chain_name if new_chain_name is not None else end_chain_id
                end_res_seq = int(helix_end_label_seq_id[i])
                helix_class = pdbx_PDB_helix_classes[i]  # Default class for alpha helices (can be adjusted based on your needs)
                helix_length = end_res_seq - start_res_seq + 1
                
                meta_content+=[f"HELIX  {helix_index:3d} {helix_pdb_id:3s} {beg_label_comp_id:3s} {start_chain_id:1s} {start_res_seq:4d}  {end_label_comp_id:3s} {end_chain_id:1s} {end_res_seq:4d}  {helix_class:4s} {helix_length:32d}\n"] #helix_details[i]
                helix_index += 1
    else:
        print(f"missing HELIX table for {pdb_file}")

    meta_content=[ x.rstrip().ljust(80," ")+"\n" for x in meta_content ]

    pdb_content = meta_content + pdb_content

    with open(pdb_file, "w") as file:
        file.writelines(pdb_content)

def get_chain_per_entity(cif_dict):
    # skip e.g. oligosaccharides or other molecules that are not the typical aminoacid (stored in _atom_site table)
    chain2type = {}
    group_PDB = cif_dict['_atom_site.group_PDB']
    label_asym_id = cif_dict['_atom_site.label_asym_id'] # assigned by PDB
    auth_asym_id = cif_dict['_atom_site.auth_asym_id'] # author picked chain id

    auth_asym_id2label_asym_id={}
    
    for g, chain_pdb, chain_auth in zip(group_PDB, label_asym_id, auth_asym_id):
        if chain_pdb in chain2type: # very important, there can be multiple chains with same names => only use the first occurence...
            continue
        chain2type[chain_pdb]=g
        if chain_auth in auth_asym_id2label_asym_id: # very important, there can be multiple chains with same names => only use the first occurence...
            continue
        auth_asym_id2label_asym_id[chain_auth]=chain_pdb

    # _struct_asym stores the table mapping the entity id to the chain id
    asym_ids = cif_dict['_struct_asym.id']
    entity_ids = cif_dict['_struct_asym.entity_id']
    entity2chain = {}
    chain2entity = {}

    for chain, entity_id in zip(asym_ids, entity_ids):
        if chain2type[chain] != "ATOM":
            continue
        if entity_id not in entity2chain:
            entity2chain[entity_id] = []
        entity2chain[entity_id] += [chain]
        chain2entity[chain]=entity_id

    return entity2chain, chain2entity, auth_asym_id2label_asym_id

def cif_to_pdb(cif_file, pdb_file, pdb_source_file=None, select_chain=None, keep=False):
   
    # from Bio.PDB.DSSP import DSSP, dssp_dict_from_pdb_file

    class ChainIDSelect(Select):
        def __init__(self, chain_id):
            self.chain_id = chain_id
        def accept_chain(self, chain):
            return chain.id == self.chain_id
        def accept_residue(self, residue): 
            return 1 if residue.id[0] == " " else 0

    parser = MMCIFParser()
    io = PDBIO()
    structure = parser.get_structure("structure", cif_file)
    cif_dict = MMCIF2Dict(cif_file)

    entity2chain, chain2entity, auth_asym_id2label_asym_id = get_chain_per_entity(cif_dict)

    chain_ori = select_chain
    if select_chain is not None and select_chain not in chain2entity:
        if select_chain in auth_asym_id2label_asym_id and auth_asym_id2label_asym_id[select_chain] in chain2entity:
            print(f"Found chain in auth_asym_id instead (author picked chain names) of label_asym_id (pdb assigned chain names), I will correct {select_chain} -> {auth_asym_id2label_asym_id[select_chain]}")
            select_chain = auth_asym_id2label_asym_id[select_chain]
        else:
            print(f"Chain {select_chain} not found in CIF {cif_file} ...")
            if not keep:
                if os.path.exists(cif_file):
                    os.remove(cif_file)
                if os.path.exists(pdb_file):
                    os.remove(pdb_file)
            exit(1)

    # replace any entry HETATM MSE with ATOM MET as done by PyMol per default (otherwise those residues are missing)
    for model in structure:
        for chain in model:
            for residue in chain:
                if residue.id[0] == 'H_MSE':  # HETATM MSE
                    residue.id = (' ', residue.id[1], residue.id[2])
                    residue.resname = 'MET'
                    for atom in residue:
                        atom.fullname = atom.fullname.replace('SE', 'SD')
                        if atom.element == 'SE':
                            atom.element = 'S'

    if select_chain is not None:
        # trim the chain2entity to only contain the selected chain
        select_entity = chain2entity[ select_chain ]
        entity2chain = { x:[ select_chain ] for x in entity2chain if x == select_entity }

    for entity_id in entity2chain:
        chain_id = entity2chain[entity_id][0] # this is select_chain if it is defined otherwise take first one
        print(re.sub(".pdb","",pdb_file))
        chain_pdb_file = f"{re.sub(".pdb","",pdb_file)}_entity_{entity_id}_chain_{chain_id}{f'_is_{chain_ori}' if chain_ori is not None and chain_ori != chain_id else ''}.pdb"
        new_structure = structure.copy()
        for model in new_structure:
            for chain in model:
                cid=chain.id
                if cid in auth_asym_id2label_asym_id:
                    cid = auth_asym_id2label_asym_id[cid]
                if cid != chain_id:
                    chain.id = f"unused_{chain}"
        for model in new_structure:
            for chain in model:
                cid = chain.id
                if cid in auth_asym_id2label_asym_id:
                    cid = auth_asym_id2label_asym_id[cid]
                if cid == chain_id:
                    chain.id = f"A"
        io.set_structure(new_structure)
        io.save(chain_pdb_file, select=ChainIDSelect("A"))
        if pdb_source_file is not None:
            copy_meta(pdb_source_file, chain_pdb_file, chain_id, "A")
        else:
            add_meta_information(chain_pdb_file, cif_dict, chain_id, "A")
        
        add_remark(chain_pdb_file,f"SUBSET CHAIN:{chain_id} ENTITY:{entity_id}")
        print(f"Saved {chain_pdb_file}")

def download_file(url, save_path, can_fail=False):
    if os.path.exists(save_path):
        print(f"Skipping: {save_path}")
        return
    response = requests.get(url, stream=True)
    if response.status_code == 200:
        with open(save_path, 'wb') as file:
            for chunk in response.iter_content(chunk_size=8192):
                file.write(chunk)
        print(f"Downloaded: {save_path}")
    else:
        if not can_fail:
            print(f"Failed to download: {url}")
            exit(1)

def fetch_pdb(pdb_id, file_format, keep=False, prefix=""):
    output_dir="."

    select_chain=None
    if re.search(r"_[^_]+$",pdb_id):
        select_chain = re.sub(r"^[^_]+_","",pdb_id)
        pdb_id = re.sub(r"_[^_]+$","",pdb_id)

    pdb_id = pdb_id.lower()

    if file_format == "pdb_split":
        url = f"{PDB_URL}{pdb_id}.cif"
        save_path_cif = os.path.join(output_dir, f"{prefix}{pdb_id}.full.cif")
        download_file(url, save_path_cif)
        url = f"{PDB_URL}{pdb_id}.pdb"
        save_path_pdb = os.path.join(output_dir, f"{prefix}{pdb_id}.full.pdb")
        download_file(url, save_path_pdb, True)
        save_path_pdb_final = os.path.join(output_dir, f"{prefix}{pdb_id}.pdb")
        if os.path.exists(save_path_pdb):
            cif_to_pdb(save_path_cif,save_path_pdb_final,save_path_pdb,select_chain,keep)
        else:
            cif_to_pdb(save_path_cif,save_path_pdb_final,None,select_chain,keep)
        if not keep and os.path.exists(save_path_cif):
            os.remove(save_path_cif)
        if not keep and os.path.exists(save_path_pdb):
            os.remove(save_path_pdb)
    else:
        file_ext = 'cif' if file_format == 'cif' else 'pdb'
        url = f"{PDB_URL}{pdb_id}.{file_ext}"
        save_path = os.path.join(output_dir, f"{prefix}{pdb_id}.{file_ext}")
        download_file(url, save_path)

def fetch_alphafold(uniprot_id, keep,prefix):
    output_dir="."
    uniprot_id = uniprot_id.upper()
    url = f"{ALPHAFOLD_URL}AF-{uniprot_id}-F1-model_v4.pdb"
    save_path = os.path.join(output_dir, f"{uniprot_id}.pdb")
    download_file(url, save_path)

def main():
    parser = argparse.ArgumentParser(description="Fetch PDB and AlphaFold files. Note: HETATM MSE are converted to ATOM MET, other HETATM are omitted")
    parser.add_argument("-p", "--pdb", nargs='*', help="PDB ID(s) (can be multiple). Use underscore to indicate a chain e.g. 6P3S_A (only works with format=pdb_split). Note: first the label_asym_id (pdb picked chain names) are used than the auth_asym_id (auther picked chain names)")
    parser.add_argument("--prefix", default='', help="Output name prefix")
    parser.add_argument("-u", "--uniprot", nargs='*', help="UniProt ID(s) to download AlphaFold predictions (can be multiple)")
    parser.add_argument("-f", "--pdb_file_format", choices=['cif', 'pdb', 'pdb_split'], default='pdb_split', help="File format for the PDB structure, pdb_split=split by entity id (default: pdb_split).")
    parser.add_argument("-k", "--keep", action="store_true", help="keep intermediate files (mainly for format=pdb_split)")

    args = parser.parse_args()

    if args.pdb:
        for x in args.pdb:
            fetch_pdb(x, args.pdb_file_format, args.keep,args.prefix)

    if args.uniprot:
        for x in args.uniprot:
            fetch_alphafold(x, args.keep, args.prefix)

if __name__ == "__main__":
    main()
