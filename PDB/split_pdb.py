#!/usr/bin/python3
#pk
import argparse
import re
from Bio.PDB import PDBParser, PDBIO, Select
from Bio.SeqUtils import seq1
from scipy.cluster.hierarchy import dendrogram, linkage, fcluster
import numpy as np
from Bio import pairwise2
from scipy.spatial.distance import squareform

version="0.0.8" # last updated on 2024-08-12 04:39:56

class ChainIDSelect(Select):
    def __init__(self, chain_id):
        self.chain_id = chain_id

    def accept_chain(self, chain):
        return chain.id == self.chain_id

def add_remark(pdb_file, remark):
    n=80-11 # 11 = "REMARK   2 ".length
    cur_str = remark.upper().rstrip()
    chunks = [ cur_str[i:i+n] for i in range(0,len(cur_str),n) ]
    chunks = [ f"REMARK   2 {chunks[i]}".ljust(80," ")+"\n" for i in range(0,len(chunks)) ]

    with open(pdb_file, "r") as file:
        pdb_content = file.readlines()

    i=-1
    for j in range(len(pdb_content)):
        if "REMARK " in pdb_content[j]:
            i=j
    if i == -1:
        i=1
    pdb_content.insert(i, "\n".join(chunks))
    with open(pdb_file, "w") as file:
        file.writelines(pdb_content)

def replacer(s, newstring, index, nofail=False):
    if not nofail and index not in range(len(s)):
        raise ValueError("index outside given string")
    if index < 0:  # add it to the beginning
        return newstring + s
    if index > len(s):  # add it to the end
        return s + newstring
    return s[:index] + newstring + s[index + 1:]

def copy_meta(pdb_source_file, pdb_file, select_chain=None, new_chain_name=None):
    with open(pdb_source_file, "r") as file:
        pdb_source_content = file.readlines()

    if select_chain is not None:
        # trim the meta data by selected chain id (and replace if there was a renaming), usually i rename them simply "A" if only one chain is extracted ...
        pdb_source_content_subset=[]
        prefix_indices = {
            "SHEET": 21, # this is the position where the chain id is stored (fixed with in PDB format jaja)
            "HELIX": 19,
            "SSBOND": 15,
            "CISPEP": 15,
            "LINK": 21,
            "SITE": 22,
            "HET ": 12,
            "MODRES": 16,
            "SEQRES": 11,
            "SEQADV": 16,
            "DBREF": 12,
        }
        for line in pdb_source_content:
            is_good=True
            for prefix, index in prefix_indices.items():
                if line.startswith(prefix):
                    if line[index] != f"{select_chain}":
                        is_good=False
                        continue
                    if new_chain_name is not None:
                        line = replacer(line, new_chain_name, index)
            if is_good:
                pdb_source_content_subset += [line]
        pdb_source_content = pdb_source_content_subset

    last_i=0
    for i in range(len(pdb_source_content)):
        if re.search("^ATOM",pdb_source_content[i]):
            break
        last_i=i

    pdb_source_content = pdb_source_content[0:last_i]

    with open(pdb_file, "r") as file:
        pdb_content = file.readlines()
    pdb_content = pdb_source_content+pdb_content

    with open(pdb_file, "w") as file:
        file.writelines(pdb_content)

def extract_sequences(structure):    
    sequences = {}
    for model in structure:
        for chain in model:
            chain_id = chain.id
            cur_seq = seq1(''.join(residue.resname for residue in chain))
            if not re.search(r"^X+$",cur_seq): 
                sequences[chain_id] = cur_seq
    return sequences

def sequence_similarity(seq1, seq2):
    alignments = pairwise2.align.globalxx(seq1, seq2)
    max_score = max([alignment[2] for alignment in alignments])
    return max_score

def create_distance_matrix(sequences):
    ids = list(sequences.keys())
    seqs = list(sequences.values())
    n = len(seqs)
    dist_matrix = np.zeros((n, n))
    for i in range(n):
        for j in range(i + 1, n):
            similarity = sequence_similarity(seqs[i], seqs[j])
            dist = 1 - similarity / max(len(seqs[i]), len(seqs[j]))
            dist_matrix[i, j] = dist_matrix[j, i] = dist

    return dist_matrix, ids

def cluster_sequences(distance_matrix, ids, threshold=0.4):
    distance_matrix = squareform(distance_matrix)
    linked = linkage(distance_matrix, 'average')
    cluster_ids = fcluster(linked, threshold, criterion='distance')
    chain_to_cluster = {ids[i]: cluster_ids[i] for i in range(len(ids))}
    return chain_to_cluster

def reorder_clusters(chain_to_cluster):
    cluster_to_chains = {}
    for chain, cluster in chain_to_cluster.items():
        if cluster not in cluster_to_chains:
            cluster_to_chains[cluster] = []
        cluster_to_chains[cluster].append(chain)
    
    smallest_chains = {cluster: sorted(chains)[0] for cluster, chains in cluster_to_chains.items()}
    sorted_clusters = sorted(smallest_chains.items(), key=lambda x: x[1])
    new_cluster_id = 1
    old_to_new_cluster_id = {}
    for old_cluster, _ in sorted_clusters:
        old_to_new_cluster_id[old_cluster] = new_cluster_id
        new_cluster_id += 1
    
    reordered_chain_to_cluster = {chain: old_to_new_cluster_id[cluster] for chain, cluster in chain_to_cluster.items()}
    return reordered_chain_to_cluster

def get_first_chain_per_cluster(chain_to_cluster):
    cluster_to_chain = {}
    for chain, cluster in chain_to_cluster.items():
        if cluster not in cluster_to_chain:
            cluster_to_chain[cluster] = chain
    return cluster_to_chain

def split_pdb(pdb_file, all_chains, cluster_threshold):
    parser = PDBParser()
    io = PDBIO()
    structure = parser.get_structure("structure", pdb_file)
    
    io.set_structure(structure)

    if not all_chains:
        print("clustering chains ...")
        sequences = extract_sequences(structure)
        distance_matrix, ids = create_distance_matrix(sequences)
        entity_chains = get_first_chain_per_cluster(reorder_clusters(cluster_sequences(distance_matrix, ids, cluster_threshold)))

        for entity_id, chain_id in entity_chains.items():
            chain_pdb_file = f"{pdb_file.rstrip('.pdb')}_cluster_{entity_id}_chain_{chain_id}.pdb"
            io.save(chain_pdb_file, select=ChainIDSelect(chain_id))
            copy_meta(pdb_file, chain_pdb_file, chain_id)
            add_remark(chain_pdb_file,f"SUBSET CHAIN:{chain_id} CLUSTER:{entity_id}")
            print(f"Saved cluster {entity_id} to {chain_pdb_file}")
    else:
        for model in structure:
            for chain in model:
                chain_id = chain.id
                chain_pdb_file = f"{pdb_file.rstrip('.pdb')}_chain_{chain_id}.pdb"
                io.save(chain_pdb_file, select=ChainIDSelect(chain_id))
                copy_meta(pdb_file, chain_pdb_file, chain_id)
                add_remark(chain_pdb_file,f"SUBSET CHAIN:{chain_id}")
                print(f"Saved {chain_pdb_file}")

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Split PDB, only store first chain per cluster of similar sequences (there is no entity id present in pdb, use cif and cif_to_pdb.py instead).')
    parser.add_argument('pdb_files', nargs='+', help='List of PDB files to process')
    parser.add_argument("-a", "--all", action="store_true", help="all chains are outputted, no clustering is perfomed")
    parser.add_argument('-t', '--threshold', type=float, default=0.4, help='Threshold cutoff for clustering chains based on sequence, the smaller the more clusters are outputted (default: 0.4)')

    args = parser.parse_args()

    if len(args.pdb_files)==0:
        parser.error("Input CIF file(s) is/are required.")
    
    for pdb in args.pdb_files:
        bn=re.sub(r"[.][^.]+$","",pdb)
        split_pdb(pdb, args.all, args.threshold)
    
