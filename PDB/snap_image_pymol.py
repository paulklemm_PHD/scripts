import sys
import pymol
from pymol import cmd
import threading
import optparse
import matplotlib.pyplot as plt
from PIL import Image, ImageDraw, ImageFont

def draw_with_timeout(pdb, dpi, width, height, ray, timeout, frames, angle, color):
    def work():
        cmd.load(pdb, 'structure')
        cmd.enable('structure')
        cmd.show('cartoon')
        # cmd.translate(vector=[-1000,0,0])
        # cmd.move(axis="x",distance=1000)
        if int(color) == 0:
            cmd.color('cyan', 'ss h')
            cmd.color('green', 'ss s')
            cmd.color('gray', "ss l+''")
        elif int(color) == 1:
            omega = ["green","cyan","magenta","yellow","salmon","grey","slate","orange","purple","pink"]
            i=0
            for model in cmd.get_object_list('all'):
                for chain in cmd.get_chains(model):
                    cmd.color( omega[i], selection=f'chain {chain}' )
                    i = i+1 if i+1 < len(omega) else 0
        cmd.bg_color(color="white")
        if int(ray) == 1:
            print("-> ray tracing !")
            cmd.set('ray_opaque_background', 1)
            cmd.set('opaque_background', 1)
            cmd.set('ray_trace_frames', 1)
            cmd.set('ray_trace_mode', 3)
            cmd.ray(width=width, height=height)
        else: 
            cmd.draw(width=width, height=height)
        for i in range(frames):
            if int(ray) == 1:
                cmd.png(f"{pdb}_{i}.png", width=width, height=height, dpi=dpi, ray=1)
            else:
                print("opengl")
                cmd.png(f"{pdb}_{i}.png", width=width, height=height, dpi=dpi, ray=0)
            cmd.rotate("x",int(angle))
    thread = threading.Thread(target=work)
    thread.start()
    thread.join(timeout)
    if thread.is_alive():
        print(f"Drawing {pdb} timed out.")
    else:
        print(f"{pdb} done...")
    cmd.delete('all')

# def make_image(pdbs, dpi, width, height, ray, timeout, frames, angle, color):
#     pymol.finish_launching(['pymol', '-R'])
#     for pdb in pdbs:
#         draw_with_timeout(pdb=pdb, dpi=dpi, width=width, height=height, ray=ray, timeout=timeout, frames=frames, angle=angle, color=color)
#     cmd.quit()

# def add_legend_to_image(image, color):
#     draw = ImageDraw.Draw(image)
#     font = ImageFont.load_default()
#     if int(color) == 0:
#         legend_texts = [("Helix: Cyan", "cyan"), ("Sheet: Green", "green"), ("Loop: Gray", "gray")]
#     elif int(color) == 1:
#         colors = ["Green", "Cyan", "Magenta", "Yellow", "Salmon", "Grey", "Slate", "Orange", "Purple", "Pink"]
#         legend_texts = [(f"Chain {i+1}: {c}", c.lower()) for i, c in enumerate(colors)]
#     y_offset = 10
#     for text, col in legend_texts:
#         draw.text((10, y_offset), text, fill=col, font=font)
#         y_offset += 20

def combine_images(pdb, frames, angle, width, height, color):
    images = [Image.open(f"{pdb}_{i}.png") for i in range(frames)]
    combined_width = width * frames
    combined_image = Image.new('RGB', (combined_width, height))
    for i, img in enumerate(images):
        combined_image.paste(img, (i * width, 0))
        draw = ImageDraw.Draw(combined_image)
        draw.text((i * width + 10, 10), f"{i * angle}°", fill="black")

    #add_legend_to_image(combined_image, color)
    combined_image.save(f"{pdb}_combined.png")
    print(f"Combined image saved as {pdb}_combined.png")

def make_image(pdbs, dpi, width, height, ray, timeout, frames, angle, color):
    #pymol.finish_launching(['pymol', '-R']) 
#    pymol.finish_launching()
    pymol.finish_launching(['pymol', '-qei'])
    for pdb in pdbs:
        draw_with_timeout(pdb=pdb, dpi=dpi, width=width, height=height, ray=ray, timeout=timeout, frames=frames, angle=angle, color=color)
    cmd.quit()
    if frames > 1:
        for pdb in pdbs:
            combine_images(pdb, frames, angle, width, height, color)

if __name__ == "__main__":
    parser = optparse.OptionParser(usage="usage: %prog [options] <input.cif/pdb> (input2 ...)")
    parser.add_option("-d", "--dpi", dest="dpi", type="int", default=300, help="Set the DPI of the output image")
    parser.add_option("-n", "--frames", dest="frames", type="int", default=1, help="Number of frames that are rotated by angle")
    parser.add_option("-a", "--anglerotation", dest="angle", type="int", default=20, help="Rotation angle")
    parser.add_option("-r", "--ray", dest="ray", type="int", default=0, help="Use ray tracing or not (1 or 0)")
    parser.add_option("-t", "--timeout", dest="timeout", type="int", default=10, help="Timeout for drawing")
    parser.add_option("-c", "--color", dest="color", type="int", default=0, help="Coloring modus 0 = by secondary structure (helix, sheet), 1 = by chain")
    parser.add_option("-W", "--width", dest="width", type="int", default=800, help="Set the width of the output image")
    parser.add_option("-H", "--height", dest="height", type="int", default=800, help="Set the height of the output image")

    (options, args) = parser.parse_args()

    if len(args) < 1:
        parser.error("No input files specified")

    make_image(args, dpi=options.dpi, width=options.width, height=options.height, ray=options.ray, timeout=options.timeout, frames=options.frames, angle=options.angle, color=options.color)
