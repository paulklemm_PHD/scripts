#!/usr/bin/perl
#pk

use strict;
use warnings 'all';

use File::Basename;
use Cwd 'abs_path';
use threads;
use Thread::Queue;
# use Term::ProgressBar;
use Getopt::Long; # for parameter specification on the command line
$|=1; #autoflush

my $version="0.0.32"; # last updated on 2024-06-13 15:20:32

my $me = basename($0);
my $num_threads = 10;
my $dry=0;
my $keep=0;
my $save=0;

my $usage = "$me        superimposes all input PDB files against one/set of reference PDB file(s). Outputs the superimposed PDB as well as the RMSD. If output is allready present, pymol is not invoked again (only for -keep).

SYNOPSIS

$me -ref=REFPDB PDB1 PDB2 ...

or

$me PDB1 PDB2 ...

DESCRIPTION
	-ref=FILE : reference PDB file, multiple files can be given as ;-separated list
				if no -ref is defined: compare all input PDBs versus all (only A-B, not B-A) 
	-cpus=i   : number of threads [default:$num_threads]
	-dry      : prints out the work instead
	-keep     : keeps intermediate files (stored in ./.pymolSuperimposePDB_tmp/)
	-save     : keeps superimposed pdb files, sets -keep

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES pymol
";

my $ref_FN = "";
my $help;

GetOptions('help!'=>\$help,'h!'=>\$help,
	'ref=s'=>\$ref_FN,
	'cpus=i'=>\$num_threads,
	'dry!'=>\$dry,
	'keep!'=>\$keep,
	'save!'=>\$save,
	'version!' => sub { print "$0\tv$version\n";exit 0; },
	'v!' => sub { print "$0\tv$version\n";exit 0; }
);

if ($help || scalar(@ARGV) == 0 ){
    print $usage;
    exit(1);
}

our $tmp_dir=".pymolSuperimposePDB_tmp";
if($save){$keep=1}
if($keep){$tmp_dir="pymolSuperimposePDB_output";}

my $modus="pymol";
system("pymol --help >/dev/null 2>&1");
if($? != 0){print STDERR "pymol not executable please install pymol (e.g. mamba install -c conda-forge -c schrodinger pymol-bundle)\n";exit 1;}

our $QUEUE = Thread::Queue->new();    # A new empty queue
my @files = @ARGV;
my @refs = split(";",$ref_FN);

if(scalar @refs == 0){
	#
	# all-versus-all modus (omitt symetric hits as they are identical)
	#
	for (my $i = 0; $i < scalar @files; $i++) {
		chomp($files[$i]);
		if(!-e $files[$i]){print STDERR "[ERROR] '$files[$i]' does not exists "}
		for (my $j = $i+1; $j < scalar @files; $j++) {
			chomp($files[$j]);
			if(!-e $files[$j]){print STDERR "[ERROR] '$files[$j]' does not exists "}

			my $fi = abs_path($files[$i]);
			my $base_fi = basename($files[$i]);

			$base_fi = basename($base_fi);

			my $ref_file = abs_path($files[$j]);
			my $ref_base = basename($files[$j]);
			my $j = $ref_base;

			if(! -d "$tmp_dir/output.$ref_base"){system("mkdir -p $tmp_dir/output.$ref_base 2>/dev/null")}		
			
			my $job="";
			my $base_fi_nosuffix=$base_fi;$base_fi_nosuffix=~s/\.pdb//g;
			my $ref_base_nosuffix=$ref_base;$ref_base_nosuffix=~s/\.pdb//g;
			$job = "mkdir -p '$tmp_dir/output.$ref_base/fix.$ref_base.vs.flex.$base_fi' 2>/dev/null; cd '$tmp_dir/output.$ref_base/fix.$ref_base.vs.flex.$base_fi'; echo 'load $ref_file, $ref_base_nosuffix\nload $fi, $base_fi_nosuffix\nsuper ${base_fi_nosuffix},${ref_base_nosuffix}\ncd ".(abs_path("$tmp_dir/output.$ref_base/fix.$ref_base.vs.flex.$base_fi"))."\n".($save ? "save ${base_fi_nosuffix}.pdb,${base_fi_nosuffix}\n" : "")."' >comp.pml; pymol -c comp.pml >log.out.unfinished 2>log.err && mv log.out.unfinished log.out;\n";
		
			if(!-e "$tmp_dir/output.$ref_base/fix.$ref_base.vs.flex.$base_fi/log.out"){
				if($dry){print STDERR "$job";next}
				$QUEUE->enqueue($job);
			}
		}
	}
}else{
	#
	# PDB versus a reference set
	#
	foreach my $ref (@refs) {
		if(!-e $ref){print STDERR "[ERROR] '$ref' does not exists "}
		# build work
		for (my $i = 0; $i < scalar @files; $i++) {
			chomp($files[$i]);
			if(!-e $files[$i]){print STDERR "[ERROR] '$files[$i]' does not exists "}

			my $fi = abs_path($files[$i]);
			my $base_fi = basename($files[$i]);

			$base_fi = basename($base_fi);

			my $ref_file = abs_path($ref);
			my $ref_base = basename($ref);
			my $j = $ref_base;

			if(! -d "$tmp_dir/output.$ref_base"){system("mkdir -p $tmp_dir/output.$ref_base 2>/dev/null")}		
			
			my $job="";
			my $base_fi_nosuffix=$base_fi;$base_fi_nosuffix=~s/\.pdb//g;
			my $ref_base_nosuffix=$ref_base;$ref_base_nosuffix=~s/\.pdb//g;
			$job = "mkdir '$tmp_dir/output.$ref_base/fix.$ref_base.vs.flex.$base_fi' 2>/dev/null; cd '$tmp_dir/output.$ref_base/fix.$ref_base.vs.flex.$base_fi'; echo 'load $ref_file, $ref_base_nosuffix\nload $fi, $base_fi_nosuffix\nsuper ${base_fi_nosuffix},${ref_base_nosuffix}\ncd ".(abs_path("$tmp_dir/output.$ref_base/fix.$ref_base.vs.flex.$base_fi"))."\n".($save ? "save ${base_fi_nosuffix}.pdb,${base_fi_nosuffix}\n" : "")."' >comp.pml; pymol -c comp.pml >log.out.unfinished 2>log.err && mv log.out.unfinished log.out;\n";		
		
			if(!-e "$tmp_dir/output.$ref_base/fix.$ref_base.vs.flex.$base_fi/log.out"){
				if($dry){print STDERR "$job";next}
				$QUEUE->enqueue($job);
			}
		}
	}
}

# start QUEUE + progress bar

our $max_n=$QUEUE->pending();
# our $progress_bar = Term::ProgressBar->new({
# 	count => $max_n,
# 	ETA   => 'linear',
# });

# spawn threads and process
for (my $i = 0; $i < $num_threads; $i++) { threads->create('systemWorker') } 

# await result
$_->join() for threads->list();

sub systemWorker {
	local $SIG{KILL} = sub { threads->exit };
	my $tid = threads->tid();
	while($QUEUE->pending()){
		# if($tid==1){ $progress_bar->update($max_n-$QUEUE->pending()); } # print STDERR "QUEUE->pending=".($QUEUE->pending())."\n"
		my $job = $QUEUE->dequeue();
		system($job);
	}
}

# make output table

my $cmd="find $tmp_dir/output.* -iname log.out | xargs grep \"Executive: RMSD\" | perl -lne 'if(/fix.(.*).pdb.vs.flex.(.*).pdb.*RMSD = +([0-9.]+) .* ([0-9]+) atoms/){print \"\$1\\t\$2\\t\$3\\t\$4\\t\".(\$3/(\$4**2)) }'";

print STDERR "\n# Extracting results with: \$ $cmd\n";

print "# fix\tflex\tRMSD\tMA\tRMSDPMAS\n";
system("$cmd");

if(!$keep){ system("rm -r $tmp_dir/"); }
