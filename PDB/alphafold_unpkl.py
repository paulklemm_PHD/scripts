#!/usr/bin/python

# based on https://github.com/jasperzuallaert/VIBFold
# added a csv output of best file

import glob
import math
import os
import numpy as np
from matplotlib import pyplot as plt
import argparse
import pickle
import re

parser = argparse.ArgumentParser()
parser.add_argument('--input_file',default="",help="input file, usually called result_*.pkl, only one input file is supported here !",dest='input_file')
parser.add_argument('--input_dir_best',default="",help="input file, usually called result_*.pkl, only one input file is supported here !",dest='input_dir_best')
args = parser.parse_args()

file=""
if args.input_dir_best != "":
	f = open(f"{args.input_dir_best}//ranking_debug.json", "r")
	found_order=0
	best=""
	for x in f:
		if re.search("order",x):
			found_order=1
		elif found_order:
			best=re.sub(r"^[^\"]*\"|\"[^\"]*$","",x)
			break
	if best != "":
		file=f"{args.input_dir_best}/result_{best}.pkl"
		print(f"found optimal structure in result_{best}.pkl")
	else:
		print(f"did not find the optimal pkl in {args.input_dir_best}/ranking_debug.json")
		exit(1)
else:
	file=args.input_file

d = pickle.load(open(file,'rb'))
basename = os.path.basename(file)
basename = basename[basename.index('model'):]

#print(d.keys())

if 'predicted_aligned_error' in d:
	np.savetxt(f"{file}.pae.csv", d['predicted_aligned_error'], delimiter=",", fmt="%.20f")
if 'ranking_confidence' in d:
	with open(f"{file}.ranking_confidence.csv", 'w') as f:
		f.write(f"{d['ranking_confidence']}")
np.savetxt(f"{file}.plddt.csv", d['plddt'], delimiter=",", fmt="%.20f")
