#pk
import requests
import time
import os
from sys import argv,exit

def post_request(pdb_file):
    url = "https://search.foldseek.com/api/ticket"
    files = {'q': open(pdb_file, 'rb')}
    data = {
        'mode': '3diaa',
        'database[]': ['afdb50', 'afdb-swissprot', 'afdb-proteome', 'bfmd', 'cath50', 'mgnify_esm30', 'pdb100', 'gmgcl_id']
    }
    
    response = requests.post(url, files=files, data=data)
    if response.status_code == 200:
        return response.json()['id']
    else:
        print(f"Failed to submit {pdb_file}. HTTP Status Code: {response.status_code} for {pdb_file}")
        return None

def check_status(ticket_id):
    url = f"https://search.foldseek.com/api/ticket/{ticket_id}"
    response = requests.get(url)
    if response.status_code == 200:
        return response.json()['status']
    else:
        print(f"Failed to check status for ticket {ticket_id}. HTTP Status Code: {response.status_code} for {pdb_file}")
        return None

def download_result(ticket_id, pdb_file):
    url = f"https://search.foldseek.com/api/result/download/{ticket_id}"
    output_file = f"{pdb_file}.foldseek.gz"
    response = requests.get(url)
    if response.status_code == 200:
        with open(output_file, 'wb') as f:
            f.write(response.content)
        print(f"Result for {pdb_file} downloaded successfully.")
    else:
        print(f"Failed to download result for ticket {ticket_id}. HTTP Status Code: {response.status_code} for {pdb_file}")

def process_pdb_files(pdb_files):
    for pdb_file in pdb_files:
        if os.path.exists(f"{pdb_file}.foldseek.gz") or os.path.exists(f"{pdb_file}.foldseek"):
            print(f"skipping {pdb_file}, output present")
            continue
        print(f"posting {pdb_file}...")
        ticket_id = post_request(pdb_file)
        if ticket_id:
            while True:
                status = check_status(ticket_id)
                if status == "COMPLETE":
                    print(f"downloading {pdb_file}...")
                    download_result(ticket_id, pdb_file)
                    break
                elif status == "PENDING" or status == "RUNNING":
                    print(f"awaiting {pdb_file} results...")
                    time.sleep(10)
                else:
                    print(f"Unexpected status {status} for ticket {ticket_id} for {pdb_file}.")
                    break

if __name__ == "__main__":
    if len(argv) < 2:
        print(f"Usage: python {__name__} <A.pdb> (<B.pdb> ...)")
        exit(1)

    process_pdb_files(argv[1:len(argv)])

