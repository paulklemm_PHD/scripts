#pk
import argparse
import os
import subprocess

version="0.0.1"

def run_foldseek(pdb_file):
    output_file = f"{pdb_file}.out"
    cmd = ["foldseek", "structureto3didescriptor", pdb_file, output_file]
    subprocess.run(cmd, check=True)

def process_output(pdb_file):
    output_file = f"{pdb_file}.out"
    
    # Process .3di file
    with open(output_file, "r") as infile, open(f"{pdb_file}.3di", "w") as out_3di:
        for line in infile:
            parts = line.strip().split('\t')
            if len(parts) >= 3:
                out_3di.write(f">{parts[0]}\n{parts[2]}\n")
    
    # Process .aa file
    with open(output_file, "r") as infile, open(f"{pdb_file}.aa", "w") as out_aa:
        for line in infile:
            parts = line.strip().split('\t')
            if len(parts) >= 2:
                out_aa.write(f">{parts[0]}\n{parts[1]}\n")

def main():
    parser = argparse.ArgumentParser(description="Convert PDB to 3di files and aa")
    parser.add_argument("pdb_files", nargs='+', help="List of PDB files to process.")
    args = parser.parse_args()
    
    for pdb_file in args.pdb_files:
        if pdb_file.endswith(".pdb"):
            
            # Skip if .3di or .aa files already exist
            if os.path.exists(f"{pdb_file}.3di") or os.path.exists(f"{pdb_file}.aa"):
                print(f"Skipping {pdb_file}, output already exists.")
                continue
            
            print(f"Processing {pdb_file}...")
            run_foldseek(pdb_file)
            process_output(pdb_file)

if __name__ == "__main__":
    main()
