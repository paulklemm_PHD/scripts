#!/usr/bin/python3
#pk
import argparse
import re
from Bio.PDB import MMCIFParser, PDBIO, Select
from Bio.PDB.MMCIF2Dict import MMCIF2Dict

version="0.0.12" # last updated on 2024-08-12 04:39:56

class ChainIDSelect(Select):
    def __init__(self, chain_id):
        self.chain_id = chain_id

    def accept_chain(self, chain):
        return chain.id == self.chain_id

def remap_chain_ids(structure):
    valid_ids = {p:1 for p in "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"}
    new_mapping = {}
    new_structure = structure.copy()
    current_id = 0
    seen={}

    for model in new_structure:
        for chain in model:
            cur_id = chain.id.upper()

            print(f"1 {chain.id} -> {cur_id}")

            if len(cur_id)>1 or cur_id not in valid_ids:
                cheap = re.sub(r"[0-9]+","",cur_id)
                if cheap in valid_ids and valid_ids[cheap] == 1:
                    new_id = cheap
                else:
                    new_id = ""
                    for k in valid_ids:
                        if valid_ids[k] == 1:
                            new_id=k
                    if new_id == "":
                        raise ValueError(f"Too many chains to fit within PDB format constraints. {valid_ids}")
                cur_id = new_id

            print(f"2 {chain.id} -> {cur_id}")

            new_mapping[chain.id] = cur_id
            valid_ids[cur_id]=0
    
    for model in new_structure:
        for chain in model:
            chain.id = chain.id.upper()
            cur_id = new_mapping[chain.id]
            print(f"3 {chain.id} -> {cur_id}")

            if cur_id in seen:
                new_id = ""
                for k in valid_ids:
                    if k not in seen and k in valid_ids and valid_ids[k] == 1:
                        new_id=k
                if new_id == "":
                    raise ValueError(f"Too many chains to fit within PDB format constraints. {valid_ids}")
                cur_id = new_id
            print(f"4 {chain.id} -> {cur_id}")

            new_mapping[chain.id] = cur_id
            seen[cur_id]=1
            valid_ids[cur_id]=0

    # the following madness ensures that no id is set while another has this id already
    # there is an error thrown by the framework of PDB
    tmp_mapping={}
    i=1
    for model in new_structure:
        for chain in model:
            tmp_mapping[i]=new_mapping[chain.id]
            chain.id = f"{i}"
            i=i+1
    for model in new_structure:
        for chain in model:
            chain.id = tmp_mapping[chain.id]

    return new_structure, new_mapping

def add_remark(pdb_file, remark):

    n=70
    cur_str = remark.upper().rstrip()
    chunks = [ cur_str[i:i+n] for i in range(0,len(cur_str),n) ]
    chunks = [ f"REMARK   2 {chunks[i]}\n" for i in range(0,len(chunks)) ]

    with open(pdb_file, "r") as file:
        pdb_content = file.readlines()

    i=-1
    for j in range(len(pdb_content)):
        if "REMARK " in pdb_content[j]:
            i=j
    if i == -1:
        i=1
    pdb_content.insert(i, "\n".join(chunks))
    
    with open(pdb_file, "w") as file:
        file.writelines(pdb_content)

def get_chain_per_entity(cif_file):
    from Bio.PDB.MMCIF2Dict import MMCIF2Dict

    cif_dict = MMCIF2Dict(cif_file)

    # skip e.g. oligosaccharides or other molecules that are not the typical aminoacid (stored in _atom_site table)
    chain2type = {}
    group_PDB = cif_dict['_atom_site.group_PDB']
    label_asym_id = cif_dict['_atom_site.label_asym_id']
    for g, chain in zip(group_PDB, label_asym_id):
        chain2type[chain]=g

    # _struct_asym stores the table mapping the entity id to the chain id
    asym_ids = cif_dict['_struct_asym.id']
    entity_ids = cif_dict['_struct_asym.entity_id']
    entity2chain = {}
    chain2entity = {}

    for chain, entity_id in zip(asym_ids, entity_ids):
        if chain2type[chain] != "ATOM":
            continue
        if entity_id not in entity2chain:
            entity2chain[entity_id] = []
        entity2chain[entity_id] += [chain]
        chain2entity[chain]=entity_id

    return entity2chain, chain2entity


def add_meta_information(pdb_file, cif_dict, select_chain=None, chain_renaming=None):
    metadata = {
        "HEADER": f"{cif_dict.get('_struct_keywords.pdbx_keywords', 'Unknown')[0]} {cif_dict.get('_entry.id', '')[0]}{f' CHAIN:{select_chain}' if select_chain is not None else ''}\n",
        "TITLE": f"{cif_dict.get('_struct.title', 'Unknown')[0]}\n",
        "COMPND": f"MOL_ID: 1;\nCOMPND   2 MOLECULE: {cif_dict.get('_entity.pdbx_description', ['Unknown'])[0]}\n",
        "SOURCE": f"ORGANISM_SCIENTIFIC: {cif_dict.get('_entity_src_gen.pdbx_gene_src_scientific_name', ['Unknown'])[0]}\n",
        "KEYWDS": f"{', '.join(cif_dict.get('_struct_keywords.pdbx_keywords', 'Unknown'))}\n",
        "EXPDTA": f"{cif_dict.get('_exptl.method', ['Unknown'])[0]}\n",
        "AUTHOR": f"{', '.join(cif_dict.get('_audit_author.name', ['Unknown']))}\n",
        "REVDAT": f"{cif_dict.get('_pdbx_database_status.recvd_initial_deposition_date', 'Unknown')[0]}\n",
        "JRNL": f"{cif_dict.get('_citation.title', 'Unknown')[0]} {', '.join(cif_dict.get('_citation_author.name', 'Unknown'))} {cif_dict.get('_citation.journal_abbrev', 'Unknown')[0]}\n",
    }

    with open(pdb_file, "r") as file:
        pdb_content = file.readlines()

    meta_content = []
    for key in metadata:
        n=70 # 70 is without the key field wich is 10 chars !!
        for cur_str in metadata[key].upper().rstrip().split("\n"):
            chunks = [ cur_str[i:i+n] for i in range(0,len(cur_str),n) ]
            chunks = [ f"{key}{f'{i+1}'.rjust(4,' ')} {chunks[i]}\n".ljust(80," ")+"\n" if i>0 else f"{key}    {chunks[i]}\n" for i in range(0,len(chunks)) ]
            meta_content += chunks

    if '_struct_sheet.id' in cif_dict:
        sheet_ids = cif_dict['_struct_sheet.id']
        sheet_beg_label_asym_id = cif_dict['_struct_sheet_range.beg_label_asym_id']
        sheet_beg_label_seq_id = cif_dict['_struct_sheet_range.beg_label_seq_id']
        sheet_end_label_asym_id = cif_dict['_struct_sheet_range.end_label_asym_id']
        sheet_end_label_seq_id = cif_dict['_struct_sheet_range.end_label_seq_id']
        sheet_num_strands = cif_dict['_struct_sheet.number_strands']
        beg_label_comp_ids = cif_dict['_struct_sheet_range.beg_label_comp_id'] 
        end_label_comp_ids = cif_dict['_struct_sheet_range.end_label_comp_id'] 

        for i, sheet_id in enumerate(sheet_ids):
            start_chain_id = sheet_beg_label_asym_id[i]
            if select_chain is not None:
                if start_chain_id != select_chain:
                    continue
                if chain_renaming is not None and start_chain_id in chain_renaming:
                    start_chain_id = chain_renaming[start_chain_id] 
            start_res_seq = int(sheet_beg_label_seq_id[i])
            end_chain_id = sheet_end_label_asym_id[i]
            start_aa=beg_label_comp_ids[i]
            end_aa=end_label_comp_ids[i]
            if select_chain is not None:
                if end_chain_id != select_chain:
                    continue
                if chain_renaming is not None and end_chain_id in chain_renaming:
                    end_chain_id = chain_renaming[end_chain_id] 
            end_res_seq = int(sheet_end_label_seq_id[i])
            num_strands = int(sheet_num_strands[i])
            meta_content+=[f"SHEET {i+1:4d} {sheet_id:>3}{num_strands:2d} {start_aa:3s} {start_chain_id:1s}{start_res_seq:4d}  {end_aa:3s} {end_chain_id:1s}{end_res_seq:4d}  0\n"]
    else:
        print(f"missing SHEET table for {pdb_file}")

    if '_struct_conf.conf_type_id' in cif_dict:
        helix_ids = cif_dict['_struct_conf.conf_type_id']
        helix_pdb_ids = cif_dict['_struct_conf.pdbx_PDB_helix_id']
        beg_label_comp_ids = cif_dict['_struct_conf.beg_label_comp_id']
        end_label_comp_ids = cif_dict['_struct_conf.end_label_comp_id'] 
        helix_beg_label_asym_id = cif_dict['_struct_conf.beg_label_asym_id']
        helix_beg_label_seq_id = cif_dict['_struct_conf.beg_label_seq_id']
        helix_end_label_asym_id = cif_dict['_struct_conf.end_label_asym_id']
        helix_end_label_seq_id = cif_dict['_struct_conf.end_label_seq_id']
        helix_details = cif_dict['_struct_conf.details']
        pdbx_PDB_helix_classes = cif_dict['_struct_conf.pdbx_PDB_helix_class']
        helix_index = 1  # Initialize helix index
        for i, helix_id in enumerate(helix_ids):
            if 'HELX_P' in helix_id:  # Filtering only HELIX records
                helix_pdb_id=helix_pdb_ids[i]
                beg_label_comp_id=beg_label_comp_ids[i]
                end_label_comp_id=end_label_comp_ids[i]
                start_chain_id = helix_beg_label_asym_id[i]
                if select_chain is not None:
                    if start_chain_id != select_chain:
                        continue
                    if chain_renaming is not None and start_chain_id in chain_renaming:
                        start_chain_id = chain_renaming[start_chain_id] 
                start_res_seq = int(helix_beg_label_seq_id[i])
                end_chain_id = helix_end_label_asym_id[i]
                if select_chain is not None:
                    if end_chain_id != select_chain:
                        continue
                    if chain_renaming is not None and end_chain_id in chain_renaming:
                        end_chain_id = chain_renaming[end_chain_id] 
                end_res_seq = int(helix_end_label_seq_id[i])
                helix_class = pdbx_PDB_helix_classes[i]  # Default class for alpha helices (can be adjusted based on your needs)
                helix_length = end_res_seq - start_res_seq + 1
                
                meta_content+=[f"HELIX  {helix_index:3d} {helix_pdb_id:3s} {beg_label_comp_id:3s} {start_chain_id:1s} {start_res_seq:4d}  {end_label_comp_id:3s} {end_chain_id:1s} {end_res_seq:4d}  {helix_class:4s} {helix_length:32d}\n"] #helix_details[i]
                helix_index += 1
    else:
        print(f"missing HELIX table for {pdb_file}")

    meta_content=[ x.rstrip().ljust(80," ")+"\n" for x in meta_content ]

    pdb_content = meta_content + pdb_content

    with open(pdb_file, "w") as file:
        file.writelines(pdb_content)

def cif_to_pdb(cif_file, pdb_file,all_chains,as_one_pdb,add_meta=True):
    parser = MMCIFParser()
    io = PDBIO()
    cif_dict = MMCIF2Dict(cif_file)
    structure = parser.get_structure("structure", cif_file)
    
    if not as_one_pdb:
        entity2chain, chain2entity = get_chain_per_entity(cif_file)

        if all_chains:
            for model in structure:
                for chain in model:
                    chain_id = chain.id
                    chain_pdb_file = f"{re.sub(r'[.]pdb$','',pdb_file)}_entity_{chain2entity[chain_id]}_chain_{chain_id}.pdb"
                    new_structure = structure.copy()
                    for model in new_structure:
                        for chain in model:
                            if chain.id != chain_id:
                                chain.id = f"unused_{chain}"
                    for model in new_structure:
                        for chain in model:
                            if chain.id == chain_id:
                                chain.id = f"A"
                    io.set_structure(new_structure)
                    io.save(chain_pdb_file, select=ChainIDSelect("A"))

                    if add_meta:
                        add_meta_information(chain_pdb_file, cif_dict, chain_id, {chain_id:"A"})
                    add_remark(chain_pdb_file,f"SUBSET CHAIN:{chain_id} ENTITY:{chain2entity[chain_id]}")
                    print(f"Saved chain {chain_id} to {chain_pdb_file}")

        else:
            for entity_id in entity2chain:
                chain_id = entity2chain[entity_id][0]
                chain_pdb_file = f"{re.sub(r'[.]pdb$','',pdb_file)}_entity_{entity_id}_chain_{chain_id}.pdb"
                new_structure = structure.copy()
                for model in new_structure:
                    for chain in model:
                        if chain.id != chain_id:
                            chain.id = f"unused_{chain}"
                for model in new_structure:
                    for chain in model:
                        if chain.id == chain_id:
                            chain.id = f"A"
                io.set_structure(new_structure)
                io.save(chain_pdb_file, select=ChainIDSelect("A"))

                if add_meta:
                    add_meta_information(chain_pdb_file, cif_dict, chain_id, {chain_id:"A"})
                add_remark(chain_pdb_file,f"SUBSET CHAIN:{chain_id} ENTITY:{entity_id}")

                # add_remark(chain_pdb_file, {chain_id: chain_map[chain_id]})
                print(f"Saved entity {entity_id} to {chain_pdb_file}")

    else:
        structure, chain_map = remap_chain_ids(structure)
        io.set_structure(structure)
        io.save(pdb_file)
        if add_meta:
            add_meta_information(chain_pdb_file, cif_dict, None, chain_map)
        add_remark(chain_pdb_file,f"REMAPPING CHAINS:{''.join([x+'->'+chain_map[x] for x in chain_map])}")

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Convert CIF to PDB, split chains into separate PDB files using label_asym_id (pdb picked chain names), only store first chain per entity ids')
    parser.add_argument('cif_files', nargs='+', help='List of PDB files to process')
    parser.add_argument("-a", "--all", action="store_true", help="all chains")
    parser.add_argument("-o", "--one", action="store_true", help="as one pdb file (does not always work as there are only a limited number of one letter chains in PDB format)")
    parser.add_argument("-n", "--nometa", action="store_true", help="do not parse meta information")

    args = parser.parse_args()

    if len(args.cif_files)==0:
        parser.error("Input CIF file(s) is/are required.")
    
    for cif in args.cif_files:
        bn=re.sub(r"[.][^.]+$","",cif)
        cif_to_pdb(cif, f"{bn}.pdb", args.all, args.one, not args.nometa)
    
