#!/usr/bin/perl
use strict;
use warnings;

# Default tmp Path is local path
use Cwd qw(); my $path = Cwd::cwd(); 
my $TMP = "$path/alphafold_output";
my $RES = "$path";
my $MODEL_NAMES = "model_1,model_2,model_3,model_4,model_5";
my $MAX_TEMPLATE_DATE = "2000-05-14";
my $PRESET = "full_dbs";
my $BENCHMARK = "false";
my @files;
use File::Basename;

# What to do?
my $FOLD = 1;
my $MAPPER = 1;
my $REMOVE = 0; # TMP
my $addtimeh = 0;
my $MODEL_PRESET="monomer";

# Parse options
foreach (@ARGV) {
	if ($_ =~ /^--tmp=(.+)/) {$TMP = $1;}
	elsif ($_ =~ /^--?results=(.+)/) {$RES = $1;}
	elsif ($_ =~ /^--?addtimeh=(.+)/) {$addtimeh = int($1);} # add hours to prediction
	elsif ($_ =~ /^--?model[_-]names=(.+)/) {$MODEL_NAMES = $1;}
	elsif ($_ =~ /^--?model[_-]preset=(monomer|multimer|monomer_casp14|monomer_ptm)/) {$MODEL_PRESET = $1;}
	elsif ($_ =~ /^--?multimer/) {$MODEL_PRESET = "multimer";}
	elsif ($_ =~ /^--?max_template_date=(.+)/) {$MAX_TEMPLATE_DATE = $1;}
	elsif ($_ =~ /^--?preset=(.+)/) {$PRESET = $1;}
	
	elsif ($_ =~ /^-/) {die("Could not parse parameter $_\n");}
	elsif ($_ =~ /^--?h/) {die("USAGE : $0 (options) FASTAFILE\n --model-preset=monomer|multimer|monomer_casp14|monomer_ptm switch between the model preset, for multimer the input fasta file is not split (default:monomer)\n --addtimeh=i increase the minimum compute time by i hours");}
	unless ($_ =~ /^-/) {push(@files,$_);}
}

# Open FASTA File and split to temporary files
my %tmp_fastas;
my %fastalen;

foreach my $file (@files) {
#my $bn=basename $file;
#$bn=~s/[^a-zA-Z0-9_.]/_/g;
$TMP = "$path/".(basename $file)."_alphafold_output";
system("mkdir $TMP 2>/dev/null");
my $long;
my $filehandle = 0;
my $skip = 0;
my $short;

open(FASTA,"<$file") || die("Could not open $file: $!");
while (<FASTA>) {
	chomp;

	# New entry, convert long header to short one for filenames
	if ($_ =~ /^>(.+)/) {
		$long = $1;
		$short = $long;
		$short =~ s/\s.*//;
		$short =~ s/[^0-9a-zA-Z_]/-/g;

		if (length($short) < 1) {
			die("Converted header is too short for $long ($file)\n");
		}
		if ($tmp_fastas{$short}) {
			die("$short as short form colides for at least two entries: '$tmp_fastas{$short}' and '$long' ($file)\n");
		}

		$skip = 0;
		if (-e "$RES/$short.pdb") {
			print STDERR "Warning: $RES/$short.pdb exist, cowardly skipping entry. Remove file if you want to recalculate!\n";
			$skip = 1;
			next;
		}

		if($MODEL_PRESET ne "multimer"){
			if ($long && $filehandle) {close(TMP); $filehandle = 0;}
			open(TMP,">$TMP/$short.fasta") || die("Could not open $TMP/$short.fasta: $!"); $filehandle = 1;
			print TMP ">$short\n";
			$tmp_fastas{$short} = $long;	# notice
			$fastalen{$short}=0;
		}else{
			$short=$file;
			$short=~s/\..*//g;
		}
		next;
	}
	
	unless ($long) {die("Fasta is not formatted correctly");}
	if ($skip) {next;} # skipped entry	

	$_=uc $_;
	if ($_ =~ /[^ACDEFGHIKLMNPQRSTVWY]/) {
		my $string = $_;
		$string =~ s/[ACDEFGHIKLMNPQRSTVWY]//g;
		$_ =~ s/[^ACDEFGHIKLMNPQRSTVWY]//g;
		print STDERR "# Illegal character(s) '$string' located in $long, I will remove those and proceed\n";
	}

	$fastalen{$short}+=length($_);	

	if($MODEL_PRESET ne "multimer"){ print TMP "$_\n"; }
}
close(FASTA);
}
if($MODEL_PRESET ne "multimer"){ close(TMP); }
else{
	my $file=$files[0];
	my $file_basename=basename $file;
	$file_basename=~s/\..*$//g;
	system("cp '$file' '$TMP/$file_basename.fasta';");
	$tmp_fastas{"$file_basename"}=$file_basename;
}


unless (keys %tmp_fastas) { die("No fasta entries found!\n"); }

# Generate Jobscripts
#my $data_dir="/scratch_shared/alphafold_db";
#my $bfd_database_path="/data/bfd/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt";
#my $mgnify_database_path="/data/mgnify/mgy_clusters_2018_12.fa";
#my $template_mmcif_dir="/data/pdb_mmcif/mmcif_files";
#my $obsolete_pdbs_path="/data/pdb_mmcif/obsolete.dat";
#my $pdb70_database_path="/data/pdb70/pdb70";
#my $uniclust30_database_path="/data/uniclust30/uniclust30_2018_08/uniclust30_2018_08";
#my $uniref90_database_path="/data/uniref90/uniref90.fasta";

print "# You may run all the prepared jobs via:\n";

foreach my $short (sort keys %tmp_fastas) {
open(SBATCH,">$TMP/$short.sh") || die("Could not open $TMP/$short.sh: $!");

#print STDERR "$short ".$fastalen{$short}."\n";
if(-e "$TMP/$short/ranked_0.pdb"){next;print STDERR "$TMP/$short/ranked_0.pdb is present, skipping...\n";next}

	my $mem=int($fastalen{$short}/100)*10+50;
	$mem=$mem>800 ? 800:$mem;
	my $time_h=int($fastalen{$short}/200)+$addtimeh;
	if($fastalen{$short}>1000){$time_h*=2} # increase for large inputs
	my $time_d=int($time_h / 24);
	if($MODEL_PRESET eq "multimer"){ $time_d+=2; }
	$time_h = $time_h % 24;
	$time_d = $time_d>=9 ? 9 : $time_d;

	print SBATCH "#!/bin/bash
#SBATCH --gpus=1
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --cpus-per-task=8
#SBATCH --mem=${mem}GB
#SBATCH --time=$time_d-$time_h:59:00
###############d-hh:mm:ss

module purge
module load singularity 
module load alphafold/2.3.1
";

if ($FOLD) {
print SBATCH "
run_alphafold_singularity.py --cpus \$SLURM_CPUS_PER_TASK --fasta-paths $short.fasta --output-dir . --model-preset $MODEL_PRESET
";
}
# singularity run -B $TMP:$TMP -B $data_dir:/data -B .:/etc --pwd /app/alphafold --nv /opt/ohpc/pub/apps/containers/alphafold/alphafold_latest.sif --fasta_paths=$TMP/$short.fasta --output_dir=$TMP --model_names=$MODEL_NAMES --preset=$PRESET --max_template_date=$MAX_TEMPLATE_DATE --data_dir=/data --uniref90_database_path=$uniref90_database_path --mgnify_database_path=$mgnify_database_path --uniclust30_database_path=$uniclust30_database_path --bfd_database_path=$bfd_database_path --pdb70_database_path=$pdb70_database_path --template_mmcif_dir=$template_mmcif_dir --obsolete_pdbs_path=$obsolete_pdbs_path --benchmark=$BENCHMARK

print SBATCH "
if [ -e $TMP/$short/ranking_debug.json ]
then
    echo \"Alphafold has finished.\"
else
    echo \"Alphafold seems to have failed. Unable to locate result file $TMP/$short/ranking_debug.json!\"
    exit 1
fi 
";

if ($REMOVE) {
print SBATCH "
rm $TMP/$short/*.pdb
rm $TMP/$short/*.json
rm $TMP/$short/*.pkl
rm $TMP/$short/msas/*
rmdir $TMP/$short/msas
rmdir $TMP/$short/
";
}
#}

close(SBATCH);

print "(cd '".(dirname "$TMP/$short.sh")."'; sbatch --nice=1000 $short.sh) ## len=$fastalen{$short} time=${time_d}-$time_h:59:00 mem=${mem}GB�\n";
}

print "# send output to sh to execute (or grep/awk to filter).\n# If jobs fail with 'CANCELLED ... DUE TO TIME LIMIT' you can increase the calculated execution time with --addtimeh=X (in hours).\n";
print "# Re-run this script to show all inclomplete jobs (if there is a ranked_0.pdb, the job is done)\n";
print "# Extract all output pdbs with e.g. find $TMP -iname \"ranked_0.pdb\" | xargs -I{} echo \"f='{}'; cp \\\$f \\\$(basename \\\$(dirname \\\$f)).pdb\"\n";
