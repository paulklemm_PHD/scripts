#!/usr/bin/perl
#pk

use strict;
use warnings;

use File::Basename;
#use Cwd 'abs_path';
#use threads;
#use Thread::Queue;
#use Term::ProgressBar;
use Getopt::Long; # for parameter specification on the command line
$|=1; #autoflush

my $version="0.0.18"; # last updated on 2023-03-01 12:21:28

my $me = basename($0);
#my $num_threads = 10;
#my $dry=0;
#my $keep=0;
#my $save=0;

my $usage = "$me        evaluates the predicted aligned error (PAE) of the alphafold prediction.

SYNOPSIS

$me DIRECTORY ...

DESCRIPTION
	-TODO

VERSION v$version
AUTHOR Paul Klemm
DEPENDENCIES python numpy,jax,jaxlib
";

my $ref_FN = "";
my $help;

GetOptions('help!'=>\$help,'h!'=>\$help,
# 	'ref=s'=>\$ref_FN,
#	'cpus=i'=>\$num_threads,
#	'dry!'=>\$dry,
#	'keep!'=>\$keep,
#	'save!'=>\$save,
	'version!' => sub { print "$me\tv$version\n";exit 0; },
	'v!' => sub { print "$me\tv$version\n";exit 0; }
);

if ($help || scalar(@ARGV) == 0 ){
    print $usage;
    exit(1);
}

my @pdb_lengths;
my @PAE;

system("python -c 'import pickle; import numpy as np; import sys;'");
if($? != 0){
	die "python module load failed (import pickle; import numpy as np; import sys) with:\n$!\n";
}

if(-e $ARGV[0]."/predicted_aligned_error.json"){
	print STDERR "found a predicted_aligned_error.json (colab ipynb alphafold)\n";
	
	if(!-e $ARGV[0]."/selected_prediction.pdb"){
		print STDERR "cannot find selected_prediction.pdb\n";
		exit 1;
	}
	@pdb_lengths = map {chomp($_);$_} `awk '\$1=="TER"{print \$5}' $ARGV[0]/selected_prediction.pdb`;

	if(!-e $ARGV[0]."/predicted_aligned_error.json"){
		print STDERR "cannot find predicted_aligned_error.json\n";
		exit 1;
	}

	my @tmp = map { $_=~s/.*\]|\[.*//g; $_ } `tr ',' '\n' <$ARGV[0]/predicted_aligned_error.json`;
	for (my $i = 0; $i < $pdb_lengths[0]; $i++) {
		push(@PAE,join(" ",@tmp[$i..$i+$pdb_lengths[1]]));
	}

}elsif(-e $ARGV[0]."/ranking_debug.json"){
	print STDERR "found a ranking_debug.json (singularity alphafold)\n";
	open(my $FH_json,"<".$ARGV[0]."/ranking_debug.json");
	my $found_order=0;
	my $best_output="";
	while(<$FH_json>){
		chomp;
		if(/"order"/){$found_order=1}
		elsif($found_order && /"([^"]+)"/){$best_output=$1; last;}
	}
	close($FH_json);

	if($best_output eq ""){
		print STDERR "cannot identify the best model from the ranking_debug.json...\n";
		exit 1;
	}
	
	if(!-e $ARGV[0]."/relaxed_$best_output.pdb"){
		print STDERR "cannot find relaxed_$best_output.pdb\n";
		exit 1;
	}
	@pdb_lengths = map {chomp($_);$_} `awk '\$1=="TER"{print \$5}' $ARGV[0]/relaxed_$best_output.pdb`;

	if(!-e $ARGV[0]."/result_$best_output.pkl"){
		print STDERR "cannot find result_$best_output.pkl\n";
		exit 1;
	}
	if(-e "$ARGV[0]/result_$best_output.pkl.pae.csv"){
		@PAE = `cat "$ARGV[0]/result_$best_output.pkl.pae.csv"`;
	}else{
		@PAE = `python -c 'import pickle; import numpy as np; import sys; d=pickle.load(open("$ARGV[0]/result_$best_output.pkl","rb"))["predicted_aligned_error"]; np.savetxt(sys.stdout, d, fmt="\%.4f")'`;	
	}
	
}else{
	print STDERR "cannot find a ranking_debug.json (singularity alphafold) or a predicted_aligned_error.json (colab ipynb alphafold)\n";
	exit 1;
}

if(scalar @pdb_lengths != 2){
	print STDERR "looks like there are not 2 input files in the presented fold\n";
	exit 1;
}


my $max_val=0;
for (my $i = 0; $i < $pdb_lengths[0]; $i++) {
	my @spl = split(/[ ,]/,$PAE[$i]);
	for (my $j = 0; $j < $pdb_lengths[0]+$pdb_lengths[1]; $j++) {
		if($max_val<$spl[$j]){$max_val=$spl[$j]}
	}
}

my $sum=0;
my $score=0;
my $score_good=0;
my $n=0;

for (my $i = 0; $i < $pdb_lengths[0]; $i++) {
	my @spl = split(/[ ,]/,$PAE[$i]);
	for (my $j = 0; $j < $pdb_lengths[0]+$pdb_lengths[1]; $j++) {
		if($j > $pdb_lengths[0]){
			chomp($spl[$j]);
			$sum+=$spl[$j];
			$n+=1;
			if($spl[$j] < $max_val/2){ $score+=1; }
			if($spl[$j] < 5){ $score_good+=1; }
		}
	}
}

print "$sum $n $score $score_good\n";
