#!/usr/bin/python

# based on https://github.com/jasperzuallaert/VIBFold
# added a csv output of all files

import glob
import math
import os
import numpy as np
from matplotlib import pyplot as plt
import argparse
import pickle
import re

def get_pae_plddt(model_names):
	out = {}
	for i,name in enumerate(model_names):
		d = pickle.load(open(name,'rb'))
		basename = os.path.basename(name)
		basename = basename[basename.index('model'):]
		out[f'{basename}'] = {'plddt': d['plddt'], 'pae':d['predicted_aligned_error']} if 'predicted_aligned_error' in d else {'plddt': d['plddt']}
	return out

def generate_output_images(feature_dict, out_dir, name, pae_plddt_per_model):
	msa = feature_dict['msa']
	seqid = (np.array(msa[0] == msa).mean(-1))
	seqid_sort = seqid.argsort()
	non_gaps = (msa != 21).astype(float)
	non_gaps[non_gaps == 0] = np.nan
	final = non_gaps[seqid_sort] * seqid[seqid_sort, None]

	##################################################################
	plt.figure(figsize=(14, 4), dpi=100)
	##################################################################
	plt.subplot(1, 2, 1)
	plt.title("Sequence coverage")
	plt.imshow(final,
			   interpolation='nearest', aspect='auto',
			   cmap="rainbow_r", vmin=0, vmax=1, origin='lower')
	plt.plot((msa != 21).sum(0), color='black')
	plt.xlim(-0.5, msa.shape[1] - 0.5)
	plt.ylim(-0.5, msa.shape[0] - 0.5)
	plt.colorbar(label="Sequence identity to query", )
	plt.xlabel("Positions")
	plt.ylabel("Sequences")

	##################################################################
	plt.subplot(1, 2, 2)
	plt.title("Predicted LDDT per position")
	for model_name, value in pae_plddt_per_model.items():
		plt.plot(value["plddt"], label=model_name)
	plt.ylim(0, 100)
	plt.ylabel("Predicted LDDT")
	plt.xlabel("Positions")
	plt.savefig(f"{out_dir}/{name+('_' if name else '')}coverage_LDDT.png")
	##################################################################

	##################################################################
	if "pae" in pae_plddt_per_model:
		num_models = 3 # columns
		num_runs_per_model = math.ceil(len(model_names)/num_models)
		fig = plt.figure(figsize=(3 * num_models, 2 * num_runs_per_model), dpi=100)
		for n, (model_name, value) in enumerate(pae_plddt_per_model.items()):
			plt.subplot(num_runs_per_model, num_models, n + 1)
			plt.title(model_name)
			plt.imshow(value["pae"], label=model_name, cmap="bwr", vmin=0, vmax=30)
			plt.colorbar()
		fig.tight_layout()
		plt.savefig(f"{out_dir}/{name+('_' if name else '')}PAE.png")
	##################################################################

parser = argparse.ArgumentParser()
parser.add_argument('--input_dir',default="",dest='input_dir',required=False)
parser.add_argument('--input_dir_best',default="",dest='input_dir_best',required=False)
parser.add_argument('--input_file',default="",dest='input_file',required=False)
args = parser.parse_args()

if args.input_dir != "":
	feature_dict = pickle.load(open(f'{args.input_dir}/features.pkl','rb'))
	model_names = sorted(glob.glob(f'{args.input_dir}/result_*.pkl'))
	pae_plddt_per_model = get_pae_plddt(model_names)
	generate_output_images(feature_dict, args.input_dir, "all_plot", pae_plddt_per_model)
elif args.input_dir_best != "":
	f = open(f"{args.input_dir_best}/ranking_debug.json", "r")
	found_order=0
	best=""
	for x in f:
		if re.search("order",x):
			found_order=1
		elif found_order:
			best=re.sub(r"^[^\"]*\"|\"[^\"]*$","",x)
			break
	if best != "":
		feature_dict = pickle.load(open(f'{args.input_dir_best}/features.pkl','rb'))
		model_names = sorted(glob.glob(f'{args.input_dir_best}/result_{best}*.pkl'))
		pae_plddt_per_model = get_pae_plddt(model_names)
		generate_output_images(feature_dict, args.input_dir_best, f"{best}_plot", pae_plddt_per_model)
	else:
		print(f"did not find the optimal pkl in {args.input_dir_best}/ranking_debug.json")
elif args.input_file != "":
	model_names = sorted(glob.glob(f'{args.input_file}'))
	pae_plddt_per_model = get_pae_plddt(model_names)
	vdir=re.sub(r"[^/]+$","",args.input_file)
	if vdir == "":
		vdir = "."
	feature_dict = pickle.load(open(f'{vdir}/features.pkl','rb'))
	generate_output_images(feature_dict, vdir, f"{args.input_file}_plot", pae_plddt_per_model)
